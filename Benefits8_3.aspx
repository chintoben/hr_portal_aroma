﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="Benefits8_3.aspx.vb" Inherits="Aroma_HRPortal.Benefits8_3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/benefit/banner1.jpg" /></div>
<!-- end banner -->

<!--
=====================================================
	Blog Page Details
=====================================================
-->
<article class="blog-details-page">
	<div class="container">
		
		<div class="post-heading">
            <ul>
			    <li><a href="index_hr.aspx">HOME</a></li>
			    <li>/</li>
			    <li><a href="Benefits_Employee.aspx">ระเบียบสวัสดิการ</a></li>
                <li>/</li>
                <li><a href="Benefits8.aspx">เงินกู้</a></li>
			</ul>
			<h2>สินเชื่อสวัสดิการพนักงาน (MOU ธนาคารออมสิน)</h2>
		</div> <!-- /.post-heading -->
		
		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix">
				<div class="blog-details-post-wrapper">
				
					<img src="new/images/banner/benefit/img11.jpg" alt="Image">

					<h4>สินเชื่อสวัสดิการสำหรับพนักงานหน่วยงานเอกชน</h4>
					<p>
						<ol>
							<li>คุณสมบัติผู้กู้
								<ul class="a">
									<li>เป็นพนักงานของบริษัท ที่มีข้อตกลงกับธนาคารออมสิน</li>
									<li>อายุงานไม่ต่ำกว่า 1 ปี</li>
									<li>อายุ 20 ปีบริบูรณ์ เมื่อรวมอายุผู้กู้กับระยะเวลาที่ชำระเงินกู้คืนต้องไม่เกิน 60 ปี</li>
									<li>เป็นผู้ฝากเงินฝากประเภทเผื่อเรียกของธนาคาร</li>
								</ul>
							</li>
							<li>วัตถุประสงค์การกู้
								<ul class="a">
									<li>เพื่อการอุปโภคบริโภค</li>
									<li>เพื่อไถ่ถอนจำนองจากสถาบันการเงินอื่น</li>
								</ul>
							</li>
							<li>จำนวนเงินให้กู้ ไม่เกิน 20 เท่าของเงินเดือนผู้กู้ แต่ไม่เกินรายละ 1,000,000.- บาท</li>
							<li>ระยะเวลาชำระเงินกู้ ไม่เกิน 10 ปี</li>
							<li>อัตราดอกเบี้ย MRR+1.25 % ต่อปี</li>
							<li>หลักประกัน ผู้ค้ำประกันต้องมีคุณสมบัติดังนี้
								<ul class="a">
									<li>ต้องทำงานในหน่วยงานเดียวกันกับผู้กู้</li>
									<li>มีอายุ 20 ปีบริบูรณ์ เมื่อรวมอายุผู้ค้ำประกันกับระยะเวลาที่ชำระเงินกู้คืนต้องไม่เกิน 60 ปี</li>
									<li>ทำงานมาแล้วครบ 3 ปี หรือ ในกรณีที่ทำงานไม่ครบ 3 ปี ต้องใช้ผู้ค้ำประกันที่ทำงานมาแล้วครบ 1 ปี จำนวน 2 คน</li>
								</ul>
							</li>
							<li>การชำระเงินกู้คืน หน่วยงานหักเงินเดือนนำส่งให้กับธนาคารออมสิน</li>
						</ol>
					</p>
					
					<h4>สินเชื่อธนาคารประชาชน</h4>
					<p>
						<ol>
							<li>คุณสมบัติผู้กู้
								<ul class="a">
									<li>เป็นพนักงานของบริษัท ที่มีข้อตกลงกับธนาคารออมสิน</li>
									<li>อายุงานไม่ต่ำกว่า 1 ปี</li>
									<li>อายุ 20 ปีบริบูรณ์ เมื่อรวมอายุผู้กู้กับระยะเวลาที่ชำระเงินกู้คืนต้องไม่เกิน 60 ปี</li>
									<li>เป็นผู้ฝากเงินฝากประเภทเผื่อเรียกของธนาคาร</li>
								</ul>
							</li>
							<li>วัตถุประสงค์การกู้
								<ul class="a">
									<li>เพื่อการอุปโภคบริโภค</li>
									<li>เพื่อไถ่ถอนจำนองจากสถาบันการเงินอื่น</li>
								</ul>
							</li>
							<li>จำนวนเงินให้กู้	ไม่เกิน 200,000.- บาท</li>
							<li>ระยะเวลาชำระเงินกู้	ไม่เกิน 8 ปี</li>
							<li>อัตราดอกเบี้ย 0.75 % ต่อเดือน (คิดอัตราดอกเบี้ยแบบ Flat Rate) หรืออัตราดอกเบี้ยเป็นไปตามประกาศธนาคารกำหนด</li>
							<li>หลักประกัน ผู้ค้ำประกันต้องมีคุณสมบัติดังนี้
								<ul class="a">
									<li>ต้องทำงานในหน่วยงานเดียวกันกับผู้กู้</li>
									<li>มีอายุ 20 ปีบริบูรณ์ เมื่อรวมอายุผู้ค้ำประกันกับระยะเวลาที่ชำระเงินกู้คืนต้องไม่เกิน 65 ปี</li>
									<li>อายุงานไม่ต่ำกว่า 1 ปี และวงเงินกู้ไม่เกิน 30,000 บาท คนค้ำประกันต้องมีเงินเดือนไม่ต่ำกว่า 7,000 บาท 1 คน วงเงินกู้เกิน 30,000 บาท คนค้ำประกันต้องมีเงินเดือนไม่ต่ำกว่า 7,000 บาท 2 คน หรือเงินเดือนตั้งแต่ 15,000 บาท 1 คน</li>
								</ul>
							</li>
							<li>การชำระเงินกู้คืน</li>
						</ol>
					</p>
					
					<h4>ขั้นตอนการดำเนินการ</h4>
					<p>
						<ol>
							<li>พนักงานที่สนใจ ติดต่อขอใบสมัครสินเชื่อได้ี่ที่ฝ่ายทรัพยากรมนุษย์้ทุกวันที่ทำการ</li>
							<li>ส่งใบสมัคร(เขียนรายละเอียดให้ครบถ้วน) พร้อมแนบเอกสารการสมัครที่ฝ่ายทรัพยากรมนุษย์ (โปรดเตรียมเอกสารให้พร้อมด้วยตนเอง)</li>
							<li>ฝ่ายทรัพยากรมนุษย์รับรองสถานะการเป็นพนักงานในใบสมัคร และส่งใบสมัครไปยังธนาคารทุกๆ 2 อาทิตย์</li>
							<li>ผู้ขอสินเชื่อทราบผลการพิจารณาภายใน 7 วันทำการ นับจากวันที่ธนาคารได้รับเอกสาร</li>
							<li>ธนาคารขอสงวนสิทธิ์ในการอนุมัติให้เงินกู้ตามความเหมาะสม</li>
						</ol>
					</p>
					
					<div class="blog-details-bt">
						<div class="btn-group">
							<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
						</div>
					</div>
				
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-8 col-xs-12 p-fix">
				<aside>
					<div class="sidebar-news-list">
						<h4>CATEGORY</h4>
						<ul>
							<li>
								<a href="Benefits1.aspx" class="tran3s">กองทุนสำรองเลี้ยงชีพ มอบเกียรติบัตรเงินรางวัล</a>
							</li>
							<li>
								<a href="Benefits2_1.aspx" class="tran3s">เงินช่วยเหลืองาน และโอกาสพิเศษต่างๆ</a>
							</li>
							<li>
								<a href="Benefits15.aspx" class="tran3s">เจ็บป่วย ประกันชีวิต สุขภาพ อุบัติเหตุ</a>
							</li>
							<li>
								<a href="Benefits5_1.aspx" class="tran3s">ปฏิบัติงานนอกสถานที่ (ในประเทศ)</a>
							</li>
							<li>
								<a href="Benefits3_1.aspx" class="tran3s">ปฏิบัติงานนอกสถานที่ (ต่างประเทศ)</a>
							</li>
							<li>
								<a href="Benefits4_1.aspx" class="tran3s">ปฏิบัติงาน EXHIBITION - EVENT (ในประเทศ)</a>
							</li>
							<li>
								<a href="Benefits7_1.aspx" class="tran3s">ค่าเลี้ยงรับรอง</a>
							</li>
							<li>
								<a href="Benefits8.aspx" class="tran3s">เงินกู้</a>
							</li>
							<li>
								<a href="Benefits9_1.aspx" class="tran3s">ส่วนลดสินค้า สำหรับพนักงาน</a>
							</li>
							<li>
								<a href="Benefits10_1.aspx" class="tran3s">เบี้ยขยัน</a>
							</li>
							<li>
								<a href="Benefits11_1.aspx" class="tran3s">ตรวจสุขภาพ</a>
							</li>
							<li>
								<a href="#" class="tran3s">สวัสดิการอื่นๆ</a>
							</li>
						</ul>
					</div>
				</aside>
				<aside>
					<div class="sidebar-news-list">
						<h4>Related Links</h4>
						<ul>
							<li>
								<a href="Benefits8_1.aspx" class="tran3s">
									สินเชื่อสวัสดิการพนักงาน (MOU ธนาคารธนชาต)
								</a>
							</li>
							<li>
								<a href="Benefits8_2.aspx" class="tran3s">
									สินเชื่อสวัสดิการพนักงาน (MOU ธนาคารกรุงศรีอยุธยา) (1)
								</a>
							</li>
							<li>
								<a href="Benefits8_3.aspx" class="tran3s">
									สินเชื่อสวัสดิการพนักงาน (MOU ธนาคารออมสิน)
								</a>
							</li>
							<li>
								<a href="Benefits8_4.aspx" class="tran3s">
									สวัสดิการเงินกู้บริษัท
								</a>
							</li>
						</ul>
					</div>
				</aside>
				<aside>
					<div class="sidebar-news-list">
						<h4>PDF Links</h4>
						<ul class="p">
							<li>
								<a href="Document/4Benefits/ขั้นตอนการดำเนินการกู้สินเชื่อ ธ.ออมสิน.pdf" class="tran3s">
								ขั้นตอนการดำเนินการกู้สินเชื่อ ธ.ออมสิน</a>
							</li>
							<li>
								<a href="Document/4Benefits/สินเชื่อสวัสดิการพนักงานร่วมกับสถาบันการเงิน.pdf" class="tran3s">
								สินเชื่อสวัสดิการพนักงานร่วมกับสถาบันการเงิน</a>
							</li>
						</ul>
					</div>
				</aside>
			</div>
		</div>
		
	</div>
</article>

</asp:Content>
