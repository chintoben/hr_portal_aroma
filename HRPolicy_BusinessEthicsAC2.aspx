﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_BusinessEthicsAC2.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_BusinessEthicsAC2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/hr.jpg" alt=""/></div>
<!-- end banner -->

<article class="blog-details-page">
	<div class="container">
		
		<div class="post-heading">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
                <li>/</li>
                <li><a href="HRPolicy_Announce.aspx">ระเบียบต่างๆ</a></li>
                <li>/</li>
                <li><a href="HRPolicy_BusinessEthicsAC.aspx">ประกาศ คำสั่ง ระเบียบเกี่ยวกับบัญชี การเงิน งบประมาณ จัดซื้อ</a></li>
            </ul>
			<h2>ระเบียบการเบิก-เคลียร์ค่าใช้จ่ายต่างๆ</h2>
		</div> <!-- /.post-heading -->
		
		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix">
				<div class="blog-details-post-wrapper">
				
					<img src="new/images/banner/hr-policy/img20.jpg" alt=""/>
					
					<p style="padding: 30px 0 0;">
         		    	เพื่อให้การเบิก – เคลียร์ เงินทดรองจ่ายของฝ่ายขาย Aroma Shop เป็นไปตามระบบที่เข้าใจตรงกัน และไม่ให้เกิดผลกระทบกับทางฝ่ายบัญชี การเงิน และฝ่ายขาย จึงขอกำหนดระยะเวลาในการดำเนินการให้เข้าใจตรงกันในการ เบิก – เคลียร์เงินทดรองจ่าย สำรองจ่ายดังนี้										
						<ol style="margin: 10px 0;">
							<li>ฝ่ายขายรวบรวมข้อมูลเอกสารต่างๆ จากเชลล์ ที่ปฏิบัติหน้าที่โดยแบ่งกลุ่มเป็น 2 กลุ่ม คือ 1 กลุ่มที่ต้องกลับมาประชุมที่สำนักงานกรุงเทพฯ ทุกสิ้นเดือน และกลุ่มที่ปฏิบัติงานนอกสถานที่ที่ไม่ได้
							 กลับเข้าสำนักงาน โดยกำหนดระยะเวลาในการจัดส่งเอกสารให้เจ้าหน้าที่ประสานงานขายเพื่อดำเนินการต่อตามเอกสารแนบ 1</li>
							<li>ประสานงานขายดำเนินการจัดทำเอกสารเคลียร์เงินทดรองจ่าย สำรองจ่าย ให้แล้วเสร็จพร้อมส่งคุณสรินทิพย์ พิจารณาภายในวันที่ 8 ไม่เกิน 10 และส่งฝ่ายงบประมาณภายในวันที่ 9 ไม่เกิน 12 
							 ของทุกเดือน</li>
							<li>ฝ่ายงบประมาณบันทึกงบประมาณภายในวันที่ 10 ไม่เกิน 13 ทุกเดือน</li>
							<li>ส่งฝ่ายการเงินเพื่อเคลียร์เงินทดรองจ่าย สำรองจ่ายภายในวันที่ 11 ไม่เกิน 15 ของทุกเดือน</li>
						</ol>
						ส่วนเอกสารการเบิกเงินทดรองจ่ายในรอบเดือนถัดไปให้ทางประสานงานขายจัดทำเอกสารพร้อมแนบหลักฐานต่างประกอบการเบิกเงินทดรองจ่ายต่าง ส่งคุณสรินทิพย์ ผู้ช่วยผู้จัดการฝ่ายขาย ภายในวันที่ 15 
						ของทุกเดือน และส่งเอกสาร ภายในวันพฤหัสบดีที่ 3 ของทุกเดือน และจะจ่ายเงินทดรองจ่ายให้ฝ่ายขายภายในวันพฤหัสบดีที่ 1 ของทุกเดือน<br><br> 

						กรณีพนักงานขายท่านใดไม่ดำเนินการตามที่กำหนดในการเคลียร์เงินทดรองจ่าย ขอยกเลิกสิทธิ์ในการดำเนินการจ่ายเงินทดรองจ่ายล่วงหน้าให้ก่อน และจะต้องเคลียร์เงินทดรองจ่ายให้แล้วเสร็จจึงจะได้เบิก
						เงินทดรองจ่ายในรอบถัดไปตามระเบียบการเบิกเงินทดรองจ่าย<br><br>

						ทั้งนี้ให้มีผลตั้งแต่วันที่ 1 กุมภาพันธ์ 2559 จนกว่าจะมีการเปลี่ยนแปลง
					</p>
					
					<div class="blog-details-bt">
						<div class="btn-group">
							<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
						</div>
					</div>
				
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-8 col-xs-12 p-fix">
				<aside>
					<div class="sidebar-news-list">
						<h4>CATEGORY</h4>
						<ul>
							<li>
								<a href="HRPolicy_BusinessEthicsHR.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบทรัพยากรมนุษย์</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAS.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบเกี่ยวกับทรัพย์สิน</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAC.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบเกี่ยวกับบัญชี การเงิน งบประมาณ จัดซื้อ</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsIT.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบเกี่ยวกับเทคโนโลยีสารสนเทศ</a>
							</li>
							<li>
								<a href="Benefits1_1.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบอื่นๆ</a>
							</li>
						</ul>
					</div>
				</aside>
				<aside>
					<div class="sidebar-news-list">
						<h4>Related Links</h4>
						<ul>
							<li>
								<a href="HRPolicy_BusinessEthicsAC1.aspx" class="tran3s">
									ระเบียบการเบิก-เคลียร์ค่าใช้จ่ายต่างๆ
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAC2.aspx" class="tran3s">
									ระเบียบการเบิกทดรองจ่ายฝ่ายขาย Aroma Shop
								</a>
							</li>
						</ul>
					</div>
				</aside>
			</div>
		</div>
		
	</div>
</article>

</asp:Content>