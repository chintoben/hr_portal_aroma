﻿Imports System.Data.SqlClient

Public Class A_Authorize
    Inherits System.Web.UI.Page
    Private sms As New PKMsg("")
    Dim role As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            'role = Request.QueryString("Role")
            'If role <> "admin" Then
            '    sms.Msg = "สามารถเข้าใช้งานได้เฉพาะ Admin เท่านั้น!!"
            '    Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            '    Exit Sub
            'End If
        End If



    End Sub

    Protected Sub btn_add_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btn_add.Click
        Dim db_Form As New Connect_HR
        Dim sqlCon As New SqlConnection
        Dim sqlCmd As New SqlCommand
        Dim sql As String = ""
        Dim TaskId As String = ""
        Dim tr As SqlTransaction
        Dim ErrorMsg As String = ""

        If txtUsername.Text = "" Or txtPassword.Text = "" Or DropRole.SelectedValue = "" Then
            sms.Msg = "กรุณาระบุข้อมูลให้ครบ!!!"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub


        End If

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_Form.Constr2)
            sqlCon.Open()
        End If

        tr = sqlCon.BeginTransaction()
        Try
            sql = "INSERT INTO [TB_Authorize] "
            sql = sql & "([Username],[Password],[Role],[Status],[CreateDate])"
            sql = sql & " Values (@F1,@F2,@F3,@F4,@F5)"
            Dim cmd As SqlCommand = New SqlCommand(sql, sqlCon, tr)
            cmd.Parameters.Add("@F1", SqlDbType.VarChar, 0).Value = txtUsername.Text
            cmd.Parameters.Add("@F2", SqlDbType.VarChar, 0).Value = txtPassword.Text
            cmd.Parameters.Add("@F3", SqlDbType.VarChar, 0).Value = DropRole.SelectedValue
            cmd.Parameters.Add("@F4", SqlDbType.VarChar, 0).Value = "A"
            cmd.Parameters.Add("@F5", SqlDbType.VarChar, 0).Value = Date.Today.ToString("yyyy-MM-dd")
            cmd.ExecuteNonQuery()

            tr.Commit()
            sqlCon.Close()
            txtUsername.Text = ""
            DropRole.SelectedValue = ""
            txtPassword.Text = ""

        Catch ex As Exception

            tr.Rollback()
            sqlCon.Close()

            Exit Sub
        End Try
        GridView1.DataBind()
    End Sub

End Class