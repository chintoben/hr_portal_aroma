﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="EmployeeRelation_Activities_Party.aspx.vb" Inherits="Aroma_HRPortal.EmployeeRelation_Activities_Party" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<!-- banner -->
<div class="base-banner">
<img src="new/images/banner/Party Banner.jpg" 
        alt="" />
</div>
<!-- end banner -->

<section id="news-section">
	<div class="container">
	
        	<div class="post-heading">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
                <li>/</li>
                <li><a href="EmployeeRelation_Activities.aspx">กิจกรรมต่างๆ</a></li>
               <%-- <li>/</li>
                <li><a href="HRPolicy_BusinessEthicsAC.aspx">ประกาศ คำสั่ง ระเบียบเกี่ยวกับบัญชี การเงิน งบประมาณ จัดซื้อ</a></li>--%>
            </ul>
			<h2>COLOURFUL-NEON</h2>
		</div> 


		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix">
				<div class="blog-details-post-wrapper"  >
				
					  
                    <a href="https://drive.google.com/drive/folders/1l72pAyg8MrUTgbnQfvRSZlq9qEO-kCj9?usp=sharing" target="_blank"><h3>Click >>> ชมรูปภาพ บรรยากาศภายในงาน COLOURFUL-NEON</h3></a>
                    <br />
                    
                    <a href="https://drive.google.com/drive/folders/1xPQKAdXdsw-Q6mead1htqCodtIMijZPK?usp=sharing" target="_blank"><h3>Click >>> ชมรูปภาพ พิธีมอบเกียรติบัตร พนักงานสถิติการทำงานยอดเยี่ยมประจำปี 2561</h3> </a>

                    
                    
                       <h4>Aroma Party 2019</h4>
                     <video width="700" height="400" controls>
                      <source src="video\1 Aroma Party 2019 - Final.mp4" type="video/mp4">
                    </video>
                  

                       <h4>หน้างาน</h4>
                     <video width="700" height="400" controls>
                      <source src="video\2 หน้างาน.mp4" type="video/mp4">
                    </video>
                  

                       <h4>เปิดงาน</h4>
                     <video width="700" height="400" controls>
                      <source src="video\3 เปิดงาน.mp4" type="video/mp4">
                    </video>
                  

                       <h4>ประกวดแต่งกายและเต้น</h4>
                     <video width="700" height="400" controls>
                      <source src="video\4 ประกวดแต่งกายและเต้น.mp4" type="video/mp4">
                    </video>

                      <h4>มอบรางวัลและจับรางวัล</h4>
                     <video width="700" height="400" controls>
                      <source src="video\P_4 VDO มอบรางวัลและจับรางวัล.mp4" type="video/mp4">
                    </video>

                      <h4>เก็บตก</h4>
                     <video width="700" height="400" controls>
                      <source src="video\P_5 VDO เก็บตก.mp4" type="video/mp4">
                    </video>
                  
                 
				


					
					<div class="blog-details-bt">
						<div class="btn-group">
							<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
						</div>
					</div>
				
				</div>
			</div>
			
		</div>

	</div>
</section>

</asp:Content>
