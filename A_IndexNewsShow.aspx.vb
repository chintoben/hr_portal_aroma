﻿Imports System.Data.OleDb

Public Class A_IndexNewsShow
    Inherits System.Web.UI.Page
    Dim dtt As New DataTable
    Private sms As New PKMsg("")
    Dim DB_HR As New Connect_HR
    Dim dt, dt1, dt2, dt3 As New DataTable
    Dim Cmd As OleDbCommand
    Dim Dr As OleDbDataReader
    Dim sqlCheck, sql, ssql, sql1, sql2, sql3, sql4, sqlCheckDay As String
    Dim ImageName As String
    Private dp As New DBProc
    Dim status, Role, IndexId As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        IndexId = Request.QueryString("IndexID")
        If IndexId = "ID" Then
            IndexId = 0
        End If

        If Not Page.IsPostBack Then
            loaddata(IndexId)
        End If
    End Sub


    Private Sub loaddata(Optional ByVal sCond As String = "")

        sql = " SELECT IndexID,Type,TxtSubject,TxtDetail ,image ,image_name,Status ,CreateBy ,CreateDate	"
        sql += " FROM TB_IndexHome "
        sql += " where IndexID = '" & IndexId & "' "

        dt = DB_HR.GetDataTable(sql)

        If dt.Rows.Count > 0 Then
            txtSubject.Text = dt.Rows(0)("TxtSubject")
            txtDetail.Text = dt.Rows(0)("TxtDetail")
            lblNum.Text = dt.Rows(0)("IndexID")

            If Not IsDBNull(dt.Rows(0)("image")) Then
                img.ImageUrl = Readimage(dt.Rows(0)("IndexID"))
            End If

        End If

        att_Read()

    End Sub

    Function ReadImage(ByVal IndexID As String)
        'Return ("Readimage.aspx?ImageID=" & IndexID)
        Return ("ReadimageBanner.aspx?ID=" & IndexID)
    End Function


    Private Sub att_Read()
        Dim dt As New Data.DataTable
        Dim sSql As String = "select * from [TB_IndexHomeAtt] where [IndexID] = '" & lblNum.Text & "'  "
        dt = DB_HR.GetDataTable(sSql)
        If dt.Rows.Count > 0 Then
            DataGrid1.DataSource = dt
            DataGrid1.DataBind()
        Else
        End If
    End Sub


End Class