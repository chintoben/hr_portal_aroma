﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="Benefits8.aspx.vb" Inherits="Aroma_HRPortal.Benefits8_1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/benefit/banner1.jpg" /></div>
<!-- end banner -->

<section id="about-us" class="blog-category">
<div class="container">
	<div class="theme-title tt-edit category-title">
        <ul>
            <li><a href="index_hr.aspx">HOME</a></li>
            <li>/</li>
            <li><a href="Benefits_Employee.aspx">ระเบียบสวัสดิการ</a></li>
        </ul>
		<h2>เงินกู้</h2>
	</div> <!-- /.theme-title -->

	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-bank"></i>
				</div>
				<p><a href="Benefits8_1.aspx" class="tran3s">สินเชื่อสวัสดิการพนักงาน<br>(MOU ธนาคารธนชาต)</a></p>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-bank-1"></i>
				</div>
				<p><a href="Benefits8_2.aspx" class="tran3s">สินเชื่อสวัสดิการพนักงาน<br>(MOU ธนาคารกรุงศรีอยุธยา) (1)</a></p>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-bank-2"></i>
				</div>
				<p><a href="Benefits8_3.aspx" class="tran3s">สินเชื่อสวัสดิการพนักงาน<br>(MOU ธนาคารออมสิน)</a></p>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-funds"></i>
				</div>
				<p><a href="Benefits8_4.aspx" class="tran3s">สวัสดิการเงินกู้บริษัท</a></p>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-checked"></i>
				</div>
				<p><a href="Document/4Benefits/ขั้นตอนการดำเนินการกู้สินเชื่อ ธ.ออมสิน.pdf" target="_blank" class="tran3s">ขั้นตอนการดำเนินการ<br>กู้สินเชื่อ ธ.ออมสิน</a></p>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-transfer"></i>
				</div>
				<p><a href="Document/4Benefits/สินเชื่อสวัสดิการพนักงานร่วมกับสถาบันการเงิน.pdf" target="_blank" class="tran3s">สินเชื่อสวัสดิการพนักงาน<br>ร่วมกับสถาบันการเงิน</a></p>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
	</div> <!-- /.row -->
	<div class="blog-category-bt">
		<div class="btn-group">
			<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
		</div>
	</div>
</div> <!-- /.container -->
</section>

</asp:Content>
