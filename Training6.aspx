﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="Flow_IT.aspx.vb" Inherits="Aroma_HRPortal.Flow_IT" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/development-training/banner.jpg" alt=""/></div>
<!-- end banner -->

<section id="about-us" class="blog-category">
<div class="container">
	<div class="theme-title tt-edit category-title">
        <ul>
            <li><a href="index_hr.aspx">HOME</a></li>
        </ul>
		<h2>การบริหารผลงาน/ประเมินผลการปฏิบัติงาน</h2>
		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p>
	</div> <!-- /.theme-title -->

	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas fa-pencil-alt"></i>
				</div>
				<h3><a href="#" class="tran3s">การเรียนรู้และพัฒนาพนักงาน (Learning & Development)</a></h3>
				<p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
				<a href="#" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas fa-pencil-alt"></i>
				</div>
				<h3><a href="#" class="tran3s">สมรรถนะ (Competency)</a></h3>
				<p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
				<a href="#" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas fa-pencil-alt"></i>
				</div>
				<h3><a href="#" class="tran3s">แผนการพัฒนาและฝึกอบรมพนักงาน (Development Road Map)</a></h3>
				<p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
				<a href="#" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas fa-pencil-alt"></i>
				</div>
				<h3><a href="#" class="tran3s">หลักสูตรพัฒนาฝึกอบรม (Development Course)</a></h3>
				<p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
				<a href="#" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
	</div> <!-- /.row -->
</div> <!-- /.container -->
</section> 

</asp:Content>
