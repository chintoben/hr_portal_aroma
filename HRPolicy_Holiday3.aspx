﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_Holiday3.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_Holiday3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/hr.jpg" alt=""/></div>
<!-- end banner -->

<article class="blog-details-page">
	<div class="container">
		
		<div class="post-heading">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
                <li>/</li>
                <li><a href="HRPolicy_Holidays.aspx">วันหยุดประจำปีบริษัท</a></li>
            </ul>
			<h2>วันหยุดประเพณี ประจำปี พ.ศ. 2562</h2>
		</div> <!-- /.post-heading -->
		
		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix">
				<div class="blog-details-post-wrapper">
				
					<img src="new/images/banner/hr-policy/img25.jpg" alt=""/>
					
					<%--<h4>บริษัท เค.วี.เอ็น. อิมปอร์ต เอกซ์ปอร์ต (1991) จำกัด  / บริษัท ไลอ้อน ทรี-สตาร์ จำกัด</h4>--%>

					<h4>ทางบริษัทฯ ได้กำหนดวันหยุดประจำปี พ.ศ. 2562 ดังนี้</h4>
					<p>
						<table class="table">
						  <tbody>
							<tr>
							  <td width="2%">1.</td>
							  <td width="15%">วันอังคารที่</td>
							  <td width="15%">1 มกราคม</td>
							  <td width="68%">วันขึ้นปีใหม่</td>
							</tr>
						<%--	<tr>
							  <td>&nbsp;</td>
							  <td>วันอังคารที่</td>
							  <td>19 กุมภาพันธ์</td>
							  <td>วันมาฆบูชา</td>
							</tr>--%>
							<tr>
							  <td>2.</td>
							  <td>วันอังคารที่</td>
							  <td>19 กุมภาพันธ์</td>
							  <td>วันมาฆบูชา</td>
							</tr>
							<tr>
							  <td>3.</td>
							  <td>วันจันทร์ที่</td>
							  <td>8  เมษายน</td>
							  <td>ชดเชยวันพระบาทสมเด็จพระพุทธยอดฟ้าจุฬาโลกมหาราช และวันที่ระลึกมหาจักรีบรมราชวงศ์  (วันเสาร์ที่ 6 เมษายน) </td>
							</tr>
							<tr>
							  <td>4.</td>
							  <td>วันจันทร์ที่</td>
							  <td>15 เมษายน</td>
							  <td>ชดเชยวันสงกรานต์ (วันเสาร์ที่ 13  เมษายน)</td>
							</tr>
							<tr>
							  <td>5.</td>
							  <td>วันอังคารที่</td>
							  <td>16 เมษายน</td>
							  <td>ชดเชยวันสงกรานต์ (วันอาทิตย์ที่ 14  เมษายน) </td>
							</tr>
							<tr>
							  <td>6.</td>
							  <td>วันพุธที่</td>
							  <td>1 พฤษภาคม</td>
							  <td>วันแรงงานแห่งชาติ</td>
							</tr>
							<tr>
							  <td>7.</td>
							  <td> วันจันทร์ที่</td>
							  <td>20 พฤษภาคม</td>
							  <td>ชดเชยวันวิสาขบูชา (วันเสาร์ที่ 18 พฤษภาคม)</td>
							</tr>
							<tr>
							  <td>8.</td>
							  <td>วันอังคารที่</td>
							  <td>16  กรกฎาคม</td>
							  <td>วันอาสาฬหบูชา</td>
							</tr>
							<tr>
							  <td>9.</td>
							  <td>วันจันทร์ที่</td>
							  <td>29 กรกฏาคม</td>
							  <td>ชดเชยวันเฉลิมพระชนมพรรษา สมเด็จพระเจ้าอยู่หัวมหาวชิราลงกรณ บดินทรเทพยวรางกูร (วันอาทิตย์ที่  28  กรกฎาคม)</td>
							</tr>
							<tr>
							  <td>10.</td>
							  <td>วันจันทร์ที่</td>
							  <td>12 สิงหาคม</td>
							  <td>วันเฉลิมพระชนมพรรษาสมเด็จพระนางเจ้าฯ พระบรมราชินีนาถในพระบาทสมเด็จพระปรมินทร มหาภูมิพลอดุลยเดช บรมนาถบพิตร</td>
							</tr>
							<tr>
							  <td>11.</td>
							  <td>วันจันทร์ที่</td>
							  <td>14 ตุลาคม</td>
							  <td>ชดเชยวันคล้ายวันสวรรคต  พระบาทสมเด็จพระปรมินทรมหาภูมิพลอดุลเดช บรมนาถบพิตร (วันอาทิตย์ที่ 13 ตุลาคม)</td>
							</tr>
							<tr>
							  <td>12.</td>
							  <td> วันพุธที่</td>
							  <td>23 ตุลาคม</td>
							  <td>วันปิยมหาราช</td>
							</tr>
							<tr>
							  <td>13.</td>
							  <td>วันพฤหัสบดีที่</td>
							  <td>5 ธันวาคม</td>
							  <td>วันคล้ายวันเฉลิมพระชนมพรรษา พระบาทสมเด็จพระปรมินทรมหาภูมิพลอดุลยเดช บรมนาถบพิตร วันชาติ และวันพ่อแห่งชาติ</td>
							</tr>
							<tr>
							  <td>14.</td>
							  <td>วันอังคารที่</td>
							  <td>10 ธันวาคม</td>
							  <td>วันรัฐธรรมนูญ</td>
							</tr>
							<tr>
							  <td>15.</td>
							  <td>วันอังคารที่</td>
							  <td>31 ธันวาคม</td>
							  <td>วันสิ้นปี</td>
							</tr>
						  </tbody>
						</table>
					</p>
					
					<p>
						จึงประกาศมาเพื่อทราบโดยทั่วกัน<br><br>

						ประกาศ ณ วันที่ 16 ตุลาคม 2561<br><br>	
		 			 
						หมายเหตุ : หากมีการประกาศเปลี่ยนแปลงวันหยุดตามราชประเพณีหรือวันสำคัญต่างๆ บริษัทฯ จะมีการพิจารณาเปลี่ยนแปลงและ แจ้งให้ทราบอีกครั้ง
					</p>
					
					<div class="blog-details-bt">
						<div class="btn-group">
							<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
						</div>
					</div>
				
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-8 col-xs-12 p-fix">
				<aside>
					<div class="sidebar-news-list">
						<h4>CATEGORY</h4>
						<ul>
							<li>
								<a href="HRPolicy_Holiday1.aspx" class="tran3s">วันหยุดประเพณี ประจำปี พ.ศ. 2560</a>
							</li>
							<li>
								<a href="HRPolicy_Holiday2.aspx" class="tran3s">วันหยุดประเพณี ประจำปี พ.ศ. 2561</a>
							</li>
                            <li>
								<a href="HRPolicy_Holiday3.aspx" class="tran3s">วันหยุดประเพณี ประจำปี พ.ศ. 2562</a>
							</li>
                            <li>
								<a href="HRPolicy_Holiday4.aspx" class="tran3s">วันหยุดประเพณี ประจำปี พ.ศ. 2563</a>
							</li>
                              <li>
								<a href="HRPolicy_Holiday5.aspx" class="tran3s">วันหยุดประเพณี ประจำปี พ.ศ. 2564</a>
							</li>
						</ul>
					</div>
				</aside>
			</div>
		</div>
		
	</div>
</article>
</asp:Content>
