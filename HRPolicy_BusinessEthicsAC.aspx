﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_BusinessEthicsAC1.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_BusinessEthicsAC1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/hr.jpg" alt=""/></div>
<!-- end banner -->

<section id="about-us" class="blog-category">
<div class="container">
	<div class="theme-title tt-edit category-title">
        <ul>
            <li><a href="index_hr.aspx">HOME</a></li>
            <li>/</li>
            <li><a href="HRPolicy_Announce.aspx">ระเบียบต่างๆ</a></li>
        </ul>
		<h2>ประกาศ คำสั่ง ระเบียบเกี่ยวกับบัญชี การเงิน งบประมาณ จัดซื้อ</h2>
	</div> <!-- /.theme-title -->

	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-6 div-empty"></div>
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-009-pay"></i>
				</div>
				<%--<p><a href="HRPolicy_BusinessEthicsAC1.aspx" class="tran3s">ระเบียบการเบิก-เคลียร์ค่าใช้จ่ายต่างๆ</a></p>--%>


                <p>
						
				<%--<a href="Document/6Flow/AS/Flowการบริหารทรัพย์สิน.pdf" target="_blank">Flow การบริหารทรัพย์สิน</a>--%>
					<a href="Document/6Flow/AS/ระเบียบเบิกและเคลียร์เงินทดรอง-สำรองจ่าย เริ่ม มี.ค.61.pdf" target="_blank">ระเบียบเบิกและเคลียร์เงินทดรอง-สำรองจ่าย เริ่ม มี.ค.61</a>	
			    </p>

			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-008-money-2"></i>
				</div>

				<p>
                
                <%--<a href="HRPolicy_BusinessEthicsAC2.aspx" class="tran3s">ระเบียบการเบิกทดรองจ่าย<br>ฝ่ายขาย Aroma Shop</a>--%>
               <a href="Document/6Flow/AS/ระเบียบเบิกเงินสดย่อย เริ่ม มี.ค.61.pdf" target="_blank">ระเบียบเบิกเงินสดย่อย เริ่ม มี.ค.61</a>	
                
                </p>

			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->

        		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-008-money-2"></i>
				</div>

				<p>
                
                <%--<a href="HRPolicy_BusinessEthicsAC2.aspx" class="tran3s">ระเบียบการเบิกทดรองจ่าย<br>ฝ่ายขาย Aroma Shop</a>--%>
               <a href="Document/6Flow/AS/อำนาจอนุมัติ.pdf" target="_blank">อำนาจอนุมัติ</a>	
                
                </p>

			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->


		<div class="col-lg-3 col-md-3 col-sm-6 div-empty"></div>
	</div> <!-- /.row -->
	<div class="blog-category-bt">
		<div class="btn-group">
			<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
		</div>
	</div>
</div> <!-- /.container -->
</section>

</asp:Content>