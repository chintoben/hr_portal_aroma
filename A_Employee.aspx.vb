﻿Imports System.Data.SqlClient
Imports System.ResolveEventArgs
Imports System.Drawing
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Public Class A_Employee
    Inherits System.Web.UI.Page
    Private sms As New PKMsg("")
    Private DB_HR As New Connect_HR
    Dim dtt As New DataTable
    Dim sqlCheck, sql As String
    Dim dt As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            'Me.BindGrid()
        End If
    End Sub


    Private Sub BindGrid()
        'Dim constr As String = ConfigurationManager.ConnectionStrings("constr").ConnectionString
        'Using con As New SqlConnection(constr)

        '    Using cmd As New SqlCommand("SELECT top 200 [site],[Employee_ID],[fullname] ,[branch]	,[department],[position],[image_name],status FROM [TB_Employee]")

        '        Using sda As New SqlDataAdapter()
        '            cmd.Connection = con
        '            sda.SelectCommand = cmd
        '            Using dt As New DataTable()
        '                sda.Fill(dt)
        '                GridView1.DataSource = dt
        '                GridView1.DataBind()
        '            End Using
        '        End Using
        '    End Using
        'End Using

        Dim sql As String = ""

        sql = "SELECT top 200 emp.[site],emp.[Employee_ID],emp.[fullname] ,emp.[branch]	,dep.[department_name] as department,emp.[position],emp.[image_name],isnull(emp.status,'InActive') as status "
        sql += " FROM [TB_Employee] emp"
        sql += " left outer join [TB_department] dep on dep.department_id = emp.[department_id]  and dep.departments_id = emp.[departments_id] "
        sql += " where emp.[status] = 'Active' "

        If drpEmployee.SelectedValue <> "1" Then
            sql += " and emp.Employee_ID = '" & drpEmployee.SelectedValue & "' "
        End If

        If drpDepartment.SelectedValue <> "1" Then
            sql += " and emp.department_id = '" & drpDepartment.SelectedValue & "' "
        End If

        ' sql += " order by emp.[fullname]"

        dt = DB_HR.GetDataTable(sql)

        If dt.Rows.Count > 0 Then
            GridView1.DataSourceID = Nothing
            GridView1.DataSource = dt
            GridView1.DataBind()
        End If

    End Sub

    'Protected Sub btn_Search_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btn_Search.Click

    '    Dim sql As String = ""

    '    sql = "SELECT top 200 emp.[site],emp.[Employee_ID],emp.[fullname] ,emp.[branch]	,dep.[department_name] as department,emp.[position],emp.[image_name],isnull(emp.status,'InActive') as status FROM [TB_Employee] emp "
    '    sql += "left outer join TB_department dep on dep.department_id = emp.department_id and dep.departments_id = emp.[departments_id] "
    '    sql += " where 1=1 "

    '    If drpEmployee.SelectedValue <> "1" Then
    '        sql += " and emp.Employee_ID = '" & drpEmployee.SelectedValue & "' "
    '    End If

    '    If drpDepartment.SelectedValue <> "1" Then
    '        sql += " and emp.department_id = '" & drpDepartment.SelectedValue & "' "
    '    End If

    '    dt = DB_HR.GetDataTable(sql)

    '    If dt.Rows.Count > 0 Then
    '        GridView1.DataSourceID = Nothing
    '        GridView1.DataSource = dt
    '        GridView1.DataBind()
    '    End If

    'End Sub

    'Protected Sub btnNew_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btnNew.Click
    '    Server.Transfer("A_employeeData.aspx?&mode=new")
    'End Sub

    'Protected Sub GridView1_PageIndexChanged(sender As Object, e As EventArgs) Handles GridView1.PageIndexChanged
    '    TaskGrid.CurrentPageIndex = e.NewPageIndex
    '    Cpage = e.NewPageIndex
    '    loaddataGrid(res_id)
    'End Sub

    Protected Sub OnPageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        GridView1.PageIndex = e.NewPageIndex
        Me.BindGrid()
    End Sub

    'search แล้ว มาคลิก next page ไม่ได้

    Protected Sub drpEmployee_Init(sender As Object, e As EventArgs) Handles drpEmployee.Init
        Dim sqlCon As New SqlConnection(DB_HR.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [employee_id] as id,[fullname] from [TB_Employee]  where [status] = 'Active'  order by  fullname"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_HR.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpEmployee.Items.Clear()
        drpEmployee.Items.Add(New ListItem("", 1))
        While Rs.Read
            drpEmployee.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
        End While
    End Sub


    Protected Sub drpDepartment_Init(sender As Object, e As EventArgs) Handles drpDepartment.Init
        Dim sqlCon As New SqlConnection(DB_HR.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select department_id as id, department_name from [TB_department] where Stat ='Active'  order by department_id  "

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_HR.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpDepartment.Items.Clear()
        drpDepartment.Items.Add(New ListItem("", 1))
        While Rs.Read
            drpDepartment.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
        End While

    End Sub



    Protected Sub GridView1_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        Me.BindGrid()
        GridView1.PageIndex = e.NewPageIndex
        GridView1.DataBind()
    End Sub

    'Protected Sub btnImport_Click(sender As Object, e As EventArgs) Handles btnImport.Click
    '    Server.Transfer("A_employeeImport.aspx")
    'End Sub





    Protected Sub btn_Search_Click(sender As Object, e As EventArgs) Handles btn_Search.Click
        Dim sql As String = ""

        sql = "SELECT top 200 emp.[site],emp.[Employee_ID],emp.[fullname] ,emp.[branch]	,dep.[department_name] as department,emp.[position],emp.[image_name],isnull(emp.status,'InActive') as status FROM [TB_Employee] emp "
        sql += "left outer join TB_department dep on dep.department_id = emp.department_id and dep.departments_id = emp.[departments_id] "
        sql += " where 1=1 "

        If drpEmployee.SelectedValue <> "1" Then
            sql += " and emp.Employee_ID = '" & drpEmployee.SelectedValue & "' "
        End If

        If drpDepartment.SelectedValue <> "1" Then
            sql += " and emp.department_id = '" & drpDepartment.SelectedValue & "' "
        End If

        dt = DB_HR.GetDataTable(sql)

        If dt.Rows.Count > 0 Then
            GridView1.DataSourceID = Nothing
            GridView1.DataSource = dt
            GridView1.DataBind()
        End If
    End Sub

    Protected Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click
        Server.Transfer("A_employeeData.aspx?&mode=new")
    End Sub

    Protected Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        Loaddata_Excel()
        Response.Clear()
        Response.Charset = "windows-874"
        Response.ContentEncoding = System.Text.Encoding.UTF8
        Response.Cache.SetCacheability(HttpCacheability.NoCache)

        Response.AddHeader("content-disposition", "attachment;filename=ข้อมูลพนักงาน (Employee Data).xls")

        Response.ContentType = "application/vnd.xls"

        Dim swWriter As New System.IO.StringWriter

        Dim htwWriter As New System.Web.UI.HtmlTextWriter(swWriter)

        'TableLevel1.RenderControl(htwWriter)

        'Response.Write(swWriter.ToString())
        Response.Write("<html><head><META http-equiv=Content-Type content=text/html; charset=utf-8></head><body><FONT face=Tahoma>" & swWriter.ToString() & "</FONT></Body></Html>")
        Response.End()
    End Sub
    Sub Loaddata_Excel()
        Dim sqlCon As New SqlConnection(DB_HR.sqlCon)
        Dim SqlCmd2 As New SqlCommand
        Dim da As New SqlDataAdapter
        Dim SqlDataReader2 As SqlDataReader
        Dim i As Integer
        Dim ds As New DataSet
        sql = "select * FROM [VW_Employee_Report]"
        'da = New SqlDataAdapter(sql, sqlCon)
        'da.Fill(ds, "ds")

        Try
            sqlCon.Open()
            SqlCmd2 = New SqlCommand(sql, sqlCon)
            SqlDataReader2 = SqlCmd2.ExecuteReader()



            Call Sethead()

            While SqlDataReader2.Read()
                Try
                    Dim RowNew As New TableRow
                    'Employee_id
                    Dim CellNew1 As New TableCell
                    CellNew1.Text = SqlDataReader2.GetString(0).ToString
                    If i Mod 2 <> 0 Then
                        CellNew1.BackColor = Color.FromArgb(255, 245, 245, 245)
                    Else
                        CellNew1.BackColor = Color.FromArgb(255, 231, 231, 231)
                    End If
                    CellNew1.Font.Size = FontUnit.XSmall
                    RowNew.Cells.Add(CellNew1)

                    'Employee_name
                    Dim CellNew2 As New TableCell
                    CellNew2.Text = SqlDataReader2.GetString(1).ToString
                    If i Mod 2 <> 0 Then
                        CellNew2.BackColor = Color.FromArgb(255, 245, 245, 245)
                    Else
                        CellNew2.BackColor = Color.FromArgb(255, 231, 231, 231)
                    End If
                    CellNew2.Font.Size = FontUnit.XSmall
                    RowNew.Cells.Add(CellNew2)

                    'Employee_nameEN
                    Dim CellNew3 As New TableCell
                    CellNew3.Text = SqlDataReader2.GetString(2).ToString
                    If i Mod 2 <> 0 Then
                        CellNew3.BackColor = Color.FromArgb(255, 245, 245, 245)
                    Else
                        CellNew3.BackColor = Color.FromArgb(255, 231, 231, 231)
                    End If
                    CellNew3.Font.Size = FontUnit.XSmall
                    RowNew.Cells.Add(CellNew3)

                    'site
                    Dim CellNew4 As New TableCell
                    CellNew4.Text = SqlDataReader2.GetString(3).ToString
                    If i Mod 2 <> 0 Then
                        CellNew4.BackColor = Color.FromArgb(255, 245, 245, 245)
                    Else
                        CellNew4.BackColor = Color.FromArgb(255, 231, 231, 231)
                    End If
                    CellNew4.Font.Size = FontUnit.XSmall
                    RowNew.Cells.Add(CellNew4)

                    'departments
                    Dim CellNew5 As New TableCell
                    CellNew5.Text = SqlDataReader2.GetString(4).ToString
                    If i Mod 2 <> 0 Then
                        CellNew5.BackColor = Color.FromArgb(255, 245, 245, 245)
                    Else
                        CellNew5.BackColor = Color.FromArgb(255, 231, 231, 231)
                    End If
                    CellNew5.Font.Size = FontUnit.XSmall
                    RowNew.Cells.Add(CellNew5)

                    'department
                    Dim CellNew6 As New TableCell
                    CellNew6.Text = SqlDataReader2.GetString(5).ToString
                    If i Mod 2 <> 0 Then
                        CellNew6.BackColor = Color.FromArgb(255, 245, 245, 245)
                    Else
                        CellNew6.BackColor = Color.FromArgb(255, 231, 231, 231)
                    End If
                    CellNew6.Font.Size = FontUnit.XSmall
                    RowNew.Cells.Add(CellNew6)

                    'Employee_Lavel
                    Dim CellNew7 As New TableCell
                    CellNew7.Text = SqlDataReader2.GetString(6).ToString
                    If i Mod 2 <> 0 Then
                        CellNew7.BackColor = Color.FromArgb(255, 245, 245, 245)
                    Else
                        CellNew7.BackColor = Color.FromArgb(255, 231, 231, 231)
                    End If
                    CellNew7.Font.Size = FontUnit.XSmall
                    RowNew.Cells.Add(CellNew7)

                    'PositionEN
                    Dim CellNew8 As New TableCell
                    CellNew8.Text = SqlDataReader2.GetString(7).ToString
                    If i Mod 2 <> 0 Then
                        CellNew8.BackColor = Color.FromArgb(255, 245, 245, 245)
                    Else
                        CellNew8.BackColor = Color.FromArgb(255, 231, 231, 231)
                    End If
                    CellNew8.Font.Size = FontUnit.XSmall
                    RowNew.Cells.Add(CellNew8)

                    'Position
                    Dim CellNew9 As New TableCell
                    CellNew9.Text = SqlDataReader2.GetString(8).ToString
                    If i Mod 2 <> 0 Then
                        CellNew9.BackColor = Color.FromArgb(255, 245, 245, 245)
                    Else
                        CellNew9.BackColor = Color.FromArgb(255, 231, 231, 231)
                    End If
                    CellNew9.Font.Size = FontUnit.XSmall
                    RowNew.Cells.Add(CellNew9)

                    'ID_Card
                    Dim CellNew10 As New TableCell
                    CellNew10.Text = SqlDataReader2.GetString(9).ToString
                    If i Mod 2 <> 0 Then
                        CellNew10.BackColor = Color.FromArgb(255, 245, 245, 245)
                    Else
                        CellNew10.BackColor = Color.FromArgb(255, 231, 231, 231)
                    End If
                    CellNew10.Font.Size = FontUnit.XSmall
                    RowNew.Cells.Add(CellNew10)

                    'Gendle
                    Dim CellNew11 As New TableCell
                    CellNew11.Text = SqlDataReader2.GetString(10).ToString
                    If i Mod 2 <> 0 Then
                        CellNew11.BackColor = Color.FromArgb(255, 245, 245, 245)
                    Else
                        CellNew11.BackColor = Color.FromArgb(255, 231, 231, 231)
                    End If
                    CellNew11.Font.Size = FontUnit.XSmall
                    RowNew.Cells.Add(CellNew11)

                    'Start_Date
                    Dim CellNew12 As New TableCell
                    CellNew12.Text = SqlDataReader2.GetString(11).ToString
                    If i Mod 2 <> 0 Then
                        CellNew12.BackColor = Color.FromArgb(255, 245, 245, 245)
                    Else
                        CellNew12.BackColor = Color.FromArgb(255, 231, 231, 231)
                    End If
                    CellNew12.Font.Size = FontUnit.XSmall
                    RowNew.Cells.Add(CellNew12)

                    'Email
                    Dim CellNew13 As New TableCell
                    CellNew13.Text = SqlDataReader2.GetString(12).ToString
                    If i Mod 2 <> 0 Then
                        CellNew13.BackColor = Color.FromArgb(255, 245, 245, 245)
                    Else
                        CellNew13.BackColor = Color.FromArgb(255, 231, 231, 231)
                    End If
                    CellNew13.Font.Size = FontUnit.XSmall
                    RowNew.Cells.Add(CellNew13)

                    'Tel_Num
                    Dim CellNew14 As New TableCell
                    CellNew14.Text = SqlDataReader2.GetString(13).ToString
                    If i Mod 2 <> 0 Then
                        CellNew14.BackColor = Color.FromArgb(255, 245, 245, 245)
                    Else
                        CellNew14.BackColor = Color.FromArgb(255, 231, 231, 231)
                    End If
                    CellNew14.Font.Size = FontUnit.XSmall
                    RowNew.Cells.Add(CellNew14)

                    'Supervisor1_ID
                    Dim CellNew15 As New TableCell
                    CellNew15.Text = SqlDataReader2.GetString(14).ToString
                    If i Mod 2 <> 0 Then
                        CellNew15.BackColor = Color.FromArgb(255, 245, 245, 245)
                    Else
                        CellNew15.BackColor = Color.FromArgb(255, 231, 231, 231)
                    End If
                    CellNew15.Font.Size = FontUnit.XSmall
                    RowNew.Cells.Add(CellNew15)

                    'Supervisor1_Name
                    Dim CellNew16 As New TableCell
                    CellNew16.Text = SqlDataReader2.GetString(15).ToString
                    If i Mod 2 <> 0 Then
                        CellNew16.BackColor = Color.FromArgb(255, 245, 245, 245)
                    Else
                        CellNew16.BackColor = Color.FromArgb(255, 231, 231, 231)
                    End If
                    CellNew16.Font.Size = FontUnit.XSmall
                    RowNew.Cells.Add(CellNew16)

                    'Supervisor1_Level
                    Dim CellNew17 As New TableCell
                    CellNew17.Text = SqlDataReader2.GetString(16).ToString
                    If i Mod 2 <> 0 Then
                        CellNew17.BackColor = Color.FromArgb(255, 245, 245, 245)
                    Else
                        CellNew17.BackColor = Color.FromArgb(255, 231, 231, 231)
                    End If
                    CellNew17.Font.Size = FontUnit.XSmall
                    RowNew.Cells.Add(CellNew17)

                    'Supervisor2_ID
                    Dim CellNew18 As New TableCell
                    CellNew18.Text = SqlDataReader2.GetString(17).ToString
                    If i Mod 2 <> 0 Then
                        CellNew18.BackColor = Color.FromArgb(255, 245, 245, 245)
                    Else
                        CellNew18.BackColor = Color.FromArgb(255, 231, 231, 231)
                    End If
                    CellNew18.Font.Size = FontUnit.XSmall
                    RowNew.Cells.Add(CellNew18)

                    'Supervisor2_Name
                    Dim CellNew19 As New TableCell
                    CellNew19.Text = SqlDataReader2.GetString(18).ToString
                    If i Mod 2 <> 0 Then
                        CellNew19.BackColor = Color.FromArgb(255, 245, 245, 245)
                    Else
                        CellNew19.BackColor = Color.FromArgb(255, 231, 231, 231)
                    End If
                    CellNew19.Font.Size = FontUnit.XSmall
                    RowNew.Cells.Add(CellNew19)

                    'Supervisor2_Level
                    Dim CellNew20 As New TableCell
                    CellNew20.Text = SqlDataReader2.GetString(19).ToString
                    If i Mod 2 <> 0 Then
                        CellNew20.BackColor = Color.FromArgb(255, 245, 245, 245)
                    Else
                        CellNew20.BackColor = Color.FromArgb(255, 231, 231, 231)
                    End If
                    CellNew20.Font.Size = FontUnit.XSmall
                    RowNew.Cells.Add(CellNew20)

                    'Supervisor3_ID
                    Dim CellNew21 As New TableCell
                    CellNew21.Text = SqlDataReader2.GetString(20).ToString
                    If i Mod 2 <> 0 Then
                        CellNew21.BackColor = Color.FromArgb(255, 245, 245, 245)
                    Else
                        CellNew21.BackColor = Color.FromArgb(255, 231, 231, 231)
                    End If
                    CellNew21.Font.Size = FontUnit.XSmall
                    RowNew.Cells.Add(CellNew21)

                    'Supervisor3_Name
                    Dim CellNew22 As New TableCell
                    CellNew22.Text = SqlDataReader2.GetString(21).ToString
                    If i Mod 2 <> 0 Then
                        CellNew22.BackColor = Color.FromArgb(255, 245, 245, 245)
                    Else
                        CellNew22.BackColor = Color.FromArgb(255, 231, 231, 231)
                    End If
                    CellNew22.Font.Size = FontUnit.XSmall
                    RowNew.Cells.Add(CellNew22)

                    'Supervisor3_Level
                    Dim CellNew23 As New TableCell
                    CellNew23.Text = SqlDataReader2.GetString(22).ToString
                    If i Mod 2 <> 0 Then
                        CellNew23.BackColor = Color.FromArgb(255, 245, 245, 245)
                    Else
                        CellNew23.BackColor = Color.FromArgb(255, 231, 231, 231)
                    End If
                    CellNew23.Font.Size = FontUnit.XSmall
                    RowNew.Cells.Add(CellNew23)


                    'Director
                    Dim CellNew24 As New TableCell
                    CellNew24.Text = SqlDataReader2.GetString(23).ToString
                    If i Mod 2 <> 0 Then
                        CellNew24.BackColor = Color.FromArgb(255, 245, 245, 245)
                    Else
                        CellNew24.BackColor = Color.FromArgb(255, 231, 231, 231)
                    End If
                    CellNew24.Font.Size = FontUnit.XSmall
                    RowNew.Cells.Add(CellNew24)

                    Me.TableLevel1.Rows.Add(RowNew)
                    i = i + 1

                Finally
                End Try
            End While
            SqlDataReader2.Close()
            sqlCon.Close()
        Catch ex As Exception
            sqlCon.Close()
        End Try
    End Sub

    Sub Sethead()

        Dim CellHead1 As New TableCell
        Dim CellHead2 As New TableCell
        Dim CellHead3 As New TableCell
        Dim CellHead4 As New TableCell
        Dim CellHead5 As New TableCell
        Dim CellHead6 As New TableCell
        Dim CellHead7 As New TableCell
        Dim CellHead8 As New TableCell
        Dim CellHead9 As New TableCell
        Dim CellHead10 As New TableCell
        Dim CellHead11 As New TableCell
        Dim CellHead12 As New TableCell
        Dim CellHead13 As New TableCell
        Dim CellHead14 As New TableCell
        Dim CellHead15 As New TableCell
        Dim CellHead16 As New TableCell
        Dim CellHead17 As New TableCell
        Dim CellHead18 As New TableCell
        Dim CellHead19 As New TableCell
        Dim CellHead20 As New TableCell
        Dim CellHead21 As New TableCell
        Dim CellHead22 As New TableCell
        Dim CellHead23 As New TableCell
        Dim CellHead24 As New TableCell

        Dim Rowhead As New TableRow

        'Me.TableLevel1.Rows.Clear()

        CellHead1.Text = "รหัสพนักงาน"
        CellHead1.BackColor = Color.FromKnownColor(KnownColor.LimeGreen)       '(255, 245, 245, 245)
        CellHead1.Font.Bold = True
        CellHead1.Font.Size = FontUnit.XSmall
        Rowhead.Cells.Add(CellHead1)

        CellHead2.Text = "ชื่อพนักงาน (ภาษาไทย)"
        CellHead2.Font.Bold = True
        CellHead2.Font.Size = FontUnit.XSmall
        CellHead2.BackColor = Color.FromKnownColor(KnownColor.LimeGreen)
        Rowhead.Cells.Add(CellHead2)

        CellHead3.Text = "ชื่อพนักงาน (ภาษาอังกฤษ)"
        CellHead3.Font.Bold = True
        CellHead3.Font.Size = FontUnit.XSmall
        CellHead3.BackColor = Color.FromKnownColor(KnownColor.LimeGreen)
        Rowhead.Cells.Add(CellHead3)

        CellHead4.Text = "บริษัท"
        CellHead4.Font.Bold = True
        CellHead4.Font.Size = FontUnit.XSmall
        CellHead4.BackColor = Color.FromKnownColor(KnownColor.LimeGreen)
        Rowhead.Cells.Add(CellHead4)

        CellHead5.Text = "ฝ่าย"
        CellHead5.Font.Bold = True
        CellHead5.Font.Size = FontUnit.XSmall
        CellHead5.BackColor = Color.FromKnownColor(KnownColor.LimeGreen)
        Rowhead.Cells.Add(CellHead5)

        CellHead6.Text = "แผนก"
        CellHead6.Font.Bold = True
        CellHead6.Font.Size = FontUnit.XSmall
        CellHead6.BackColor = Color.FromKnownColor(KnownColor.LimeGreen)
        Rowhead.Cells.Add(CellHead6)

        CellHead7.Text = "ระดับพนักงาน"
        CellHead7.Font.Bold = True
        CellHead7.Font.Size = FontUnit.XSmall
        CellHead7.BackColor = Color.FromKnownColor(KnownColor.LimeGreen)
        Rowhead.Cells.Add(CellHead7)

        CellHead8.Text = "ตำแหน่ง (ภาษาอังกฤษ)"
        CellHead8.Font.Bold = True
        CellHead8.Font.Size = FontUnit.XSmall
        CellHead8.BackColor = Color.FromKnownColor(KnownColor.LimeGreen)
        Rowhead.Cells.Add(CellHead8)

        CellHead9.Text = "ตำแหน่ง (ภาษาไทย)"
        CellHead9.Font.Bold = True
        CellHead9.Font.Size = FontUnit.XSmall
        CellHead9.BackColor = Color.FromKnownColor(KnownColor.LimeGreen)
        Rowhead.Cells.Add(CellHead9)

        CellHead10.Text = "บัตรประชาชน"
        CellHead10.Font.Bold = True
        CellHead10.Font.Size = FontUnit.XSmall
        CellHead10.BackColor = Color.FromKnownColor(KnownColor.LimeGreen)
        Rowhead.Cells.Add(CellHead10)

        CellHead11.Text = "เพศ"
        CellHead11.Font.Bold = True
        CellHead11.Font.Size = FontUnit.XSmall
        CellHead11.BackColor = Color.FromKnownColor(KnownColor.LimeGreen)
        Rowhead.Cells.Add(CellHead11)

        CellHead12.Text = "วันเริ่มงาน"
        CellHead12.Font.Bold = True
        CellHead12.Font.Size = FontUnit.XSmall
        CellHead12.BackColor = Color.FromKnownColor(KnownColor.LimeGreen)
        Rowhead.Cells.Add(CellHead12)

        CellHead13.Text = "เบอร์โทร"
        CellHead13.Font.Bold = True
        CellHead13.Font.Size = FontUnit.XSmall
        CellHead13.BackColor = Color.FromKnownColor(KnownColor.LimeGreen)
        Rowhead.Cells.Add(CellHead13)

        CellHead14.Text = "Email"
        CellHead14.Font.Bold = True
        CellHead14.Font.Size = FontUnit.XSmall
        CellHead14.BackColor = Color.FromKnownColor(KnownColor.LimeGreen)
        Rowhead.Cells.Add(CellHead14)

        CellHead15.Text = "รหัสหัวหน้างาน1"
        CellHead15.Font.Bold = True
        CellHead15.Font.Size = FontUnit.XSmall
        CellHead15.BackColor = Color.FromKnownColor(KnownColor.LimeGreen)
        Rowhead.Cells.Add(CellHead15)

        CellHead16.Text = "ชื่อหัวหน้างาน1"
        CellHead16.Font.Bold = True
        CellHead16.Font.Size = FontUnit.XSmall
        CellHead16.BackColor = Color.FromKnownColor(KnownColor.LimeGreen)
        Rowhead.Cells.Add(CellHead16)

        CellHead17.Text = "Levelหัวหน้างาน1"
        CellHead17.Font.Bold = True
        CellHead17.Font.Size = FontUnit.XSmall
        CellHead17.BackColor = Color.FromKnownColor(KnownColor.LimeGreen)
        Rowhead.Cells.Add(CellHead17)

        CellHead18.Text = "รหัสหัวหน้างาน2"
        CellHead18.Font.Bold = True
        CellHead18.Font.Size = FontUnit.XSmall
        CellHead18.BackColor = Color.FromKnownColor(KnownColor.LimeGreen)
        Rowhead.Cells.Add(CellHead18)

        CellHead19.Text = "ชื่อหัวหน้างาน2"
        CellHead19.Font.Bold = True
        CellHead19.Font.Size = FontUnit.XSmall
        CellHead19.BackColor = Color.FromKnownColor(KnownColor.LimeGreen)
        Rowhead.Cells.Add(CellHead19)

        CellHead20.Text = "Levelหัวหน้างาน2"
        CellHead20.Font.Bold = True
        CellHead20.Font.Size = FontUnit.XSmall
        CellHead20.BackColor = Color.FromKnownColor(KnownColor.LimeGreen)
        Rowhead.Cells.Add(CellHead20)

        CellHead21.Text = "รหัสหัวหน้างาน3"
        CellHead21.Font.Bold = True
        CellHead21.Font.Size = FontUnit.XSmall
        CellHead21.BackColor = Color.FromKnownColor(KnownColor.LimeGreen)
        Rowhead.Cells.Add(CellHead21)

        CellHead22.Text = "ชื่อหัวหน้างาน3"
        CellHead22.Font.Bold = True
        CellHead22.Font.Size = FontUnit.XSmall
        CellHead22.BackColor = Color.FromKnownColor(KnownColor.LimeGreen)
        Rowhead.Cells.Add(CellHead22)

        CellHead23.Text = "Levelหัวหน้างาน3"
        CellHead23.Font.Bold = True
        CellHead23.Font.Size = FontUnit.XSmall
        CellHead23.BackColor = Color.FromKnownColor(KnownColor.LimeGreen)
        Rowhead.Cells.Add(CellHead23)

        CellHead24.Text = "ผู้อำนวยการ"
        CellHead24.Font.Bold = True
        CellHead24.Font.Size = FontUnit.XSmall
        CellHead24.BackColor = Color.FromKnownColor(KnownColor.LimeGreen)
        Rowhead.Cells.Add(CellHead24)

        'Me.TableLevel1.Rows.Add(Rowhead)

    End Sub



End Class