﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_Authorize.Master" CodeBehind="A_IndexBanner.aspx.vb" Inherits="Aroma_HRPortal.A_IndexBanner" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<br />
<br />

                 <div class="col-lg-12">
                <h3>
                    Banner Slide Show</h3>
                
                </div>


<table class="TBBody" style="font-family: tahoma; font-size: small; width: 1283px;">
          
         
            <tr>
                <td align="right" class="style4">
                    <asp:Label ID="Label16" runat="server" CssClass="fn_ContentTital" 
                        Text="ID :"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style1">


                    <asp:Label ID="lblNum" runat="server" Text="0"></asp:Label>


                    </td>
                <td align="left" valign="middle" class="style5">
                    &nbsp;</td>
                <td align="right" class="style7">
                    &nbsp;</td>
                <td align="right" class="style18">
                    &nbsp;</td>
                <td align="left" valign="middle" class="style1" rowspan="3">


                    <asp:Image ID="img" runat="server" ImageUrl="~/Images/Employee/noImage.jpg" 
                        Height="104px" Width="267px" BorderWidth="1px" />


                </td>
                <td align="left" class="style1">
                    &nbsp;</td>
            </tr>

          

            <tr>
                <td align="right" class="style4">
                    <asp:Label ID="Label18" runat="server" CssClass="fn_ContentTital" 
                        Text="* " 
                        ToolTip="ประเภททรัพย์สิน" ForeColor="Red"></asp:Label>
                    <asp:Label ID="Label8" runat="server" CssClass="fn_ContentTital" 
                        Text="ชื่อรูป :"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style1">


                    <asp:TextBox ID="txtSubject" runat="server" CssClass="fn_Content" Width="250px"></asp:TextBox>


                    </td>
                <td align="left" valign="middle" class="style5">
                    &nbsp;</td>
                <td align="right" class="style7">
                    </td>
                <td align="right" class="style18">
                    &nbsp;</td>
                <td align="left" class="style1">
                    &nbsp;
                </td>
            </tr>

          
      <%--      <tr>
                <td align="right" class="style4">
                    </td>
                
                 <td class="style1" colspan="3">
                     </td>
                
                <td align="right" class="style18">
                    </td>
                <td align="left" valign="middle" class="style1">
                    </td>
                <td align="left" class="style1">
                    </td>
            </tr>--%>
          
         
            <tr>
                <td align="right" class="style3">
                    <asp:Label ID="Label15" runat="server" CssClass="fn_ContentTital" 
                        Text="รายละเอียด :"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style3">
                    <asp:TextBox ID="txtDetail" runat="server" Width="346px" Height="47px" 
                        TextMode="MultiLine"></asp:TextBox>
          
                

                    </td>
                <td align="left" valign="middle" class="style6">
                    </td>
                <td align="left" class="style3">
                    </td>
                <td align="right" class="style3">
                    </td>
                <td align="left" class="style3">
                    </td>
            </tr>
          
         
            <tr>
                <td align="right" class="style2">
                    <asp:Label ID="Label13" runat="server" CssClass="fn_ContentTital" 
                        Text="สถานะ :" ToolTip="ประเภททรัพย์สิน"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style2" colspan="2">
                    <asp:RadioButtonList ID="rdoStatus" runat="server" 
                        RepeatColumns="2" Width="188px" Height="25px">
                        <asp:ListItem>Active</asp:ListItem>
                        <asp:ListItem>InActive</asp:ListItem>
                    </asp:RadioButtonList>
                    </td>
                <td align="left" class="style2">
                    </td>
                <td align="right" class="style2">



                    <asp:Label ID="Label19" runat="server" CssClass="fn_ContentTital" 
                        Text="* " 
                        ToolTip="ประเภททรัพย์สิน" ForeColor="Red"></asp:Label>



                    <asp:Label ID="lb1" runat="server" 
                        Text="รูปภาพ : " Visible="False"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style2">

                            <asp:FileUpload ID="FileUpload1" runat="server" CssClass="sample" 
                                style="font-family: Tahoma" Width="275px" />

                    </td>
                <td align="left" class="style2">
                    </td>
            </tr>
          
         
            <tr>
                <td align="right" class="style4">
                    &nbsp;</td>
                <td align="left" valign="middle" class="style4" colspan="2">
                    <asp:Label ID="Label17" runat="server" CssClass="fn_ContentTital" 
                        Text="* แสดงรูปบนหน้า Home สูงสุด 4 รูป (ระบบเลือกรูปล่าสุด)" 
                        ToolTip="ประเภททรัพย์สิน" ForeColor="Red"></asp:Label>
                    </td>
                <td align="left" class="style7">
                    </td>
                <td align="right" class="style18">
                    </td>
                <td align="left" valign="middle" class="style4">
                    </td>
                <td align="left" class="style4">
                    </td>
            </tr>
                 
         
            <tr>
                <td align="right" class="style2">
                    </td>
                <td align="left" valign="middle" class="style2" colspan="2">
                    <asp:Button ID="btnSave" runat="server" Text="Save" 
                    BackColor="#990000" Font-Bold="True" ForeColor="White" Height="30px" 
                    ToolTip="Save" Width="100px" Font-Names="Tahoma" Font-Size="Medium" />
                    <asp:Button ID="btnClear" runat="server" Text="Clear" 
                    BackColor="#990000" Font-Bold="True" ForeColor="White" Height="30px" 
                    ToolTip="ยกเลิก" Width="100px" Font-Names="Tahoma" Font-Size="Medium" />
                    </td>
                <td align="left" class="style7">
                    </td>
                <td align="right" class="style2">
                    </td>
                <td align="left" valign="middle" class="style2">
                    </td>
                <td align="left" class="style2">
                    </td>
            </tr>
                 
         
           
         
            <tr>
                <td align="right" class="style2">
                    &nbsp;</td>
                <td align="left" valign="middle" class="style2" colspan="2">
                    &nbsp;</td>
                <td align="left" class="style7">
                    &nbsp;</td>
                <td align="right" class="style2">
                    &nbsp;</td>
                <td align="left" valign="middle" class="style2">
                    &nbsp;</td>
                <td align="left" class="style2">
                    &nbsp;</td>
            </tr>
                 
         
           
         
            <tr>
                <td align="right" class="style2">
                    &nbsp;</td>
                <td align="left" valign="middle" class="style2" colspan="6">
                  
                    <asp:DataGrid ID="DataGrid4" runat="server" AllowSorting="True" 
                        AutoGenerateColumns="False" CellPadding="4" DataKeyField="IndexID" Font-Size="Small" 
                        ForeColor="Black" GridLines="Horizontal" Width="100%" BackColor="White" 
                        BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="0px">
                        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                        <SelectedItemStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                        <Columns>
                            <asp:EditCommandColumn CancelText="Cancel" EditText="Select" UpdateText="Update" > 
                            </asp:EditCommandColumn>
                                                        
                            <asp:BoundColumn DataField="IndexID" HeaderText="ID"></asp:BoundColumn>
                            <asp:BoundColumn DataField="type" HeaderText="type"></asp:BoundColumn>
                            <asp:BoundColumn DataField="txtSubject" HeaderText="ชื่อรูป"></asp:BoundColumn>
                            <asp:BoundColumn DataField="txtDetail" HeaderText="รายละเอียด"></asp:BoundColumn>
                            <asp:BoundColumn DataField="image_name" HeaderText="รูป"></asp:BoundColumn>
                            <asp:BoundColumn DataField="status" HeaderText="สถานะ"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CreateBy" HeaderText="CreateBy"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CreateDate" HeaderText="CreateDate"></asp:BoundColumn>

                          <asp:ButtonColumn CommandName="Delete" Text="Delete"></asp:ButtonColumn>     
                        </Columns>
                        <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                    </asp:DataGrid>
                    </td>
            </tr>
                 
         
           
         
            <tr>
                <td align="right" class="style4">
                    &nbsp;</td>
                <td align="left" valign="middle" class="style1" colspan="2" rowspan="4">
                    &nbsp;</td>
                <td align="left" class="style7">
                    &nbsp;</td>
                <td align="right" class="style18">



                    &nbsp;</td>
                <td align="left" valign="middle" class="style1">

                            &nbsp;</td>
                <td align="left" class="style1">
                            <asp:TextBox ID="txtPic" runat="server" BorderColor="White" BorderWidth="0px" 
                                Height="0px" Width="93px"></asp:TextBox>
                    </td>
            </tr>
          
         
           
         
            <tr>
                <td align="right" class="style4">
                    &nbsp;</td>
                <td align="left" class="style7">
                    &nbsp;</td>
                <td align="right" class="style18">



                    &nbsp;</td>
                <td align="left" valign="middle" class="style1">

                            &nbsp;</td>
                <td align="left" class="style1">
                            &nbsp;</td>
            </tr>
          
         
           
         
            <tr>
                <td align="right" class="style4">
                    &nbsp;</td>
                <td align="left" class="style7">
                    &nbsp;</td>
                <td align="right" class="style18">



                    &nbsp;</td>
                <td align="left" valign="middle" class="style1">

                            &nbsp;</td>
                <td align="left" class="style1">
                            &nbsp;</td>
            </tr>
          
                    
        </table>


</asp:Content>
