﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="Public_RelationsDetail.aspx.vb" Inherits="Aroma_HRPortal.Flow_IT" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/benefit/banner1.jpg" /></div>
<!-- end banner -->

<!--
=====================================================
	Blog Page Details
=====================================================
-->
<article class="blog-details-page">
	<div class="container">
		
		<div class="post-heading">
            <ul>
			    <li><a href="index_hr.aspx">HOME</a></li>
			    <li>/</li>
			    <li><a href="Public_Relations.aspx">ประชาสัมพันธ์ๆ</a></li>
			</ul>
			<h2>Playback: Akufo-Addo speaks to business community</h2>
		</div> <!-- /.post-heading -->
		
		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix">
				<div class="blog-details-post-wrapper">
				
					<img src="new/images/banner/benefit/img2.jpg" alt="Image">

					<h4>
						บริษัท เค.วี.เอ็น. อิมปอร์ต เอกซ์ปอร์ต (1991) จำกัด/บริษัท ไลอ้อน ทรี-สตาร์ จำกัด
					</h4>
					
					<table class="table">
				  	  <thead class="thead-inverse">
					  	<tr>
					  	  <th>สวัสดิการของขวัญครบรอบวันเกิดพนักงาน</th>
					  	  <th></th>
					  	</tr>
					  </thead>
					  <tbody>
					  	<tr>
					  	  <td>พนักงานระดับ A ของขวัญมูลค่า</td>
					  	  <td>400 บาท</td>
					  	</tr>
					  	<tr>
					  	  <td>พนักงานระดับ B ของขวัญมูลค่า</td>
						  <td>200 บาท</td>
						</tr>
						<tr>
					  	  <td>พนักงานระดับ C ของขวัญมูลค่า</td>
					  	  <td>200 บาท</td>
					  	</tr>
					  	<tr>
					  	  <td>พนักงานระดับ D ของขวัญมูลค่า</td>
						  <td>200 บาท</td>
						</tr>
					  </tbody>
					</table>
					
					<h4>เกณฑ์พิจารณา</h4>
					<p>
						<ol>
							<li>พนักงานที่มีสิทธิเบิกตั้งแต่วันผ่านทดลองงาน (กรณีพนักงานใหม่ หากครบรอบวันเกิดก่อนวันผ่านทดลองงาน จะได้รับสิทธิในปีถัดไป)</li>
							<li>ของขวัญเป็นรายปีไป</li>
							<li>มูลค่าของขวัญอาจเปลี่ยนแปลงได้ตามความเหมาะสม</li>
						</ol>
					</p>
					
					<h4>เอกสารประกอบการเบิกจ่าย</h4>
					<p>
						<ol>
							<li>พนักงานติดต่อฝ่ายทรัพยากรมนุษย์ เพื่อรับสิทธิ/ตรวจสอบสิทธิ</li>
							<li>ใช้สิทธิ/เบิกจ่าย ภายในระยะเวลากำหนด ซึ่งจะระบุไว้บนบัตรเป็นรายปีไป</li>
						</ol>
						การมอบเกียรติบัตร และรางวัลพนักงานปฏิบัติงานติดต่อกัน อายุงานครบ 10, 20, 30 ปีขึ้นไป (มอบให้ในปีถัดไป)<br>
						สำหรับพนักงาน (B, C, D)
					</p>
					
					<h4>เกณฑ์พิจารณา</h4>
					<p>
						<ol>
							<li>มูลค่ารางวัลเท่ากันทุกระดับพนักงาน (B , C , D)</li>
							<li>อายุงาน 10 ปี ได้รับเกียรติบัตรสีเงิน พร้อมสร้อยคอทองคำ 25 สตางค์</li>
							<li>อายุงาน 20 ปี ได้รับเกียรติบัตรสีทอง พร้อมสร้อยคอทองคำ 50 สตางค์</li>
							<li>อายุงาน 30 ปีขึ้นไป ได้รับเกียรติบัตรสีทอง พร้อมสร้อยคอทองคำ 1 บาท</li>
						</ol>
					</p>
					
					<h4>เอกสารประกอบการเบิกจ่าย</h4>
					<p>
						<ol>
							<li>อายุการทำงานครบเกณฑ์ข้างต้น โดยนับตั้งแต่วันที่เริ่มงาน ถึงวันที่ 31 ธันวาคม ของปี อาทิ สิทธิได้รับรางวัลปี พ.ศ.2560 แต่จะไปมอบในปี พ.ศ.2561 ในวันงานเลี้ยงประจำปี/โอกาสพิเศษ ก็จะนับอายุงานตั้งแต่วันที่เริ่มงาน ถึงวันที่ 31 ธันวาคม 2560 เป็นต้น</li>
							<li>พิจารณาจากผลการปฏิบัติงาน ในระดับดีขึ้นไป (A, B)</li>
							<li>ไม่มีประวัติตักเตือนเป็นลายลักษณ์อักษร หรือลงโทษทางวินัย เนื่องจากกระทำผิดทางวินัย ข้อใดข้อหนึ่ง ตลอดระยะเวลาการทำงานครบแต่ละช่วงอายุงานไป</li>
							<li>การอนุมัติขั้นสุดท้ายอยู่ในดุลยพินิจของคณะกรรมการบริหาร</li>
						</ol>
					</p>
					
					<h4>การมอบเกียรติบัตร และรางวัลพนักงานสถิติการทำงานดีเยี่ยม พนักงานที่ได้รับสิทธิ ระดับ D</h4>
					<h4>เกณฑ์พิจารณา</h4>
					<p>
						<ol>
							<li>พนักงานที่ี่สแกน/บันทึกเวลา และถูกคิดสาย</li>
							<li>ขยันติดต่อกันในแต่ละปี ได้รับเกียรติบัตร พร้อมของรางวัล</li>
							<li>ขยันติดต่อกันครบ 5 ปี ได้รับเกียรติบัตร พร้อมของรางวัล และเงิน รางวัล 2,000 บาท</li>
						</ol>
					</p>
					
					<h4>เอกสารประกอบการเบิกจ่าย</h4>
					<p>
						<ol>
							<li>พิจารณาจากได้รับเบี้ยขยันครบ 12 เดือนในแต่ละปี (นับจาก ม.ค. - ธ.ค. ของปี)</li>
							<li>จะมอบรางวัลในวันงานเลี้ยงประจำปี/โอกาสพิเศษ</li>
						</ol>
					</p>
					
					<h4>หมายเหตุ</h4>
					<p>
						<ol>
							<li>รายการสวัสดิการข้างต้น พนักงานที่ได้รับสิทธิจะต้องผ่านการทดลองงานและได้บรรจุเป็นพนักงานบริษัทแล้วเท่านั้น ยกเว้นบางสวัสดิการให้เป็นไปตามหลักเกณฑ์/เงื่อนไขสวัสดิการนั้นๆ กำหนด</li>
							<li>ระดับพนักงาน A คือ ผู้ช่วยรองกรรมการผู้จัดการขึ้นไป, ระดับ B คือ ผู้จัดการฝ่าย - ผู้จัดการทั่วไป, ระดับ C คือ หัวหน้าส่วน - ผู้จัดการแผนก, ระดับ D คือ พนักงาน - หัวหน้างาน</li>
							<li>การเบิกจ่าย พนักงานจะต้องดำเนินการเขียนใบเบิก พร้อมแนบเอกสารหลักฐานประกอบการเบิกทุกครั้ง ถึงจะได้รับการพิจารณาอนุมัติเบิกจ่าย</li>
							<li>กำหนดตั้งเรื่องเบิกจ่ายผ่านฝ่ายทรัพยากรมนุษย์ตรวจสอบ และส่งต่อฝ่ายบัญชีและการเงินทำจ่าย ตามเวลากำหนดข้างต้น มิฉะนั้นจะถือว่าพนักงานสละสิทธิ โดยไม่มีข้อโต้แย้งแต่อย่างใดทั้งสิ้น</li>
							<li>การเบิกสวัสดิการพนักงาน ที่ไม่สามารถใช้สิทธิเบิกได้ มีกรณีต่างๆ ดังนี้ ทำร้ายร่างกายตน การใช้สารเสพติดตามกฏหมายสารเสพติด การกระทำการใดๆ เพื่อความสวยงามโดยไม่มีข้อบ่งชี้ทางการแพทย์ การรักษาที่อยู่ ระหว่างการค้นคว้าทดลอง การรักษาภาวะมีบุตรยาก การตรวจเนื้อเยื่อเพื่อการผ่าตัดเปลี่ยนอวัยวะ การผ่าตัดเปลี่ยนอวัยวะ การเปลี่ยนเพศ การผสมเทียม ทันตกรรม และแว่นตา เป็นต้น</li>
							<li>ผู้มีอำนาจอนุมัติเบิกจ่ายรายการสวัสดิการข้างต้น ได้แก่กรรมการบริหาร หรือผู้ที่ได้รับมอบหมาย</li>
							<li>กรณีตรวจสอบพบว่าผู้บังคับบัญชาต้นสังกัดลงนามรับรองผิดพลาดจากการเหตุการณ์จริง / เดินทางจริง ส่วนที่ผิดพลาดผู้ลงนามรับรองนั้นๆ จะต้องเป็นผู้จ่ายเงินคืนให้บริษัท</li>
							<li>บริษัทขอสงวนไว้ซึ่งสิทธิที่จะยกเลิก แก้ไข เพิ่มเติม เปลี่ยนแปลงอัตราข้างต้นได้ตามเห็นสมควร</li>
							<li>ทั้งนี้ให้มีผลตั้งแต่วันที่ 1 ตุลาคม 2560 เป็นต้นไป จนกว่าจะมีประกาศเปลี่ยนแปลง</li>
						</ol>
					</p>
					
					<div class="blog-details-bt">
						<div class="btn-group">
							<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
						</div>
					</div>
				
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-8 col-xs-12 p-fix">
                <aside>
				    <div class="sidebar-recent-post">
					    <h4>RECENT POST</h4>

					    <div class="recent-single-post clear-fix">
						    <img src="new/images/index/blog1.jpg" alt="Image" class="float-left"/>
						    <div class="post float-left">
							    <a href="#" class="tran3s">Playback: Akufo-Addo speaks to business community in United States.</a>
							    <span>January 18, 2017</span>
						    </div> <!-- /.post -->
					    </div> <!-- /.recent-single-post -->
					
					    <div class="recent-single-post clear-fix">
						    <img src="new/images/index/blog2.jpg" alt="Image" class="float-left"/>
						    <div class="post float-left">
							    <a href="#" class="tran3s">Playback: Akufo-Addo speaks to business community in United States.</a>
							    <span>January 18, 2017</span>
						    </div> <!-- /.post -->
					    </div> <!-- /.recent-single-post -->
					
					    <div class="recent-single-post clear-fix">
						    <img src="new/images/index/blog3.jpg" alt="Image" class="float-left"/>
						    <div class="post float-left">
							    <a href="#" class="tran3s">Playback: Akufo-Addo speaks to business community in United States.</a>
							    <span>January 18, 2017</span>
						    </div> <!-- /.post -->
					    </div> <!-- /.recent-single-post -->
					
					    <div class="recent-single-post clear-fix">
						    <img src="new/images/index/blog1.jpg" alt="Image" class="float-left"/>
						    <div class="post float-left">
							    <a href="#" class="tran3s">Playback: Akufo-Addo speaks to business community in United States.</a>
							    <span>January 18, 2017</span>
						    </div> <!-- /.post -->
					    </div> <!-- /.recent-single-post -->
				    </div>	
                </aside>
			</div>
		</div>
		
	</div>
</article>

</asp:Content>

