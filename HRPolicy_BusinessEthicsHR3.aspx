﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_BusinessEthicsHR3.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_BusinessEthicsHR3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<style>
	.swiper-container {
		width: 100%;
		height: 100%;
	}
	.swiper-slide {
		text-align: center;
		background: #fff;
	}
	.swiper-slide img 
	{
		width: 100%;
	}
	.swiper-pagination-bullet-active {
		opacity: 1;
		background: #af2f2f;
	}
</style>

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/hr.jpg" alt=""/></div>
<!-- end banner -->

<article class="blog-details-page">
	<div class="container">
		
		<div class="post-heading">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
                <li>/</li>
                <li><a href="HRPolicy_Announce.aspx">ระเบียบต่างๆ</a></li>
                <li>/</li>
                <li><a href="HRPolicy_BusinessEthicsHR.aspx">ประกาศ คำสั่ง ระเบียบทรัพยากรมนุษย์</a></li></li>
            </ul>
			<h2>การแต่งกายพนักงานขาย (พีซี)</h2>
		</div> <!-- /.post-heading -->
		
		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix">
				<div class="blog-details-post-wrapper">
				
					<img src="new/images/banner/hr-policy/img7.jpg" alt="Image">

					<p style="padding: 30px 0 10px;">
						เพื่อให้เกิดภาพลักษณ์ที่ดี และมีความเป็นระเบียบเรียบร้อยในการปฏิบัติงาน ทางกลุ่มบริษัท อโรม่า จึงเห็นสมควรกำหนด และใคร่ขอความร่วมมือมายังพนักงานขาย (พีซี) ทุกท่าน ในการปฏิบัติอย่างเคร่งครัดเรื่อง การแต่งกายทุกวันที่มาปฏิบัติงาน ดังนี้
						<ol>
							<li>ทรงผม
								<ul class="a">
									<li>พนักงานชาย ให้ไว้ผมรองทรง หรือทรงสั้น รวมทั้งจัดทรงให้ดูแล้วสุภาพเรียบร้อย และสะอาด โดยงดทำสีผม</li>
									<li>พนักงานหญิง อนุโลมให้ไว้ผมยาวได้ แต่ต้องจัดทรงให้ดูแล้วสุภาพเรียบร้อย และสะอาด โดยงดทำสีผม</li>
								</ul>
							</li>
							<li>ใบหน้า
							 	<ul class="a">
									<li>พนักงานชาย ต้องดูแลผิวบริเวณใบหน้าให้ดูดี และสะอาดอยู่เสมอ</li>
								 	<li>พนักงานหญิง ต้องดูแลผิวบริเวณใบหน้าให้ดูดี และสะอาดอยู่เสมอ รวมทั้งแต่งหน้าด้วยสีที่สุภาพ ไม่ฉูดฉาด</li>
								</ul>
							</li>
							<li>เสื้อ
								<ul class="a">
									<li>ทั้งพนักงานชาย และหญิง ให้ใส่เสื้อฟอร์มที่บริษัทฯ กำหนด โดยต้องสวมใส่ไว้ข้างในกางเกงตลอดเวลาปฏิบัติงาน</li>
								</ul>
							</li>
							<li>เล็บมือ
								<ul class="a">
									<li>ทั้งพนักงานชาย และหญิง ต้องดูแลเล็บมือให้สะอาด และสวยงามอยู่เสมอ โดยงดทำสีเล็บด้วยสีสัน ลวดลาย ฉูดฉาด</li>
								</ul>
							</li>
							<li>การเกง
							 	<ul class="a">
									<li>ทั้งพนักงานชาย และหญิง ให้สวมกางเกงผ้าทำงานสีดำ เท่านั้น</li>
								</ul>
							</li>
							<li>รองเท้า
							 	<ul class="a">
									<li>ทั้งพนักงานชาย และหญิง ให้สวมรองเท้าคัทชู หรือรองเท้าหนังสีดำ เท่านั้น</li>
								</ul>
							</li>
							<li>บัตรพนักงาน
								<ul class="a">
									<li>ทั้งพนักงานชาย และหญิง ต้องติด/คล้องบัตรพนักงาน (ตามแต่ละสถานที่ปฏิบัติงานกำหนด) ตลอดเวลาขณะปฏิบัติงาน</li>
								</ul>
							</li>
						</ol>
					</p>

					<p><br>
						ทั้งนี้ การแต่งกายให้เป็นไปตามกฏระเบียบของบริษัท/ห้าง ที่พนักงานผู้นั้นประจำอยู่ เว้นแต่บริษัท/ห้าง ดังกล่าวมิได้กำหนดไว้ จึงแต่งกายตามที่ระบุไว้ข้างต้น<br><br>

						อนึ่ง หากบริษัท หรือตัวแทนบริษัทฯ (ผู้บังคับบัญชาต้นสังกัด ฝ่ายทรัพยากรมนุษย์) พบว่าพนักงานท่านใดจงใจฝ่าฝืน เพิก ละเลย ไม่ให้ความร่วมมือปฏิบัติตามระเบียบดังกล่าว โดยไม่มีเหตุผลอันควร พนักงานผู้นั้นจะถูกตักเตือนเป็นลายลักษณ์อักษรในทันที ซึ่งจะมีผลต่อการประเมินผลการปฏิบัติงาน เพื่อเสนอพิจารณาปรับเงินเดือน หรือโบนัสภายในปีนั้นๆ<br><br>

						ระเบียบนี้ ให้ถือปฏิบัติตั้งแต่บัดนี้เป็นต้นไป<br><br>

						จึงประกาศมาเพื่อทราบ และขอความร่วมมือปฏิบัติอย่างเคร่งครัดโดยทั่วกัน<br><br>

						ประกาศ ณ วันที่ 13 มิถุนายน 2549
					</p>
					
					<div class="blog-details-bt">
						<div class="btn-group">
							<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
						</div>
					</div>
				
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-8 col-xs-12 p-fix">
				<aside>
					<div class="sidebar-news-list">
						<h4>CATEGORY</h4>
						<ul>
							<li>
								<a href="HRPolicy_BusinessEthicsHR.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบทรัพยากรมนุษย์</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAS.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบเกี่ยวกับทรัพย์สิน</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAC.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบเกี่ยวกับบัญชี การเงิน งบประมาณ จัดซื้อ</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsIT.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบเกี่ยวกับเทคโนโลยีสารสนเทศ</a>
							</li>
							<li>
								<a href="Benefits1_1.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบอื่นๆ</a>
							</li>
						</ul>
					</div>
				</aside>
				<aside>
					<div class="sidebar-news-list">
						<h4>Related Links</h4>
						<ul>
							<li>
								<a href="HRPolicy_BusinessEthicsHR1.aspx" class="tran3s">
									การคล้อง ติดบัตรพนักงาน
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR2.aspx" class="tran3s">
									การลาพักผ่อนประจำปี
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR3.aspx" class="tran3s">
									การแต่งกายพนักงานขาย (พีซี)
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR4.aspx" class="tran3s">
									การลงนามอนุมัติเกินอำนาจดำเนินการ หรือความเสียหายจากการปฏิบัติหน้าที่
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR5.aspx" class="tran3s">
									ระเบียบบ้านพักพนักงาน
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR6.aspx" class="tran3s">
									ระเบียบการลงโทษพนักงานทุจริต
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR7.aspx" class="tran3s">
									ทดลองจัดวันเวลาทำงานและวันหยุด (เป็นกรณีพิเศษ) ทำงานวันจันทร์ - ศุกร์
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR8.aspx" class="tran3s">
									ระเบียบการแต่งกายพนักงานประจำสำนักงาน
								</a>
							</li>
						</ul>
					</div>
				</aside>
			</div>
		</div>
		
	</div>
</article>
    
<!-- Swiper JS -->
<script src="new/js/swiper.min.js"></script>

<!-- Initialize Swiper -->
<script>
	var swiper1 = new Swiper('.swiper1', {
		spaceBetween: 30,
		pagination: {
        el: '.swiper-pagination1',
        clickable: true,
      },
    });
</script>

</asp:Content>
