﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_WhoisWho9.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_Contact" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/HR.jpg" alt=""/></div>
<!-- end banner -->

<section id="team-section">
	<div class="container">
		<div class="theme-title">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
                <li>/</li>
                <li><a href="HRPolicy_WhoisWho.aspx">ติดต่อฝ่าย-แผนกต่างๆ</a></li>
            </ul>
			<h2>รายชื่อติดต่อ-การตลาด (Marketing)</h2>
			<!--p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p-->
		</div> <!-- /.theme-title -->

		<div class="clear-fix team-member-wrapper">
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who2019/3.mkt/คุณศิริกาญจน์%20%20เงินลายลักษณ์.JPG" alt="" 
                            height="150"/>
						<div class="opacity tran4s">
							<h4>คุณศิริกาญจน์  เงินลายลักษณ์ (บีบี้)</h4>
							<span>ผู้จัดการฝ่ายอาวุโส</span>
							<p>
								เบอร์ต่อภายใน : 508<br />
                    		
                    			Email : <a href="mailto:sirikarnnge@aromathailand.com ">sirikarnnge@aromathailand.com </a>
							</p>
						</div>
					</div> 
					<div class="member-name">
						<h5>คุณศิริกาญจน์  เงินลายลักษณ์ (บีบี้)</h5>
						<p>ผู้จัดการฝ่ายอาวุโส</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> 
				</div> 
			</div> 

			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who2019/3.mkt/คุณนภัค%20%20ลี้ถาวร.JPG" alt="" 
                            height="150"/>
						<div class="opacity tran4s">
							<h4>คุณนภัค ลี้ถาวร (นีนี่)</h4>
							<span>ผู้จัดการฝ่าย</span>
							<p>
								เบอร์ต่อภายใน : 349<br />
                    			<%--โทรศัพท์มือถือ : -<br />--%>
                    			Email : <a href="mailto:napakl@aromathailand.com">napakl@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณนภัค ลี้ถาวร (นีนี่)</h5>
						<p>ผู้จัดการฝ่าย</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->
			
		
			
			<%--<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who/Marketing/4.คุณขจรยศ เอื้อพงศ์สุขกิจ.png" alt=""/>
						<div class="opacity tran4s">
							<h4>คุณขจรยศ เอื้อพงศ์สุขกิจ (อ๊อด)</h4>
							<span>ผู้จัดการฝ่าย</span>
							<p>
								เบอร์ต่อภายใน : 123<br />
                    			Email : <a href="mailto:khajornyotu@aromathailand.com">khajornyotu@aromathailand.com</a>
							</p>
						</div>
					</div> 
					<div class="member-name">
						<h5>คุณขจรยศ เอื้อพงศ์สุขกิจ (อ๊อด)</h5>
						<p>ผู้จัดการฝ่าย</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> 
				</div> 
			</div> --%>
			
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who2019/3.mkt/คุณณัฐกฤตา%20%20ขาวขำ.jpg" alt="" 
                            height="150"/>
						<div class="opacity tran4s">
							<h4>คุณณัฐกฤตา ขาวขำ (แอนดี้)</h4>
							<span>ผู้จัดการฝ่าย</span>
							<p>
								เบอร์ต่อภายใน : 123<br />
                    			<%--โทรศัพท์มือถือ : -<br />--%>
                    			Email : <a href="mailto:nuttakittak@aromathailand.com">nuttakittak@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณณัฐกฤตา ขาวขำ (แอนดี้)</h5>
						<p>ผู้จัดการฝ่าย</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->
			
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who2019/3.mkt/คุณอนุวัฒน์%20%20วรรณราด.jpg" alt="" 
                            height="150"/>
						<div class="opacity tran4s">
							<h4>คุณอนุวัฒน์ วรรณราด (แจ๊ค)</h4>
							<span>หัวหน้าแผนก</span>
							<p>
								เบอร์ต่อภายใน : 335<br />
                    			<%--โทรศัพท์มือถือ : -<br />--%>
                    			Email : <a href="mailto:anuwatw@aromathailand.com">anuwatw@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณอนุวัฒน์ วรรณราด (แจ๊ค)</h5>
						<p>หัวหน้าแผนก</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->
			
		</div> <!-- /.team-member-wrapper -->
		
		<div class="blog-category-bt">
			<div class="btn-group">
				<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
			</div>
		</div>
		
	</div> <!-- /.conatiner -->
</section>
 
</asp:Content>
