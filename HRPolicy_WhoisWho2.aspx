﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_WhoisWho2.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_Contact" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/hr.jpg" alt=""/></div>
<!-- end banner -->

<section id="team-section">
	<div class="container">
		<div class="theme-title">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
                <li>/</li>
                <li><a href="HRPolicy_WhoisWho.aspx">ติดต่อฝ่าย-แผนกต่างๆ</a></li>
            </ul>
			<h2>รายชื่อติดต่อ-ขาย Aroma Shop</h2>
			<!--p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p-->
		</div> <!-- /.theme-title -->

		<div class="clear-fix team-member-wrapper">
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who2019/2.sales/คุณศุภชัย%20%20กิจจานนท์เจริญ.jpg" alt="" 
                            height="150"/>
						<div class="opacity tran4s">
							<h4>คุณศุภชัย กิจจานนท์เจริญ (ต้อม)</h4>
							<span>ผู้จัดการฝ่าย</span>
							<p>
								เบอร์ต่อภายใน : 131<br />
                    			<%--โทรศัพท์มือถือ : -<br />--%>
                    			Email : <a href="mailto:supachaik@aromathailand.com">supachaik@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณศุภชัย กิจจานนท์เจริญ (ต้อม)</h5>
						<p>ผู้จัดการฝ่าย</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->

            
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who2019/2.sales/คุณกัษมา%20%20แก้วเจริญธารากูล.JPG" alt="" 
                            height="150"/>
						<div class="opacity tran4s">
							<h4>คุณกัษมา  แก้วเจริญธารากูล (ฝ้าย)</h4>
							<span>ผู้จัดการฝ่าย</span>
							<p>
								เบอร์ต่อภายใน : 121<br />
              
                    			Email : <a href="mailto:katsamakae@aromathailand.com">katsamakae@aromathailand.com </a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณกัษมา  แก้วเจริญธารากูล (ฝ้าย)</h5>
						<p>ผู้จัดการฝ่าย</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> 
				</div> 
			</div> 

			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who2019/2.sales/คุณธวัชชัย%20%20สำเภากิจ.jpg" alt="" 
                            height="150"/>
						<div class="opacity tran4s">
							<h4>คุณธวัชชัย  สำเภากิจ (ปุ่น)</h4>
							<span>ผู้จัดการฝ่าย</span>
							<p>
								เบอร์ต่อภายใน : 120<br />
                    			<%--โทรศัพท์มือถือ : -<br />--%>
                    			Email : <a href="mailto:thawatchais@aromathailand.com">thawatchais@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณธวัชชัย  สำเภากิจ (ปุ่น)</h5>
						<p>ผู้จัดการฝ่าย</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->

			
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who2019/2.sales/คุณสรินทิพย์%20%20รื่นสว่างวงค์.jpg" 
                            alt="" height="150"/>
						<div class="opacity tran4s">
							<h4>คุณสรินทิพย์  รื่นสว่างวงค์ (ปู)</h4>
							<span>ผู้ช่วยผู้จัดการฝ่าย</span>
							<p>
								เบอร์ต่อภายใน : 120<br />
                        		<%--โทรศัพท์มือถือ : 081-6164697<br />--%>
                        		Email : <a href="mailto:sarintipr@aromathailand.com">sarintipr@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณสรินทิพย์  รื่นสว่างวงค์ (ปู)</h5>
						<p>ผู้ช่วยผู้จัดการฝ่ายขาย SHOP</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->
			
            	<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who2019/2.sales/คุณสุชาติ%20%20มรรคเจริญ.jpg" alt="" 
                            height="150"/>
						<div class="opacity tran4s">
							<h4>คุณสุชาติ  มรรคเจริญ (อ้วน)</h4>
							<span>ผู้จัดการแผนก</span>
							<p>
								เบอร์ต่อภายใน : -<br />
                        		<%--โทรศัพท์มือถือ : 081-6164697<br />--%>
                        		Email : <a href="mailto:suchartm@aromathailand.com">suchartm@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณสุชาติ  มรรคเจริญ (อ้วน)</h5>
						<p>ผู้จัดการแผนก</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->

			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who2019/2.sales/คุณณัฐพล%20%20นันทิพัฒน์ภากร.jpg" alt="" 
                            height="150"/>
						<div class="opacity tran4s">
							<h4>คุณณัฐพล นันทิพัฒน์ภากร (ต้อย)</h4>
							<span>ผู้จัดการแผนก</span>
							<p>
								เบอร์ต่อภายใน : 121<br />
                        		<%--โทรศัพท์มือถือ : 081-6164697<br />--%>
                        		Email : <a href="mailto:nuttapoln@aromathailand.com">nuttapoln@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณณัฐพล นันทิพัฒน์ภากร (ต้อย)</h5>
						<p>ผู้จัดการแผนก</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->
			
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who2019/2.sales/คุณวรรณา%20%20พลอยสระศรี.JPG" alt="" 
                            height="150"/>
						<div class="opacity tran4s">
							<h4>คุณวรรณา พลอยสระศรี (ติ๋ม)</h4>
							<span>ผู้จัดการฝ่ายขาย Shop</span>
							<p>
								เบอร์ต่อภายใน : -<br />
                        		<%--โทรศัพท์มือถือ : 081-6164697<br />--%>
                        		Email : <a href="mailto:wannap@aromathailand.com">wannap@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณวรรณา พลอยสระศรี (ติ๋ม)</h5>
						<p>ผู้จัดการฝ่ายขาย Shop</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->
			
		
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who2019/2.sales/คุณพัชริดา%20%20สุธาเนตร.jpg" alt="" 
                            height="150"/>
						<div class="opacity tran4s">
							<h4>คุณพัชริดา สุธาเนตร (พัด)</h4>
							<span>ผู้จัดการแผนก</span>
							<p>
								เบอร์ต่อภายใน : -<br />
                        		<%--โทรศัพท์มือถือ : 081-6164697<br />--%>
                        		Email : <a href="mailto:patcharda@aromathailand.com">patcharda@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณพัชริดา สุธาเนตร (พัด)</h5>
						<p>ผู้จัดการแผนก</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->


            	
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who2019/2.sales/คุณบุญศักดิ์%20%20ไม่ลืมญาติ.JPG" alt="" 
                            height="150"/>
						<div class="opacity tran4s">
							<h4>คุณบุญศักดิ์  ไม่ลืมญาติ (มิน)</h4>
							<span>ผู้ช่วยผู้จัดการแผนก</span>
							<p>
								เบอร์ต่อภายใน : -<br />
                        		<%--โทรศัพท์มือถือ : 081-6164697<br />--%>
                        		Email : <a href="mailto:boonsakm@aromathailand.com">boonsakm@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณบุญศักดิ์  ไม่ลืมญาติ (มิน)</h5>
						<p>ผู้ช่วยผู้จัดการแผนก</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->
			
		</div> <!-- /.team-member-wrapper -->
		
		<div class="blog-category-bt">
			<div class="btn-group">
				<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
			</div>
		</div>
		
	</div> <!-- /.conatiner -->
</section>
 
</asp:Content>
