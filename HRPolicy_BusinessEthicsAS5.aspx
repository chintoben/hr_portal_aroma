﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_BusinessEthicsAS5.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_BusinessEthicsAS5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/hr.jpg" alt=""/></div>
<!-- end banner -->

<article class="blog-details-page">
	<div class="container">
		
		<div class="post-heading">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
                <li>/</li>
                <li><a href="HRPolicy_Announce.aspx">ระเบียบต่างๆ</a></li>
                <li>/</li>
                <li><a href="HRPolicy_BusinessEthicsAS.aspx">ประกาศ คำสั่ง ระเบียบเกี่ยวกับทรัพย์สิน</a></li>
            </ul>
			<h2>ระเบียบการใช้รถที่บริษัทเช่าให้พนักงานใช้ปฏิบัติงาน</h2>
		</div> <!-- /.post-heading -->
		
		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix">
				<div class="blog-details-post-wrapper">
				
					<img src="new/images/banner/hr-policy/img17.jpg" alt=""/>
					
					<p style="padding: 30px 0 0;">
         		    	ตามที่ อโรม่า กรุ๊ป ได้จัดให้มียานยนต์/รถยนต์เช่าจากบริษัทภายนอก เพื่อให้พนักงานใช้ในการปฏิบัติงานที่เกี่ยวข้องในงานกิจการของบริษัท ดังนั้นเพื่อให้มีความเป็นระเบียบเรียบร้อยในการปฏิบัติเกี่ยวกับการใช้ยานยนต์/รถยนต์เช่าจากบริษัทภายนอกข้างต้น ทาง อโรม่า กรุ๊ป จึงเห็นสมควรกำหนด และ่ขอความร่วมมือมายังพนักงานที่ได้รับสิทธิทุกท่าน ในการปฏิบัติอย่างเคร่งครัด ดังนี้
					</p>
					
					<ol style="margin: 10px 0;">
						<li>ยานยนต์/รถยนต์เช่าพร้อมอุปกรณ์จากบริษัทภายนอก ใช้เฉพาะในการปฏิบัติงานที่เกี่ยวข้องในงานกิจการของบริษัท เท่านั้น</li>
						<li>ผู้มีสิทธิใช้ได้เฉพาะพนักงาน อโรม่า กรุ๊ป และเฉพาะพนักงานที่ได้รับสิทธิเท่านั้น</li>
						<li>พนักงานผู้ได้รับสิทธิ จะต้องไม่ให้บุคคลอื่น นำยานยนต์/รถยนต์เช่าพร้อมอุปกรณ์ข้างต้นไปใช้สิทธิแทน โดยไม่ได้รับอนุญาตจากบริษัท</li>
						<li>พนักงานผู้ได้รับสิทธิ จะต้องดูแลและรักษายานยนต์/รถยนต์เช่า และอุปกรณ์รถเป็นอย่างดี และพร้อมใช้งานตลอดเวลา รวมถึงจะต้องใช้รถเช่าด้วยความระมัดระวัง 
						 ขับรถยนต์ถูกต้องตามกฏหมาย/จราจร และจอดรถในที่ปลอดภัยทุกครั้ง</li>
						<li>พนักงานผู้ได้รับสิทธิ จะต้องไม่กระทำการใดๆ อันเป็นการแก้ไข ดัดแปลง ต่อเติม หรือติดสติ๊กเกอร์ ไม่ว่าส่วนหนึ่งส่วนใดของยานยนต์/รถยนต์พร้อมอุปกรณ์ที่เช่าเป็นอันขาด</li>
						<li>พนักงานผู้ได้รับสิทธิ จะต้องนำยานยนต์/รถยนต์เช่า มาจอดเก็บไว้ที่บริษัทหลังเวลาเลิกงาน หรือสถานที่อื่นใดตามที่บริษัทกำหนด เพื่อความเป็นระเบียบเรียบร้อยในการใช้ และเพื่อความปลอดภัยในทรัพย์สิน</li>
						<li>พนักงานผู้ที่ได้รับสิทธิจะต้องไม่นำยานยนต์/รถยนต์เช่า ไปใช้ในทางที่ผิดกฏหมาย หรือกระทำการใดๆ ที่ผิดกฏหมาย หรือไปใช้ในทางที่ฝ่าฝืนระเบียบข้อบังคับ ประกาศ คำสั่งบริษัท</li>
						<li>พนักงานผู้ที่ได้รับสิทธิจะต้องนำยานยนต์/รถยนต์เช่าเข้าศูนย์บริการที่ผู้ให้เช่าได้จัดให้มีขึ้น เพื่อทำการตรวจสอบสภาพเครื่องยนต์และอุปกรณ์ต่างๆ 
						 ตามระยะเวลาเงื่อนไขผู้ผลิตรถยนต์กำหนดและระบุในสมุดรับประกัน โดยไม่ต้องเสียค่าใช้จ่าย</li>
						<li>ยานยนต์/รถยนต์เช่าพร้อมอุปกรณ์จากบริษัทภายนอก ได้จัดทำประกันภัยประเภท 1 หรือประเภทอื่นตามกำหนด รวมถึง พรบ.คุ้มครองผู้ประสบภัยจากรถด้วยแล้ว 
							กรณีบริษัทผู้รับประกันภัยเรียกเก็บค่าความเสียหายส่วนแรกตามเงื่อนไขและความคุ้มครองในกรมธรรม์ อันเนื่องจากการชนหรือคว่ำ หรือไม่สามารถแจ้งคู่กรณีอีกฝ่ายหนึ่งได้ เป็นต้น 
							ความเสียหายส่วนแรกนี้พนักงานผู้ได้รับสิทธิใช้จะต้องเป็นผู้รับผิดชอบที่เกิดขึ้นจริงทั้งสิ้น</li>
						<li>กรณีเกิดการชำรุด สูญหาย เสีย/ขัดข้องเนื่องจากการใช้งานตามปกติ หรือใดๆ ก็ตาม พนักงานจะต้องรีบแจ้งบริษัทผู้ให้เช่าและฝ่ายทรัพยากรมนุษย์ของบริษัททราบในทันที</li>
						<li>ในกรณีพนักงานผู้ได้รับสิทธิพ้นจากหน้าที่ตำแหน่งงาน หรือตามที่บริษัทแจ้ง หรือพนักงานลาออก หรือพ้นสภาพการเป็นพนักงาน หรือไม่ว่าด้วยเหตุใดๆ ก็ตาม 
							พนักงานจะต้องส่งมอบคืนยานยนต์/รถยนต์เช่า พร้อมอุปกรณ์ข้างต้นในสภาพเรียบร้อยและครบถ้วนให้กับทางบริษัทในทันที หากมีความเสียหายใดๆ เกิดขึ้นแก่ยานยนต์/รถยนต์เช่า พร้อมอุปกรณ์ 
							พนักงานผู้ที่มีสิทธินั้นๆ ยินดีที่จะรับผิดชอบค่าเสียหาย หรือค่าใช้จ่ายอื่นใด ที่เกิดขึ้นจริงทั้งสิ้น
						<li>จากข้อ 1. - 11. กรณีพนักงานผู้ได้รับสิทธิเพิกเฉย ละเลย ฝ่าฝืนการปฏิบัติพนักงานจะต้องรับผิดชอบค่าเสียหาย ความเสียหาย ค่าบริการ ค่าใช้จ่ายอื่นใด เต็มจำนวนตามที่เกิดขึ้นจริงทั้งสิ้น 
							หรือตามที่บริษัทเห็นควรตามแต่กรณี โดยอยู่ในดุลยพินิจของกรรมการบริหารบริษัท</li>
						<li>ในกรณีที่บริษัท เป็นผู้สำรองค่าเสียหาย และ/ หรือ ค่าใช้จ่ายอื่นใด ตามที่กล่าวมาข้างต้นตามแต่กรณี พนักงานผู้ได้รับสิทธิตกลงยินยอมรับผิดชดใช้ค่าเสียหาย ความเสียหาย ค่าบริการ 
							หรือค่าใช้จ่ายอื่นใด ที่เกิดขึ้นจริงทั้งหมดคืนให้แก่บริษัทภายในระยะเวลาที่บริษัทกำหนด</li>
						<li>บริษัทขอสงวนไว้ซึ่งสิทธิที่จะยกเลิก แก้ไข เปลี่ยนแปลงระเบียบการใช้ ผู้รับได้รับสิทธิ ยี่ห้อ/แบบและรุ่นยานยนต์/รถยนต์เช่าได้ตามเห็นสมควร โดยจะแจ้งให้ทราบเป็นคราวๆ ไป</li>
					</ol>
					
					<p><br>
						จึงประกาศมาเพื่อทราบโดยทั่วกัน และปฏิบัติอย่างเคร่งครัด<br><br>

						ทั้งนี้ ให้มีผลบังคับใช้นับตั้งแต่วันที่ 1 กรกฏาคม 2553 เป็นต้นไป<br><br>

						ประกาศ ณ วันที่ 29 มิถุนายน 2553
					</p>
					
					<div class="blog-details-bt">
						<div class="btn-group">
							<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
						</div>
					</div>
				
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-8 col-xs-12 p-fix">
				<aside>
					<div class="sidebar-news-list">
						<h4>CATEGORY</h4>
						<ul>
							<li>
								<a href="HRPolicy_BusinessEthicsHR.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบทรัพยากรมนุษย์</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAS.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบเกี่ยวกับทรัพย์สิน</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAC.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบเกี่ยวกับบัญชี การเงิน งบประมาณ จัดซื้อ</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsIT.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบเกี่ยวกับเทคโนโลยีสารสนเทศ</a>
							</li>
							<li>
								<a href="Benefits1_1.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบอื่นๆ</a>
							</li>
						</ul>
					</div>
				</aside>
				<aside>
					<div class="sidebar-news-list">
						<h4>Related Links</h4>
						<ul>
							<li>
								<a href="HRPolicy_BusinessEthicsAS1.aspx" class="tran3s">
									ระเบียบการใช้บัตรน้ำมัน Synergy Card Fleet Card บัตรน้ำมัน
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAS2.aspx" class="tran3s">
									ระเบียบเรื่องการเบิก และใช้ทรัพย์สินบริษัท
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAS3.aspx" class="tran3s">
									ระเบียบการใช้โทรศัพท์เคลื่อนที่ (APPLE IPHONE 7128)
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAS4.aspx" class="tran3s">
									ระเบียบการใช้โทรศัพท์เคลื่อนที่ (Samsung Galaxy A9 Pro)
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAS5.aspx" class="tran3s">
									ระเบียบการใช้รถที่บริษัทเช่าให้พนักงานใช้ปฏิบัติงาน
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAS6.aspx" class="tran3s">
									ระเบียบเงินประกันชุดฟอร์มพนักงาน
								</a>
							</li>
						</ul>
					</div>
				</aside>
			</div>
		</div>
		
	</div>
</article>

</asp:Content>
