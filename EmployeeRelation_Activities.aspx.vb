﻿Imports System.IO
Imports System.Data.SqlClient

Public Class EmployeeRelation_Activities
    Inherits System.Web.UI.Page
    Dim db_Form As New Connect_HR
    Dim DB_HR As New Connect_HR

    'https://www.aspsnippets.com/Articles/Upload-files-save-in-folder-and-display-in-ASPNet-GridView-with-Download-and-Delete-option.aspx
    'https://www.aspforums.net/Threads/356593/Save-file-in-folder-disk-and-File-Path-in-database-in-Windows-Forms-WinForms-Application-using-C-and-VBNet/

    Dim Link1, Link2, Link3, Link4, Link5, Link6 As String
    Dim Link22, Link32, Link42, Link52, Link62 As String
    Private sms As New PKMsg("")

    Dim L11 As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            loadImage()

        End If
    End Sub

    Sub loadImage()
        Dim sqlconnection1 As New SqlConnection(db_Form.sqlCon)
        Dim SqlCmd2 As New SqlCommand
        Dim SqlDataReader2 As SqlDataReader
        Dim sql As String = ""
        Dim ChangeDateSQL2 As String = ""
        Dim i As Integer
        Dim sql2 As String = ""

        sql = "  select top 6 IndexID, image ,TxtSubject, TxtDetail , isnull(DateActivity,'') as DateActivity ,isnull(FileName,'') as FileName, isnull(FilePath,'') as FilePath , isnull(TxtLinkImage,'') as TxtLinkImage  "

        sql += " from TB_IndexHome where type = 'Activity'  order by IndexID  desc "

        sqlconnection1 = New SqlConnection(db_Form.sqlCon)


        sqlconnection1.Open()

        SqlCmd2.Connection = sqlconnection1
        SqlCmd2.CommandTimeout = 0
        SqlCmd2.CommandText = sql
        SqlDataReader2 = SqlCmd2.ExecuteReader

        While SqlDataReader2.Read()
            Try
                If i = 0 Then
                    Image1.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    lblHeader1.Text = SqlDataReader2.GetValue(2)
                    lblDate1.Text = SqlDataReader2.GetValue(4)
                    lblDetail1.Text = SqlDataReader2.GetValue(3)
                    Label1.Text = SqlDataReader2.GetValue(5)
                    Label11.Text = SqlDataReader2.GetValue(7)

                    If SqlDataReader2.GetValue(7) = "" Then
                        Button11.Visible = False
                    End If

                ElseIf i = 1 Then
                    Image2.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    lblHeader2.Text = SqlDataReader2.GetValue(2)
                    lblDate2.Text = SqlDataReader2.GetValue(4)
                    lblDetail2.Text = SqlDataReader2.GetValue(3)
                    Label2.Text = SqlDataReader2.GetValue(5)
                    Label21.Text = SqlDataReader2.GetValue(7)

                    If SqlDataReader2.GetValue(7) = "" Then
                        Button21.Visible = False
                    End If

                ElseIf i = 2 Then
                    Image3.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    lblHeader3.Text = SqlDataReader2.GetValue(2)
                    lblDate3.Text = SqlDataReader2.GetValue(4)
                    lblDetail3.Text = SqlDataReader2.GetValue(3)
                    Label3.Text = SqlDataReader2.GetValue(5)
                    Label31.Text = SqlDataReader2.GetValue(7)

                    If SqlDataReader2.GetValue(7) = "" Then
                        Button31.Visible = False
                    End If

                ElseIf i = 3 Then
                    Image4.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    lblHeader4.Text = SqlDataReader2.GetValue(2)
                    lblDate4.Text = SqlDataReader2.GetValue(4)
                    lblDetail4.Text = SqlDataReader2.GetValue(3)
                    Label4.Text = SqlDataReader2.GetValue(5)
                    Label41.Text = SqlDataReader2.GetValue(7)

                    If SqlDataReader2.GetValue(7) = "" Then
                        Button41.Visible = False
                    End If

                ElseIf i = 4 Then
                    Image5.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    lblHeader5.Text = SqlDataReader2.GetValue(2)
                    lblDate5.Text = SqlDataReader2.GetValue(4)
                    lblDetail5.Text = SqlDataReader2.GetValue(3)
                    Label5.Text = SqlDataReader2.GetValue(5)
                    Label51.Text = SqlDataReader2.GetValue(7)

                    If SqlDataReader2.GetValue(7) = "" Then
                        Button51.Visible = False
                    End If

                ElseIf i = 5 Then
                    Image6.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    lblHeader6.Text = SqlDataReader2.GetValue(2)
                    lblDate6.Text = SqlDataReader2.GetValue(4)
                    lblDetail6.Text = SqlDataReader2.GetValue(3)
                    Label6.Text = SqlDataReader2.GetValue(5)
                    Label61.Text = SqlDataReader2.GetValue(7)

                    If SqlDataReader2.GetValue(7) = "" Then
                        Button61.Visible = False
                    End If

                End If

                i = i + 1
            Finally
            End Try
        End While
        SqlDataReader2.Close()


        ' returnFilename2(Link2)

        ' returnFilename4(Link4)


    End Sub


    Function returnFilename2(ByVal Link2 As String) As Single
        Return Link2
    End Function

    Function returnFilename4(ByVal Link4 As String) As Single
        Return Link4 = Link4
    End Function

    Function ReadImage(strID As String)
        Return ("ReadimageBanner.aspx?ID=" & strID)
    End Function

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click


        ' Response.Redirect("https://drive.google.com/drive/folders/1Y8YKMk3iDhmMzoCKDMtGbXb_V4Fy8MsD?usp=sharing")

        If Label1.Text = "" Then
            sms.Msg = "ไม่พบข้อมูล"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
            'Label2.ForeColor = Drawing.Color.Black
            'Label2.Text = "ไม่มีเอกสาร"

        End If

        Dim fname = Label1.Text  ' "18.ชมรมฟุตบอล Aroma Group 2017" 'Context.Request.QueryString("file")
        Dim actualFile = Path.Combine("C://Upload/Activities/", fname) & ""

        If File.Exists(actualFile) Then
            Context.Response.ContentType = "application/pdf"
            Context.Response.AddHeader("Content-Disposition", "attachment; filename=""" & Path.GetFileName(actualFile) & """")
            Context.Response.TransmitFile(actualFile)
        Else
            Context.Response.Clear()
            Context.Response.TrySkipIisCustomErrors = True
            Context.Response.StatusCode = 404
            Context.Response.Write("<html><head><title>404 - File not found</title><style>body {font-family: sans-serif;}</style></head><body><h1>404 - File not found</h1><p>Sorry, that file is not available .</p></body></html>")
            Context.Response.End()
        End If


    End Sub


    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

        If Label2.Text = "" Then
            sms.Msg = "ไม่พบข้อมูล"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
            'Label2.ForeColor = Drawing.Color.Black
            'Label2.Text = "ไม่มีเอกสาร"

        End If

        Dim fname = Label2.Text  ' "18.ชมรมฟุตบอล Aroma Group 2017" 'Context.Request.QueryString("file")
        Dim actualFile = Path.Combine("C://Upload/Activities/", fname) & ""

        If File.Exists(actualFile) Then
            Context.Response.ContentType = "application/pdf"
            Context.Response.AddHeader("Content-Disposition", "attachment; filename=""" & Path.GetFileName(actualFile) & """")
            Context.Response.TransmitFile(actualFile)
        Else
            Context.Response.Clear()
            Context.Response.TrySkipIisCustomErrors = True
            Context.Response.StatusCode = 404
            Context.Response.Write("<html><head><title>404 - File not found</title><style>body {font-family: sans-serif;}</style></head><body><h1>404 - File not found</h1><p>Sorry, that file is not available .</p></body></html>")
            Context.Response.End()
        End If

    End Sub

    Protected Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click

        If Label4.Text = "" Then
            sms.Msg = "ไม่พบข้อมูล"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If

        Dim fname = Label4.Text ' "18.ชมรมฟุตบอล Aroma Group 2017" 'Context.Request.QueryString("file")
        Dim actualFile = Path.Combine("C://Upload/Activities/", fname) & ""


        If File.Exists(actualFile) Then
            Context.Response.ContentType = "application/pdf"
            Context.Response.AddHeader("Content-Disposition", "attachment; filename=""" & Path.GetFileName(actualFile) & """")
            Context.Response.TransmitFile(actualFile)
        Else
            Context.Response.Clear()
            Context.Response.TrySkipIisCustomErrors = True
            Context.Response.StatusCode = 404
            Context.Response.Write("<html><head><title>404 - File not found</title><style>body {font-family: sans-serif;}</style></head><body><h1>404 - File not found</h1><p>Sorry, that file is not available .</p></body></html>")
            Context.Response.End()
        End If
    End Sub

    Protected Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        If Label3.Text = "" Then
            sms.Msg = "ไม่พบข้อมูล"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If

        Dim fname = Label3.Text ' "18.ชมรมฟุตบอล Aroma Group 2017" 'Context.Request.QueryString("file")
        Dim actualFile = Path.Combine("C://Upload/Activities/", fname) & ""

        If File.Exists(actualFile) Then
            Context.Response.ContentType = "application/pdf"
            Context.Response.AddHeader("Content-Disposition", "attachment; filename=""" & Path.GetFileName(actualFile) & """")
            Context.Response.TransmitFile(actualFile)
        Else
            Context.Response.Clear()
            Context.Response.TrySkipIisCustomErrors = True
            Context.Response.StatusCode = 404
            Context.Response.Write("<html><head><title>404 - File not found</title><style>body {font-family: sans-serif;}</style></head><body><h1>404 - File not found</h1><p>Sorry, that file is not available .</p></body></html>")
            Context.Response.End()
        End If
    End Sub

    Protected Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click

        If Label5.Text = "" Then
            sms.Msg = "ไม่พบข้อมูล"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If

        Dim fname = Label5.Text ' "18.ชมรมฟุตบอล Aroma Group 2017" 'Context.Request.QueryString("file")
        Dim actualFile = Path.Combine("C://Upload/Activities/", fname) & ""

        If File.Exists(actualFile) Then
            Context.Response.ContentType = "application/pdf"
            Context.Response.AddHeader("Content-Disposition", "attachment; filename=""" & Path.GetFileName(actualFile) & """")
            Context.Response.TransmitFile(actualFile)
        Else
            Context.Response.Clear()
            Context.Response.TrySkipIisCustomErrors = True
            Context.Response.StatusCode = 404
            Context.Response.Write("<html><head><title>404 - File not found</title><style>body {font-family: sans-serif;}</style></head><body><h1>404 - File not found</h1><p>Sorry, that file is not available .</p></body></html>")
            Context.Response.End()
        End If
    End Sub

    Protected Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click

        If Label6.Text = "" Then
            sms.Msg = "ไม่พบข้อมูล"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If

        Dim fname = Label6.Text ' "18.ชมรมฟุตบอล Aroma Group 2017" 'Context.Request.QueryString("file")
        Dim actualFile = Path.Combine("C://Upload/Activities/", fname) & ""

        If File.Exists(actualFile) Then
            Context.Response.ContentType = "application/pdf"
            Context.Response.AddHeader("Content-Disposition", "attachment; filename=""" & Path.GetFileName(actualFile) & """")
            Context.Response.TransmitFile(actualFile)
        Else
            Context.Response.Clear()
            Context.Response.TrySkipIisCustomErrors = True
            Context.Response.StatusCode = 404
            Context.Response.Write("<html><head><title>404 - File not found</title><style>body {font-family: sans-serif;}</style></head><body><h1>404 - File not found</h1><p>Sorry, that file is not available .</p></body></html>")
            Context.Response.End()
        End If
    End Sub

  

    Protected Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click
        Response.Redirect("" & Label11.Text & "")
    End Sub

    Protected Sub Button21_Click(sender As Object, e As EventArgs) Handles Button21.Click
        Response.Redirect("" & Label21.Text & "")
    End Sub

    Protected Sub Button31_Click(sender As Object, e As EventArgs) Handles Button31.Click
        Response.Redirect("" & Label31.Text & "")
    End Sub

    Protected Sub Button41_Click(sender As Object, e As EventArgs) Handles Button41.Click
        Response.Redirect("" & Label41.Text & "")
    End Sub

    Protected Sub Button51_Click(sender As Object, e As EventArgs) Handles Button51.Click
        Response.Redirect("" & Label51.Text & "")
    End Sub

    Protected Sub Button61_Click(sender As Object, e As EventArgs) Handles Button61.Click
        Response.Redirect("" & Label61.Text & "")
    End Sub
End Class