﻿Imports System.Data.SqlClient
Imports System.IO

Public Class Employee_Image
    Inherits System.Web.UI.Page
    Dim DB_HR As New Connect_HR
    Private sms As New PKMsg("")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'lblemp_id.Text = "1256026"

        If lblemp_id.Text <> "" Then
            '   imgTag.ImageUrl = ReadImage(lblemp_id.Text)
        End If


    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If FileUpload1.FileName <> "" Then
            Call insertIMG1()
            insertImage1()  ' LOGO
        End If
    End Sub



    Sub insertIMG1()
        Dim CurrentFileName As String
        CurrentFileName = FileUpload1.FileName

        If FileUpload1.PostedFile.ContentLength > 131072 Then
            sms.Msg = "The size of big too file , the size of the file must 128 KB not exceed!!!"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If
        Dim CurrentPath As String = Server.MapPath("~/Image_Employee/")

        FileUpload1.PostedFile.SaveAs(CurrentPath & FileUpload1.FileName)
        FileUpload1.PostedFile.SaveAs("c:\temp\" & FileUpload1.FileName)
        imgTag.ImageUrl = CurrentPath & FileUpload1.FileName
        Label1.Text = "c:\temp\" & FileUpload1.FileName
        ' TextBox1.Text = "c:\temp\" & FileUpload1.FileName

    End Sub

    Sub insertImage1()
        Dim db As New Connect_HR
        Dim sql As String
        Dim tr As SqlTransaction
        Dim sqlcon As New SqlConnection(DB_HR.sqlCon)

        If sqlcon.State = ConnectionState.Closed Then
            sqlcon = New SqlConnection(DB_HR.sqlCon)
            sqlcon.Open()
        End If

        tr = sqlcon.BeginTransaction

        Try
            Dim cmd As SqlCommand = New SqlCommand(sql, sqlcon, tr)
            If Label1.Text <> "" Then
                Dim FileN As FileStream
                Dim Ln As Int32
                Dim oBinaryReader As BinaryReader
                Dim oImgByteArray As Byte()

                FileN = New FileStream(Label1.Text, FileMode.Open, FileAccess.Read)
                oBinaryReader = New BinaryReader(FileN)
                oImgByteArray = oBinaryReader.ReadBytes(CInt(FileN.Length))
                Ln = CInt(FileN.Length)
                oBinaryReader.Close()
                FileN.Close()

                sql = "update TB_Employee set image_name=@FF1, image=@FF2 where Employee_ID  = '" & TextBox1.Text & "'"

                Dim cmd3 As SqlCommand = New SqlCommand(sql, sqlcon, tr)
                cmd3.Parameters.Add("@FF1", SqlDbType.VarChar, 0).Value = TextBox1.Text + Path.GetExtension(Label1.Text).ToLower
                cmd3.Parameters.Add("@FF2", SqlDbType.Image, 0).Value = oImgByteArray
                cmd3.CommandTimeout = 0
                cmd3.ExecuteNonQuery()

            End If

            tr.Commit()
            sqlcon.Close()
        Catch ex As Exception
            tr.Rollback()
            sqlcon.Close()
            Exit Sub
        End Try
    End Sub

    Function ReadImage(ByVal strID As String)
        Return ("Readimage.aspx?ImageID=" & strID)
    End Function


    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If FileUpload2.FileName <> "" Then
            Call insertIMG2()
            insertImage2()  ' LOGO
        End If
    End Sub


    Protected Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If FileUpload3.FileName <> "" Then
            Call insertIMG3()
            insertImage3()  ' LOGO
        End If
    End Sub

    Protected Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        If FileUpload4.FileName <> "" Then
            Call insertIMG4()
            insertImage4()  ' LOGO
        End If
    End Sub

    Protected Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        If FileUpload5.FileName <> "" Then
            Call insertIMG5()
            insertImage5()  ' LOGO
        End If
    End Sub

    Protected Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        If FileUpload6.FileName <> "" Then
            Call insertIMG6()
            insertImage6()  ' LOGO
        End If
    End Sub

    Protected Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        If FileUpload7.FileName <> "" Then
            Call insertIMG7()
            insertImage7()  ' LOGO
        End If
    End Sub

    Protected Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        If FileUpload8.FileName <> "" Then
            Call insertIMG8()
            insertImage8()  ' LOGO
        End If
    End Sub

    Protected Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        If FileUpload9.FileName <> "" Then
            Call insertIMG9()
            insertImage9()  ' LOGO
        End If
    End Sub

    Protected Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click
        If FileUpload10.FileName <> "" Then
            Call insertIMG10()
            insertImage10()  ' LOGO
        End If
    End Sub

    '2
    Sub insertIMG2()
        Dim CurrentFileName As String
        CurrentFileName = FileUpload2.FileName

        If FileUpload2.PostedFile.ContentLength > 131072 Then
            sms.Msg = "The size of big too file , the size of the file must 128 KB not exceed!!!"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If
        Dim CurrentPath As String = Server.MapPath("~/Image_Employee/")

        FileUpload2.PostedFile.SaveAs(CurrentPath & FileUpload2.FileName)
        FileUpload2.PostedFile.SaveAs("c:\temp\" & FileUpload2.FileName)
        imgTag.ImageUrl = CurrentPath & FileUpload2.FileName
        Label2.Text = "c:\temp\" & FileUpload2.FileName
        ' TextBox1.Text = "c:\temp\" & FileUpload1.FileName

    End Sub

    Sub insertImage2()
        Dim db As New Connect_HR
        Dim sql As String
        Dim tr As SqlTransaction
        Dim sqlcon As New SqlConnection(DB_HR.sqlCon)

        If sqlcon.State = ConnectionState.Closed Then
            sqlcon = New SqlConnection(DB_HR.sqlCon)
            sqlcon.Open()
        End If

        tr = sqlcon.BeginTransaction

        Try
            Dim cmd As SqlCommand = New SqlCommand(sql, sqlcon, tr)
            If Label1.Text <> "" Then
                Dim FileN As FileStream
                Dim Ln As Int32
                Dim oBinaryReader As BinaryReader
                Dim oImgByteArray As Byte()

                FileN = New FileStream(Label2.Text, FileMode.Open, FileAccess.Read)
                oBinaryReader = New BinaryReader(FileN)
                oImgByteArray = oBinaryReader.ReadBytes(CInt(FileN.Length))
                Ln = CInt(FileN.Length)
                oBinaryReader.Close()
                FileN.Close()

                sql = "update TB_Employee set image_name=@FF1, image=@FF2 where Employee_ID  = '" & TextBox2.Text & "'"

                Dim cmd3 As SqlCommand = New SqlCommand(sql, sqlcon, tr)
                cmd3.Parameters.Add("@FF1", SqlDbType.VarChar, 0).Value = TextBox2.Text + Path.GetExtension(Label2.Text).ToLower
                cmd3.Parameters.Add("@FF2", SqlDbType.Image, 0).Value = oImgByteArray
                cmd3.CommandTimeout = 0
                cmd3.ExecuteNonQuery()

            End If

            tr.Commit()
            sqlcon.Close()
        Catch ex As Exception
            tr.Rollback()
            sqlcon.Close()
            Exit Sub
        End Try
    End Sub

    '3

    Sub insertIMG3()
        Dim CurrentFileName As String
        CurrentFileName = FileUpload3.FileName

        If FileUpload3.PostedFile.ContentLength > 131072 Then
            sms.Msg = "The size of big too file , the size of the file must 128 KB not exceed!!!"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If
        Dim CurrentPath As String = Server.MapPath("~/Image_Employee/")

        FileUpload3.PostedFile.SaveAs(CurrentPath & FileUpload3.FileName)
        FileUpload3.PostedFile.SaveAs("c:\temp\" & FileUpload3.FileName)
        imgTag.ImageUrl = CurrentPath & FileUpload3.FileName
        Label3.Text = "c:\temp\" & FileUpload3.FileName
        ' TextBox3.Text = "c:\temp\" & FileUpload3.FileName

    End Sub

    Sub insertImage3()
        Dim db As New Connect_HR
        Dim sql As String
        Dim tr As SqlTransaction
        Dim sqlcon As New SqlConnection(DB_HR.sqlCon)

        If sqlcon.State = ConnectionState.Closed Then
            sqlcon = New SqlConnection(DB_HR.sqlCon)
            sqlcon.Open()
        End If

        tr = sqlcon.BeginTransaction

        Try
            Dim cmd As SqlCommand = New SqlCommand(sql, sqlcon, tr)
            If Label3.Text <> "" Then
                Dim FileN As FileStream
                Dim Ln As Int32
                Dim oBinaryReader As BinaryReader
                Dim oImgByteArray As Byte()

                FileN = New FileStream(Label3.Text, FileMode.Open, FileAccess.Read)
                oBinaryReader = New BinaryReader(FileN)
                oImgByteArray = oBinaryReader.ReadBytes(CInt(FileN.Length))
                Ln = CInt(FileN.Length)
                oBinaryReader.Close()
                FileN.Close()

                sql = "update TB_Employee set image_name=@FF1, image=@FF2 where Employee_ID  = '" & TextBox3.Text & "'"

                Dim cmd3 As SqlCommand = New SqlCommand(sql, sqlcon, tr)
                cmd3.Parameters.Add("@FF1", SqlDbType.VarChar, 0).Value = TextBox3.Text + Path.GetExtension(Label3.Text).ToLower
                cmd3.Parameters.Add("@FF2", SqlDbType.Image, 0).Value = oImgByteArray
                cmd3.CommandTimeout = 0
                cmd3.ExecuteNonQuery()

            End If

            tr.Commit()
            sqlcon.Close()
        Catch ex As Exception
            tr.Rollback()
            sqlcon.Close()
            Exit Sub
        End Try
    End Sub

    '4
    Sub insertIMG4()
        Dim CurrentFileName As String
        CurrentFileName = FileUpload4.FileName

        If FileUpload4.PostedFile.ContentLength > 131072 Then
            sms.Msg = "The size of big too file , the size of the file must 128 KB not exceed!!!"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If
        Dim CurrentPath As String = Server.MapPath("~/Image_Employee/")

        FileUpload4.PostedFile.SaveAs(CurrentPath & FileUpload4.FileName)
        FileUpload4.PostedFile.SaveAs("c:\temp\" & FileUpload4.FileName)
        imgTag.ImageUrl = CurrentPath & FileUpload4.FileName
        Label4.Text = "c:\temp\" & FileUpload4.FileName
        ' TextBox4.Text = "c:\temp\" & FileUpload4.FileName

    End Sub

    Sub insertImage4()
        Dim db As New Connect_HR
        Dim sql As String
        Dim tr As SqlTransaction
        Dim sqlcon As New SqlConnection(DB_HR.sqlCon)

        If sqlcon.State = ConnectionState.Closed Then
            sqlcon = New SqlConnection(DB_HR.sqlCon)
            sqlcon.Open()
        End If

        tr = sqlcon.BeginTransaction

        Try
            Dim cmd As SqlCommand = New SqlCommand(sql, sqlcon, tr)
            If Label4.Text <> "" Then
                Dim FileN As FileStream
                Dim Ln As Int32
                Dim oBinaryReader As BinaryReader
                Dim oImgByteArray As Byte()

                FileN = New FileStream(Label4.Text, FileMode.Open, FileAccess.Read)
                oBinaryReader = New BinaryReader(FileN)
                oImgByteArray = oBinaryReader.ReadBytes(CInt(FileN.Length))
                Ln = CInt(FileN.Length)
                oBinaryReader.Close()
                FileN.Close()

                sql = "update TB_Employee set image_name=@FF1, image=@FF2 where Employee_ID  = '" & TextBox4.Text & "'"

                Dim cmd3 As SqlCommand = New SqlCommand(sql, sqlcon, tr)
                cmd3.Parameters.Add("@FF1", SqlDbType.VarChar, 0).Value = TextBox4.Text + Path.GetExtension(Label4.Text).ToLower
                cmd3.Parameters.Add("@FF2", SqlDbType.Image, 0).Value = oImgByteArray
                cmd3.CommandTimeout = 0
                cmd3.ExecuteNonQuery()

            End If

            tr.Commit()
            sqlcon.Close()
        Catch ex As Exception
            tr.Rollback()
            sqlcon.Close()
            Exit Sub
        End Try
    End Sub

    '5
    Sub insertIMG5()
        Dim CurrentFileName As String
        CurrentFileName = FileUpload5.FileName

        If FileUpload5.PostedFile.ContentLength > 131072 Then
            sms.Msg = "The size of big too file , the size of the file must 128 KB not exceed!!!"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If
        Dim CurrentPath As String = Server.MapPath("~/Image_Employee/")

        FileUpload5.PostedFile.SaveAs(CurrentPath & FileUpload5.FileName)
        FileUpload5.PostedFile.SaveAs("c:\temp\" & FileUpload5.FileName)
        imgTag.ImageUrl = CurrentPath & FileUpload5.FileName
        Label5.Text = "c:\temp\" & FileUpload5.FileName
        ' TextBox5.Text = "c:\temp\" & FileUpload5.FileName

    End Sub

    Sub insertImage5()
        Dim db As New Connect_HR
        Dim sql As String
        Dim tr As SqlTransaction
        Dim sqlcon As New SqlConnection(DB_HR.sqlCon)

        If sqlcon.State = ConnectionState.Closed Then
            sqlcon = New SqlConnection(DB_HR.sqlCon)
            sqlcon.Open()
        End If

        tr = sqlcon.BeginTransaction

        Try
            Dim cmd As SqlCommand = New SqlCommand(sql, sqlcon, tr)
            If Label5.Text <> "" Then
                Dim FileN As FileStream
                Dim Ln As Int32
                Dim oBinaryReader As BinaryReader
                Dim oImgByteArray As Byte()

                FileN = New FileStream(Label5.Text, FileMode.Open, FileAccess.Read)
                oBinaryReader = New BinaryReader(FileN)
                oImgByteArray = oBinaryReader.ReadBytes(CInt(FileN.Length))
                Ln = CInt(FileN.Length)
                oBinaryReader.Close()
                FileN.Close()

                sql = "update TB_Employee set image_name=@FF1, image=@FF2 where Employee_ID  = '" & TextBox5.Text & "'"

                Dim cmd3 As SqlCommand = New SqlCommand(sql, sqlcon, tr)
                cmd3.Parameters.Add("@FF1", SqlDbType.VarChar, 0).Value = TextBox5.Text + Path.GetExtension(Label5.Text).ToLower
                cmd3.Parameters.Add("@FF2", SqlDbType.Image, 0).Value = oImgByteArray
                cmd3.CommandTimeout = 0
                cmd3.ExecuteNonQuery()

            End If

            tr.Commit()
            sqlcon.Close()
        Catch ex As Exception
            tr.Rollback()
            sqlcon.Close()
            Exit Sub
        End Try
    End Sub

    '6
    Sub insertIMG6()
        Dim CurrentFileName As String
        CurrentFileName = FileUpload6.FileName

        If FileUpload6.PostedFile.ContentLength > 131072 Then
            sms.Msg = "The size of big too file , the size of the file must 128 KB not exceed!!!"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If
        Dim CurrentPath As String = Server.MapPath("~/Image_Employee/")

        FileUpload6.PostedFile.SaveAs(CurrentPath & FileUpload6.FileName)
        FileUpload6.PostedFile.SaveAs("c:\temp\" & FileUpload6.FileName)
        imgTag.ImageUrl = CurrentPath & FileUpload6.FileName
        Label6.Text = "c:\temp\" & FileUpload6.FileName
        ' TextBox6.Text = "c:\temp\" & FileUpload6.FileName

    End Sub

    Sub insertImage6()
        Dim db As New Connect_HR
        Dim sql As String
        Dim tr As SqlTransaction
        Dim sqlcon As New SqlConnection(DB_HR.sqlCon)

        If sqlcon.State = ConnectionState.Closed Then
            sqlcon = New SqlConnection(DB_HR.sqlCon)
            sqlcon.Open()
        End If

        tr = sqlcon.BeginTransaction

        Try
            Dim cmd As SqlCommand = New SqlCommand(sql, sqlcon, tr)
            If Label6.Text <> "" Then
                Dim FileN As FileStream
                Dim Ln As Int32
                Dim oBinaryReader As BinaryReader
                Dim oImgByteArray As Byte()

                FileN = New FileStream(Label6.Text, FileMode.Open, FileAccess.Read)
                oBinaryReader = New BinaryReader(FileN)
                oImgByteArray = oBinaryReader.ReadBytes(CInt(FileN.Length))
                Ln = CInt(FileN.Length)
                oBinaryReader.Close()
                FileN.Close()

                sql = "update TB_Employee set image_name=@FF1, image=@FF2 where Employee_ID  = '" & TextBox6.Text & "'"

                Dim cmd3 As SqlCommand = New SqlCommand(sql, sqlcon, tr)
                cmd3.Parameters.Add("@FF1", SqlDbType.VarChar, 0).Value = TextBox6.Text + Path.GetExtension(Label6.Text).ToLower
                cmd3.Parameters.Add("@FF2", SqlDbType.Image, 0).Value = oImgByteArray
                cmd3.CommandTimeout = 0
                cmd3.ExecuteNonQuery()

            End If

            tr.Commit()
            sqlcon.Close()
        Catch ex As Exception
            tr.Rollback()
            sqlcon.Close()
            Exit Sub
        End Try
    End Sub

    '7
    Sub insertIMG7()
        Dim CurrentFileName As String
        CurrentFileName = FileUpload7.FileName

        If FileUpload7.PostedFile.ContentLength > 131072 Then
            sms.Msg = "The size of big too file , the size of the file must 128 KB not exceed!!!"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If
        Dim CurrentPath As String = Server.MapPath("~/Image_Employee/")

        FileUpload7.PostedFile.SaveAs(CurrentPath & FileUpload7.FileName)
        FileUpload7.PostedFile.SaveAs("c:\temp\" & FileUpload7.FileName)
        imgTag.ImageUrl = CurrentPath & FileUpload7.FileName
        Label7.Text = "c:\temp\" & FileUpload7.FileName
        ' TextBox7.Text = "c:\temp\" & FileUpload7.FileName

    End Sub

    Sub insertImage7()
        Dim db As New Connect_HR
        Dim sql As String
        Dim tr As SqlTransaction
        Dim sqlcon As New SqlConnection(DB_HR.sqlCon)

        If sqlcon.State = ConnectionState.Closed Then
            sqlcon = New SqlConnection(DB_HR.sqlCon)
            sqlcon.Open()
        End If

        tr = sqlcon.BeginTransaction

        Try
            Dim cmd As SqlCommand = New SqlCommand(sql, sqlcon, tr)
            If Label7.Text <> "" Then
                Dim FileN As FileStream
                Dim Ln As Int32
                Dim oBinaryReader As BinaryReader
                Dim oImgByteArray As Byte()

                FileN = New FileStream(Label7.Text, FileMode.Open, FileAccess.Read)
                oBinaryReader = New BinaryReader(FileN)
                oImgByteArray = oBinaryReader.ReadBytes(CInt(FileN.Length))
                Ln = CInt(FileN.Length)
                oBinaryReader.Close()
                FileN.Close()

                sql = "update TB_Employee set image_name=@FF1, image=@FF2 where Employee_ID  = '" & TextBox7.Text & "'"

                Dim cmd3 As SqlCommand = New SqlCommand(sql, sqlcon, tr)
                cmd3.Parameters.Add("@FF1", SqlDbType.VarChar, 0).Value = TextBox7.Text + Path.GetExtension(Label7.Text).ToLower
                cmd3.Parameters.Add("@FF2", SqlDbType.Image, 0).Value = oImgByteArray
                cmd3.CommandTimeout = 0
                cmd3.ExecuteNonQuery()

            End If

            tr.Commit()
            sqlcon.Close()
        Catch ex As Exception
            tr.Rollback()
            sqlcon.Close()
            Exit Sub
        End Try
    End Sub

    '8
    Sub insertIMG8()
        Dim CurrentFileName As String
        CurrentFileName = FileUpload8.FileName

        If FileUpload8.PostedFile.ContentLength > 131072 Then
            sms.Msg = "The size of big too file , the size of the file must 128 KB not exceed!!!"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If
        Dim CurrentPath As String = Server.MapPath("~/Image_Employee/")

        FileUpload8.PostedFile.SaveAs(CurrentPath & FileUpload8.FileName)
        FileUpload8.PostedFile.SaveAs("c:\temp\" & FileUpload8.FileName)
        imgTag.ImageUrl = CurrentPath & FileUpload8.FileName
        Label8.Text = "c:\temp\" & FileUpload8.FileName
        ' TextBox8.Text = "c:\temp\" & FileUpload8.FileName

    End Sub

    Sub insertImage8()
        Dim db As New Connect_HR
        Dim sql As String
        Dim tr As SqlTransaction
        Dim sqlcon As New SqlConnection(DB_HR.sqlCon)

        If sqlcon.State = ConnectionState.Closed Then
            sqlcon = New SqlConnection(DB_HR.sqlCon)
            sqlcon.Open()
        End If

        tr = sqlcon.BeginTransaction

        Try
            Dim cmd As SqlCommand = New SqlCommand(sql, sqlcon, tr)
            If Label8.Text <> "" Then
                Dim FileN As FileStream
                Dim Ln As Int32
                Dim oBinaryReader As BinaryReader
                Dim oImgByteArray As Byte()

                FileN = New FileStream(Label8.Text, FileMode.Open, FileAccess.Read)
                oBinaryReader = New BinaryReader(FileN)
                oImgByteArray = oBinaryReader.ReadBytes(CInt(FileN.Length))
                Ln = CInt(FileN.Length)
                oBinaryReader.Close()
                FileN.Close()

                sql = "update TB_Employee set image_name=@FF1, image=@FF2 where Employee_ID  = '" & TextBox8.Text & "'"

                Dim cmd3 As SqlCommand = New SqlCommand(sql, sqlcon, tr)
                cmd3.Parameters.Add("@FF1", SqlDbType.VarChar, 0).Value = TextBox8.Text + Path.GetExtension(Label8.Text).ToLower
                cmd3.Parameters.Add("@FF2", SqlDbType.Image, 0).Value = oImgByteArray
                cmd3.CommandTimeout = 0
                cmd3.ExecuteNonQuery()

            End If

            tr.Commit()
            sqlcon.Close()
        Catch ex As Exception
            tr.Rollback()
            sqlcon.Close()
            Exit Sub
        End Try
    End Sub

    '9
    Sub insertIMG9()
        Dim CurrentFileName As String
        CurrentFileName = FileUpload9.FileName

        If FileUpload9.PostedFile.ContentLength > 131072 Then
            sms.Msg = "The size of big too file , the size of the file must 128 KB not exceed!!!"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If
        Dim CurrentPath As String = Server.MapPath("~/Image_Employee/")

        FileUpload9.PostedFile.SaveAs(CurrentPath & FileUpload9.FileName)
        FileUpload9.PostedFile.SaveAs("c:\temp\" & FileUpload9.FileName)
        imgTag.ImageUrl = CurrentPath & FileUpload9.FileName
        Label9.Text = "c:\temp\" & FileUpload9.FileName
        ' TextBox9.Text = "c:\temp\" & FileUpload9.FileName

    End Sub

    Sub insertImage9()
        Dim db As New Connect_HR
        Dim sql As String
        Dim tr As SqlTransaction
        Dim sqlcon As New SqlConnection(DB_HR.sqlCon)

        If sqlcon.State = ConnectionState.Closed Then
            sqlcon = New SqlConnection(DB_HR.sqlCon)
            sqlcon.Open()
        End If

        tr = sqlcon.BeginTransaction

        Try
            Dim cmd As SqlCommand = New SqlCommand(sql, sqlcon, tr)
            If Label9.Text <> "" Then
                Dim FileN As FileStream
                Dim Ln As Int32
                Dim oBinaryReader As BinaryReader
                Dim oImgByteArray As Byte()

                FileN = New FileStream(Label9.Text, FileMode.Open, FileAccess.Read)
                oBinaryReader = New BinaryReader(FileN)
                oImgByteArray = oBinaryReader.ReadBytes(CInt(FileN.Length))
                Ln = CInt(FileN.Length)
                oBinaryReader.Close()
                FileN.Close()

                sql = "update TB_Employee set image_name=@FF1, image=@FF2 where Employee_ID  = '" & TextBox9.Text & "'"

                Dim cmd3 As SqlCommand = New SqlCommand(sql, sqlcon, tr)
                cmd3.Parameters.Add("@FF1", SqlDbType.VarChar, 0).Value = TextBox9.Text + Path.GetExtension(Label9.Text).ToLower
                cmd3.Parameters.Add("@FF2", SqlDbType.Image, 0).Value = oImgByteArray
                cmd3.CommandTimeout = 0
                cmd3.ExecuteNonQuery()

            End If

            tr.Commit()
            sqlcon.Close()
        Catch ex As Exception
            tr.Rollback()
            sqlcon.Close()
            Exit Sub
        End Try
    End Sub

    '10
    Sub insertIMG10()
        Dim CurrentFileName As String
        CurrentFileName = FileUpload10.FileName

        If FileUpload10.PostedFile.ContentLength > 131072 Then
            sms.Msg = "The size of big too file , the size of the file must 128 KB not exceed!!!"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If
        Dim CurrentPath As String = Server.MapPath("~/Image_Employee/")

        FileUpload10.PostedFile.SaveAs(CurrentPath & FileUpload10.FileName)
        FileUpload10.PostedFile.SaveAs("c:\temp\" & FileUpload10.FileName)
        imgTag.ImageUrl = CurrentPath & FileUpload10.FileName
        Label10.Text = "c:\temp\" & FileUpload10.FileName
        ' TextBox10.Text = "c:\temp\" & FileUpload10.FileName

    End Sub

    Sub insertImage10()
        Dim db As New Connect_HR
        Dim sql As String
        Dim tr As SqlTransaction
        Dim sqlcon As New SqlConnection(DB_HR.sqlCon)

        If sqlcon.State = ConnectionState.Closed Then
            sqlcon = New SqlConnection(DB_HR.sqlCon)
            sqlcon.Open()
        End If

        tr = sqlcon.BeginTransaction

        Try
            Dim cmd As SqlCommand = New SqlCommand(sql, sqlcon, tr)
            If Label10.Text <> "" Then
                Dim FileN As FileStream
                Dim Ln As Int32
                Dim oBinaryReader As BinaryReader
                Dim oImgByteArray As Byte()

                FileN = New FileStream(Label10.Text, FileMode.Open, FileAccess.Read)
                oBinaryReader = New BinaryReader(FileN)
                oImgByteArray = oBinaryReader.ReadBytes(CInt(FileN.Length))
                Ln = CInt(FileN.Length)
                oBinaryReader.Close()
                FileN.Close()

                sql = "update TB_Employee set image_name=@FF1, image=@FF2 where Employee_ID  = '" & TextBox10.Text & "'"

                Dim cmd3 As SqlCommand = New SqlCommand(sql, sqlcon, tr)
                cmd3.Parameters.Add("@FF1", SqlDbType.VarChar, 0).Value = TextBox10.Text + Path.GetExtension(Label10.Text).ToLower
                cmd3.Parameters.Add("@FF2", SqlDbType.Image, 0).Value = oImgByteArray
                cmd3.CommandTimeout = 0
                cmd3.ExecuteNonQuery()

            End If

            tr.Commit()
            sqlcon.Close()
        Catch ex As Exception
            tr.Rollback()
            sqlcon.Close()
            Exit Sub
        End Try
    End Sub

End Class