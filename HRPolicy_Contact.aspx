﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_Contact.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_Contact" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/HR.jpg" alt=""/></div>
<!-- end banner -->

<section id="team-section">
	<div class="container">
		<div class="theme-title">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
            </ul>
			<h2>ติดต่อฝ่ายทรัพยากรมนุษย์</h2>
			<!--p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p-->
		</div> <!-- /.theme-title -->

		<div class="clear-fix team-member-wrapper">
			<div class="float-left"> 
				<div class="single-team-member">
					<div class="img">
						<img src="Images/HR/KNong_2.jpg" alt="" height="300"/>
						<div class="opacity tran4s">
							<h4>คุณศิริพร  นิลกำแหง (น้อง)</h4>
							<span>ผู้อำนวยการฝ่ายทรัพยากรมนุษย์</span>
							<p>
								เบอร์ต่อภายใน : 404<br />
                    			<%--โทรศัพท์มือถือ : -<br />--%>
                    			Email : <a href="mailto:siripornn@aromathailand.com">siripornn@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณศิริพร  นิลกำแหง (น้อง)</h5>
						<p>ผู้อำนวยการฝ่ายทรัพยากรมนุษย์</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left --> <%--คุณน้อง--%>

			<%--<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="Images/HR/คุณ เอกชัย  บูรณธน(โต้ง).jpg" alt="" height="300"/>
						<div class="opacity tran4s">
							<h4>คุณ เอกชัย  บูรณธน (โต้ง)</h4>
							<span>ผู้จัดการฝ่ายทรัพยากรมนุษย์</span>
							<p>
								เบอร์ต่อภายใน : 400<br />
                    			โทรศัพท์มือถือ : -<br />
                    			Email : <a href="mailto:ekkachaibur@aromathailand.com">ekkachaibur@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณ เอกชัย  บูรณธน (โต้ง)</h5>
						<p>ผู้จัดการฝ่ายทรัพยากรมนุษย์</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->--%> <%--คุณโต้ง--%>

			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="Images/HR/KView.jpg" alt="" height="300" 
                            width="300"/>
						<div class="opacity tran4s">
							<h4>คุณ ธนินท์รัฐ  จิรโชติสิทธิพร(วิว)</h4>
							<span>ผู้จัดการฝ่ายทรัพยากรมนุษย์<br /> และพัฒนาองค์กร</span>
							<p>
								เบอร์ต่อภายใน : 405<br />
                    			<%--โทรศัพท์มือถือ : -<br />--%>
                    			Email : <a href="mailto:taninratjir@aromathailand.com">taninratjir@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณ ธนินท์รัฐ  จิรโชติสิทธิพร(วิว)</h5>
						<p>ผู้จัดการฝ่ายทรัพยากรมนุษย์<br />และพัฒนาองค์กร</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->  <%--คุณวิว--%>
			
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="Images/HR/KAe.jpg" alt="" height="300"/>
						<div class="opacity tran4s">
							<h4>คุณจิรวัฒน์ หงอกสิมมา (เอ)</h4>
							<span>ผู้จัดการแผนกสรรหาว่าจ้าง</span>
							<p>
								เบอร์ต่อภายใน : 514<br />
                        		<%--โทรศัพท์มือถือ : 081-6164697<br />--%>
                        		Email : <a href="mailto:jirawatn@aromathailand.com">jirawatn@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณจิรวัฒน์ หงอกสิมมา (เอ)</h5>
						<p>ผู้จัดการแผนกสรรหาว่าจ้าง</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left --> <%--คุณเอ--%>

            <div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="Images/HR/KOpOp_2.jpg" alt="" height="300"/>
						<div class="opacity tran4s">
							<h4>คุณกมลธิชา  วราอัศวปติ (อ๊บอ๊บ)</h4>
							<span>ผู้จัดการแผนกการเรียนรู้ <br /> และการพัฒนา</span>
							<p>
								เบอร์ต่อภายใน : 518<br />
                        		<%--โทรศัพท์มือถือ : 081-6164697<br />--%>
                        		Email : <a href="mailto:kamonthichawar@aromathailand.com">kamonthichawar@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณกมลธิชา  วราอัศวปติ (อ๊บอ๊บ)</h5>
						<p>ผู้จัดการแผนกการเรียนรู้ <br /> และการพัฒนา</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left --> <%--คุณอ๊บอ๊บ--%>
		
			
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="Images/HR/KPla.jpg" alt="" height="300"/>
						<div class="opacity tran4s">
							<h4>คุณปราณี บุญแท้แท้ (ปลา)</h4>
							<span>หัวหน้างาน แผนกรายได้ <br />และผลประโยชน์ตอบแทน-พนักงานสังกัด KVN</span>
							<p>
								เบอร์ต่อภายใน : 401<br />
                        		<%--โทรศัพท์มือถือ : -<br />--%>
                        		Email : <a href="mailto:praneeb@aromathailand.com">praneeb@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณปราณี บุญแท้แท้ (ปลา)</h5>
						<p>หัวหน้างาน แผนกรายได้ <br />และผลประโยชน์ตอบแทน-พนักงานสังกัด KVN</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left --> <%--คุณปลา--%>
			
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="Images/HR/KOil.jpg" alt="" height="300"/>
						<div class="opacity tran4s">
							<h4>คุณปรียาภัทร์ ไชยเดช (ออย)</h4>
							<span>หัวหน้างาน แผนกรายได้ <br />และผลประโยชน์ตอบแทน-พนักงานสังกัด ESP</span>
							<p>
								เบอร์ต่อภายใน : 402<br />
                        		<%--โทรศัพท์มือถือ : 085-0189583<br />--%>
                        		Email : <a href="mailto:preeyapatc@aromathailand.com">preeyapatc@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณปรียาภัทร์ ไชยเดช (ออย)</h5>
						<p>หัวหน้างาน แผนกรายได้ <br />และผลประโยชน์ตอบแทน-พนักงานสังกัด ESP</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left --> <%--คุณออย--%>
			
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="Images/HR/KEst.jpg" alt="" height="300"/>
						<div class="opacity tran4s">
							<h4>คุณพงษ์พันธ์ กระแสร์ (เอส)</h4>
							<span>เจ้าหน้าที่ แผนกพัฒนาฝึกอบรม <br />และงานแรงงานสัมพันธ์</span>
							<p>
								เบอร์ต่อภายใน : 515<br />
                        		<%--โทรศัพท์มือถือ : 089-8100931<br />--%>
                        		Email : <a href="mailto:phongphunk@aromathailand.com">phongphunk@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณพงษ์พันธ์ กระแสร์ (เอส)</h5>
						<p>เจ้าหน้าที่ แผนกพัฒนาฝึกอบรม <br />และงานแรงงานสัมพันธ์</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left --> <%--คุณเอส--%>
			
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="Images/HR/KToon.jpg" alt="" height="300"/>
						<div class="opacity tran4s">
							<h4>คุณอัจฉรา บุตรสิงห์ (ตูน)</h4>
							<span>หัวหน้างาน แผนกความผูกพันของบุคลากร</span>
							<p>
								เบอร์ต่อภายใน : 517<br />
                        		<%--โทรศัพท์มือถือ : 081-6164697<br />--%>
                        		Email : <a href="mailto:attcharab@aromathailand.com">attcharab@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณอัจฉรา บุตรสิงห์ (ตูน)</h5>
						<p>หัวหน้างาน แผนกความผูกพันของบุคลากร</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left --> <%--คุณตูน--%>
			

			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="Images/HR/KBird.jpg" alt="" height="300"/>
						<div class="opacity tran4s">
							<h4>คุณบริพัณฑ์ มโนธรรม (เบิร์ด)</h4>
							<span>เจ้าหน้าที่สรรหาว่าจ้าง</span>
							<p>
								เบอร์ต่อภายใน : 516<br />
                        		<%--โทรศัพท์มือถือ : -<br />--%>
                        		Email : <a href="mailto:boriphanman@aromathailand.com">boriphanman@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณบริพัณฑ์ มโนธรรม (เบิร์ด)</h5>
						<p>เจ้าหน้าที่สรรหาว่าจ้าง</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left --> <%--คุณเบิร์ด--%>

            <div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="Images/HR/KNan.jpg" alt="" height="300" 
                            width="300"/>
						<div class="opacity tran4s">
							<h4>คุณศิรินทรา  มาลี (แนน)</h4>
							<span>เจ้าหน้าที่ แผนกรายได้ <br />และผลประโยชน์ตอบแทน-พนักงานสังกัด LION,UBP , BE RICH</span>
							<p>
								เบอร์ต่อภายใน : 401<br />
                        		<%--โทรศัพท์มือถือ : -<br />--%>
                        		Email : <a href="mailto:sirinthramal@aromathailand.com">sirinthramal@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณศิรินทรา  มาลี (แนน)</h5>
						<p>เจ้าหน้าที่ แผนกรายได้ <br />และผลประโยชน์ตอบแทน-พนักงานสังกัด LION,UBP , BE RICH</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left --> <%--คุณแนน--%>
			


		</div> <!-- /.team-member-wrapper -->
	</div> <!-- /.conatiner -->
</section>
 
</asp:Content>
