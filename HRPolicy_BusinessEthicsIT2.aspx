﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_BusinessEthicsIT2.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_BusinessEthicsIT2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/hr.jpg" alt=""/></div>
<!-- end banner -->

<article class="blog-details-page">
	<div class="container">
		
		<div class="post-heading">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
                <li>/</li>
                <li><a href="HRPolicy_Announce.aspx">ระเบียบต่างๆ</a></li>
                <li>/</li>
                <li><a href="HRPolicy_BusinessEthicsIT.aspx">ประกาศ คำสั่ง ระเบียบเกี่ยวกับเทคโนโลยีสารสนเทศ</a></li>
            </ul>
			<h2>นโยบายการใช้งานระบบสารสนเทศของ Aroma Group</h2>
		</div> <!-- /.post-heading -->
		
		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix">
				<div class="blog-details-post-wrapper">
				
					<img src="new/images/banner/hr-policy/img22.jpg" alt=""/>
   					
   					<p style="padding: 30px 0 0;">
         		    	<ol>
         		    		<li>ผู้ใช้งานทุกคนจะมีปัญชีผู้ใช้งาน (Username) เป็นของตนเอง และ login ด้วยปัญชีผู้ใช้งาน (Username) ของตนเอง เพื่อแสดงตัวตน (authentication) 
							 เข้าสู่ เครื่องคอมพิวเตอร์ ที่ได้กำหนด Rules การใช้งานของแต่ละหน่วยงาน ที่ทางฝ่ายสารสนเทศ จัดเตรียมไว้ และผู้ใช้จะต้องไม่อนุญาตให้ผู้อื่นใช้งานผ่านบัญชีผู้ใช้ (Username) 
							 ของตน โดยเด็ดขาด หากเกิดปัญหาใดเจ้าของบัญชีผู้ใช้งานต้องเป็นผู้รับผิดชอบ</li>
							<li>อุปกรณ์ Computer ทุกเครื่อง ที่ใช้งานติดต่อกับระบบเครือข่ายของ Aroma group นั้นจะต้อง ดำเนินการ Join Domain และติดตั้ง Software Antivirus 
							 ที่ี่ทางบริษัทฯกำหนดเท่านั้น</li>
							<li>บริษัทฯไม่อนุญาตให้นำอุปกรณ์ไอทีส่วนตัว รวมทั้งมือถือของบริษัท และอุปกรณ์อื่นๆ ที่ไม่ได้รับอนุญาต เข้ามาเชื่อมต่อกับระบบเครือข่ายของ Aroma Group เช่น Wi-Fi หรือ 
							 Hotspot</li>
							<li>บริษัทฯ มีนโยบาย ปฏิบัติตามข้อกำหนดของ พ.ร.บ. ว่าด้วยการกระทำผิดทางคอมพิวเตอร์ ผู้ดูแลระบบจะทำการบันทึก และจัดเก็บข้อมูลการใช้งานของทุกท่าน ดังนั้น ผู้ใช้งานต้องไม่เข้าใช้
							 Website ที่ผิดกฎหมาย หรือ Website ที่ไม่ได้รับอนุญาต</li>
							<li>บริษัทฯ มีนโยบายการใช้งานซอฟท์แวร์ที่ถูกต้องตามกฎหมาย ดังนั้นผู้ใช้งานจะต้องไม่ละเมิดลิขสิทธิ์โปรแกรม หรือซอฟท์แวร์ใด หรือการติดตั้งใดๆ ที่ไม่ใช่โปรแกรมพื้นฐาน หรือที่ไม่มีลิขสิทธิ์ 
							 ลงใน Computer ของบริษัทฯ โดยเด็ดขาดผู้รับผิดชอบเครื่องคอมพิวเตอร์เครื่องนั้นๆ จะต้องเป็นผู้รับผิดชอบต่อการฟ้องร้องทางกฎหมายละเมิดลิขสิทธิ์ ซึ่งมีค่าปรับขั้นต่ำ 200,000 บาท</li>
         		    	</ol>
         		    	
         		    	<br>
         		    	ทั้งนี้ให้มีผลตั้งแต่ วันที่ 4 กันยายน พ.ศ. 2560 เป็นต้นไป<br><br>
         		    	
         		    	ประกาศ ณ วันที่ 4 กันยายน พ.ศ. 2560 
         		    </p>
					
					<div class="blog-details-bt">
						<div class="btn-group">
							<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
						</div>
					</div>
				
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-8 col-xs-12 p-fix">
				<aside>
					<div class="sidebar-news-list">
						<h4>CATEGORY</h4>
						<ul>
							<li>
								<a href="HRPolicy_BusinessEthicsHR.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบทรัพยากรมนุษย์</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAS.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบเกี่ยวกับทรัพย์สิน</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAC.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบเกี่ยวกับบัญชี การเงิน งบประมาณ จัดซื้อ</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsIT.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบเกี่ยวกับเทคโนโลยีสารสนเทศ</a>
							</li>
							<li>
								<a href="Benefits1_1.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบอื่นๆ</a>
							</li>
						</ul>
					</div>
				</aside>
				<aside>
					<div class="sidebar-news-list">
						<h4>Related Links</h4>
						<ul>
							<li>
								<a href="HRPolicy_BusinessEthicsIT1.aspx" class="tran3s">
									ระเบียบการใช้คอมพิวเตอร์ และระบบเครือข่าย
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsIT2.aspx" class="tran3s">
									นโยบายการใช้งานระบบสารสนเทศของ Aroma Group
								</a>
							</li>
						</ul>
					</div>
				</aside>
			</div>
		</div>
		
	</div>
</article>

</asp:Content>