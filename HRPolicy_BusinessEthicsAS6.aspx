﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_BusinessEthicsAS6.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_BusinessEthicsAS6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/hr.jpg" alt=""/></div>
<!-- end banner -->

<article class="blog-details-page">
	<div class="container">
		
		<div class="post-heading">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
                <li>/</li>
                <li><a href="HRPolicy_Announce.aspx">ระเบียบต่างๆ</a></li>
                <li>/</li>
                <li><a href="HRPolicy_BusinessEthicsAS.aspx">ประกาศ คำสั่ง ระเบียบเกี่ยวกับทรัพย์สิน</a></li>
            </ul>
			<h2>ระเบียบเงินประกันชุดฟอร์มพนักงาน</h2>
		</div> <!-- /.post-heading -->
		
		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix">
				<div class="blog-details-post-wrapper">
				
					<img src="new/images/banner/hr-policy/img18.jpg" alt=""/>
					
					<p style="padding: 30px 0 0;">
         		    	ตามที่บริษัทฯ ได้มีสวัสดิการแจกชุดฟอร์มให้กับพนักงานที่บริษัทกำหนดให้สวมใส่ชุดฟอร์มในการปฏิบัติหน้าที่ โดยจะได้รับแจกคนละ 3 ชุด/ปี ดังนั้นเพื่อให้มีความเป็นระเบียบเรียบร้อย ในการเบิกจ่ายและส่งมอบคืน ทางกลุ่มบริษัท อโรม่า จึงเห็นสมควรกำหนด และแจ้งมายังพนักงานทุกท่าน ในการปฏิบัติอย่างเคร่งครัด ว่าด้วยเรื่อง เงินประกันชุดฟอร์มพนักงาน ดังนี้
					</p>
					
					<ol style="margin: 10px 0;">
						<li>ประกาศฉบับนี้บังคับใช้เฉพาะพนักงานกลุ่มบริษัท อโรม่า ที่บริษัทกำหนดให้สวมใส่ชุดฟอร์มและได้รับแจกเป็นสวัสดิการชุดฟอร์มพนักงานเพื่อสวมใส่ในวันเวลาปฏิบัติหน้าที่เท่านั้น</li>
						<li>พนักงานที่ได้สิทธิแจกชุดฟอร์มจะได้ภายใน 3 วันนับจากวันเริ่มงานหรือตามความจำเป็นในการปฏิบัติหน้าที่ คนละไม่เกิน 3 ชุด/ปี แต่พนักงานจะต้องจ่ายเงินประกันค่าชุดฟอร์ม
							ตามจำนวนที่ได้รับในมูลค่าราคาทุนที่บริษัทสั่งซื้อมา</li>
						<li>การเบิกจ่ายชุดฟอร์มในปีถัดไป พนักงานจะต้องนำชุดฟอร์มที่ได้รับแจกมาส่งมอบคืนถึงจะได้รับสิทธิเบิกจ่ายชุดฟอร์มใหม่ได้ตามจำนวนที่ส่งมอบคืน</li>
						<li>การจ่ายเงินประกันค่าชุดฟอร์มพนักงาน โดยอนุโลมทางบริษัทฯ จะหักจากระบบบัญชีเงินเดือนเป็นจำนวน 3 งวดๆละเท่าๆกัน กรณีส่วนเกินจะถูกหักรวมเข้ากับงวดสุดท้าย 
							ซึ่งจะเริ่มหักในงวดเดือนที่เบิกจ่ายเป็นต้นไปจนกว่าจะหักครบตามจำนวนและมูลค่าตามข้อที่ 2.</li>
						<li>การรับคืนเงินประกันค่าชุดฟอร์มพนักงาน จะได้รับคืนภายในวันที่สิ้นสุดการเป็นพนักงานของบริษัทและผ่านการอนุมัติการลาออกจากกรรมการบริหารแล้ว 
							และจะต้องส่งมอบชุดฟอร์มพนักงานคืนให้กับฝ่ายทรัพยากรมนุษย์ครบถ้วนตรงตามจำนวนที่ได้รับไป โดยให้ติดต่อขอรับเงินประกันชุดฟอร์มพนักงานคืนจากแผนกบัญชีและแผนกการเงิน 
							ผ่านฝ่ายทรัพยากรมนุษย์</li>
						<li>บริษัทฯ ขอสงวนไว้ซึ่งสิทธิที่จะยกเลิก แก้ไข เปลี่ยนแปลงหลักเกณฑ์เงื่อนไขข้างต้นได้ตามเห็นสมควร โดยจะแจ้งให้ทราบเป็นคราวๆ ไป</li>
					</ol>
					
					<p><br>
						ทั้งนี้ ให้มีผลบังคับใช้สำหรับพนักงานที่ได้รับแจกชุดฟอร์มพนักงานประจำปี 2551 เป็นต้นไป<br><br>

						จึงประกาศมาเพื่อทราบโดยทั่วกัน และปฏิบัติอย่างเคร่งครัด<br><br>

						ประกาศ ณ วันที่ 5 กุมภาพันธ์ 2551
					</p>
					
					<div class="blog-details-bt">
						<div class="btn-group">
							<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
						</div>
					</div>
				
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-8 col-xs-12 p-fix">
				<aside>
					<div class="sidebar-news-list">
						<h4>CATEGORY</h4>
						<ul>
							<li>
								<a href="HRPolicy_BusinessEthicsHR.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบทรัพยากรมนุษย์</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAS.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบเกี่ยวกับทรัพย์สิน</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAC.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบเกี่ยวกับบัญชี การเงิน งบประมาณ จัดซื้อ</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsIT.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบเกี่ยวกับเทคโนโลยีสารสนเทศ</a>
							</li>
							<li>
								<a href="Benefits1_1.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบอื่นๆ</a>
							</li>
						</ul>
					</div>
				</aside>
				<aside>
					<div class="sidebar-news-list">
						<h4>Related Links</h4>
						<ul>
							<li>
								<a href="HRPolicy_BusinessEthicsAS1.aspx" class="tran3s">
									ระเบียบการใช้บัตรน้ำมัน Synergy Card Fleet Card บัตรน้ำมัน
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAS2.aspx" class="tran3s">
									ระเบียบเรื่องการเบิก และใช้ทรัพย์สินบริษัท
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAS3.aspx" class="tran3s">
									ระเบียบการใช้โทรศัพท์เคลื่อนที่ (APPLE IPHONE 7128)
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAS4.aspx" class="tran3s">
									ระเบียบการใช้โทรศัพท์เคลื่อนที่ (Samsung Galaxy A9 Pro)
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAS5.aspx" class="tran3s">
									ระเบียบการใช้รถที่บริษัทเช่าให้พนักงานใช้ปฏิบัติงาน
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAS6.aspx" class="tran3s">
									ระเบียบเงินประกันชุดฟอร์มพนักงาน
								</a>
							</li>
						</ul>
					</div>
				</aside>
			</div>
		</div>
		
	</div>
</article>

</asp:Content>