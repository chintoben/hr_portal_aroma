﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_BusinessEthicsAS1.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_BusinessEthicsAS1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/hr.jpg" alt=""/></div>
<!-- end banner -->

<section id="about-us" class="blog-category">
<div class="container">
	<div class="theme-title tt-edit category-title">
        <ul>
            <li><a href="index_hr.aspx">HOME</a></li>
            <li>/</li>
            <li><a href="HRPolicy_Announce.aspx">ระเบียบต่างๆ</a></li>
        </ul>
		<h2>ประกาศ คำสั่ง ระเบียบเกี่ยวกับทรัพย์สิน</h2>
	</div> <!-- /.theme-title -->

	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-013-gas-station"></i>
				</div>
				<p><a href="HRPolicy_BusinessEthicsAS1.aspx" class="tran3s">ระเบียบการใช้บัตรน้ำมัน Synergy Card Fleet Card บัตรน้ำมัน</a></p>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-015-money-1"></i>
				</div>
				<p><a href="HRPolicy_BusinessEthicsAS2.aspx" class="tran3s">ระเบียบเรื่องการเบิก และใช้ทรัพย์สินบริษัท</a></p>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-012-cell-phone"></i>
				</div>
				<p><a href="HRPolicy_BusinessEthicsAS3.aspx" class="tran3s">ระเบียบการใช้โทรศัพท์เคลื่อนที่<br>(APPLE IPHONE 7128)</a></p>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-011-samsung-galaxy"></i>
				</div>
				<p><a href="HRPolicy_BusinessEthicsAS4.aspx" class="tran3s">ระเบียบการใช้โทรศัพท์เคลื่อนที่<br>(Samsung Galaxy A9 Pro)</a></p>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-014-transport-2"></i>
				</div>
				<p><a href="HRPolicy_BusinessEthicsAS5.aspx" class="tran3s">ระเบียบการใช้รถที่บริษัทเช่า<br>ให้พนักงานใช้ปฏิบัติงาน</a></p>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-010-shirt"></i>
				</div>
				<p><a href="HRPolicy_BusinessEthicsAS6.aspx" class="tran3s">ระเบียบเงินประกันชุดฟอร์มพนักงาน</a></p>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
	</div> <!-- /.row -->
	<div class="blog-category-bt">
		<div class="btn-group">
			<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
		</div>
	</div>
</div> <!-- /.container -->
</section>

</asp:Content>
