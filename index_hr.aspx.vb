﻿Imports System.Data.SqlClient
Imports System.Data.OleDb

Public Class index_hr
    Inherits System.Web.UI.Page
    Dim dt As New DataTable()
    ' Dim strConnString As [String] = System.Configuration.ConfigurationManager.ConnectionStrings("HRConnectionString").ConnectionString()
    Dim db_Form As New Connect_HR
    Dim DB_HR As New Connect_HR
    Dim sql As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        '' Insert by Ben 
        'If Session("empcode") Is Nothing Then
        '    ScriptManager.RegisterStartupScript(Me, Page.GetType, "call", "window.location.href = 'Login.aspx';", True)
        '    Exit Sub
        'End If
        '' Insert by Ben 


        Dim domainAndUserName As String _
        = Environment.UserDomainName & "\\" & Environment.UserName

        If Not IsPostBack Then

            insertCount()
            sql = " select count(id) as c from [TB_CountUser] "
            'sql += " where TimeA_Original "
            dt = DB_HR.GetDataTable(sql)
            If dt.Rows.Count >= 0 Then
                lblCount.Text = "จำนวนการเข้าใช้งานทั้งหมด " & dt.Rows(0)("c") & " ครั้ง"
            End If


            loadImage()
            'loadNews()
            'loadActivity()

        End If
    End Sub

    Sub insertCount()
        Dim Cn As New SqlConnection(DB_HR.sqlCon)
        Dim sqlcon As New OleDbConnection(DB_HR.cnPAStr)
        Dim sqlcom As New OleDbCommand
        Dim sSql As String = "INSERT INTO [TB_CountUser] Values(  "
        sSql += "'" & Environment.UserDomainName & "'"
        sSql += ",'" & Environment.UserName & "'"
        sSql += ", getdate() )"

        If sqlcon.State = ConnectionState.Closed Then
            sqlcon = New OleDbConnection(DB_HR.cnPAStr)
            sqlcon.Open()
        End If
        With sqlcom
            .Connection = sqlcon
            .CommandType = CommandType.Text
            .CommandText = sSql
            .ExecuteNonQuery()
        End With
    End Sub

    Sub loadImage()
        Dim sqlconnection1 As New SqlConnection(db_Form.sqlCon)
        Dim SqlCmd2 As New SqlCommand
        Dim SqlDataReader2 As SqlDataReader
        Dim sql As String = ""
        Dim ChangeDateSQL2 As String = ""
        Dim i As Integer
        Dim sql2 As String = ""

        sql = "  select top 4 IndexID, image from TB_IndexHome where type = 'Banner' and status='Active'  order by IndexID  desc "

        sqlconnection1 = New SqlConnection(db_Form.sqlCon)


        sqlconnection1.Open()

        SqlCmd2.Connection = sqlconnection1
        SqlCmd2.CommandTimeout = 0
        SqlCmd2.CommandText = sql
        SqlDataReader2 = SqlCmd2.ExecuteReader

        While SqlDataReader2.Read()
            Try
                If i = 0 Then
                    Image1.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    'HyperLink1.NavigateUrl = "https://www.dotnetheaven.com/article/hyperlink-control-in-asp.net-using-vb.net"

                    'Label1.Text = SqlDataReader2.GetValue(1)
                ElseIf i = 1 Then
                    Image2.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    'Label2.Text = SqlDataReader2.GetValue(1)
                ElseIf i = 2 Then
                    Image3.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    'Label3.Text = SqlDataReader2.GetValue(1)
                ElseIf i = 3 Then
                    Image4.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    'Label4.Text = SqlDataReader2.GetValue(1)
                    'ElseIf i = 4 Then
                    '    Image5.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    '    'Label5.Text = SqlDataReader2.GetValue(1)

                    'ElseIf i = 4 Then
                    '    Image5.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))


                End If

                i = i + 1
            Finally
            End Try
        End While
        SqlDataReader2.Close()
    End Sub


    Function ReadImage(strID As String)
        Return ("ReadimageBanner.aspx?ID=" & strID)

    End Function


    Sub loadNews()
        Dim sqlconnection1 As New SqlConnection(db_Form.sqlCon)
        Dim SqlCmd2 As New SqlCommand
        Dim SqlDataReader2 As SqlDataReader
        Dim sql As String = ""
        Dim ChangeDateSQL2 As String = ""
        Dim i As Integer
        Dim sql2 As String = ""

        sql = "  select top 5 IndexID, txtSubject from TB_IndexHome where type = 'News' order by IndexID  desc "

        sqlconnection1 = New SqlConnection(db_Form.sqlCon)


        sqlconnection1.Open()

        SqlCmd2.Connection = sqlconnection1
        SqlCmd2.CommandTimeout = 0
        SqlCmd2.CommandText = sql
        SqlDataReader2 = SqlCmd2.ExecuteReader

        While SqlDataReader2.Read()
            Try
                'If i = 0 Then
                '    'Image1.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                '    txtNews1.Text = SqlDataReader2.GetValue(1)
                '    txtNewsID1.Text = SqlDataReader2.GetValue(0)
                'ElseIf i = 1 Then
                '    'Image2.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                '    txtNews2.Text = SqlDataReader2.GetValue(1)
                '    txtNewsID2.Text = SqlDataReader2.GetValue(0)
                'ElseIf i = 2 Then
                '    'Image3.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                '    txtNews3.Text = SqlDataReader2.GetValue(1)
                '    txtNewsID3.Text = SqlDataReader2.GetValue(0)
                'ElseIf i = 3 Then
                '    'Image4.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                '    txtNews4.Text = SqlDataReader2.GetValue(1)
                '    txtNewsID4.Text = SqlDataReader2.GetValue(0)
                'ElseIf i = 4 Then
                '    'Image4.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                '    txtNews5.Text = SqlDataReader2.GetValue(1)
                '    txtNewsID5.Text = SqlDataReader2.GetValue(0)

                'End If

                i = i + 1
            Finally
            End Try
        End While
        SqlDataReader2.Close()
    End Sub

    Sub loadActivity()
        Dim sqlconnection1 As New SqlConnection(db_Form.sqlCon)
        Dim SqlCmd2 As New SqlCommand
        Dim SqlDataReader2 As SqlDataReader
        Dim sql As String = ""
        Dim ChangeDateSQL2 As String = ""
        Dim i As Integer
        Dim sql2 As String = ""

        sql = "  select top 4 IndexID, txtSubject, isnull(image,'') as image from TB_IndexHome where type = 'Activity'  order by IndexID  desc "

        sqlconnection1 = New SqlConnection(db_Form.sqlCon)


        sqlconnection1.Open()

        SqlCmd2.Connection = sqlconnection1
        SqlCmd2.CommandTimeout = 0
        SqlCmd2.CommandText = sql
        SqlDataReader2 = SqlCmd2.ExecuteReader

        While SqlDataReader2.Read()
            Try
                'If i = 0 Then
                '    ImgAct1.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                '    txtAct1.Text = SqlDataReader2.GetValue(1)
                '    txtActID1.Text = SqlDataReader2.GetValue(0)
                'ElseIf i = 1 Then
                '    ImgAct2.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                '    txtAct2.Text = SqlDataReader2.GetValue(1)
                '    txtActID2.Text = SqlDataReader2.GetValue(0)
                'ElseIf i = 2 Then
                '    ImgAct3.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                '    txtAct3.Text = SqlDataReader2.GetValue(1)
                '    txtActID3.Text = SqlDataReader2.GetValue(0)

                'End If

                i = i + 1
            Finally
            End Try
        End While
        SqlDataReader2.Close()
    End Sub


End Class