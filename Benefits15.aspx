﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="Benefits15.aspx.vb" Inherits="Aroma_HRPortal.Benefits15_1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/benefit/banner1.jpg" /></div>
<!-- end banner -->

<section id="about-us" class="blog-category">
<div class="container">
	<div class="theme-title tt-edit category-title">
        <ul>
            <li><a href="index_hr.aspx">HOME</a></li>
            <li>/</li>
            <li><a href="Benefits_Employee.aspx">ระเบียบสวัสดิการ</a></li>
        </ul>
		<h2>เจ็บป่วย ประกันชีวิต สุขภาพ อุบัติเหตุ</h2>
	</div> <!-- /.theme-title -->

	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-6 div-empty"></div>
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-123-first-aid-kit"></i>
				</div>
				<p><a href="Benefits15_1.aspx" class="tran3s">รายการสวัสดิการค่ารักษาพยาบาล-ประกันกลุ่ม (ฉบับที่ 1 พ.ศ. 2556) Rev</a></p>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-105-doctor"></i>
				</div>
				<p><a href="Document/4Benefits/ตารางคุ้มครองและสิทธิประโยชน์ประกันกลุ่มปี 60-61.pdf" target="_blank" class="tran3s">ตารางคุ้มครองและสิทธิประโยชน์ประกันกลุ่มปี 60-61</a></p>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6 div-empty"></div>
	</div> <!-- /.row -->
	<div class="blog-category-bt">
		<div class="btn-group">
			<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
		</div>
	</div>
</div> <!-- /.container -->
</section>

</asp:Content>
