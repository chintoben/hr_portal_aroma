﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="index_Birthday.aspx.vb" Inherits="Aroma_HRPortal.index_Birthday" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="dot net templates  | aspxtemplates.com">
    <meta name="author" content="aspxtemplates.com">
    <!-- Bootstrap Core CSS -->
    <title>Aroma Group</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom New CSS -->
    <link href="new/css/style-birthday.css" rel="stylesheet" type="text/css" />
    <link href="new/css/hover.css" rel="stylesheet" type="text/css" />
    <link href="new/css/fontawesome-all.css" rel="stylesheet" type="text/css" />
    <link href="new/css/swiper.css" rel="stylesheet" type="text/css" />
    <link href="new/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- Custom New JS -->
    <!--script src="new/js/theme.js"></script-->
    
    <style>
		.swiper-container {
			width: 100%;
			height: 100%;
		}
		.swiper-slide {
			text-align: center;
			background: #fff;
		}
	</style>

</head>

<body>

<form id="form1" runat="server" style="width: 350px; height:350px;  "> 
<div class="swiper-container">
    <div class="swiper-wrapper">
  
  	<div class="swiper-slide">
	<div ID="div1" runat="server">
		<div class="single-team-member">
			<div class="img">
				<asp:Image ID="Image1" runat="server" />
				<div class="opacity tran4s">
					<h3><%=Label1.Text%></h3>
					<%--<span>Web Developer</span>
					<p>On the other hand, We denounce ut with righteo indignation and dislike men who are so beguiled and demoralized.</p>--%>
				</div>
			</div> <!-- /.img -->
			<div class="member-name">
				<h5><%=Label1.Text%></h5>
				<%--<p>Web Developer</p>--%>
					<ul>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
					</ul>
			</div> <!-- /.member-name -->
		</div> <!-- /.single-team-member -->
	</div>
	</div>
	
	<div class="swiper-slide">
	<div ID="div2" runat="server">
		<div class="single-team-member">
			<div class="img">
				<asp:Image ID="Image2" runat="server" />
				<div class="opacity tran4s">
					<h3><%=Label2.Text%></h3>
					<%--<span>Web Developer</span>
					<p>On the other hand, We denounce ut with righteo indignation and dislike men who are so beguiled and demoralized.</p>--%>
				</div>
			</div> <!-- /.img -->
			<div class="member-name">
				<h5><%=Label2.Text%></h5>
				<%--<p>Web Developer</p>--%>
					<ul>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
					</ul>
			</div> <!-- /.member-name -->
		</div> <!-- /.single-team-member -->
	</div>
	</div>
	
	<div class="swiper-slide">
	<div ID="div3" runat="server">
		<div class="single-team-member">
			<div class="img">
				<asp:Image ID="Image3" runat="server" />
				<div class="opacity tran4s">
					<h3><%=Label13.Text%></h3>
					<%--<span>Web Developer</span>
					<p>On the other hand, We denounce ut with righteo indignation and dislike men who are so beguiled and demoralized.</p>--%>
				</div>
			</div> <!-- /.img -->
			<div class="member-name">
				<h5><%=Label3.Text%></h5>
				<%--<p>Web Developer</p>--%>
					<ul>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
					</ul>
			</div> <!-- /.member-name -->
		</div> <!-- /.single-team-member -->
	</div>
	</div>
	
	<div class="swiper-slide">
	<div ID="div4" runat="server">
		<div class="single-team-member">
			<div class="img">
				<asp:Image ID="Image4" runat="server" />
				<div class="opacity tran4s">
					<h3><%=Label4.Text%></h3>
					<%--<span>Web Developer</span>
					<p>On the other hand, We denounce ut with righteo indignation and dislike men who are so beguiled and demoralized.</p>--%>
				</div>
			</div> <!-- /.img -->
			<div class="member-name">
				<h5><%=Label4.Text%></h5>
				<%--<p>Web Developer</p>--%>
					<ul>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
					</ul>
			</div> <!-- /.member-name -->
		</div> <!-- /.single-team-member -->
	</div>
	</div>
	
	<div class="swiper-slide">
	<div ID="div5" runat="server">
		<div class="single-team-member">
			<div class="img">
				<asp:Image ID="Image5" runat="server" />
				<div class="opacity tran4s">
					<h3><%=Label5.Text%></h3>
					<%--<span>Web Developer</span>
					<p>On the other hand, We denounce ut with righteo indignation and dislike men who are so beguiled and demoralized.</p>--%>
				</div>
			</div> <!-- /.img -->
			<div class="member-name">
				<h5><%=Label5.Text%></h5>
				<%--<p>Web Developer</p>--%>
					<ul>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
					</ul>
			</div> <!-- /.member-name -->
		</div> <!-- /.single-team-member -->
	</div>
	</div>
	
	<div class="swiper-slide">
	<div ID="div6" runat="server">
		<div class="single-team-member">
			<div class="img">
				<asp:Image ID="Image6" runat="server" />
				<div class="opacity tran4s">
					<h3><%=Label6.Text%></h3>
					<%--<span>Web Developer</span>
					<p>On the other hand, We denounce ut with righteo indignation and dislike men who are so beguiled and demoralized.</p>--%>
				</div>
			</div> <!-- /.img -->
			<div class="member-name">
				<h5><%=Label6.Text%></h5>
				<%--<p>Web Developer</p>--%>
					<ul>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
					</ul>
			</div> <!-- /.member-name -->
		</div> <!-- /.single-team-member -->
	</div>
	</div>
	
	<div class="swiper-slide">
	<div ID="div7" runat="server">
		<div class="single-team-member">
			<div class="img">
				<asp:Image ID="Image7" runat="server" />
				<div class="opacity tran4s">
					<h3><%=Label7.Text%></h3>
					<%--<span>Web Developer</span>
					<p>On the other hand, We denounce ut with righteo indignation and dislike men who are so beguiled and demoralized.</p>--%>
				</div>
			</div> <!-- /.img -->
			<div class="member-name">
				<h5><%=Label7.Text%></h5>
				<%--<p>Web Developer</p>--%>
					<ul>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
					</ul>
			</div> <!-- /.member-name -->
		</div> <!-- /.single-team-member -->
	</div>
	</div>
	
	<div class="swiper-slide">
	<div ID="div8" runat="server">
		<div class="single-team-member">
			<div class="img">
				<asp:Image ID="Image8" runat="server" />
				<div class="opacity tran4s">
					<h3><%=Label8.Text%></h3>
					<%--<span>Web Developer</span>
					<p>On the other hand, We denounce ut with righteo indignation and dislike men who are so beguiled and demoralized.</p>--%>
				</div>
			</div> <!-- /.img -->
			<div class="member-name">
				<h5><%=Label8.Text%></h5>
				<%--<p>Web Developer</p>--%>
					<ul>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
					</ul>
			</div> <!-- /.member-name -->
		</div> <!-- /.single-team-member -->
	</div>
	</div>
	
	<div class="swiper-slide">
	<div ID="div9" runat="server">
		<div class="single-team-member">
			<div class="img">
				<asp:Image ID="Image9" runat="server" />
				<div class="opacity tran4s">
					<h3><%=Label9.Text%></h3>
					<%--<span>Web Developer</span>
					<p>On the other hand, We denounce ut with righteo indignation and dislike men who are so beguiled and demoralized.</p>--%>
				</div>
			</div> <!-- /.img -->
			<div class="member-name">
				<h5><%=Label9.Text%></h5>
				<%--<p>Web Developer</p>--%>
					<ul>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
					</ul>
			</div> <!-- /.member-name -->
		</div> <!-- /.single-team-member -->
	</div>
	</div>
	
	<div class="swiper-slide">
	<div ID="div10" runat="server">
		<div class="single-team-member">
			<div class="img">
				<asp:Image ID="Image10" runat="server" />
				<div class="opacity tran4s">
					<h3><%=Label10.Text%></h3>
					<%--<span>Web Developer</span>
					<p>On the other hand, We denounce ut with righteo indignation and dislike men who are so beguiled and demoralized.</p>--%>
				</div>
			</div> <!-- /.img -->
			<div class="member-name">
				<h5><%=Label10.Text%></h5>
				<%--<p>Web Developer</p>--%>
					<ul>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
					</ul>
			</div> <!-- /.member-name -->
		</div> <!-- /.single-team-member -->
	</div>
	</div>
	
	<div class="swiper-slide">
	<div ID="div11" runat="server">
		<div class="single-team-member">
			<div class="img">
				<asp:Image ID="Image11" runat="server" />
				<div class="opacity tran4s">
					<h3><%=Label11.Text%></h3>
				<%--	<span>Web Developer</span>
					<p>On the other hand, We denounce ut with righteo indignation and dislike men who are so beguiled and demoralized.</p>--%>
				</div>
			</div> <!-- /.img -->
			<div class="member-name">
				<h5><%=Label11.Text%></h5>
				<%--<p>Web Developer</p>--%>
					<ul>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
					</ul>
			</div> <!-- /.member-name -->
		</div> <!-- /.single-team-member -->
	</div>
	</div>
	
    <div ID="div12" runat="server">
	<div class="swiper-slide">
		<div class="single-team-member">
			<div class="img">
				<asp:Image ID="Image12" runat="server" />
				<div class="opacity tran4s">
					<h3><%=Label12.Text%></h3>
				<%--	<span>Web Developer</span>
					<p>On the other hand, We denounce ut with righteo indignation and dislike men who are so beguiled and demoralized.</p>--%>
				</div>
			</div> <!-- /.img -->
			<div class="member-name">
				<h5><%=Label12.Text%></h5>
				<%--<p>Web Developer</p>--%>
					<ul>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
					</ul>
			</div> <!-- /.member-name -->
		</div> <!-- /.single-team-member -->
	</div>
	</div>
	
    	<div ID="div13" runat="server">
	<div class="swiper-slide">
		<div class="single-team-member">
			<div class="img">
				<asp:Image ID="Image13" runat="server" />
				<div class="opacity tran4s">
					<h3><%=Label13.Text%></h3>
			<%--		<span>Web Developer</span>
					<p>On the other hand, We denounce ut with righteo indignation and dislike men who are so beguiled and demoralized.</p>--%>
				</div>
			</div> <!-- /.img -->
			<div class="member-name">
				<h5><%=Label13.Text%></h5>
				<%--<p>Web Developer</p>--%>
					<ul>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
					</ul>
			</div> <!-- /.member-name -->
		</div> <!-- /.single-team-member -->
	</div>
	</div>
	
   <div ID="div14" runat="server">
	<div class="swiper-slide">
		<div class="single-team-member">
			<div class="img">
				<asp:Image ID="Image14" runat="server" />
				<div class="opacity tran4s">
					<h3><%=Label14.Text%></h3>
					<%--<span>Web Developer</span>
					<p>On the other hand, We denounce ut with righteo indignation and dislike men who are so beguiled and demoralized.</p>--%>
				</div>
			</div> <!-- /.img -->
			<div class="member-name">
				<h5><%=Label14.Text%></h5>
				<%--<p>Web Developer</p>--%>
					<ul>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
					</ul>
			</div> <!-- /.member-name -->
		</div> <!-- /.single-team-member -->
	</div>
	</div>
	
	<div ID="div15" runat="server">
	<div class="swiper-slide">
		<div class="single-team-member">
			<div class="img">
				<asp:Image ID="Image15" runat="server" />
				<div class="opacity tran4s">
					<h3><%=Label15.Text%></h3>
					<%--<span>Web Developer</span>
					<p>On the other hand, We denounce ut with righteo indignation and dislike men who are so beguiled and demoralized.</p>--%>
				</div>
			</div> <!-- /.img -->
			<div class="member-name">
				<h5><%=Label15.Text%></h5>
				<%--<p>Web Developer</p>--%>
					<ul>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
					</ul>
			</div> <!-- /.member-name -->
		</div> <!-- /.single-team-member -->
	</div>
	</div>

	<div ID="div16" runat="server">	
	<div class="swiper-slide">
		<div class="single-team-member">
			<div class="img">
				<asp:Image ID="Image16" runat="server" />
				<div class="opacity tran4s">
					<h3><%=Label16.Text%></h3>
					<%--<span>Web Developer</span>
					<p>On the other hand, We denounce ut with righteo indignation and dislike men who are so beguiled and demoralized.</p>--%>
				</div>
			</div> <!-- /.img -->
			<div class="member-name">
				<h5><%=Label16.Text%></h5>
				<%--<p>Web Developer</p>--%>
					<ul>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
					</ul>
			</div> <!-- /.member-name -->
		</div> <!-- /.single-team-member -->
	</div>
	</div>

	<div ID="div17" runat="server">
	<div class="swiper-slide">
		<div class="single-team-member">
			<div class="img">
				<asp:Image ID="Image17" runat="server" />
				<div class="opacity tran4s">
					<h3><%=Label17.Text%></h3>
				<%--	<span>Web Developer</span>
					<p>On the other hand, We denounce ut with righteo indignation and dislike men who are so beguiled and demoralized.</p>--%>
				</div>
			</div> <!-- /.img -->
			<div class="member-name">
				<h5><%=Label17.Text%></h5>
				<%--<p>Web Developer</p>--%>
					<ul>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
					</ul>
			</div> <!-- /.member-name -->
		</div> <!-- /.single-team-member -->
	</div>
	</div>
	
    	<div ID="div18" runat="server">
	<div class="swiper-slide">
		<div class="single-team-member">
			<div class="img">
				<asp:Image ID="Image18" runat="server" />
				<div class="opacity tran4s">
					<h3><%=Label18.Text%></h3>
				<%--	<span>Web Developer</span>
					<p>On the other hand, We denounce ut with righteo indignation and dislike men who are so beguiled and demoralized.</p>--%>
				</div>
			</div> <!-- /.img -->
			<div class="member-name">
				<h5><%=Label18.Text%></h5>
				<%--<p>Web Developer</p>--%>
					<ul>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
					</ul>
			</div> <!-- /.member-name -->
		</div> <!-- /.single-team-member -->
	</div>
	</div>


	<div ID="div19" runat="server">	
	<div class="swiper-slide">
		<div class="single-team-member">
			<div class="img">
				<asp:Image ID="Image19" runat="server" />
				<div class="opacity tran4s">
					<h3><%=Label19.Text%></h3>
					<%--<span>Web Developer</span>
					<p>On the other hand, We denounce ut with righteo indignation and dislike men who are so beguiled and demoralized.</p>--%>
				</div>
			</div> <!-- /.img -->
			<div class="member-name">
				<h5><%=Label19.Text%></h5>
			<%--	<p>Web Developer</p>--%>
					<ul>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
					</ul>
			</div> <!-- /.member-name -->
		</div> <!-- /.single-team-member -->
	</div>
	</div>

	<div ID="div20" runat="server">	
	<div class="swiper-slide">
		<div class="single-team-member">
			<div class="img">
				<asp:Image ID="Image20" runat="server" />
				<div class="opacity tran4s">
					<h3><%=Label20.Text%></h3>
				<%--	<span>Web Developer</span>
					<p>On the other hand, We denounce ut with righteo indignation and dislike men who are so beguiled and demoralized.</p>--%>
				</div>
			</div> <!-- /.img -->
			<div class="member-name">
				<h5><%=Label20.Text%></h5>
				<%--<p>Web Developer</p>--%>
					<ul>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
					</ul>
			</div> <!-- /.member-name -->
		</div> <!-- /.single-team-member -->
	</div>
	</div>
	
	<div class="swiper-slide">
	<div ID="div21" runat="server">
		<div class="single-team-member">
			<div class="img">
				<asp:Image ID="Image21" runat="server" />
				<div class="opacity tran4s">
					<h3><%=Label21.Text%></h3>
				<%--	<span>Web Developer</span>
					<p>On the other hand, We denounce ut with righteo indignation and dislike men who are so beguiled and demoralized.</p>--%>
				</div>
			</div> <!-- /.img -->
			<div class="member-name">
				<h5><%=Label21.Text%></h5>
				<%--<p>Web Developer</p>--%>
					<ul>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
					</ul>
			</div> <!-- /.member-name -->
		</div> <!-- /.single-team-member -->
	</div>
	</div>
	
	<div class="swiper-slide">
	<div ID="div22" runat="server">
		<div class="single-team-member">
			<div class="img">
				<asp:Image ID="Image22" runat="server" />
				<div class="opacity tran4s">
					<h3><%=Label22.Text%></h3>
				<%--	<span>Web Developer</span>
					<p>On the other hand, We denounce ut with righteo indignation and dislike men who are so beguiled and demoralized.</p>--%>
				</div>
			</div> <!-- /.img -->
			<div class="member-name">
				<h5><%=Label22.Text%></h5>
				<%--<p>Web Developer</p>--%>
					<ul>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
					</ul>
			</div> <!-- /.member-name -->
		</div> <!-- /.single-team-member -->
	</div>
	</div>
	
	<div class="swiper-slide">
	<div ID="div23" runat="server">
		<div class="single-team-member">
			<div class="img">
				<asp:Image ID="Image23" runat="server" />
				<div class="opacity tran4s">
					<h3><%=Label23.Text%></h3>
				<%--	<span>Web Developer</span>
					<p>On the other hand, We denounce ut with righteo indignation and dislike men who are so beguiled and demoralized.</p>--%>
				</div>
			</div> <!-- /.img -->
			<div class="member-name">
				<h5><%=Label23.Text%></h5>
				<%--<p>Web Developer</p>--%>
					<ul>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
					</ul>
			</div> <!-- /.member-name -->
		</div> <!-- /.single-team-member -->
	</div>
	</div>
	
	<div class="swiper-slide">
	<div ID="div24" runat="server">
		<div class="single-team-member">
			<div class="img">
				<asp:Image ID="Image24" runat="server" />
				<div class="opacity tran4s">
					<h3><%=Label24.Text%></h3>
					<%--<span>Web Developer</span>
					<p>On the other hand, We denounce ut with righteo indignation and dislike men who are so beguiled and demoralized.</p>--%>
				</div>
			</div> <!-- /.img -->
			<div class="member-name">
				<h5><%=Label24.Text%></h5>
				<%--<p>Web Developer</p>--%>
					<ul>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
					</ul>
			</div> <!-- /.member-name -->
		</div> <!-- /.single-team-member -->
	</div>
	</div>
	
	<div class="swiper-slide">
	<div ID="div25" runat="server">
		<div class="single-team-member">
			<div class="img">
				<asp:Image ID="Image25" runat="server" />
				<div class="opacity tran4s">
					<h3><%=Label25.Text%></h3>
				<%--	<span>Web Developer</span>
					<p>On the other hand, We denounce ut with righteo indignation and dislike men who are so beguiled and demoralized.</p>--%>
				</div>
			</div> <!-- /.img -->
			<div class="member-name">
				<h5><%=Label25.Text%></h5>
				<%--<p>Web Developer</p>--%>
					<ul>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
						<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
					</ul>
			</div> <!-- /.member-name -->
		</div> <!-- /.single-team-member -->
	</div>
	</div>

	<asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
	<asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
	<asp:Label ID="Label3" runat="server" Text="Label"></asp:Label>
	<asp:Label ID="Label4" runat="server" Text="Label"></asp:Label>
	<asp:Label ID="Label5" runat="server" Text="Label"></asp:Label>
	<asp:Label ID="Label6" runat="server" Text="Label"></asp:Label>
	<asp:Label ID="Label7" runat="server" Text="Label"></asp:Label>
	<asp:Label ID="Label8" runat="server" Text="Label"></asp:Label>
	<asp:Label ID="Label9" runat="server" Text="Label"></asp:Label>
	<asp:Label ID="Label10" runat="server" Text="Label"></asp:Label>
	<asp:Label ID="Label11" runat="server" Text="Label"></asp:Label>
	<asp:Label ID="Label12" runat="server" Text="Label"></asp:Label>
	<asp:Label ID="Label13" runat="server" Text="Label"></asp:Label>
	<asp:Label ID="Label14" runat="server" Text="Label"></asp:Label>
	<asp:Label ID="Label15" runat="server" Text="Label"></asp:Label>
	<asp:Label ID="Label16" runat="server" Text="Label"></asp:Label>
	<asp:Label ID="Label17" runat="server" Text="Label"></asp:Label>
	<asp:Label ID="Label18" runat="server" Text="Label"></asp:Label>
	<asp:Label ID="Label19" runat="server" Text="Label"></asp:Label>
	<asp:Label ID="Label20" runat="server" Text="Label"></asp:Label>
	<asp:Label ID="Label21" runat="server" Text="Label"></asp:Label>
	<asp:Label ID="Label22" runat="server" Text="Label"></asp:Label>
	<asp:Label ID="Label23" runat="server" Text="Label"></asp:Label>
	<asp:Label ID="Label24" runat="server" Text="Label"></asp:Label>
	<asp:Label ID="Label25" runat="server" Text="Label"></asp:Label>

	</div>
</div>
</form>
     
<!-- Swiper JS -->
<script src="new/js/swiper.min.js"></script>

<!-- Initialize Swiper -->
<script>
    var swiper = new Swiper('.swiper-container', {
		slidesPerView: 1,
		loop: true,
		autoplay: {
        	delay: 3000,
        	disableOnInteraction: false,
      	},
    });
</script>

     <%--  <table width=90% style="border:2px dotted hotpink;" cellpadding="4" 
                            cellspacing="4"><tr><td style="border:2px dotted lightpink;"><font size="1">เขียนตรงนี้เลย</td></tr></table>
--%>
   
   <%--<form id="form1" runat="server" width="200px" Height="230px"  >      
     <div class="slideshow-container">
        
        <div ID="div1" runat="server"   class="mySlides fade" >
          <asp:Image ID="Image1" runat="server"  width="200px" Height="230px" />
          <div class="text"><%=Label1.Text%></div>
        </div>

        <div ID="div2" runat="server"   class="mySlides fade" >
          <asp:Image ID="Image2" runat="server"  width="200px" Height="230px" />
           <div class="text"><%=Label2.Text%></div>
        </div>

        <div ID="div3" runat="server"  class="mySlides fade" >
          <asp:Image ID="Image3" runat="server"  width="200px" Height="230px" />
          <div class="text"><%=Label3.Text%></div>
        </div>

         <div ID="div4" runat="server"  class="mySlides fade" >
          <asp:Image ID="Image4" runat="server"  width="200px" Height="230px" />
          <div class="text"><%=Label4.Text%></div>
        </div>

         <div ID="div5" runat="server"  class="mySlides fade" >
          <asp:Image ID="Image5" runat="server"  width="200px" Height="230px" />
          <div class="text"><%=Label5.Text%></div>
        </div>

         <div ID="div6" runat="server"  class="mySlides fade" >
          <asp:Image ID="Image6" runat="server"  width="200px" Height="230px" />
          <div class="text"><%=Label6.Text%></div>
        </div>

         <div ID="div7" runat="server"  class="mySlides fade" >
          <asp:Image ID="Image7" runat="server"  width="200px" Height="230px" />
          <div class="text"><%=Label7.Text%></div>
        </div>

         <div ID="div8" runat="server"  class="mySlides fade" >
          <asp:Image ID="Image8" runat="server"  width="200px" Height="230px" />
          <div class="text"><%=Label8.Text%></div>
        </div>

         <div ID="div9" runat="server"  class="mySlides fade" >
          <asp:Image ID="Image9" runat="server"  width="200px" Height="230px" />
          <div class="text"><%=Label9.Text%></div>
        </div>

         <div ID="div10" runat="server"  class="mySlides fade" >
          <asp:Image ID="Image10" runat="server"  width="200px" Height="230px" />
          <div class="text"><%=Label10.Text%></div>
        </div>

         <div ID="div11" runat="server"  class="mySlides fade" >
          <asp:Image ID="Image11" runat="server"  width="200px" Height="230px" />
          <div class="text"><%=Label11.Text%></div>
        </div>

         <div ID="div12" runat="server"  class="mySlides fade" >
          <asp:Image ID="Image12" runat="server"  width="200px" Height="230px" />
          <div class="text"><%=Label12.Text%></div>
        </div>

         <div ID="div13" runat="server"  class="mySlides fade" >
          <asp:Image ID="Image13" runat="server"  width="200px" Height="230px" />
          <div class="text"><%=Label13.Text%></div>
        </div>

         <div ID="div14" runat="server"  class="mySlides fade" >
          <asp:Image ID="Image14" runat="server"  width="200px" Height="230px" />
          <div class="text"><%=Label14.Text%></div>
        </div>

         <div ID="div15" runat="server"  class="mySlides fade" >
          <asp:Image ID="Image15" runat="server"  width="200px" Height="230px" />
          <div class="text"><%=Label15.Text%></div>
        </div>

         <div ID="div16" runat="server"  class="mySlides fade" >
          <asp:Image ID="Image16" runat="server"  width="200px" Height="230px" />
          <div class="text"><%=Label16.Text%></div>
        </div>

         <div ID="div17" runat="server"  class="mySlides fade" >
          <asp:Image ID="Image17" runat="server"  width="200px" Height="230px" />
          <div class="text"><%=Label17.Text%></div>
        </div>

         <div ID="div18" runat="server"  class="mySlides fade" >
          <asp:Image ID="Image18" runat="server"  width="200px" Height="230px" />
          <div class="text"><%=Label18.Text%></div>
        </div>

         <div ID="div19" runat="server"  class="mySlides fade" >
          <asp:Image ID="Image19" runat="server"  width="200px" Height="230px" />
          <div class="text"><%=Label19.Text%></div>
        </div>

         <div ID="div20" runat="server"  class="mySlides fade" >
          <div class="numbertext">3 / 3</div>
          <asp:Image ID="Image20" runat="server"  width="200px" Height="230px" />
          <div class="text"><%=Label20.Text%></div>

        <div ID="div21" runat="server"  class="mySlides fade" >
          <asp:Image ID="Image21" runat="server"  width="200px" Height="230px" />
          <div class="text"><%=Label21.Text%></div>
        </div>
        </div>

         <div ID="div22" runat="server"  class="mySlides fade" >
          <asp:Image ID="Image22" runat="server"  width="200px" Height="230px" />
          <div class="text"><%=Label22.Text%></div>
        </div>

         <div ID="div23" runat="server"  class="mySlides fade" >
          <asp:Image ID="Image23" runat="server"  width="200px" Height="230px" />
          <div class="text"><%=Label23.Text%></div>
        </div>

         <div ID="div24" runat="server"  class="mySlides fade" >
          <asp:Image ID="Image24" runat="server"  width="200px" Height="230px" />
          <div class="text"><%=Label24.Text%></div>
        </div>

         <div ID="div25" runat="server"  class="mySlides fade" >
          <asp:Image ID="Image25" runat="server"  width="200px" Height="230px" />
          <div class="text"><%=Label25.Text%></div>
        </div>

    </div>

   
    <div style="text-align:center">
      <span class="dot"></span>
      <span class="dot"></span>
      <span class="dot"></span>
      <span class="dot"></span>
      <span class="dot"></span>
      <span class="dot"></span>
      <span class="dot"></span>
      <span class="dot"></span>
      <span class="dot"></span>
      <span class="dot"></span>
      <span class="dot"></span>
      <span class="dot"></span>
      <span class="dot"></span>
      <span class="dot"></span>
      <span class="dot"></span>
      <span class="dot"></span>
      <span class="dot"></span>
      <span class="dot"></span>
      <span class="dot"></span>
      <span class="dot"></span>
      <span class="dot"></span>
      <span class="dot"></span>
      <span class="dot"></span>
      <span class="dot"></span>
      <span class="dot"></span>
      <span class="dot"></span>
      <span class="dot"></span>
    </div>

    <asp:Label ID="Label1" runat="server" Text="Label" ForeColor="White"></asp:Label>
    <asp:Label ID="Label2" runat="server" Text="Label" ForeColor="White"></asp:Label>
    <asp:Label ID="Label3" runat="server" Text="Label" ForeColor="White"></asp:Label>
    <asp:Label ID="Label4" runat="server" Text="Label" ForeColor="White"></asp:Label>
    <asp:Label ID="Label5" runat="server" Text="Label" ForeColor="White"></asp:Label>
    <asp:Label ID="Label6" runat="server" Text="Label" ForeColor="White"></asp:Label>
    <asp:Label ID="Label7" runat="server" Text="Label" ForeColor="White"></asp:Label>
    <asp:Label ID="Label8" runat="server" Text="Label" ForeColor="White"></asp:Label>
    <asp:Label ID="Label9" runat="server" Text="Label" ForeColor="White"></asp:Label>
    <asp:Label ID="Label10" runat="server" Text="Label" ForeColor="White"></asp:Label>
    <asp:Label ID="Label11" runat="server" Text="Label" ForeColor="White"></asp:Label>
    <asp:Label ID="Label12" runat="server" Text="Label" ForeColor="White"></asp:Label>
    <asp:Label ID="Label13" runat="server" Text="Label" ForeColor="White"></asp:Label>
    <asp:Label ID="Label14" runat="server" Text="Label" ForeColor="White"></asp:Label>
    <asp:Label ID="Label15" runat="server" Text="Label" ForeColor="White"></asp:Label>
    <asp:Label ID="Label16" runat="server" Text="Label" ForeColor="White"></asp:Label>
    <asp:Label ID="Label17" runat="server" Text="Label" ForeColor="White"></asp:Label>
    <asp:Label ID="Label18" runat="server" Text="Label" ForeColor="White"></asp:Label>
    <asp:Label ID="Label19" runat="server" Text="Label" ForeColor="White"></asp:Label>
    <asp:Label ID="Label20" runat="server" Text="Label" ForeColor="White"></asp:Label>
    <asp:Label ID="Label21" runat="server" Text="Label" ForeColor="White"></asp:Label>
    <asp:Label ID="Label22" runat="server" Text="Label" ForeColor="White"></asp:Label>
    <asp:Label ID="Label23" runat="server" Text="Label" ForeColor="White"></asp:Label>
    <asp:Label ID="Label24" runat="server" Text="Label" ForeColor="White"></asp:Label>
    <asp:Label ID="Label25" runat="server" Text="Label" ForeColor="White"></asp:Label>


<script>
    var slideIndex = 0;
    showSlides();

    function showSlides() {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("dot");
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        slideIndex++;
        if (slideIndex > slides.length) { slideIndex = 1 }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex - 1].style.display = "block";
        dots[slideIndex - 1].className += " active";
        setTimeout(showSlides, 2000); // Change image every 2 seconds
    }


</script>


           


           </form>--%>

</body>
</html>
