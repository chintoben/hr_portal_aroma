﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_WhoisWhoAll.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_WhoisWhoAll" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/HR.jpg" alt=""/></div>
<!-- end banner -->

<section id="team-section">
	<div class="container">
		<div class="theme-title">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
              <%--  <li>/</li>
                <li><a href="HRPolicy_WhoisWho.aspx">ติดต่อฝ่าย-แผนกต่างๆ</a></li>--%>
            </ul>
			<h2>รายชื่อติดต่อ - Aroma Group</h2>
			<!--p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p-->
		</div> <!-- /.theme-title -->


        <div class="theme-title">
            
		<%--	<h2>รายชื่อติดต่อ - Aroma Group</h2>--%>

                <div class="form-group">
                  
                     <asp:Label ID="Label2" runat="server" Text="ฝ่าย  :" Width="143px" 
                            Font-Bold="True"></asp:Label>
                  &nbsp;<asp:DropDownList ID="drpDepartment" 
                       runat="server"


                                        Width="60%" AutoPostBack="True" Font-Size="X-Large" 
                         Font-Bold="True">
                                    </asp:DropDownList>                     
                </div>
		
		</div> <!-- /.theme-title -->


		<div class="clear-fix team-member-wrapper">

                                     <asp:GridView id="myGridView" runat="server" AutoGenerateColumns="False" DataKeyNames="Employee_ID"
                                                Width="100%" CellPadding="4" ForeColor="#333333" GridLines="None" 
                                                      CellSpacing="5">
	                                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
	                                    <Columns>
	                                    <asp:TemplateField HeaderText="รหัสพนักงาน">
		                                    <ItemTemplate>
			                                    <asp:Label id="lblEmployee_ID" runat="server"></asp:Label>
		                                    </ItemTemplate>
                                            <%-- <ControlStyle Width="5%" />--%>
	                                    </asp:TemplateField>

	                                    <asp:TemplateField HeaderText="ชื่อ - นามสกุล">
		                                    <ItemTemplate>
			                                    <asp:Label id="lblfullname" runat="server"></asp:Label>
		                                    </ItemTemplate>
                                             <%--<ControlStyle Width="40%" />--%>
	                                    </asp:TemplateField>

                                          <asp:TemplateField HeaderText="ชื่อเล่น">
		                                    <ItemTemplate>
			                                    <asp:Label id="lblEmployee_Nickname" runat="server"></asp:Label>
		                                    </ItemTemplate>
                                             <%--<ControlStyle Width="5%" />--%>
	                                    </asp:TemplateField>


                                           <asp:TemplateField HeaderText="ตำแหน่ง">
		                                    <ItemTemplate>
			                                    <asp:Label id="lblposition" runat="server"></asp:Label>
		                                    </ItemTemplate>
                                             <%--<ControlStyle Width="10%" />--%>
	                                    </asp:TemplateField>


                                     <%--      <asp:TemplateField HeaderText="ฝ่าย">
		                                    <ItemTemplate>
			                                    <asp:Label id="lbldepartments_name" runat="server"></asp:Label>
		                                    </ItemTemplate>
                                             <ControlStyle Width="20%" />
	                                    </asp:TemplateField>
--%>
                                           <asp:TemplateField HeaderText="ฝ่าย">
		                                    <ItemTemplate>
			                                    <asp:Label id="lbldepartment_name" runat="server"></asp:Label>
		                                    </ItemTemplate>
                                             <%--<ControlStyle Width="20%" />--%>
	                                    </asp:TemplateField>

                                          <asp:TemplateField HeaderText="อีเมลล์">
		                                    <ItemTemplate>
			                                    <asp:Label id="lblEmail" runat="server"></asp:Label>
		                                    </ItemTemplate>
	                                         <%-- <ControlStyle Width="100px" />--%>
	                                    </asp:TemplateField>

                                          <asp:TemplateField HeaderText="เบอร์ต่อภายใน">
		                                    <ItemTemplate>
			                                    <asp:Label id="lblExt" runat="server"></asp:Label>
		                                    </ItemTemplate>
                                            <%--  <ControlStyle Width="5%" />--%>
	                                    </asp:TemplateField>



	                                    <asp:TemplateField HeaderText="" ItemStyle-Height="160" ItemStyle-Width="200">
		                                    <ItemTemplate>
			                                    <asp:Image ID="image" runat="server" />
		                                    </ItemTemplate>
	                                        <ControlStyle Height="150px" Width="150px" />
                                    <ItemStyle Height="160px" Width="200px"></ItemStyle>

                                    </asp:TemplateField>

                                    <%--    <asp:TemplateField HeaderText="ลบ">
                                    <ItemTemplate>
                                    <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click">Delete </asp:LinkButton>
                                    </ItemTemplate>
                                    </asp:TemplateField>--%>

	                                    </Columns>
                                            <EditRowStyle BackColor="#999999" />
                                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                    </asp:GridView>

      
		</div><!-- /.team-member-wrapper -->
		
		<div class="blog-category-bt">
			<div class="btn-group">
				<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
			</div>
		</div>
		
           <script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
            <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/css/select2.min.css" rel="stylesheet" />
            <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/js/select2.min.js" type="text/javascript"></script>
            <script type="text/javascript">
                $(document).ready(function () {
                    $("#drpDepartment").select2({
                    });
                });

             
            </script>  


	</div> <!-- /.conatiner -->
</section>
 
</asp:Content>
