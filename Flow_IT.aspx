﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="Flow_IT.aspx.vb" Inherits="Aroma_HRPortal.Flow_IT" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/flow/banner.jpg" alt=""/></div>
<!-- end banner -->

<article class="blog-details-page">
	<div class="container">
		
		<div class="post-heading">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
            </ul>
			<h2>หน่วยงาน IT</h2>
		</div> <!-- /.post-heading -->
		
		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix" style="float: none; margin: 0 auto;">
				<div class="blog-details-post-wrapper">
				    
					<img src="new/images/banner/benefit/IT_Portal.jpg" alt="Image">

					<p style="padding: 30px 0 0;">
				        <ul class="p">
							<li><a href="Document/6Flow/IT/FlowPurchase.jpg" target="_blank">Workflow การสั่งซื้ออุปกรณ์ IT</a></li>
                            <li><a href="Document/6Flow/IT/Zimbra Training.pdf" target="_blank">ZIMBRA MAIL</a></li>

				        </ul>
					</p>
				
				</div>
			</div>
		</div>
		
	</div>
</article>

</asp:Content>
