﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_Holiday1.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_Holiday1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/hr.jpg" alt=""/></div>
<!-- end banner -->

<article class="blog-details-page">
	<div class="container">
		
		<div class="post-heading">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
                <li>/</li>
                <li><a href="HRPolicy_Holidays.aspx">วันหยุดประจำปีบริษัท</a></li>
            </ul>
			<h2>วันหยุดประเพณี ประจำปี พ.ศ. 2560</h2>
		</div> <!-- /.post-heading -->
		
		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix">
				<div class="blog-details-post-wrapper">
				
					<img src="new/images/banner/hr-policy/img24.jpg" alt=""/>
					
					<h4>บริษัท เค.วี.เอ็น. อิมปอร์ต เอกซ์ปอร์ต (1991) จำกัด  / บริษัท ไลอ้อน ทรี-สตาร์ จำกัด</h4>

					<h4>วันหยุดประเพณี ประจำปี พ.ศ. 2560 (เพิ่ม)</h4>
					<p>
						เพื่อให้การบริหารงานของบริษัทฯ สอดคล้องกับวันหยุดราชการประจำปี และวันหยุดตามประเพณีของสถาบันการเงินที่กำหนดเพิ่ม ทางบริษัทจึงเห็นสมควรกำหนดวันหยุดเพิ่ม และขอยกเลิกวันหยุดประเพณีที่เคยประกาศไปก่อนนี้ ดังนี้<br>
					</p>
					
					<h4>กำหนดวันหยุดประเพณีเพิ่ม</h4>
					<p>
						<ol>
							<li>วันศุกร์ที่	28 กรกฎาคม	วันเฉลิมพระชนมพรรษาสมเด็จพระเจ้าอยู่หัวมหาวชิราลงกรณ บดินทรเทพยวรางกูร</li>
							<li>วันศุกร์ที่ 13 ตุลาคม	วันคล้ายวันสวรรคตพระบาทสมเด็จพระปรมินทรมหาภูมิพลอดุลยเดชบรมนาถบพิตร</li>
						</ol>
					</p>
					
					<h4>ยกเลิกวันหยุดประเพณี</h4>
					<p>
						<ol>
							<li>วันศุกร์ที่ 5 พฤษภาคม วันฉัตรมงคล</li>
						</ol>
					</p>
					
					<h4>วันหยุดประเพณี ประจำปี พ.ศ. 2560 (สรุปที่ประกาศเพิ่ม และยกเลิก)</h4>

					<h4>ทางบริษัทฯ ได้กำหนดวันหยุดประจำปี พ.ศ. 2560 ดังนี้</h4>
					<p>
						<table class="table">
						  <tbody>
							<tr>
							  <td width="2%">1.</td>
							  <td width="15%">วันอังคารที่</td>
							  <td width="15%">3 มกราคม</td>
							  <td width="68%">วันหยุดชดเชยวันขึ้นปีใหม่</td>
							</tr>
							<tr>
							  <td>2.</td>
							  <td>วันจันทร์ที่</td>
							  <td>13 กุมภาพันธ์</td>
							  <td>วันหยุดชดเชยวันมาฆบูชา</td>
							</tr>
							<tr>
							  <td>3.</td>
							  <td>วันพฤหัสบดีที่</td>
							  <td>6 เมษายน</td>
							  <td>วันจักรี</td>
							</tr>
							<tr>
							  <td>4.</td>
							  <td>วันพฤหัสบดีที่</td>
							  <td>13 เมษายน</td>
							  <td>วันสงกรานต์</td>
							</tr>
							<tr>
							  <td>5.</td>
							  <td>วันศุกร์ที่</td>
							  <td>14 เมษายน</td>
							  <td>วันสงกรานต์</td>
							</tr>
							<tr>
							  <td>6.</td>
							  <td>วันจันทร์ที่</td>
							  <td>1 พฤษภาคม</td>
							  <td>วันแรงงานแห่งชาติ</td>
							</tr>
							<tr>
							  <td>7.</td>
							  <td>วันพุธที่</td>
							  <td>10 พฤษภาคม</td>
							  <td>วันวิสาขบูชา</td>
							</tr>
							<tr>
							  <td>8.</td>
							  <td>วันจันทร์ที่</td>
							  <td>10 กรกฏาคม</td>
							  <td>วันหยุดชดเชยวันอาสาฬบูชา</td>
							</tr>
							<tr>
							  <td>9.</td>
							  <td>วันศุกร์ที่</td>
							  <td>28 กรกฏาคม</td>
							  <td>วันเฉลิมพระชนมพรรษาสมเด็จพระเจ้าอยู่หัวมหาวชิราลงกรณบดินทรเทพยวรางกูร</td>
							</tr>
							<tr>
							  <td>10.</td>
							  <td>วันจันทร์ที่</td>
							  <td>14 สิงหาคม</td>
							  <td>วันหยุดชดเชยวันเฉลิมพระชนมพรรษาสมเด็จพระนางเจ้าพระบรมราชินีนาถ</td>
							</tr>
							<tr>
							  <td>11.</td>
							  <td>วันศุกร์ที่</td>
							  <td>13 ตุลาคม</td>
							  <td>วันคล้ายวันสวรรคตพระบาทสมเด็จพระปรมินทรมหาภูมิพลอดุลยเดช บรมนาถบพิตร</td>
							</tr>
							<tr>
							  <td>12.</td>
							  <td>วันจันทร์ที่</td>
							  <td>23 ตุลาคม</td>
							  <td>วันปิยมหาราช</td>
							</tr>
							<tr>
							  <td>13.</td>
							  <td>วันอังคารที่</td>
							  <td>5 ธันวาคม</td>
							  <td>วันเฉลิมพระชนมพรรษาพระบาทสมเด็จพระปรมินทรมหาภูมิพลอดุลยเดช</td>
							</tr>
							<tr>
							  <td>14.</td>
							  <td>วันจันทร์ที่</td>
							  <td>11 ธันวาคม</td>
							  <td>วันหยุดชดเชยวันรัฐธรรมนูญ</td>
							</tr>
							<tr>
							  <td>15.</td>
							  <td>วันอังคารที่</td>
							  <td>2 มกราคม 2561</td>
							  <td>วันหยุดชดเชยวันสิ้นปี 31 ธันวาคม 2560</td>
							</tr>
						  </tbody>
						</table>
					</p>
					
					<p>
						จึงประกาศมาเพื่อทราบโดยทั่วกัน<br><br>

						ประกาศ ณ วันที่ 20 เมษายน 2560<br><br>	
		 			 
						หมายเหตุ : หากมีการประกาศเปลี่ยนแปลงวันหยุดตามราชประเพณีหรือวันสำคัญต่างๆ บริษัทฯ จะมีการพิจารณาเปลี่ยนแปลงและ แจ้งให้ทราบอีกครั้ง
					</p>
					
					<div class="blog-details-bt">
						<div class="btn-group">
							<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
						</div>
					</div>
				
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-8 col-xs-12 p-fix">
				<aside>
					<div class="sidebar-news-list">
						<h4>CATEGORY</h4>
						<ul>
							<li>
								<a href="HRPolicy_Holiday1.aspx" class="tran3s">วันหยุดประเพณี ประจำปี พ.ศ. 2560</a>
							</li>
							<li>
								<a href="HRPolicy_Holiday2.aspx" class="tran3s">วันหยุดประเพณี ประจำปี พ.ศ. 2561</a>
							</li>
                            <li>
								<a href="HRPolicy_Holiday3.aspx" class="tran3s">วันหยุดประเพณี ประจำปี พ.ศ. 2562</a>
							</li>
                              <li>
								<a href="HRPolicy_Holiday4.aspx" class="tran3s">วันหยุดประเพณี ประจำปี พ.ศ. 2563</a>
							</li>
                             <li>
								<a href="HRPolicy_Holiday5.aspx" class="tran3s">วันหยุดประเพณี ประจำปี พ.ศ. 2564</a>
							</li>
						</ul>
					</div>
				</aside>
			</div>
		</div>
		
	</div>
</article>

</asp:Content>
