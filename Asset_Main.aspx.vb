﻿Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Data
Imports System.IO
Imports System.Configuration
Imports System.Collections
Imports System.Web.UI.Page
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
'Imports ICSharpCode.SharpZipLib.Zip
Imports System.Web.UI


Public Class Asset_Main
    Inherits System.Web.UI.Page
    Dim DB_HR As New Connect_HR
    Private sms As New PKMsg("")
    Dim dtEmp4 As New DataTable
    Dim sql As String
    Dim sql2 As String
    Dim dt As New DataTable
    Dim dtEmp As New DataTable
    Dim dtEmp2 As New DataTable
    Dim dtEmp3 As New DataTable
    Dim dt2 As New DataTable
    Dim dtt As New DataTable
    Dim Employee_id, type, other, brand, model, Serial, Asset, Remark, sqlCheck As String
    Dim datePur As String
    Dim mode As String
    'Dim datePur As Date

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        mode = Request.QueryString("mode")
        id = Request.QueryString("id")
        datePur = Request.QueryString("datePur")

        If mode = "update" Then
            txtID.Text = ID

            Dim sql As String
            sql = " select emp.Employee_id as Employee_id, emp.fullname as fullname,isnull(dep.department_name,'') as department from TB_Employee emp    "
            sql += " left outer join TB_department dep on dep.department_id = emp.department_id COLLATE SQL_Latin1_General_CP1_CI_AS  "
            sql += " where emp.Employee_id = '" & Trim(txtID.Text) & "' "
            dt = DB_HR.GetDataTable(sql)
            If dt.Rows.Count > 0 Then
                txtName.Items.Clear()
                txtName.Items.Add(dt.Rows(0)("fullname"))
                txtDept.Text = dt.Rows(0)("department")
            End If

            loadGridview()
        End If

        If Not Page.IsPostBack Then

        Else
            Dim sms = New PKMsg("")
            Dim st As String

            st = sms.ReadtempCtrlval("txtDate", Request.UserHostAddress.ToString, Session.SessionID)
            If st <> Nothing Then txtDate.Text = st
        End If

    End Sub


    Private Sub loadGridview(Optional ByVal sCond As String = "")

        sql = " SELECT num,[type],[AssetName],[Serial],[Asset],Location,MIS_asset,[datePur],[Remark]"
        sql += " FROM [TB_Asset] "
        sql += " where employee_id = '" & txtID.Text & "'  "

        sql += IIf(sCond = "", "", " and (" & sCond & ") ")

        sql += " order by num "

        dt = DB_HR.GetDataTable(sql)
        If dt.Rows.Count > 0 Then
            DataGrid4.Visible = True
            DataGrid4.DataSource = dt
            DataGrid4.DataBind()
        Else
            DataGrid4.Visible = False
        End If

        Session("selCond") = sCond

    End Sub

    Private Function SearchDataSub(ByVal Tb As String, ByVal Fld As String, Optional ByVal Cond As String = "")
        Dim sql As String
        Dim Sqladp As SqlDataAdapter
        Dim DataSearch As New DataSet
        Dim dtrow As DataRow
        Dim SCon As New SqlConnection(DB_HR.sqlCon)
        sql = "select top 1 " & Fld & " from " & Tb
        If Cond <> "" Then
            sql = sql & " where " & Cond
        End If
        Sqladp = New SqlDataAdapter(sql, SCon)
        Sqladp.Fill(DataSearch, "data1")
        If DataSearch.Tables("data1").Rows.Count <> 0 Then  'Not IsDBNull(DataSearch.Tables("data1").Rows(0)) Then
            dtrow = DataSearch.Tables("data1").Rows(0)
            SearchDataSub = dtrow.Item(0)
        Else
            SearchDataSub = ""
        End If
        SCon.Close()
    End Function

    Protected Sub btnBase_Click(sender As Object, e As EventArgs) Handles btnBase.Click
        If txtID.Text = "" Or drpType.SelectedValue = "" Then
            sms.Msg = "กรุณาระบุข้อมูล Employee ID"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If

        If drpType.SelectedValue = "" Then
            sms.Msg = "กรุณาระบุประเภททรัพย์สิน"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If

        sqlCheck += " SELECT * FROM [TB_Asset] "
        sqlCheck += " Where  [Employee_id] = '" & txtID.Text & "'"
        dt = DB_HR.GetDataTable(sqlCheck)
        If dt.Rows.Count <= 0 Then
            lblNum.Text = "1"
            insert()
        Else
            'Update
            If lblNum.Text <> "0" Then
                Update()
            Else
                genNum()
                insert2()
            End If
        End If

        If FileUpload1.FileName <> "" Then
            Call insertIMG()
            insertLogo()  ' LOGO
        End If

        Clear()
        'Call loadGridview()

    End Sub


    Sub insert()
        Dim sql As String = "INSERT INTO [TB_Asset] ([Num],[Employee_id],[type],[AssetName],[Serial],[Asset],[MIS_asset],[Location],[datePur],[Remark] )"
        sql &= "VALUES (1,'" & txtID.SelectedValue & "','" & drpType.Text & "','" & txtAssetName.Text & "','" & txtSerial.Text & "'"
        sql &= " ,' " & txtAsset.Text & "' , '" & txtMISAsset.Text & "' , '" & txtLocation.Text & "' , '" & DateTime.Parse(txtDate.Text).ToString("yyyy-MM-dd") & "' , '" & txtRemark.Text & "' ) "
        dtt = DB_HR.GetDataTable(sql)
        loadGridview()

    End Sub

    Sub insert2()
        Dim sql As String = "INSERT INTO [TB_Asset] ([Num],[Employee_id],[type],[AssetName],[Serial],[Asset],[MIS_asset],[Location],[datePur],[Remark] )"
        sql &= "VALUES (" & lblNum.Text & ",'" & txtID.Text & "','" & drpType.Text & "','" & txtAssetName.Text & "','" & txtSerial.Text & "'"
        sql &= " ,' " & txtAsset.Text & "' , '" & txtMISAsset.Text & "' , '" & txtLocation.Text & "' , '" & DateTime.Parse(txtDate.Text).ToString("yyyy-MM-dd") & "' , '" & txtRemark.Text & "') "
        dtt = DB_HR.GetDataTable(sql)
        loadGridview()
    End Sub

    Sub Update()
        Dim sSql As String = "UPDATE [TB_Asset] SET type='" & drpType.Text & "', AssetName='" & txtAssetName.Text & "' "
        sSql += " , Serial='" & txtSerial.Text & "', Asset='" & txtAsset.Text & "' , MIS_asset='" & txtMISAsset.Text & "', Location='" & txtLocation.Text & "', datePur='" & DateTime.Parse(txtDate.Text).ToString("yyyy-MM-dd") & "' "
        sSql += " , Remark='" & txtRemark.Text & "'"
        sSql += " Where Employee_ID = '" & txtID.Text & "' and Num = '" & lblNum.Text & "' "
        dtt = DB_HR.GetDataTable(sSql)
        loadGridview()
    End Sub

    Sub genNum()
        Dim conn As New Connect_HR
        Dim sql2 As String = "Select Max(Num) as Num from [TB_Asset] where Employee_id = '" & txtID.Text & "'"

        dt2 = DB_HR.GetDataTable(sql2)

        Dim newID As Integer = CInt(dt2.Rows(0)("Num"))
        newID += 1
        lblNum.Text = newID

    End Sub

    Sub Clear()
        ' drpType.Text = ""
        txtAsset.Text = ""
        txtAssetName.Text = ""
        txtDate.Text = ""
        txtModel.Text = ""
        txtSerial.Text = ""
        txtRemark.Text = ""
        txtLocation.Text = ""
        lblNum.Text = 0
        txtMISAsset.Text = ""

        drpType.SelectedValue = "0"

        img.ImageUrl = ReadImage("0")

    End Sub


    Sub insertIMG()
        Dim CurrentFileName As String
        CurrentFileName = FileUpload1.FileName

        'If (Path.GetExtension(CurrentFileName).ToLower <> ".jpg") Then
        '    sms.Msg = "You choose the file dishonestly !!! , Please choose be file .jpg"
        '    Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        '    Exit Sub
        'End If

        'If FileUpload1.PostedFile.ContentLength > 131072 Then
        '    sms.Msg = "The size of big too file , the size of the file must 128 KB not exceed!!!"
        '    Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        '    Exit Sub
        'End If
        Dim CurrentPath As String = Server.MapPath("~/Temp/")

        'FileUpload1.PostedFile.SaveAs(CurrentPath & FileUpload1.FileName)
        FileUpload1.PostedFile.SaveAs("c:\temp\" & FileUpload1.FileName)
        ' img.ImageUrl = CurrentPath & FileUpload1.FileName
        txtPic.Text = "c:\temp\" & FileUpload1.FileName

    End Sub

    Sub insertLogo()
        Dim db As New Connect_HR
        Dim sql As String
        Dim tr As SqlTransaction
        Dim sqlcon As New SqlConnection(DB_HR.sqlCon)

        If sqlcon.State = ConnectionState.Closed Then
            sqlcon = New SqlConnection(DB_HR.sqlCon)
            sqlcon.Open()
        End If

        tr = sqlcon.BeginTransaction

        Try
            Dim cmd As SqlCommand = New SqlCommand(sql, sqlcon, tr)
            If txtPic.Text <> "" Then
                Dim FileN As FileStream
                Dim Ln As Int32
                Dim oBinaryReader As BinaryReader
                Dim oImgByteArray As Byte()

                FileN = New FileStream(txtPic.Text, FileMode.Open, FileAccess.Read)
                oBinaryReader = New BinaryReader(FileN)
                oImgByteArray = oBinaryReader.ReadBytes(CInt(FileN.Length))
                Ln = CInt(FileN.Length)
                oBinaryReader.Close()
                FileN.Close()

                sql = "update [TB_Asset] set image_asset=@FF2 where employee_id  = '" & txtID.Text & "' and Num = '" & lblNum.Text & "' "
                Dim cmd3 As SqlCommand = New SqlCommand(sql, sqlcon, tr)
                cmd3.Parameters.Add("@FF2", SqlDbType.Image, 0).Value = oImgByteArray
                cmd3.CommandTimeout = 0
                cmd3.ExecuteNonQuery()

            End If

            tr.Commit()
            sqlcon.Close()
        Catch ex As Exception
            tr.Rollback()
            sqlcon.Close()
            Exit Sub
        End Try
    End Sub


    Protected Sub DataGrid4_DeleteCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid4.DeleteCommand
        Dim SelectID As String
        SelectID = Convert.ToString(DataGrid4.DataKeys(e.Item.ItemIndex))
        Dim s As Integer
        Dim ssql As String = "delete from [TB_Asset] where [num] = '" & SelectID & "' and Employee_id = '" & txtID.Text & "' "
        s = DB_HR.Execute(ssql)
        Call loadGridview()
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Server.Transfer("Asset_Search.aspx")
    End Sub

    Protected Sub txtID_Init(sender As Object, e As EventArgs) Handles txtID.Init
        Dim sqlCon As New SqlConnection(DB_HR.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String
        sql = "select Employee_id as id,Employee_id as fullname from TB_Employee order by id  "
        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_HR.sqlCon)
            sqlCon.Open()
        End If

        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With

        txtID.Items.Clear()
        txtID.Items.Add(New ListItem("", 0))
        While Rs.Read
            'txtID.DataTextField = "id"
            'txtID.DataValueField = "fullname"
            ' txtID.Items.Add(New ListItem(Str(Rs.GetString(1)), Rs.GetString(1)))
            txtID.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
            'txtID.Items.Add(New ListItem(Str(Rs.GetString(0)), Rs.GetString(0)))
            'txtID.Items.Add(New ListItem(Str(Rs.GetString(0)), Rs.GetString(1), Rs.GetString(0)))

        End While

    End Sub

    Protected Sub drpType_Init(sender As Object, e As EventArgs) Handles drpType.Init
        Dim sqlCon As New SqlConnection(DB_HR.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "SELECT [Type] as id ,Type  FROM [dbo].[TB_Asset_Type] group by [Type]"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_HR.sqlCon)
            sqlCon.Open()
        End If

        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpType.Items.Clear()
        drpType.Items.Add(New ListItem("", 0))
        While Rs.Read
            'drpProvince.Items.Add(New ListItem(Str(Rs.GetInt32(0)) + " " + Rs.GetString(1), Rs.GetInt32(0)))
            drpType.Items.Add(New ListItem(Rs.GetString(1), Rs.GetString(0)))
            'txtMgr.Items.Add(New ListItem(Rs.GetString(1), Rs.GetString(0)))
        End While
    End Sub



    Protected Sub DataGrid4_EditCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid4.EditCommand
        Dim SelectID As String
        SelectID = Convert.ToString(DataGrid4.DataKeys(e.Item.ItemIndex))

        'Clear image
        Clear()

        sql = " SELECT asset.[ID] , isnull(asset.[Num],0) as  Num, asset.[Employee_id] ,isnull(emp.fullname,'') as fullname , isnull(emp.department_id,'') as department "
        sql += " ,isnull(asset.[type],'') as type, isnull(AssetName,'') as AssetName " ', isnull(asset.SubType,'') as SubType, isnull(asset.[other],'') as other , isnull(asset.[brand],'') as brand "
        sql += " ,isnull(asset.[serial],'') as serial , isnull(asset.[asset],'') as asset , isnull(asset.MIS_asset,'') as MIS_asset  "
        sql += " ,isnull(asset.Location,'') as Location , isnull(asset.[datePur],'') as datePur , isnull(asset.[Remark],'') as Remark , asset.image_asset "
        sql += " from [TB_Asset] asset "
        sql += " left outer join TB_Employee emp on emp.Employee_ID = asset.Employee_id  COLLATE SQL_Latin1_General_CP1_CI_AS "
        sql += " where asset.[Employee_ID] = '" & txtID.Text & "'  and  asset.Num = '" & SelectID & "'"

        dt = DB_HR.GetDataTable(sql)

        If dt.Rows.Count > 0 Then
            txtID.Text = dt.Rows(0)("Employee_ID")
            txtName.Text = dt.Rows(0)("fullname")
            txtDept.Text = dt.Rows(0)("department")
            lblNum.Text = dt.Rows(0)("num")
            drpType.SelectedValue = dt.Rows(0)("type")
            'drpTypeSub.SelectedValue = dt.Rows(0)("SubType")
            txtAssetName.Text = dt.Rows(0)("AssetName")
            'txtModel.Text = dt.Rows(0)("model")
            txtSerial.Text = dt.Rows(0)("serial")
            txtAsset.Text = dt.Rows(0)("asset")
            txtMISAsset.Text = dt.Rows(0)("MIS_asset")
            txtLocation.Text = dt.Rows(0)("Location")
            txtDate.Text = dt.Rows(0)("datePur")
            txtRemark.Text = dt.Rows(0)("Remark")

            If Not IsDBNull(dt.Rows(0)("image_asset")) Then
                img.ImageUrl = ReadImage(dt.Rows(0)("Num"))
            End If

        End If

    End Sub

    Function ReadImage(ByVal strID As String)
        Return ("ReadimageAsset.aspx?num=" & strID & "&id=" & txtID.Text)
    End Function

    Protected Sub txtID_SelectedIndexChanged(sender As Object, e As EventArgs) Handles txtID.SelectedIndexChanged
        Dim sql As String
        sql = " select emp.Employee_id as id, emp.fullname as fullname,isnull(dep.department_name,'') as department from TB_Employee emp  left outer join TB_department dep on dep.department_id = emp.department_id COLLATE SQL_Latin1_General_CP1_CI_AS  "
        'sql += " from TB_Employee "
        sql += " where emp.Employee_id = '" & txtID.Text & "' "

        dt = DB_HR.GetDataTable(sql)

        If dt.Rows.Count > 0 Then
            txtName.Items.Clear()
            txtName.Items.Add(dt.Rows(0)("fullname"))
            txtDept.Text = dt.Rows(0)("department")
        End If
    End Sub



    Protected Sub txtName_Init(sender As Object, e As EventArgs) Handles txtName.Init
        Dim sqlCon As New SqlConnection(DB_HR.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select emp.Employee_id as id, emp.fullname as fullname,isnull(dep.department_name,'') as department from TB_Employee emp  left outer join TB_department dep on dep.department_id = emp.department_id COLLATE SQL_Latin1_General_CP1_CI_AS "
        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_HR.sqlCon)
            sqlCon.Open()
        End If

        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With

        txtName.Items.Clear()
        txtName.Items.Add(New ListItem("", 1))
        While Rs.Read
            txtName.Items.Add(New ListItem(Rs.GetString(1), Rs.GetString(1)))
            'txtID.Items.Add(New ListItem(Str(Rs.GetString(0)), Rs.GetString(1), Rs.GetString(0)))

        End While
    End Sub


    Protected Sub txtName_SelectedIndexChanged(sender As Object, e As EventArgs) Handles txtName.SelectedIndexChanged
        Dim sql As String
        sql = " select Employee_id ,fullname ,department_id  "
        sql += " from TB_Employee "
        sql += " where fullname Like '%" & Trim(txtName.Text) & "%' "

        dt = DB_HR.GetDataTable(sql)

        If dt.Rows.Count > 0 Then
            txtID.Items.Clear()
            txtID.Items.Add(dt.Rows(0)("Employee_id"))
            txtDept.Text = dt.Rows(0)("department_id")
        End If
    End Sub

    Protected Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        Clear()

    End Sub
End Class