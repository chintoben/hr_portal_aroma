﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="Flow_IT.aspx.vb" Inherits="Aroma_HRPortal.Flow_IT" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<style>
	.swiper-container {
		width: 100%;
		height: 100%;
	}
	.swiper-slide {
		text-align: center;
		background: #fff;
	}
	.swiper-slide img 
	{
		width: 100%;
	}
	.swiper-pagination-bullet-active {
		opacity: 1;
		background: #af2f2f;
	}
</style>

<!-- banner -->
<!-- Swiper -->
<div class="swiper-container swiper1">
    <div class="swiper-wrapper">
        <div class="swiper-slide" data-hash="slide1"><img src="new/images/banner/benefit/banner1.jpg" /></div>
        <div class="swiper-slide" data-hash="slide2">Slide 2</div>
        <div class="swiper-slide" data-hash="slide3">Slide 3</div>
    </div>
    <!-- Add Pagination -->
   	<div class="swiper-pagination swiper-pagination1"></div>
    <!-- Add Arrows -->
    <!--div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div-->
</div>
<!-- end banner -->

<!-- content -->
<div class="container">
<div class="row">
    <div class="base-content">
    	
    	<div class="base-content-head">DEVELOPMENT AND TRAINING (พัฒนาและฝึกอบรม)</div>
		<div class="base-head-bot-line"></div>
 		<div class="base-content-head-detail">หลักสูตรพัฒนาและฝึกอบรม</div>
  		
   		<div class="box-benefits2">
   			<div class="size-circle">
				<div class="box-circle">
				<a href="Flow_IT.aspx">
					<div class="circle">
						<div class="circle-icon"><i class="fa fa-usd" aria-hidden="true"></i></div>
						<div class="circle-txt">
							การเรียนรู้และพัฒนาพนักงาน (Learning & Development)
						</div>
					</div>
				</a>
				</div>
			</div>
  			<div class="size-circle">
				<div class="box-circle">
				 <a href="Flow_SC.aspx">
					<div class="circle">
						<div class="circle-icon"><i class="fa fa-usd" aria-hidden="true"></i></div>
						<div class="circle-txt">
							สมรรถนะ (Competency)
						</div>
					</div>
				</a>
				</div>
			</div>
  			<div class="size-circle">
				<div class="box-circle">
				 <a href="Flow_FN.aspx">
					<div class="circle">
						<div class="circle-icon"><i class="fa fa-usd" aria-hidden="true"></i></div>
						<div class="circle-txt">
							แผนการพัฒนาและฝึกอบรมพนักงาน (Development Road Map)
						</div>
					</div>
				</a>
				</div>
			</div>
  			<div class="size-circle">
				<div class="box-circle">
				 <a href="Flow_AS.aspx">
					<div class="circle">
						<div class="circle-icon"><i class="fa fa-usd" aria-hidden="true"></i></div>
						<div class="circle-txt">
							หลักสูตรพัฒนาฝึกอบรม (Development Course)
						</div>
					</div>
				</a>
				</div>
			</div>
   			<div class="clear"></div>
   		</div>
   		
   		<div class="size-bt-benefits">
   			<div class="btn-group">
   				<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
   			</div>
   		</div>
    	
	</div>
</div>
</div>

<!-- Swiper JS -->
<script src="new/js/swiper.min.js"></script>

<!-- Initialize Swiper -->
<script>
	var swiper1 = new Swiper('.swiper1', {
		spaceBetween: 30,
		pagination: {
        el: '.swiper-pagination1',
        clickable: true,
      },
    });
</script>  

</asp:Content>
