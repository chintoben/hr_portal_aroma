﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_WhoisWho22.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_Contact" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/HR.jpg" alt=""/></div>
<!-- end banner -->

<section id="team-section">
	<div class="container">
		<div class="theme-title">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
                <li>/</li>
                <li><a href="HRPolicy_WhoisWho.aspx">ติดต่อฝ่าย-แผนกต่างๆ</a></li>
            </ul>
			<h2>รายชื่อติดต่อ-เทคโนโลยีสารสนเทศ (MIS)</h2>
			<!--p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p-->
		</div> <!-- /.theme-title -->

		<div class="clear-fix team-member-wrapper">
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who2019/9.mis/คุณอรนุช%20%20จินารัตน์.jpg" alt="" 
                            height="150"/>
						<div class="opacity tran4s">
							<h4>คุณอรนุช จินารัตน์ (โอ๋)</h4>
							<span>ผู้อำนวยการฝ่าย</span>
							<p>
								เบอร์ต่อภายใน : 526<br />
                    			<%--โทรศัพท์มือถือ : -<br />--%>
                    			Email : <a href="mailto:oranuchj@aromathailand.com">oranuchj@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณอรนุช จินารัตน์ (โอ๋)</h5>
						<p>ผู้อำนวยการฝ่าย</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->

			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who2019/9.mis/คุณศิริวุฒิ%20%20มันตะสูตร.jpg" alt="" 
                            height="150"/>
						<div class="opacity tran4s">
							<h4>คุณศิริวุฒิ มันตะสูตร (เอ๋ย)</h4>
							<span>ผู้จัดการฝ่าย</span>
							<p>
								เบอร์ต่อภายใน : 527<br />
                    			<%--โทรศัพท์มือถือ : -<br />--%>
                    			Email : <a href="mailto:siriwutm@aromathailand.com">siriwutm@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณศิริวุฒิ มันตะสูตร (เอ๋ย)</h5>
						<p>ผู้จัดการฝ่าย</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->
			
            <div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who2019/9.mis/คุณกิตติ%20%20แสงเงิน.jpg" alt="" 
                            height="150"/>
						<div class="opacity tran4s">
							<h4>คุณกิตติ แสงเงิน (กิตติ)</h4>
							<span>ผู้จัดการแผนก</span>
							<p>
								เบอร์ต่อภายใน : 02-9332354 ต่อ 610<br />
                    			<%--โทรศัพท์มือถือ : -<br />--%>
                    			Email : <a href="mailto:kittis@aromathailand.com">kittis@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณกิตติ แสงเงิน (กิตติ)</h5>
						<p>ผู้จัดการแผนก</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->
			

			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who2019/9.mis/คุณณัฐรีพรรณ%20%20นิทธยุสกุลโชติ.jpg" alt="" 
                            height="150"/>
						<div class="opacity tran4s">
							<h4>คุณณัฐรีพรรณ นิทธยุสกุลโชติ (ไนซ์)</h4>
							<span>ผู้จัดการแผนก</span>
							<p>
								เบอร์ต่อภายใน : 522<br />
                    			<%--โทรศัพท์มือถือ : -<br />--%>
                    			Email : <a href="mailto:nuttareepann@aromathailand.com">nuttareepann@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณณัฐรีพรรณ นิทธยุสกุลโชติ (ไนซ์)</h5>
						<p>ผู้จัดการแผนก</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->
			
			
		</div><!-- /.team-member-wrapper -->
		
		<div class="blog-category-bt">
			<div class="btn-group">
				<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
			</div>
		</div>
		
	</div> <!-- /.conatiner -->
</section>
 
</asp:Content>
