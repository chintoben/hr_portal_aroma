﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_Authorize.Master" CodeBehind="A_EmployeeImport.aspx.vb" Inherits="Aroma_HRPortal.A_EmployeeImport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            width: 841px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.min.js"></script>

                    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.16/jquery-ui.min.js"></script>

                    <link type="text/css" rel="Stylesheet" href="http://ajax.microsoft.com/ajax/jquery.ui/1.8.16/themes/redmond/jquery-ui.css" />


                     <script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
                    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/css/select2.min.css" rel="stylesheet" />
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/js/select2.min.js" type="text/javascript"></script>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $("#drpBranch").select2({
                            });
                        });
                    </script>
                    
                     <script type="text/javascript">
                         $(document).ready(function () {
                             $("#drpDepartment").select2({
                             });
                         });
                    </script>

                   <script type="text/javascript">
                       $(document).ready(function () {
                           $("#drpDepartments").select2({
                           });
                       });
                    </script>
        <br />
        <br />
         <br />


          <div class="btn-group">
                                 <asp:button id="backButton" runat="server" text="Back" class="btn btn-primary"
OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>

   </div>

   <div class="col-lg-12">
                <h3>
                  Upload Employee Data
                </h3>
                
                </div>
                
            <br/>
      <br/>
   
                 <%--<div class="panel panel-info">
                 <div class="panel-heading">   Upload Employee Data </div>
                </div>--%>


                <table>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="style1">
                        &nbsp;</td>
                </tr>
                
                
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        <asp:Label ID="Label5" runat="server" Font-Bold="True" Font-Size="Medium" 
                            ForeColor="Red" Text="*"></asp:Label>
                        <asp:Label ID="Label4" runat="server" Font-Bold="True" Text="เลือกไฟล์ (Excel)" 
                            Width="120px"></asp:Label>
                    </td>
                    <td class="style1">
                       <input id="Attach1" style="WIDTH: 50%; " type="file" name="Attach1" runat="server"   ></td>
                </tr>
                
                
                <tr>
                    <td>
                        <asp:TextBox ID="TextBox1" runat="server" BorderColor="White" BorderWidth="0px" 
                            Width="50px"></asp:TextBox></td>
                    <td></td>
                    <td class="style1">
                       &nbsp;<p class="help-block">
												
                                                    <asp:Label ID="Label3" runat="server" Text="some help text here."></asp:Label>

                                                    <br />

                                                      <asp:Label ID="lblMissItem" runat="server" Text=""></asp:Label>

                                                    
												</p>

                                                  <asp:listbox id="lstMissItem" style="Z-INDEX: 103; POSITION: absolute; TOP: 484px; left: 33px;"
				                                    runat="server" >		
			                                    </asp:listbox>
            			
                        	                    <asp:datagrid id="Gridview2" style="Z-INDEX: 101; POSITION: absolute; TOP: 300px; LEFT: 282px; right: 543px;"
				                                    runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" >
                                                    <AlternatingItemStyle BackColor="White" />
                                                    <EditItemStyle BackColor="#2461BF" />
                                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                    <ItemStyle BackColor="#EFF3FB" />
                                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                    <SelectedItemStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                </asp:datagrid>
			
			                                    


											<%--</div>--%>

                                              <asp:Button ID="btnSubmit" runat="server" Text="Import"  class="btn btn-primary" />


                              <br />
                               <br />
                                <br />
                              <asp:Table id="Table2" 
                                                    runat="server"
				                                    GridLines="Both"></asp:Table>
                    
                    </td>
                </tr>
                
                
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="style1">
                        &nbsp;</td>
                </tr>
                
                
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        <asp:Label ID="Label6" runat="server" Font-Bold="True" Text="*** หมายเหตุ" 
                            Width="120px"></asp:Label>
                    </td>
                    <td class="style1">
												
                                                    <asp:Label ID="Label7" runat="server" 
                            Text="1. Import File Excel  (ข้อมูลตาม Template)"></asp:Label>

                    </td>
                </tr>
                
                
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="style1">
												
                                                    <asp:Label ID="Label9" runat="server" 
                            Text="2. ระบุข้อมูลบน Sheet1"></asp:Label>

                    </td>
                </tr>
                
                
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="style1">
												
                                                    <asp:Label ID="Label8" runat="server" 
                            Text="3. ต้องระบุข้อมูลทุกคอลัมภ์ หากไม่มีข้อมูลระบุ &quot;-&quot; แทน"></asp:Label>

                    </td>
                </tr>
                
                
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="style1">
                        &nbsp;</td>
                </tr>
                
                
                </table>




             

                            

</asp:Content>
