﻿Imports System.Data.SqlClient

Public Class index_NewEmployee
    Inherits System.Web.UI.Page
    Dim dt As New DataTable()
    ' Dim strConnString As [String] = System.Configuration.ConfigurationManager.ConnectionStrings("HRConnectionString").ConnectionString()
    Dim db_Form As New Connect_HR
    Dim DB_HR As New Connect_HR
    Dim sql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            CheckImg()
            loadImage()

        End If
    End Sub

    Sub loadImage()
        Dim sqlconnection1 As New SqlConnection(db_Form.sqlCon)
        Dim SqlCmd2 As New SqlCommand
        Dim SqlDataReader2 As SqlDataReader
        Dim sql As String = ""
        Dim ChangeDateSQL2 As String = ""
        Dim i As Integer
        Dim sql2 As String = ""

        sql = "  select  employee_id, fullname + ' (เริ่มงาน ' + CONVERT(varchar(10), (StartDate)) +  ')'  as fullname "
        sql += "  from  TB_Employee   "
        sql += "  where [status] = 'Active' "
        'sql += " and (image_name <> '' ) "
        sql += " and month(StartDate) = month(getdate()) and year(StartDate) = year(getdate()) "
        sql += " order by StartDate desc "

        sqlconnection1 = New SqlConnection(db_Form.sqlCon)
        sqlconnection1.Open()

        SqlCmd2.Connection = sqlconnection1
        SqlCmd2.CommandTimeout = 0
        SqlCmd2.CommandText = sql
        SqlDataReader2 = SqlCmd2.ExecuteReader

        While SqlDataReader2.Read()
            Try
                If i = 0 Then
                    Image1.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    Label1.Text = SqlDataReader2.GetValue(1)
                ElseIf i = 1 Then
                    Image2.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    Label2.Text = SqlDataReader2.GetValue(1)
                ElseIf i = 2 Then
                    Image3.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    Label3.Text = SqlDataReader2.GetValue(1)
                ElseIf i = 3 Then
                    Image4.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    Label4.Text = SqlDataReader2.GetValue(1)
                ElseIf i = 4 Then
                    Image5.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    Label5.Text = SqlDataReader2.GetValue(1)
                ElseIf i = 5 Then
                    Image6.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    Label6.Text = SqlDataReader2.GetValue(1)
                ElseIf i = 6 Then
                    Image7.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    Label7.Text = SqlDataReader2.GetValue(1)
                ElseIf i = 7 Then
                    Image8.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    Label8.Text = SqlDataReader2.GetValue(1)
                ElseIf i = 8 Then
                    Image9.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    Label9.Text = SqlDataReader2.GetValue(1)
                ElseIf i = 9 Then
                    Image10.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    Label10.Text = SqlDataReader2.GetValue(1)
                ElseIf i = 10 Then
                    Image11.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    Label11.Text = SqlDataReader2.GetValue(1)
                ElseIf i = 11 Then
                    Image12.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    Label12.Text = SqlDataReader2.GetValue(1)
                ElseIf i = 12 Then
                    Image13.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    Label13.Text = SqlDataReader2.GetValue(1)
                ElseIf i = 13 Then
                    Image14.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    Label14.Text = SqlDataReader2.GetValue(1)
                ElseIf i = 14 Then
                    Image15.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    Label15.Text = SqlDataReader2.GetValue(1)
                    'ElseIf i = 15 Then
                    '    Image16.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    '    Label16.Text = SqlDataReader2.GetValue(1)
                    'ElseIf i = 16 Then
                    '    Image17.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    '    Label17.Text = SqlDataReader2.GetValue(1)
                    'ElseIf i = 17 Then
                    '    Image18.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    '    Label18.Text = SqlDataReader2.GetValue(1)
                    'ElseIf i = 18 Then
                    '    Image19.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    '    Label19.Text = SqlDataReader2.GetValue(1)
                    'ElseIf i = 19 Then
                    '    Image20.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    '    Label20.Text = SqlDataReader2.GetValue(1)
                    'ElseIf i = 20 Then
                    '    Image21.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    '    Label21.Text = SqlDataReader2.GetValue(1)
                    'ElseIf i = 21 Then
                    '    Image22.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    '    Label22.Text = SqlDataReader2.GetValue(1)
                    'ElseIf i = 22 Then
                    '    Image23.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    '    Label23.Text = SqlDataReader2.GetValue(1)
                    'ElseIf i = 23 Then
                    '    Image24.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    '    Label24.Text = SqlDataReader2.GetValue(1)
                    'ElseIf i = 24 Then
                    '    Image25.ImageUrl = ReadImage(SqlDataReader2.GetValue(0))
                    '    Label25.Text = SqlDataReader2.GetValue(1)
                End If

                i = i + 1
            Finally
            End Try
        End While
        SqlDataReader2.Close()
    End Sub

    Sub CheckImg()
        Dim sql As String = ""
        'sql = "  select top 10 employee_id,fullname "
        'sql += "  from  TB_Employee where [status] = 'Active' "

        sql = "  select top 15 employee_id, fullname + ' (เริ่มงาน ' + CONVERT(varchar(10), (StartDate)) +  ')'  as fullname "
        sql += "  from  TB_Employee   "
        sql += "  where [status] = 'Active' "
        ' sql += " and (image_name <> '' ) "
        sql += " and month(StartDate) = month(getdate()) and year(StartDate) = year(getdate()) "
        sql += " order by StartDate desc "


        ' sql += " and (image_name <> '' ) "
        dt = db_Form.GetDataTable(sql)
        If dt.Rows.Count = 1 Then
            div2.Visible = False
            div3.Visible = False
            div4.Visible = False
            div5.Visible = False
            div6.Visible = False
            div7.Visible = False
            div8.Visible = False
            div9.Visible = False
            div10.Visible = False
            div11.Visible = False
            div12.Visible = False
            div13.Visible = False
            div14.Visible = False
            div15.Visible = False
            'div16.Visible = False
            'div17.Visible = False
            'div18.Visible = False
            'div19.Visible = False
            'div20.Visible = False
            'div21.Visible = False
            'div22.Visible = False
            'div23.Visible = False
            'div24.Visible = False
            'div25.Visible = False
        ElseIf dt.Rows.Count = 2 Then
            div3.Visible = False
            div4.Visible = False
            div5.Visible = False
            div6.Visible = False
            div7.Visible = False
            div8.Visible = False
            div9.Visible = False
            div10.Visible = False
            div11.Visible = False
            div12.Visible = False
            div13.Visible = False
            div14.Visible = False
            div15.Visible = False
            'div16.Visible = False
            'div17.Visible = False
            'div18.Visible = False
            'div19.Visible = False
            'div20.Visible = False
            'div21.Visible = False
            'div22.Visible = False
            'div23.Visible = False
            'div24.Visible = False
            'div25.Visible = False
        ElseIf dt.Rows.Count = 3 Then
            div4.Visible = False
            div5.Visible = False
            div6.Visible = False
            div7.Visible = False
            div8.Visible = False
            div9.Visible = False
            div10.Visible = False
            div11.Visible = False
            div12.Visible = False
            div13.Visible = False
            div14.Visible = False
            div15.Visible = False
            'div16.Visible = False
            'div17.Visible = False
            'div18.Visible = False
            'div19.Visible = False
            'div20.Visible = False
            'div21.Visible = False
            'div22.Visible = False
            'div23.Visible = False
            'div24.Visible = False
            'div25.Visible = False
        ElseIf dt.Rows.Count = 4 Then
            div5.Visible = False
            div6.Visible = False
            div7.Visible = False
            div8.Visible = False
            div9.Visible = False
            div10.Visible = False
            div11.Visible = False
            div12.Visible = False
            div13.Visible = False
            div14.Visible = False
            div15.Visible = False
            'div16.Visible = False
            'div17.Visible = False
            'div18.Visible = False
            'div19.Visible = False
            'div20.Visible = False
            'div21.Visible = False
            'div22.Visible = False
            'div23.Visible = False
            'div24.Visible = False
            'div25.Visible = False
        ElseIf dt.Rows.Count = 5 Then
            div6.Visible = False
            div7.Visible = False
            div8.Visible = False
            div9.Visible = False
            div10.Visible = False
            div11.Visible = False
            div12.Visible = False
            div13.Visible = False
            div14.Visible = False
            div15.Visible = False
            'div16.Visible = False
            'div17.Visible = False
            'div18.Visible = False
            'div19.Visible = False
            'div20.Visible = False
            'div21.Visible = False
            'div22.Visible = False
            'div23.Visible = False
            'div24.Visible = False
            'div25.Visible = False
        ElseIf dt.Rows.Count = 6 Then
            div7.Visible = False
            div8.Visible = False
            div9.Visible = False
            div10.Visible = False
            div11.Visible = False
            div12.Visible = False
            div13.Visible = False
            div14.Visible = False
            div15.Visible = False
            'div16.Visible = False
            'div17.Visible = False
            'div18.Visible = False
            'div19.Visible = False
            'div20.Visible = False
            'div21.Visible = False
            'div22.Visible = False
            'div23.Visible = False
            'div24.Visible = False
            'div25.Visible = False
        ElseIf dt.Rows.Count = 7 Then
            div8.Visible = False
            div9.Visible = False
            div10.Visible = False
            div11.Visible = False
            div12.Visible = False
            div13.Visible = False
            div14.Visible = False
            div15.Visible = False
            'div16.Visible = False
            'div17.Visible = False
            'div18.Visible = False
            'div19.Visible = False
            'div20.Visible = False
            'div21.Visible = False
            'div22.Visible = False
            'div23.Visible = False
            'div24.Visible = False
            'div25.Visible = False
        ElseIf dt.Rows.Count = 8 Then
            div9.Visible = False
            div10.Visible = False
            div11.Visible = False
            div12.Visible = False
            div13.Visible = False
            div14.Visible = False
            div15.Visible = False
            'div16.Visible = False
            'div17.Visible = False
            'div18.Visible = False
            'div19.Visible = False
            'div20.Visible = False
            'div21.Visible = False
            'div22.Visible = False
            'div23.Visible = False
            'div24.Visible = False
            'div25.Visible = False
        ElseIf dt.Rows.Count = 9 Then
            div10.Visible = False
            div11.Visible = False
            div12.Visible = False
            div13.Visible = False
            div14.Visible = False
            div15.Visible = False
            'div16.Visible = False
            'div17.Visible = False
            'div18.Visible = False
            'div19.Visible = False
            'div20.Visible = False
            'div21.Visible = False
            'div22.Visible = False
            'div23.Visible = False
            'div24.Visible = False
            'div25.Visible = False
        ElseIf dt.Rows.Count = 10 Then
            div11.Visible = False
            div12.Visible = False
            div13.Visible = False
            div14.Visible = False
            div15.Visible = False
            'div16.Visible = False
            'div17.Visible = False
            'div18.Visible = False
            'div19.Visible = False
            'div20.Visible = False
            'div21.Visible = False
            'div22.Visible = False
            'div23.Visible = False
            'div24.Visible = False
            'div25.Visible = False
        ElseIf dt.Rows.Count = 11 Then
            div12.Visible = False
            div13.Visible = False
            div14.Visible = False
            div15.Visible = False
            'div16.Visible = False
            'div17.Visible = False
            'div18.Visible = False
            'div19.Visible = False
            'div20.Visible = False
            'div21.Visible = False
            'div22.Visible = False
            'div23.Visible = False
            'div24.Visible = False
            'div25.Visible = False
        ElseIf dt.Rows.Count = 11 Then
            div12.Visible = False
            div13.Visible = False
            div14.Visible = False
            div15.Visible = False
            'div16.Visible = False
            'div17.Visible = False
            'div18.Visible = False
            'div19.Visible = False
            'div20.Visible = False
            'div21.Visible = False
            'div22.Visible = False
            'div23.Visible = False
            'div24.Visible = False
            'div25.Visible = False
        ElseIf dt.Rows.Count = 12 Then
            div13.Visible = False
            div14.Visible = False
            div15.Visible = False
            'div16.Visible = False
            'div17.Visible = False
            'div18.Visible = False
            'div19.Visible = False
            'div20.Visible = False
            'div21.Visible = False
            'div22.Visible = False
            'div23.Visible = False
            'div24.Visible = False
            'div25.Visible = False
        ElseIf dt.Rows.Count = 13 Then
            div14.Visible = False
            div15.Visible = False
            'div16.Visible = False
            'div17.Visible = False
            'div18.Visible = False
            'div19.Visible = False
            'div20.Visible = False
            'div21.Visible = False
            'div22.Visible = False
            'div23.Visible = False
            'div24.Visible = False
            'div25.Visible = False
        ElseIf dt.Rows.Count = 14 Then
            div15.Visible = False
            'div16.Visible = False
            'div17.Visible = False
            'div18.Visible = False
            'div19.Visible = False
            'div20.Visible = False
            'div21.Visible = False
            'div22.Visible = False
            'div23.Visible = False
            'div24.Visible = False
            'div25.Visible = False
            'ElseIf dt.Rows.Count = 15 Then
            '    div16.Visible = False
            '    div17.Visible = False
            '    div18.Visible = False
            '    div19.Visible = False
            '    div20.Visible = False
            '    div21.Visible = False
            '    div22.Visible = False
            '    div23.Visible = False
            '    div24.Visible = False
            '    div25.Visible = False
            'ElseIf dt.Rows.Count = 16 Then
            '    div17.Visible = False
            '    div18.Visible = False
            '    div19.Visible = False
            '    div20.Visible = False
            '    div21.Visible = False
            '    div22.Visible = False
            '    div23.Visible = False
            '    div24.Visible = False
            '    div25.Visible = False
            'ElseIf dt.Rows.Count = 17 Then
            '    div18.Visible = False
            '    div19.Visible = False
            '    div20.Visible = False
            '    div21.Visible = False
            '    div22.Visible = False
            '    div23.Visible = False
            '    div24.Visible = False
            '    div25.Visible = False
            'ElseIf dt.Rows.Count = 18 Then
            '    div19.Visible = False
            '    div20.Visible = False
            '    div21.Visible = False
            '    div22.Visible = False
            '    div23.Visible = False
            '    div24.Visible = False
            '    div25.Visible = False
            'ElseIf dt.Rows.Count = 19 Then
            '    div20.Visible = False
            '    div21.Visible = False
            '    div22.Visible = False
            '    div23.Visible = False
            '    div24.Visible = False
            '    div25.Visible = False
            'ElseIf dt.Rows.Count = 20 Then
            '    div21.Visible = False
            '    div22.Visible = False
            '    div23.Visible = False
            '    div24.Visible = False
            '    div25.Visible = False
            'ElseIf dt.Rows.Count = 21 Then
            '    div22.Visible = False
            '    div23.Visible = False
            '    div24.Visible = False
            '    div25.Visible = False
            'ElseIf dt.Rows.Count = 22 Then
            '    div23.Visible = False
            '    div24.Visible = False
            '    div25.Visible = False
            'ElseIf dt.Rows.Count = 23 Then
            '    div24.Visible = False
            '    div25.Visible = False
            'ElseIf dt.Rows.Count = 24 Then
            '    div25.Visible = False
        End If

    End Sub

    Function ReadImage(strID As String)
        'Return ("Readimage.aspx?ImageID=" & strID)

        Sql = " select [fullname]"
        Sql += " from [TB_Employee] "
        Sql += " where [Employee_id] = '" & strID & "' and  [image] is null "
        dt = DB_HR.GetDataTable(Sql)
        If dt.Rows.Count > 0 Then
            Return ("Readimage.aspx?ImageID=9999")
        Else
            Return ("Readimage.aspx?ImageID=" & strID)
        End If


        'Dim strImageID As String = strID 'Request.QueryString("ImageID")
        'Dim sqlcmd2 As New SqlCommand
        'Dim sqlread2 As SqlDataReader


        'Dim sqlcon As New SqlConnection(DB_HR.sqlCon)

        'If sqlcon.State = ConnectionState.Closed Then
        '    sqlcon = New SqlConnection(DB_HR.sqlCon)
        '    sqlcon.Open()
        'End If
        'sqlcmd2 = New SqlCommand("select image from TB_Employee  where Employee_ID ='" + Trim(strImageID) + "'", sqlcon)
        'sqlread2 = sqlcmd2.ExecuteReader()

        'While sqlread2.Read

        '    Response.BinaryWrite(sqlread2.Item("image"))
        '    Response.Flush()

        'End While
        'sqlcon.Close()

        'Return()


    End Function



End Class