﻿Public Class BrowserEmployee
    Inherits System.Web.UI.Page
    Private DB_HR As New Connect_HR
    ' Private db_SubContractPortal As New Connect_SubContractPortal
    Dim Co As Integer
    Dim Index As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Put user code to initialize the page here
        If Not (Page.IsPostBack) Then
            loaddata("", "")
        End If
        control.Value = Request.QueryString("textbox").ToString()

    End Sub


    Sub loaddata(ByVal Cd As String, ByVal Ne As String)
        Dim sql As String
        Dim sql2 As String = ""
        Grid1.DataSource = Nothing
        sql = " select ltrim(rtrim(Employee_ID)) as code,fullname as description from TB_Employee Where 1=1  "
        If Cd <> "" Then
            If sql2 = "" Then
                sql2 = " and [Employee_ID] like '%" & Cd & "%'"
            Else
                sql2 = sql2 & " and [Employee_ID] like '%" & Cd & "%'"
            End If
        End If
        If Ne <> "" Then
            If sql2 = "" Then
                sql2 = " and [fullname] like '%" & Ne & "%' "
            Else
                sql2 = sql2 & " and [fullname] like '%" & Ne & "%' "
            End If
        End If
        sql = sql & sql2 & " order by [Employee_ID]"
        Dim dt As Data.DataTable = DB_HR.GetDataTable(sql)
        Grid1.DataSource = dt
        Grid1.DataBind()
        Grid1.CurrentPageIndex = 0
    End Sub

    
    Protected Sub Grid_paging(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles Grid1.PageIndexChanged
        Grid1.CurrentPageIndex = e.NewPageIndex
        loaddata(txtCode.Text, TxtName.Text)
    End Sub


    Protected Sub Sec_Index(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles Grid1.ItemCommand
        Dim itemCell As String
        If e.CommandSource.commandname = "Click_Code" Then
            Grid1.SelectedIndex = e.Item.ItemIndex
            itemCell = Grid1.DataKeys.Item(e.Item.ItemIndex)
            Dim strScript As String = "<script>window.opener.document.forms(0)." + control.Value + ".value = '"

            strScript += Trim(itemCell)  '(Grid1.DataKeys.Item(Index))
            strScript += "';self.close()"
            strScript += "</" + "script>"

            RegisterClientScriptBlock("GetValue", strScript)

            '  <a 
            'href = "javascript:;"
            'onclick = "window.open('BrowserEmployee.aspx?textbox=txtID','self','width=570,height=500,left=270,top=180')" <> img
            '         border="0" class="style285" src="images/find.png" /></a>&nbsp;   


            '<script>window.opener.document.forms(0).txtID.value = '11157065';self.close()</script>
            ' Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "<script>window.close();</script>", False)

        End If

    End Sub

    Sub Chang_Index(ByVal sender As Object, ByVal e As EventArgs)
        Dim strScript As String = "<script>window.opener.document.forms(0)." + Control.Value + ".value = '"
        strScript += Trim(Index) '(Grid1.DataKeys.Item(Index))
        strScript += "';self.close()"
        strScript += "</" + "script>"
        RegisterClientScriptBlock("GetValue", strScript)
    End Sub

    Protected Sub SerachB_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles SerachB.Click
        loaddata(txtCode.Text, TxtName.Text)
    End Sub

    Protected Sub ClearB_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ClearB.Click
        txtCode.Text = ""
        TxtName.Text = ""
        loaddata("", "")
    End Sub

    Protected Sub txtCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCode.TextChanged
        loaddata(txtCode.Text, TxtName.Text)
    End Sub

    Protected Sub txtName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtName.TextChanged
        loaddata(txtCode.Text, TxtName.Text)
    End Sub

End Class