﻿Imports System.Drawing
Imports System.Data.SqlClient

Public Class Asset_Search
    Inherits System.Web.UI.Page
    Dim Cpage As Integer
    Dim DB_HR As New Connect_HR
    Dim sess As String
    Dim fname As String
    Dim sql As String
    Public Property ExportFormatType As Object
    Public Property ExportDestinationType As Object

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        loaddatatable()
    End Sub

    Protected Sub btn_Search_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btn_Search.Click
        Cpage = 0
        loaddatatable()
    End Sub

    Sub loaddatatable()
        Dim sqlconnection1 As New SqlConnection(DB_HR.sqlCon)
        Dim SqlCmd2 As New SqlCommand
        Dim SqlDataReader2 As SqlDataReader
        Dim sql As String = ""
        Dim ChangeDateSQL2 As String = ""
        Dim i As Integer
        Dim sql2 As String = ""

        sql = " SELECT ROW_NUMBER() OVER(ORDER BY asset.[Employee_id] DESC) AS ID  ,asset.[Employee_id] "
        sql += " ,emp.fullname ,dep.department_name as department"
        sql += " ,asset.[type] ,asset.[AssetName] "
        sql += " ,asset.[serial] ,asset.[asset] , asset.MIS_asset , asset.location ,asset.[datePur] ,asset.[Remark]"
        sql += " FROM [dbo].[TB_Asset] asset "
        sql += " left outer join TB_Employee emp on emp.Employee_ID = asset.Employee_id COLLATE SQL_Latin1_General_CP1_CI_AS "
        sql += " left outer join TB_department dep on dep.department_id = emp.department_id COLLATE SQL_Latin1_General_CP1_CI_AS  "

        sql += " Where 1=1 "

        If txtEmpId.Text <> "" Then
            sql += " and asset.[Employee_id]  like '%" & Trim(txtEmpId.Text) & "%'"
        End If

        If txtName.Text <> "" Then
            sql += " and emp.fullname like '%" & Trim(txtName.Text) & "%'"
        End If

        If drpDep.Text <> "0" Then
            sql += " and dep.department_name = '" & Trim(drpDep.Text) & "'"
        End If


        sqlconnection1 = New SqlConnection(DB_HR.sqlCon)
        sqlconnection1.Open()

        SqlCmd2.Connection = sqlconnection1
        SqlCmd2.CommandTimeout = 0
        SqlCmd2.CommandText = sql
        SqlDataReader2 = SqlCmd2.ExecuteReader

        setheadSearch()

        While SqlDataReader2.Read()
            Try
                Dim RowNew As New TableRow


                Dim CellNew2 As New TableCell
                If SqlDataReader2.GetValue(0) = 0 Then
                    CellNew2.Text = ""
                Else
                    CellNew2.Text = SqlDataReader2.GetValue(0)
                End If
                If i Mod 2 <> 0 Then
                    CellNew2.BackColor = Color.FromArgb(255, 245, 245, 245)
                Else
                    CellNew2.BackColor = Color.FromArgb(255, 231, 231, 231)
                End If
                CellNew2.Font.Size = FontUnit.Small
                RowNew.Cells.Add(CellNew2)


                Dim CellNew12 As New TableCell
                Dim ImgBtt3 As New HyperLink
                If SqlDataReader2.GetValue(1) = "" Then
                    CellNew12.Text = ""
                Else
                    ImgBtt3.Text = SqlDataReader2.GetValue(1)
                End If
                ImgBtt3.NavigateUrl = "Asset_Main.aspx?id=" + SqlDataReader2.GetValue(1).ToString + "&mode=update"

                ' ImgBtt3.Target = "New"

                If i Mod 2 <> 0 Then
                    CellNew12.BackColor = Color.FromArgb(255, 245, 245, 245)
                Else
                    CellNew12.BackColor = Color.FromArgb(255, 231, 231, 231)
                End If
                CellNew12.Controls.Add(ImgBtt3)
                CellNew12.Font.Size = FontUnit.Small
                CellNew12.Font.Bold = True
                CellNew12.HorizontalAlign = HorizontalAlign.Center
                RowNew.Cells.Add(CellNew12)


                Dim CellNew3 As New TableCell
                If SqlDataReader2.GetValue(2).ToString = "" Then
                    CellNew3.Text = ""
                Else
                    CellNew3.Text = SqlDataReader2.GetValue(2)
                End If
                If i Mod 2 <> 0 Then
                    CellNew3.BackColor = Color.FromArgb(255, 245, 245, 245)
                Else
                    CellNew3.BackColor = Color.FromArgb(255, 231, 231, 231)
                End If
                CellNew3.Font.Size = FontUnit.Small
                CellNew3.HorizontalAlign = HorizontalAlign.Center
                RowNew.Cells.Add(CellNew3)


                Dim CellNew4 As New TableCell
                If SqlDataReader2.GetValue(3).ToString = "" Then
                    CellNew4.Text = ""
                Else
                    CellNew4.Text = SqlDataReader2.GetValue(3)
                End If
                If i Mod 2 <> 0 Then
                    CellNew4.BackColor = Color.FromArgb(255, 245, 245, 245)
                Else
                    CellNew4.BackColor = Color.FromArgb(255, 231, 231, 231)
                End If
                CellNew4.Font.Size = FontUnit.Small
                CellNew4.HorizontalAlign = HorizontalAlign.Center
                RowNew.Cells.Add(CellNew4)


                Dim CellNew5 As New TableCell
                If SqlDataReader2.GetValue(4).ToString = "" Then
                    CellNew5.Text = ""
                Else
                    CellNew5.Text = SqlDataReader2.GetValue(4)
                End If
                If i Mod 2 <> 0 Then
                    CellNew5.BackColor = Color.FromArgb(255, 245, 245, 245)
                Else
                    CellNew5.BackColor = Color.FromArgb(255, 231, 231, 231)
                End If
                CellNew5.Font.Size = FontUnit.Small
                CellNew5.HorizontalAlign = HorizontalAlign.Center
                RowNew.Cells.Add(CellNew5)


                Dim CellNew6 As New TableCell
                If SqlDataReader2.GetValue(5).ToString = "" Then
                    CellNew6.Text = ""
                Else
                    CellNew6.Text = SqlDataReader2.GetValue(5)
                End If
                If i Mod 2 <> 0 Then
                    CellNew6.BackColor = Color.FromArgb(255, 245, 245, 245)
                Else
                    CellNew6.BackColor = Color.FromArgb(255, 231, 231, 231)
                End If
                CellNew6.Font.Size = FontUnit.Small
                CellNew6.HorizontalAlign = HorizontalAlign.Center
                RowNew.Cells.Add(CellNew6)


                Dim CellNew7 As New TableCell
                If SqlDataReader2.GetValue(6).ToString = "" Then
                    CellNew7.Text = ""
                Else
                    CellNew7.Text = SqlDataReader2.GetValue(6)
                End If
                If i Mod 2 <> 0 Then
                    CellNew7.BackColor = Color.FromArgb(255, 245, 245, 245)
                Else
                    CellNew7.BackColor = Color.FromArgb(255, 231, 231, 231)
                End If
                CellNew7.Font.Size = FontUnit.Small
                RowNew.Cells.Add(CellNew7)

                Dim CellNew8 As New TableCell
                If SqlDataReader2.GetValue(7).ToString = "" Then
                    CellNew8.Text = ""
                Else
                    CellNew8.Text = SqlDataReader2.GetValue(7)
                End If
                If i Mod 2 <> 0 Then
                    CellNew8.BackColor = Color.FromArgb(255, 245, 245, 245)
                Else
                    CellNew8.BackColor = Color.FromArgb(255, 231, 231, 231)
                End If
                CellNew8.Font.Size = FontUnit.Small
                RowNew.Cells.Add(CellNew8)


                Dim CellNew11 As New TableCell
                If SqlDataReader2.GetValue(8).ToString = "" Then
                    CellNew11.Text = ""
                Else
                    CellNew11.Text = SqlDataReader2.GetValue(8)
                End If
                If i Mod 2 <> 0 Then
                    CellNew11.BackColor = Color.FromArgb(255, 245, 245, 245)
                Else
                    CellNew11.BackColor = Color.FromArgb(255, 231, 231, 231)
                End If
                CellNew11.Font.Size = FontUnit.Small
                RowNew.Cells.Add(CellNew11)

                Dim CellNew10 As New TableCell
                If SqlDataReader2.GetValue(9).ToString = "" Then
                    CellNew10.Text = ""
                Else
                    CellNew10.Text = SqlDataReader2.GetValue(9)
                End If
                If i Mod 2 <> 0 Then
                    CellNew10.BackColor = Color.FromArgb(255, 245, 245, 245)
                Else
                    CellNew10.BackColor = Color.FromArgb(255, 231, 231, 231)
                End If
                CellNew10.Font.Size = FontUnit.Small
                RowNew.Cells.Add(CellNew10)


                Dim CellNew9 As New TableCell
                If SqlDataReader2.GetValue(10).ToString = "" Then
                    CellNew9.Text = ""
                Else
                    CellNew9.Text = SqlDataReader2.GetValue(10)
                End If
                If i Mod 2 <> 0 Then
                    CellNew9.BackColor = Color.FromArgb(255, 245, 245, 245)
                Else
                    CellNew9.BackColor = Color.FromArgb(255, 231, 231, 231)
                End If
                CellNew9.Font.Size = FontUnit.Small
                RowNew.Cells.Add(CellNew9)

                Dim CellNew13 As New TableCell
                If SqlDataReader2.GetValue(11).ToString = "" Then
                    CellNew13.Text = ""
                Else
                    CellNew13.Text = SqlDataReader2.GetValue(11)
                End If
                If i Mod 2 <> 0 Then
                    CellNew13.BackColor = Color.FromArgb(255, 245, 245, 245)
                Else
                    CellNew13.BackColor = Color.FromArgb(255, 231, 231, 231)
                End If
                CellNew13.Font.Size = FontUnit.Small
                RowNew.Cells.Add(CellNew13)


                Me.TableLevel1.Rows.Add(RowNew)
                i = i + 1
            Finally
            End Try
        End While
        SqlDataReader2.Close()
    End Sub

    Private Sub setheadSearch()
        Dim CellHead1 As New TableCell
        Dim CellHead2 As New TableCell
        Dim CellHead3 As New TableCell
        Dim CellHead4 As New TableCell
        Dim CellHead5 As New TableCell
        Dim CellHead6 As New TableCell
        Dim CellHead7 As New TableCell
        Dim CellHead8 As New TableCell
        Dim CellHead9 As New TableCell
        Dim CellHead10 As New TableCell
        Dim CellHead11 As New TableCell
        Dim CellHead12 As New TableCell
        Dim CellHead13 As New TableCell
        Dim CellHead14 As New TableCell
        Dim CellHead15 As New TableCell
        Dim CellHead16 As New TableCell
        Dim Rowhead As New TableRow

        Me.TableLevel1.Rows.Clear()

        CellHead1.Text = "No"
        CellHead1.BackColor = Color.FromKnownColor(KnownColor.GrayText)
        CellHead1.Font.Bold = True
        CellHead1.Font.Size = FontUnit.Small
        CellHead1.Width = 20
        Rowhead.Cells.Add(CellHead1)

        CellHead2.Text = "รหัสพนักงาน"
        CellHead2.Font.Bold = True
        CellHead2.Font.Size = FontUnit.Small
        CellHead2.BackColor = Color.FromKnownColor(KnownColor.GrayText)
        CellHead2.Width = 100
        Rowhead.Cells.Add(CellHead2)

        CellHead3.Text = "ชื่อ-นามสกุล"
        CellHead3.Font.Bold = True
        CellHead3.Font.Size = FontUnit.Small
        CellHead3.BackColor = Color.FromKnownColor(KnownColor.GrayText)
        CellHead3.Width = 150
        Rowhead.Cells.Add(CellHead3)

        CellHead4.Text = "หน่วยงาน"
        CellHead4.Font.Bold = True
        CellHead4.Font.Size = FontUnit.Small
        CellHead4.BackColor = Color.FromKnownColor(KnownColor.GrayText)
        CellHead4.Width = 150
        Rowhead.Cells.Add(CellHead4)

        CellHead5.Text = "ประเภททรัพย์สิน"
        CellHead5.Font.Bold = True
        CellHead5.Font.Size = FontUnit.Small
        CellHead5.BackColor = Color.FromKnownColor(KnownColor.GrayText)
        CellHead5.Width = 140
        Rowhead.Cells.Add(CellHead5)

        CellHead6.Text = "ชื่ออุปกรณ์"
        CellHead6.Font.Bold = True
        CellHead6.Font.Size = FontUnit.Small
        CellHead6.BackColor = Color.FromKnownColor(KnownColor.GrayText)
        CellHead6.Width = 100
        Rowhead.Cells.Add(CellHead6)

        CellHead7.Text = "Serial"
        CellHead7.Font.Bold = True
        CellHead7.Font.Size = FontUnit.Small
        CellHead7.BackColor = Color.FromKnownColor(KnownColor.GrayText)
        CellHead7.Width = 100
        Rowhead.Cells.Add(CellHead7)

        CellHead8.Text = "Asset"
        CellHead8.Font.Bold = True
        CellHead8.Font.Size = FontUnit.Small
        CellHead8.BackColor = Color.FromKnownColor(KnownColor.GrayText)
        CellHead8.Width = 60
        Rowhead.Cells.Add(CellHead8)

        CellHead11.Text = "MIS Asset"
        CellHead11.Font.Bold = True
        CellHead11.Font.Size = FontUnit.Small
        CellHead11.BackColor = Color.FromKnownColor(KnownColor.GrayText)
        CellHead11.Width = 60
        Rowhead.Cells.Add(CellHead11)

        CellHead9.Text = "Location"
        CellHead9.Font.Bold = True
        CellHead9.Font.Size = FontUnit.Small
        CellHead9.BackColor = Color.FromKnownColor(KnownColor.GrayText)
        CellHead9.Width = 60
        Rowhead.Cells.Add(CellHead9)

        CellHead12.Text = "วันที่ซื้อ"
        CellHead12.Font.Bold = True
        CellHead12.Font.Size = FontUnit.Small
        CellHead12.BackColor = Color.FromKnownColor(KnownColor.GrayText)
        CellHead12.Width = 60
        Rowhead.Cells.Add(CellHead12)

        CellHead10.Text = "หมายเหตุ"
        CellHead10.Font.Bold = True
        CellHead10.Font.Size = FontUnit.Small
        CellHead10.BackColor = Color.FromKnownColor(KnownColor.GrayText)
        CellHead10.Width = 100
        Rowhead.Cells.Add(CellHead10)

        Me.TableLevel1.Rows.Add(Rowhead)
    End Sub

    Protected Sub btnNew_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btnNew.Click
        Server.Transfer("Asset_Main.aspx?&mode=new")
    End Sub


    Protected Sub btnExport_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btnExport.Click
        Loaddata_Excel()
        Response.Clear()
        Response.Charset = ""
        Response.ContentEncoding = System.Text.Encoding.UTF8
        Response.Cache.SetCacheability(HttpCacheability.NoCache)

        Response.AddHeader("content-disposition", "attachment;filename=AssetData.xls")

        Response.ContentType = "application/vnd.xls"

        Dim swWriter As New System.IO.StringWriter

        Dim htwWriter As New System.Web.UI.HtmlTextWriter(swWriter)

        TableLevel1.RenderControl(htwWriter)

        Response.Write("<html><head><META http-equiv=Content-Type content=text/html; charset=utf-8></head><body><FONT face=Tahoma size=8px>" & swWriter.ToString() & "</FONT></Body></Html>")
        Response.End()


        'Dim sqlReport As String = " 1=1 "

        ''If txtssDate.Text <> "" And txteDate.Text <> "" Then
        ''    sqlReport += " and {ReportData.DateForm} >= '" & DateTime.Parse(txtssDate.Text).ToString("yyyy-MM-dd") & "' and {ReportData.DateForm} <= '" & DateTime.Parse(txteDate.Text).ToString("yyyy-MM-dd") & "'"
        ''ElseIf txtssDate.Text <> "" And txteDate.Text = "" Then
        ''    sqlReport += " and {ReportData.DateForm} >= '" & DateTime.Parse(txtssDate.Text).ToString("yyyy-MM-dd") & "'"
        ''ElseIf txtssDate.Text = "" And txteDate.Text <> "" Then
        ''    sqlReport += " and {ReportData.DateForm} <= '" & DateTime.Parse(txteDate.Text).ToString("yyyy-MM-dd") & "'"
        ''End If


        'If txtEmpId.Text <> "" Then
        '    sqlReport += " and {ReportData.Employee_id} = '" & Trim(txtEmpId.Text) & "'"
        'End If

        'If txtName.Text <> "" Then
        '    sqlReport += " and {ReportData.fullname} = '" & Trim(txtName.Text) & "'"
        'End If

        'If drpDep.Text <> "0" Then
        '    sqlReport += " and {ReportData.department} = '" & Trim(drpDep.Text) & "'"
        'End If


        'Server.Transfer("LoadReport.aspx?Form=Asset&sql=" & sqlReport)
    End Sub

    Sub Loaddata_Excel()
        Dim sqlconnection1 As SqlConnection
        Dim SqlCmd2 As New SqlCommand
        Dim SqlDataReader2 As SqlDataReader


        Dim sql As String

        Dim ChangeDateSQL2 As String = ""
        Dim i As Integer
        sql = " SELECT ROW_NUMBER() OVER(ORDER BY asset.[Employee_id] DESC) AS ID  ,asset.[Employee_id] "
        sql += " ,emp.fullname ,dep.department_name as  department"
        sql += " ,asset.[type] ,asset.[AssetName] "
        sql += " ,asset.[serial] ,asset.[asset] , asset.MIS_asset , asset.location ,asset.[datePur] ,asset.[Remark]"
        sql += " FROM [dbo].[TB_Asset] asset "
        sql += " left outer join TB_Employee emp on emp.Employee_ID = asset.Employee_id COLLATE SQL_Latin1_General_CP1_CI_AS "
        sql += " left outer join TB_department dep on dep.department_id = emp.department_id COLLATE SQL_Latin1_General_CP1_CI_AS  "

        sql += " Where 1=1 "

        If txtEmpId.Text <> "" Then
            sql += " and asset.[Employee_id]  like '%" & Trim(txtEmpId.Text) & "%'"
        End If

        If txtName.Text <> "" Then
            sql += " and emp.fullname like '%" & Trim(txtName.Text) & "%'"
        End If

        If drpDep.Text <> "0" Then
            sql += " and dep.department_name  = '" & Trim(drpDep.Text) & "'"
        End If


        sqlconnection1 = New SqlConnection(DB_HR.sqlCon)
        sqlconnection1.Open()

        SqlCmd2.Connection = sqlconnection1
        SqlCmd2.CommandTimeout = 0
        SqlCmd2.CommandText = sql
        SqlDataReader2 = SqlCmd2.ExecuteReader

        setheadSearch()

        While SqlDataReader2.Read()
            Try
                Dim RowNew As New TableRow


                Dim CellNew2 As New TableCell
                If SqlDataReader2.GetValue(0) = 0 Then
                    CellNew2.Text = ""
                Else
                    CellNew2.Text = SqlDataReader2.GetValue(0)
                End If
                If i Mod 2 <> 0 Then
                    CellNew2.BackColor = Color.FromArgb(255, 245, 245, 245)
                Else
                    CellNew2.BackColor = Color.FromArgb(255, 231, 231, 231)
                End If
                CellNew2.Font.Size = FontUnit.Small
                RowNew.Cells.Add(CellNew2)


                Dim CellNew12 As New TableCell
                Dim ImgBtt3 As New HyperLink
                If SqlDataReader2.GetValue(1) = "" Then
                    CellNew12.Text = ""
                Else
                    ImgBtt3.Text = SqlDataReader2.GetValue(1)
                End If
                ImgBtt3.NavigateUrl = "Asset_Main.aspx?id=" + SqlDataReader2.GetValue(1).ToString + "&mode=update"

                ' ImgBtt3.Target = "New"

                If i Mod 2 <> 0 Then
                    CellNew12.BackColor = Color.FromArgb(255, 245, 245, 245)
                Else
                    CellNew12.BackColor = Color.FromArgb(255, 231, 231, 231)
                End If
                CellNew12.Controls.Add(ImgBtt3)
                CellNew12.Font.Size = FontUnit.Small
                CellNew12.Font.Bold = True
                CellNew12.HorizontalAlign = HorizontalAlign.Center
                RowNew.Cells.Add(CellNew12)


                Dim CellNew3 As New TableCell
                If SqlDataReader2.GetValue(2).ToString = "" Then
                    CellNew3.Text = ""
                Else
                    CellNew3.Text = SqlDataReader2.GetValue(2)
                End If
                If i Mod 2 <> 0 Then
                    CellNew3.BackColor = Color.FromArgb(255, 245, 245, 245)
                Else
                    CellNew3.BackColor = Color.FromArgb(255, 231, 231, 231)
                End If
                CellNew3.Font.Size = FontUnit.Small
                CellNew3.HorizontalAlign = HorizontalAlign.Center
                RowNew.Cells.Add(CellNew3)


                Dim CellNew4 As New TableCell
                If SqlDataReader2.GetValue(3).ToString = "" Then
                    CellNew4.Text = ""
                Else
                    CellNew4.Text = SqlDataReader2.GetValue(3)
                End If
                If i Mod 2 <> 0 Then
                    CellNew4.BackColor = Color.FromArgb(255, 245, 245, 245)
                Else
                    CellNew4.BackColor = Color.FromArgb(255, 231, 231, 231)
                End If
                CellNew4.Font.Size = FontUnit.Small
                CellNew4.HorizontalAlign = HorizontalAlign.Center
                RowNew.Cells.Add(CellNew4)


                Dim CellNew5 As New TableCell
                If SqlDataReader2.GetValue(4).ToString = "" Then
                    CellNew5.Text = ""
                Else
                    CellNew5.Text = SqlDataReader2.GetValue(4)
                End If
                If i Mod 2 <> 0 Then
                    CellNew5.BackColor = Color.FromArgb(255, 245, 245, 245)
                Else
                    CellNew5.BackColor = Color.FromArgb(255, 231, 231, 231)
                End If
                CellNew5.Font.Size = FontUnit.Small
                CellNew5.HorizontalAlign = HorizontalAlign.Center
                RowNew.Cells.Add(CellNew5)


                Dim CellNew6 As New TableCell
                If SqlDataReader2.GetValue(5).ToString = "" Then
                    CellNew6.Text = ""
                Else
                    CellNew6.Text = SqlDataReader2.GetValue(5)
                End If
                If i Mod 2 <> 0 Then
                    CellNew6.BackColor = Color.FromArgb(255, 245, 245, 245)
                Else
                    CellNew6.BackColor = Color.FromArgb(255, 231, 231, 231)
                End If
                CellNew6.Font.Size = FontUnit.Small
                CellNew6.HorizontalAlign = HorizontalAlign.Center
                RowNew.Cells.Add(CellNew6)


                Dim CellNew7 As New TableCell
                If SqlDataReader2.GetValue(6).ToString = "" Then
                    CellNew7.Text = ""
                Else
                    CellNew7.Text = SqlDataReader2.GetValue(6)
                End If
                If i Mod 2 <> 0 Then
                    CellNew7.BackColor = Color.FromArgb(255, 245, 245, 245)
                Else
                    CellNew7.BackColor = Color.FromArgb(255, 231, 231, 231)
                End If
                CellNew7.Font.Size = FontUnit.Small
                RowNew.Cells.Add(CellNew7)

                Dim CellNew8 As New TableCell
                If SqlDataReader2.GetValue(7).ToString = "" Then
                    CellNew8.Text = ""
                Else
                    CellNew8.Text = SqlDataReader2.GetValue(7)
                End If
                If i Mod 2 <> 0 Then
                    CellNew8.BackColor = Color.FromArgb(255, 245, 245, 245)
                Else
                    CellNew8.BackColor = Color.FromArgb(255, 231, 231, 231)
                End If
                CellNew8.Font.Size = FontUnit.Small
                RowNew.Cells.Add(CellNew8)


                Dim CellNew11 As New TableCell
                If SqlDataReader2.GetValue(8).ToString = "" Then
                    CellNew11.Text = ""
                Else
                    CellNew11.Text = SqlDataReader2.GetValue(8)
                End If
                If i Mod 2 <> 0 Then
                    CellNew11.BackColor = Color.FromArgb(255, 245, 245, 245)
                Else
                    CellNew11.BackColor = Color.FromArgb(255, 231, 231, 231)
                End If
                CellNew11.Font.Size = FontUnit.Small
                RowNew.Cells.Add(CellNew11)

                Dim CellNew10 As New TableCell
                If SqlDataReader2.GetValue(9).ToString = "" Then
                    CellNew10.Text = ""
                Else
                    CellNew10.Text = SqlDataReader2.GetValue(9)
                End If
                If i Mod 2 <> 0 Then
                    CellNew10.BackColor = Color.FromArgb(255, 245, 245, 245)
                Else
                    CellNew10.BackColor = Color.FromArgb(255, 231, 231, 231)
                End If
                CellNew10.Font.Size = FontUnit.Small
                RowNew.Cells.Add(CellNew10)


                Dim CellNew9 As New TableCell
                If SqlDataReader2.GetValue(10).ToString = "" Then
                    CellNew9.Text = ""
                Else
                    CellNew9.Text = SqlDataReader2.GetValue(10)
                End If
                If i Mod 2 <> 0 Then
                    CellNew9.BackColor = Color.FromArgb(255, 245, 245, 245)
                Else
                    CellNew9.BackColor = Color.FromArgb(255, 231, 231, 231)
                End If
                CellNew9.Font.Size = FontUnit.Small
                RowNew.Cells.Add(CellNew9)

                Dim CellNew13 As New TableCell
                If SqlDataReader2.GetValue(11).ToString = "" Then
                    CellNew13.Text = ""
                Else
                    CellNew13.Text = SqlDataReader2.GetValue(11)
                End If
                If i Mod 2 <> 0 Then
                    CellNew13.BackColor = Color.FromArgb(255, 245, 245, 245)
                Else
                    CellNew13.BackColor = Color.FromArgb(255, 231, 231, 231)
                End If
                CellNew13.Font.Size = FontUnit.Small
                RowNew.Cells.Add(CellNew13)


                Me.TableLevel1.Rows.Add(RowNew)
                i = i + 1
            Finally
            End Try
        End While
        SqlDataReader2.Close()
    End Sub

    Sub Sethead_Excel()

        Dim CellHead1 As New TableCell
        Dim CellHead2 As New TableCell
        Dim CellHead3 As New TableCell
        Dim CellHead31 As New TableCell
        Dim CellHead4 As New TableCell
        Dim CellHead41 As New TableCell
        Dim CellHead5 As New TableCell
        Dim CellHead51 As New TableCell
        Dim CellHead6 As New TableCell
        Dim CellHead7 As New TableCell
        Dim CellHead8 As New TableCell
        Dim CellHead9 As New TableCell
        Dim CellHead10 As New TableCell
        Dim CellHead11 As New TableCell
        Dim CellHead12 As New TableCell
        Dim CellHead13 As New TableCell
        Dim CellHead14 As New TableCell
        Dim CellHead15 As New TableCell
        Dim CellHead16 As New TableCell
        Dim CellHead17 As New TableCell
        Dim CellHead18 As New TableCell
        Dim CellHead19 As New TableCell
        Dim CellHead20 As New TableCell
        Dim CellHead21 As New TableCell
        Dim CellHead22 As New TableCell
        Dim CellHeadPrjNo As New TableCell
        Dim CellHead23 As New TableCell
        Dim cellhead24 As New TableCell
        Dim cellhead25 As New TableCell
        Dim CellImportCost As New TableCell
        Dim CellTotalAmount As New TableCell
        Dim cellhead32 As New TableCell
        Dim cellhead33 As New TableCell
        Dim cellhead34 As New TableCell
        Dim cellhead35 As New TableCell
        Dim cellhead36 As New TableCell
        Dim cellhead37 As New TableCell
        Dim NeedByDate As New TableCell

        Dim Rowhead As New TableRow

        Me.TableLevel1.Rows.Clear()

        CellHead1.Text = "No"
        CellHead1.BackColor = Color.FromKnownColor(KnownColor.GrayText)
        CellHead1.Font.Bold = True
        CellHead1.Font.Size = FontUnit.Small
        CellHead1.Width = 20
        Rowhead.Cells.Add(CellHead1)

        CellHead2.Text = "รหัสพนักงาน"
        CellHead2.Font.Bold = True
        CellHead2.Font.Size = FontUnit.Small
        CellHead2.BackColor = Color.FromKnownColor(KnownColor.GrayText)
        CellHead2.Width = 100
        Rowhead.Cells.Add(CellHead2)

        CellHead3.Text = "ชื่อ-นามสกุล"
        CellHead3.Font.Bold = True
        CellHead3.Font.Size = FontUnit.Small
        CellHead3.BackColor = Color.FromKnownColor(KnownColor.GrayText)
        CellHead3.Width = 150
        Rowhead.Cells.Add(CellHead3)

        CellHead4.Text = "หน่วยงาน"
        CellHead4.Font.Bold = True
        CellHead4.Font.Size = FontUnit.Small
        CellHead4.BackColor = Color.FromKnownColor(KnownColor.GrayText)
        CellHead4.Width = 150
        Rowhead.Cells.Add(CellHead4)

        CellHead5.Text = "ประเภททรัพย์สิน"
        CellHead5.Font.Bold = True
        CellHead5.Font.Size = FontUnit.Small
        CellHead5.BackColor = Color.FromKnownColor(KnownColor.GrayText)
        CellHead5.Width = 140
        Rowhead.Cells.Add(CellHead5)

        CellHead6.Text = "ชื่ออุปกรณ์"
        CellHead6.Font.Bold = True
        CellHead6.Font.Size = FontUnit.Small
        CellHead6.BackColor = Color.FromKnownColor(KnownColor.GrayText)
        CellHead6.Width = 100
        Rowhead.Cells.Add(CellHead6)

        CellHead7.Text = "Serial"
        CellHead7.Font.Bold = True
        CellHead7.Font.Size = FontUnit.Small
        CellHead7.BackColor = Color.FromKnownColor(KnownColor.GrayText)
        CellHead7.Width = 100
        Rowhead.Cells.Add(CellHead7)

        CellHead8.Text = "Asset"
        CellHead8.Font.Bold = True
        CellHead8.Font.Size = FontUnit.Small
        CellHead8.BackColor = Color.FromKnownColor(KnownColor.GrayText)
        CellHead8.Width = 60
        Rowhead.Cells.Add(CellHead8)

        CellHead11.Text = "MIS Asset"
        CellHead11.Font.Bold = True
        CellHead11.Font.Size = FontUnit.Small
        CellHead11.BackColor = Color.FromKnownColor(KnownColor.GrayText)
        CellHead11.Width = 60
        Rowhead.Cells.Add(CellHead11)

        CellHead9.Text = "Location"
        CellHead9.Font.Bold = True
        CellHead9.Font.Size = FontUnit.Small
        CellHead9.BackColor = Color.FromKnownColor(KnownColor.GrayText)
        CellHead9.Width = 60
        Rowhead.Cells.Add(CellHead9)

        CellHead12.Text = "วันที่ซื้อ"
        CellHead12.Font.Bold = True
        CellHead12.Font.Size = FontUnit.Small
        CellHead12.BackColor = Color.FromKnownColor(KnownColor.GrayText)
        CellHead12.Width = 60
        Rowhead.Cells.Add(CellHead12)

        CellHead10.Text = "หมายเหตุ"
        CellHead10.Font.Bold = True
        CellHead10.Font.Size = FontUnit.Small
        CellHead10.BackColor = Color.FromKnownColor(KnownColor.GrayText)
        CellHead10.Width = 100
        Rowhead.Cells.Add(CellHead10)

        Me.TableLevel1.Rows.Add(Rowhead)

    End Sub

    Protected Sub drpDep_Init(sender As Object, e As EventArgs) Handles drpDep.Init
        Dim sqlCon As New SqlConnection(DB_HR.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String
        sql = "select department_name from TB_department group by department_name order by department_name "
        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_HR.sqlCon)
            sqlCon.Open()
        End If

        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With

        drpDep.Items.Clear()
        drpDep.Items.Add(New ListItem("", 0))
        While Rs.Read
            'txtID.DataTextField = "id"
            'txtID.DataValueField = "fullname"
            ' txtID.Items.Add(New ListItem(Str(Rs.GetString(1)), Rs.GetString(1)))
            drpDep.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
            'txtID.Items.Add(New ListItem(Str(Rs.GetString(0)), Rs.GetString(0)))
            'txtID.Items.Add(New ListItem(Str(Rs.GetString(0)), Rs.GetString(1), Rs.GetString(0)))

        End While
    End Sub
End Class