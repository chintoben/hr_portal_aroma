﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_WhoisWho1.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_Contact" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/HR.jpg" alt=""/></div>
<!-- end banner -->

<section id="team-section">
	<div class="container">
		<div class="theme-title">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
                <li>/</li>
                <li><a href="HRPolicy_WhoisWho.aspx">ติดต่อฝ่าย-แผนกต่างๆ</a></li>
            </ul>
			<h2>รายชื่อติดต่อ-สำนักคณะกรรมการ, เลขานุการ (Board Director)</h2>
			<!--p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p-->
		</div> <!-- /.theme-title -->

		<div class="clear-fix team-member-wrapper">
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who/BoradDirector/คุณจินตนา%20%20กิ่งก้าน.jpg" alt="" 
                            height="150"/>
						<div class="opacity tran4s">
			    			<h4>คุณจินตนา กิ่งก้าน (จิน)</h4>
							<span>รักษาการณ์ผู้อำนวยการฝ่ายบัญชีและการเงิน</span>
							<p>
								เบอร์ต่อภายใน : 7746<br />
                    			<%--โทรศัพท์มือถือ : -<br />--%>
                    			Email : <a href="mailto:jintanak@aromathailand.com">jintanak@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณจินตนา กิ่งก้าน (จิน)</h5>
						<p>รักษาการณ์ผู้อำนวยการฝ่ายบัญชีและการเงิน</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->

			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who/BoradDirector/คุณสุริยาพร%20%20ดารารักษ์.jpg" alt="" 
                            height="150"/>
						<div class="opacity tran4s">
							<h4>คุณสุริยาพร ดารารักษ์ (แมว)</h4>
							<span>เลขานุการกรรมการบริหาร</span>
							<p>
								เบอร์ต่อภายใน : 7745<br />
                    			<%--โทรศัพท์มือถือ : -<br />--%>
                    			Email : <a href="mailto:suriyapornd@aromathailand.com">suriyapornd@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณสุริยาพร ดารารักษ์ (แมว)</h5>
						<p>เลขานุการกรรมการบริหาร</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->

			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who/BoradDirector/คุณกนิษฐา%20%20แกล้วกล้า.jpg" alt="" 
                            height="150"/>
						<div class="opacity tran4s">
							<h4>คุณกนิษฐา แกล้วกล้า (อิ๋ว)</h4>
							<span>เลขานุการกรรมการบริหาร</span>
							<p>
								เบอร์ต่อภายใน : 142<br />
                    			<%--โทรศัพท์มือถือ : -<br />--%>
                    			Email : <a href="mailto:kanithak@aromathailand.com">kanithak@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณกนิษฐา แกล้วกล้า (อิ๋ว)</h5>
						<p>เลขานุการกรรมการบริหาร</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->
			
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who2019/1.board%20director/คุณสุมาลี%20%20จีระสันติกุล.jpg" 
                            alt="" height="150"/>
						<div class="opacity tran4s">
							<h4>คุณสุมาลี จีระสันติกุล (ลี)</h4>
							<span>หัวหน้าแผนก</span>
							<p>
								เบอร์ต่อภายใน : 7745<br />
                        		<%--โทรศัพท์มือถือ : 081-6164697<br />--%>
                        		Email : <a href="mailto:sumaleej@aromathailand.com">sumaleej@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณสุมาลี จีระสันติกุล (ลี)</h5>
						<p>หัวหน้าแผนก</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->
			
		</div> <!-- /.team-member-wrapper -->		
					
		<div class="blog-category-bt">
			<div class="btn-group">
				<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
			</div>
		</div>
		
	</div> <!-- /.conatiner -->
</section>
 
</asp:Content>
