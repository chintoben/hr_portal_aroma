﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Login_new.aspx.vb" Inherits="Aroma_HRPortal.Login_new" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        * {
            box-sizing: border-box;
        }

        body {
            background-color: #EEE;
        }

        .clear {
            clear: both;
        }

        .log {
            width: 400px;
            margin: 5% auto;
            background-color: #FFF;
            padding: 30px 0;
        }

            .log h2 {
                text-align: center;
                color: #ed2553;
                font-weight: bold;
                font-size: 26px;
                margin-bottom: 50px;
            }

            .log .input-cont {
                position: relative;
                margin: 0 50px 60px;
            }

                .log .input-cont:last-of-type {
                    margin-bottom: 30px;
                }

                .log .input-cont input {
                    position: relative;
                    z-index: 1;
                    width: 100%;
                    height: 40px;
                    outline: none;
                    color: #212121;
                    font-size: 22px;
                    font-weight: 400;
                    background: none;
                    border: none;
                }

                    .log .input-cont input:focus {
                        outline: none;
                    }

                .log .input-cont label {
                    position: absolute;
                    color: #948c8c;
                    top: 0;
                    left: 0;
                    line-height: 40px;
                    -webkit-transition: .3s;
                    -moz-transition: .3s;
                    -o-transition: .3s;
                    transition: .3s;
                }

                .log .input-cont input:focus + label {
                    margin-top: -30px;
                    -webkit-transform: scale(.8);
                    -moz-transform: scale(.8);
                    -o-transform: scale(.8);
                    transform: scale(.8);
                    color: #bdbdbd;
                }

            .log .border1,
            .log .border2 {
                position: absolute;
                height: 1px;
                background-color: #9E9E9E;
                left: 0;
                bottom: 0;
                width: 100%;
            }

                .log .border1::after,
                .log .border1::before,
                .log .border2::after,
                .log .border2::before {
                    content: "";
                    position: absolute;
                    bottom: 0;
                    width: 0;
                    height: 2px;
                    -webkit-transition: .5s;
                    -moz-transition: .5s;
                    -o-transition: .5s;
                    transition: .5s;
                }

                .log .border1::after,
                .log .border2::after {
                    right: 50%;
                    background-color: #ed2553;
                }

                .log .border1::before,
                .log .border2::before {
                    left: 50%;
                    background-color: #ed2553;
                }

            .log .input-cont input:focus ~ .border1::after,
            .log .input-cont input:focus ~ .border1::before,
            .log .input-cont input:focus ~ .border2::after,
            .log .input-cont input:focus ~ .border2::before {
                width: 50%;
            }

            .log .check,
            .log a {
                float: left;
                width: calc(50% - 50px);
                display: block;
                font-size: 12px;
                margin-bottom: 30px;
            }

            .log .check {
                margin-left: 50px;
            }

            .log a {
                text-align: right;
                text-decoration: none;
                color: #ed2553;
            }

                .log a:hover {
                    text-decoration: underline;
                    color: #F00;
                }

        .newboxshadow {
            box-shadow: 0 2px 4px 0 rgba(0,0,0,0.3);
        }
    </style>

    <style>
        .wave-btn {
            height: 60px;
            border-radius: 10px 10px 0px 10px;
            transition: all 0.8s ease 0s;
            text-decoration: none;
            overflow: hidden;
        }




            .wave-btn:hover {
                border-radius: 10px;
                transition: all 0.8s ease 0s;
                box-shadow: 0 0 40px rgba(73, 115, 255, 0.6);
            }

                .wave-btn:hover .wave-btn__waves {
                    top: -50px;
                }




        @keyframes waves {
            0% {
                transform: translate3d(-50%, -96%, 0) rotate(0deg) scale(1);
            }

            100% {
                transform: translate3d(-50%, -96%, 0) rotate(360deg) scale(1);
            }
        }
    </style>

    <style>
        .modal-confirm {
            color: #636363;
            width: 400px;
        }

            .modal-confirm .modal-content {
                padding: 20px;
                border-radius: 5px;
                border: none;
                text-align: center;
                font-size: 14px;
            }

            .modal-confirm .modal-header {
                border-bottom: none;
                position: relative;
            }

            .modal-confirm h4 {
                text-align: center;
                font-size: 26px;
                margin: 30px 0 -10px;
            }

            .modal-confirm .close {
                position: absolute;
                top: -5px;
                right: -2px;
            }

            .modal-confirm .modal-body {
                color: #999;
            }

            .modal-confirm .modal-footer {
                border: none;
                text-align: center;
                border-radius: 5px;
                font-size: 13px;
                padding: 10px 15px 25px;
            }

                .modal-confirm .modal-footer a {
                    color: #999;
                }

            .modal-confirm .icon-box {
                width: 80px;
                height: 80px;
                margin: 0 auto;
                border-radius: 50%;
                z-index: 9;
                text-align: center;
                border: 3px solid #f15e5e;
            }

                .modal-confirm .icon-box i {
                    color: #f15e5e;
                    font-size: 46px;
                    display: inline-block;
                    margin-top: 13px;
                }

            .modal-confirm .btn, .modal-confirm .btn:active {
                color: #fff;
                border-radius: 4px;
                background: #60c7c1;
                text-decoration: none;
                transition: all 0.4s;
                line-height: normal;
                min-width: 120px;
                border: none;
                min-height: 40px;
                border-radius: 3px;
                margin: 0 5px;
            }

            .modal-confirm .btn-secondary {
                background: #c1c1c1;
            }

                .modal-confirm .btn-secondary:hover, .modal-confirm .btn-secondary:focus {
                    background: #a8a8a8;
                }

            .modal-confirm .btn-danger {
                background: #f15e5e;
            }

                .modal-confirm .btn-danger:hover, .modal-confirm .btn-danger:focus {
                    background: #ee3535;
                }

        .trigger-btn {
            display: inline-block;
            margin: 100px auto;
        }
    </style>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

    <div class="log newboxshadow">
        <div class="input-cont" style="text-align: center;">
            <img src="Images/logotrans.png" style="margin-right: 5px; height: auto; width: 150px;">
            <h2 style="color: #336ea0ba; margin:22px;">HR PORTAL LOGIN</h2>
        </div>



        <div class="input-cont">
            <input id="txtusername" type="text" />
            <label>Username</label>
            <div class="border1"></div>
        </div>
        <div class="input-cont">
            <input id="txtpassword" type="password" />
            <label>Password</label>
            <div class="border2"></div>
        </div>

        <span class="check">
            <button id="btlogin" class="wave-btn" style="color: White; background-color: #990000; font-family: Tahoma; font-size: Medium; font-weight: bold; height: 30px; width: 100px;">Login</button>
        </span>
        <span class="">
            <input type="checkbox" id="chkrem">
            <label style="width: 0px; display: inherit;">Remember Me</label>
        </span>

        <div class="clear"></div>

    </div>

    <script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>

    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script>


        // var webapi = 'https://armapplication.com/SWC_Service';
        var webapi = 'http://localhost:26843';

        $(document).ready(function () {

            $('#btlogin').click(function () {
                getlogin();
            })
    
        });


        function getlogin() {

            $.ajax({
                type: "POST",
                url: webapi + "/Login_new.aspx/getlogin",
                data: '{username: "' + $("#txtusername").val() + '" , password: "' + $("#txtpassword").val() + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    console.log(response);
                    if (response.d === "") {
                        localStorage.removeItem('empid');
                        localStorage.removeItem('empname');
                        localStorage.removeItem('empdep');
                        $('#myModal').modal('toggle');

                    } else {
                        var data = JSON.parse(response.d)
                        var ttl = 86400000 // 1 วัน
                        if ($('#chkrem').prop('checked', true)) {
                            ttl = 31536000000;  // 604800000 7 วัน      31536000000  1 ปี
                        }

                        setWithExpiry('empid', data[0].employee_id, ttl)

                        localStorage.setItem('empname', data[0].fullname);
                        localStorage.setItem('empdep', data[0].department_name);
                        window.location.href = 'index_hr.aspx';

                    }

                },
                error: function (data) {
                    console.log("Error");
                }
            });
        }

        function setWithExpiry(key, value, ttl) {
            const now = new Date()

            const item = {
                value: value,
                expiry: now.getTime() + ttl,
            }
            localStorage.setItem(key, JSON.stringify(item))
        }
        function getWithExpiry(key) {
            const itemStr = localStorage.getItem(key)
            // if the item doesn't exist, return null
            if (!itemStr) {
                return null
            }
            const item = JSON.parse(itemStr)
            const now = new Date()
            // compare the expiry time of the item with the current time
            if (now.getTime() > item.expiry) {
                // If the item is expired, delete the item from storage
                // and return null
                localStorage.removeItem(key)
                return null
            }
            return item.value
        }
    </script>



    <!-- Modal HTML -->
    <div id="myModal" class="modal fade">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header flex-column">
                    <div class="icon-box">
                        <i class="material-icons">&#xE5CD;</i>
                    </div>
                    <h4 class="modal-title w-100">ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง !</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <p>กรุณากรอกชื่อผู้ใช้งานรหัสผ่านให้ถูกต้อง หรือแจ้งผู้ดูแลระบบ...</p>
                </div>
                <div class="modal-footer justify-content-center">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


</body>
</html>
