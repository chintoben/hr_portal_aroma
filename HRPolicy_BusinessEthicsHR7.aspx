﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_BusinessEthicsHR7.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_BusinessEthicsHR7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<style>
	.swiper-container {
		width: 100%;
		height: 100%;
	}
	.swiper-slide {
		text-align: center;
		background: #fff;
	}
	.swiper-slide img 
	{
		width: 100%;
	}
	.swiper-pagination-bullet-active {
		opacity: 1;
		background: #af2f2f;
	}
</style>

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/hr.jpg" alt=""/></div>
<!-- end banner -->

<article class="blog-details-page">
	<div class="container">
		
		<div class="post-heading">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
                <li>/</li>
                <li><a href="HRPolicy_Announce.aspx">ระเบียบต่างๆ</a></li>
                <li>/</li>
                <li><a href="HRPolicy_BusinessEthicsHR.aspx">ประกาศ คำสั่ง ระเบียบทรัพยากรมนุษย์</a></li></li>
            </ul>
			<h2>ทดลองจัดวันเวลาทำงานและวันหยุด (เป็นกรณีพิเศษ) ทำงานวันจันทร์ - ศุกร์</h2>
		</div> <!-- /.post-heading -->
		
		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix">
				<div class="blog-details-post-wrapper">
				
					<img src="new/images/banner/hr-policy/img11.jpg" alt=""/>
         		    
         		    <p style="padding: 30px 0 0">
         		    	เนื่องจากสภาพการจราจร การติดต่อราชการและบุคคลภายนอก รวมถึงสภาพการแข่งขันทางธุรกิจว่าด้วยการสรรหาและคัดเลือกพนักงาน และเพื่อให้การบริหารงานทรัพยากรมนุษย์ เป็นไปด้วยความระเบียบเรียบร้อย ทาง อโรม่า กรุ๊ป จึงเห็นสมควรทดลองจัดวันเวลาทำงานและวันหยุด เป็นกรณีพิเศษ) และประกาศแจ้งมายังพนักงานประจำสำนักงานใหญ่ ดังนี้
					</p>
					
					<ol>
						<li>ประกาศฉบับนี้บังคับใช้เฉพาะพนักงานซึ่งปฏิบัติงานเป็นการประจำสำนักงานใหญ่ งานประจำสำนักงานโรงงานบางปะกงเท่านั้น เนื่องจากเป็นงานด้านบริหาร งานจัดการ และงานสำนักงาน</li>
						<li>ประกาศฉบับนี้ไม่บังคับใช้พนักงานซึ่งปฏิบัติงานประจำสายการผลิต งานปฏิบัติการ/บริการลูกค้า/หน้าร้าน/สาขาทั้งกรุงเทพ-ปริมณฑล และต่างจังหวัด งานแนะนำสินค้า (พีซี) 
						 เนื่องจากเป็นงานที่ต้องผลิต บริการลูกค้าภายนอก งานที่ต้องทำติดต่อกันไป ถ้าหยุดจะเสียหายแก่งาน</li>
						<li>บริษัทฯ และพนักงานขอทดลองจัดวันเวลาทำงานและวันหยุดเป็นกรณีพิเศษ ตามระเบียบข้อบังคับการทำงาน ลงวันที่ 1 มิถุนายน 2551 จากวันทำงานปกติวันจันทร์ ถึง วันเสาร์ เวลา 08.00 - 
						 17.00 น. เวลาพัก 12.00 - 13.00 น. โดยขอนำเวลาทำงานของวันเสาร์ เวลา 08.00 – 17.00 น. มารวมกับเวลาทำงานในวันทำงานปกติวันจันทร์ – วันศุกร์ 
						 ซึ่งรวมเวลาทำงานสัปดาห์หนึ่งไม่เกินสี่สิบแปดชั่วโมงตามกฏหมายกำหนด ดังนี้
						 	<ul class="a">
						 		<li>เป็นวันทำงานปกติ วันจันทร์ ถึง วันศุกร์ เวลาทำงาน 08.00 – 18.00 น. เวลาพัก 12.00 - 13.00 น. วันหยุดประจำสัปดาห์ คือ วันเสาร์ และวันอาทิตย์</li>
								<li>กรณีพนักงานรายเดือนสำนักงาน ผู้บังคับบัญชาต้นสังกัดเห็นสมควรให้พนักงานปฏิบัติงานล่วงเวลาในวันทำงาน ทำงานในวันหยุดและล่วงเวลาในวันหยุดตามความจำเป็น เนื่องจากลักษณะ หรือสภาพของงานต้องทำติดต่อกันไป ถ้าหยุดจะเสียหายแก่งาน หรือเป็นงานฉุกเฉิน ให้นำเสนอผู้บังคับบัญชาตามระดับชั้น และกรรมการบริหารสายงานอนุมัติก่อนถึงจะได้รับค่าล่วงเวลาในวันทำงานค่าทำงานในวันหยุด และค่าล่วงเวลาในวันหยุด ยกเว้นพนักงานรายวันหรือรายชั่วโมง หรือพนักงานคำนวนตามผลงาน กรณีทำงานในวันทำงานปกติเกินกว่า 8 ชั่วโมง พนักงานมีสิทธิได้รับค่าตอบแทน 1.5 เท่าของอัตราค่าจ้างต่อชั่วโมงในวันทำงานตามจำนวนชั่วโมงที่ทำจริง และตามที่ได้รับมอบหมายเท่านั้น</li>
								<li>กรณีงานคลังสินค้า จัดส่ง ช่างเทคนิค ทางบริษัทฯ มีงานจำเป็นทำต่อเนื่อง งานด่วน ฉุกเฉิน ให้บริการลูกค้า งานที่ต้องทำติดต่อกันไป ถ้าหยุดจะเสียหายแก่งาน บริษัทฯมอบหมายให้
							 ผู้บริหารต้นสังกัด หรือผู้จัดการฝ่ายต้นสังกัดตามแต่กรณี เป็นผู้กำหนดให้พนักงานมาปฏิบัติงาน/จัดตารางการทำงาน หรือให้พนักงานมาทำงานในวันหยุด และทำงานล่วงเวลาในวันหยุด
							 ตามแต่กรณี โดยบริษัทฯ จะจ่ายค่าตอบแทนการทำงานในวันหยุด และค่าล่วงเวลา ในวันหยุดตามชั่วโมงที่ปฏิบัติ/ทำงานจริง และตามที่ได้รับมอบหมายเท่านั้น</li>
						 	</ul>
						</li>
						<li>กรณีบริษัทฯ มีการจัดกิจกรรม โครงการ ประชุม อบรม/สัมมนา เพื่อส่งเสริมความรู้ ทักษะ และประโยชน์อื่นๆ แก่พนักงาน บริษัทฯ ขอความร่วมมือและกำหนดให้พนักงานเข้าร่วมกิจกรรม โครงการ 
						 ประชุม อบรม/สัมมนาทุกท่าน หรือ ตามรายชื่อที่ได้รับแจ้ง โดยทางบริษัทจะเป็นผู้ออกค่าใช้จ่ายในงาน ค่าอาหาร เครื่องดื่ม หรือตามที่บริษัทฯ ประกาศแจ้งเป็นกรณีๆ ไป ให้แก่พนักงานแทนค่าทำงาน
						 วันหยุด และค่าล่วงเวลาในวันหยุด</li>
						<li>บริษัทฯ ขอสงวนไว้ซึ่งสิทธิที่จะแก้ไข เปลี่ยนแปลง และยกเลิกประกาศฉบับนี้ได้ตลอดเวลาตามเห็นสมควร</li>
					</ol>
					
					<p><br>
						ทั้งนี้ ให้มีผลบังคับใช้ตั้งแต่วันที่ 1 มิถุนายน 2556 เป็นต้นไป กรณีเปลี่ยนแปลงจะมีการประกาศแจ้งให้พนักงานทราบโดยทั่วกันอีกครั้ง<br><br>
						
						จึงประกาศมาเพื่อทราบโดยทั่วกัน<br><br>

						ประกาศ ณ วันที่ 17 พฤษภาคม 2556
					</p>
					
					<div class="blog-details-bt">
						<div class="btn-group">
							<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
						</div>
					</div>
				
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-8 col-xs-12 p-fix">
				<aside>
					<div class="sidebar-news-list">
						<h4>CATEGORY</h4>
						<ul>
							<li>
								<a href="HRPolicy_BusinessEthicsHR.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบทรัพยากรมนุษย์</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAS.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบเกี่ยวกับทรัพย์สิน</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAC.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบเกี่ยวกับบัญชี การเงิน งบประมาณ จัดซื้อ</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsIT.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบเกี่ยวกับเทคโนโลยีสารสนเทศ</a>
							</li>
							<li>
								<a href="Benefits1_1.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบอื่นๆ</a>
							</li>
						</ul>
					</div>
				</aside>
				<aside>
					<div class="sidebar-news-list">
						<h4>Related Links</h4>
						<ul>
							<li>
								<a href="HRPolicy_BusinessEthicsHR1.aspx" class="tran3s">
									การคล้อง ติดบัตรพนักงาน
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR2.aspx" class="tran3s">
									การลาพักผ่อนประจำปี
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR3.aspx" class="tran3s">
									การแต่งกายพนักงานขาย (พีซี)
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR4.aspx" class="tran3s">
									การลงนามอนุมัติเกินอำนาจดำเนินการ หรือความเสียหายจากการปฏิบัติหน้าที่
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR5.aspx" class="tran3s">
									ระเบียบบ้านพักพนักงาน
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR6.aspx" class="tran3s">
									ระเบียบการลงโทษพนักงานทุจริต
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR7.aspx" class="tran3s">
									ทดลองจัดวันเวลาทำงานและวันหยุด (เป็นกรณีพิเศษ) ทำงานวันจันทร์ - ศุกร์
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR8.aspx" class="tran3s">
									ระเบียบการแต่งกายพนักงานประจำสำนักงาน
								</a>
							</li>
						</ul>
					</div>
				</aside>
			</div>
		</div>
		
	</div>
</article>
    
<!-- Swiper JS -->
<script src="new/js/swiper.min.js"></script>

<!-- Initialize Swiper -->
<script>
	var swiper1 = new Swiper('.swiper1', {
		spaceBetween: 30,
		pagination: {
        el: '.swiper-pagination1',
        clickable: true,
      },
    });
</script>

</asp:Content>
