﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="Benefits11_1.aspx.vb" Inherits="Aroma_HRPortal.Benefits11_1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/benefit/banner1.jpg" /></div>
<!-- end banner -->

<!--
=====================================================
	Blog Page Details
=====================================================
-->
<article class="blog-details-page">
	<div class="container">
		
		<div class="post-heading">
            <ul>
			    <li><a href="index_hr.aspx">HOME</a></li>
			    <li>/</li>
			    <li><a href="Benefits_Employee.aspx">ระเบียบสวัสดิการ</a></li>
			</ul>
			<h2>ตรวจสุขภาพ</h2>
		</div> <!-- /.post-heading -->
		
		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix">
				<div class="blog-details-post-wrapper">
				
					<img src="new/images/banner/benefit/img15.jpg" alt="Image">
         		    
         		    <h4>ตรวจสุขภาพประจำปี</h4>
					<p>พนักงานทระดับA, B, C, Dได้รับสิทธิ</p>
					
					<h4>เกณฑ์พิจารณา</h4>
					<p>
						<ol>
							<li>โปรแกรมการตรวจดตามบริษัทกำหนด</li>
							<li>ค่าใช้จ่ายการตรวจทางบริษัทเป็นผู้ออกให้</li>
							<li>พนักงานที่มีอายุการทำงานไม่น้อยกว่า6เดือนนับถึงวันที่ตรวจหรือตามที่บริษัทกำหนด</li>
							<li>กำหนดวันเวลาตรวจทางฝ่ายทรัพยากรมนุษย์จะแจ้งเป็นปีๆ ไป</li>
							<li>กรณีพบว่าพนักงานท่านใดเพิกเฉยละเลยฝ่าฝืนการไม่ปฏิบัติตรวจสุขภาพตามวันเวลากำหนดโดยไม่มีเหตุผลอันสมควรทางบริษัทฯจำเป็นอย่างยิ่งที่จะต้องให้พนักงานไปตรวจ</li>
							และเป็นผู้ออกค่าใช้จ่ายการตรวจสุขภาพเองทั้งสิ้นตามโปรแกรมที่บริษัทกำหนดและดำเนินนการอื่ื่นๆ ตามระเบียบบริษัทกำหนด</li>
						</ol>
					</p>

					<h4>หมายเหตุ</h4>
					<p>
						<ol>
							<li>รายการสวัสดิการข้างต้นพนักงานที่ได้รับสิทธิจะต้องผ่านการทดลองงานและได้บรรจุเป็นพนักงานบริษัทแล้วเท่านั้นยกเว้นบางสวัสดิการให้เป็นไปตามหลักเกณฑ์/เงื่อนไขสวัสดิการนั้นๆ กำหนด</li>
							<li>ระดับพนักงาน A คือ ผู้ช่วยรองกรรมการผู้จัดการขึ้นไป, ระดับ B คือผู้จัดการฝ่าย-ผู้จัดการทั่วไป, ระดับ C คือ หัวหน้าส่วน-ผู้จัดการแผนก, ระดับ D คือ พนักงาน-หัวหน้างาน</li>
							<li>การเบิกจ่ายพนักงานจะต้องดำเนินการเขียนใบเบิกพร้อมแนบเอกสารหลักฐานประกอบการเบิกทุกครั้งถึงจะได้รับการพิจารณาอนุมัติเบิกจ่าย</li>
							<li>กำหนดตั้งเรื่องเบิกจ่ายผ่านฝ่ายทรัพยากรมนุษย์ตรวจสอบและส่งต่อฝ่ายบัญชีและการเงินทำจ่ายตามเวลากำหนดข้างต้นมิฉะนั้นจะถือว่าพนักงานสละสิทธิโดยไม่มีข้อโต้แย้งแต่อย่างใดทั้งสิ้น</li>
							<li>การเบิกสวัสดิการพนักงานที่ไม่สามารถใช้สิทธิเบิกได้มีกรณีต่างๆดังนี้ทำร้ายร่างกายตนการใช้สารเสพติดตามกฏหมายสารเสพติดการกระทำการใดๆเพื่อความสวยงามโดยไม่มีข้อบ่งชี้ทางการแพทย์
							การรักษาที่อยู่ระหว่างการค้นคว้าทดลองการรักษาภาวะมีบุตรยากการตรวจเนื้อเยื่อเพื่อการผ่าตัดเปลี่ยนอวัยวะการผ่าตัดเปลี่ยนอวัยวะการเปลี่ยนเพศการผสมเทียมทันตกรรมและแว่นตาเป็นต้น</li>
							<li>ผู้มีอำนาจอนุมัติเบิกจ่ายรายการสวัสดิการข้างต้นได้แก่กรรมการบริหารหรือผู้ที่ได้รับมอบหมาย</li>
							<li>กรณีตรวจสอบพบว่าผู้บังคับบัญชาต้นสังกัดลงนามรับรองผิดพลาดจากการเหตุการณ์จริง/เดินทางจริงส่วนที่ผิดพลาดผู้ลงนามรับรองนั้นๆจะต้องเป็นผู้จ่ายเงินคืนให้บริษัท</li>
							<li>บริษัทขอสงวนไว้ซึ่งสิทธิที่จะยกเลิกแก้ไขเพิ่มเติมเปลี่ยนแปลงอัตราข้างต้นได้ตามเห็นสมควร</li>
							<li>ทั้งนี้ให้มีผลตั้งแต่วันที่1มกราคม2558เป็นต้นไปจนกว่าจะมีประกาศเปลี่ยนแปลง</li>
						</ol>
					</p>
					
					<div class="blog-details-bt">
						<div class="btn-group">
							<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
						</div>
					</div>
				
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-8 col-xs-12 p-fix">
				<aside>
					<div class="sidebar-news-list">
						<h4>CATEGORY</h4>
						<ul>
							<li>
								<a href="Benefits1.aspx" class="tran3s">กองทุนสำรองเลี้ยงชีพ มอบเกียรติบัตรเงินรางวัล</a>
							</li>
							<li>
								<a href="Benefits2_1.aspx" class="tran3s">เงินช่วยเหลืองาน และโอกาสพิเศษต่างๆ</a>
							</li>
							<li>
								<a href="Benefits15.aspx" class="tran3s">เจ็บป่วย ประกันชีวิต สุขภาพ อุบัติเหตุ</a>
							</li>
							<li>
								<a href="Benefits5_1.aspx" class="tran3s">ปฏิบัติงานนอกสถานที่ (ในประเทศ)</a>
							</li>
							<li>
								<a href="Benefits3_1.aspx" class="tran3s">ปฏิบัติงานนอกสถานที่ (ต่างประเทศ)</a>
							</li>
							<li>
								<a href="Benefits4_1.aspx" class="tran3s">ปฏิบัติงาน EXHIBITION - EVENT (ในประเทศ)</a>
							</li>
							<li>
								<a href="Benefits7_1.aspx" class="tran3s">ค่าเลี้ยงรับรอง</a>
							</li>
							<li>
								<a href="Benefits8.aspx" class="tran3s">เงินกู้</a>
							</li>
							<li>
								<a href="Benefits9_1.aspx" class="tran3s">ส่วนลดสินค้า สำหรับพนักงาน</a>
							</li>
							<li>
								<a href="Benefits10_1.aspx" class="tran3s">เบี้ยขยัน</a>
							</li>
							<li>
								<a href="Benefits11_1.aspx" class="tran3s">ตรวจสุขภาพ</a>
							</li>
							<li>
								<a href="#" class="tran3s">สวัสดิการอื่นๆ</a>
							</li>
						</ul>
					</div>
				</aside>
			</div>
		</div>
		
	</div>
</article>

</asp:Content>
