﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_BusinessEthicsAS1.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_BusinessEthicsAS1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/hr.jpg" alt=""/></div>
<!-- end banner -->

<article class="blog-details-page">
	<div class="container">
		
		<div class="post-heading">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
                <li>/</li>
                <li><a href="HRPolicy_Announce.aspx">ระเบียบต่างๆ</a></li>
                <li>/</li>
                <li><a href="HRPolicy_BusinessEthicsAS.aspx">ประกาศ คำสั่ง ระเบียบเกี่ยวกับทรัพย์สิน</a></li>
            </ul>
			<h2>ระเบียบการใช้บัตรน้ำมัน Synergy Card Fleet Card บัตรน้ำมัน</h2>
		</div> <!-- /.post-heading -->
		
		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix">
				<div class="blog-details-post-wrapper">
				
					<img src="new/images/banner/hr-policy/img13.jpg" alt=""/>
         		    
         		    <p style="padding: 30px 0 0;">
         		    	ด้วยกลุ่มบริษัท อโรม่า ได้มีนโยบายว่าด้วยเรื่อง การเติมน้ำมันเชื้อเพลิงยานยนต์โดยการใช้บัตรน้ำมัน Synergy Card / Fleet Card แทนการชำระเงินสด ดังนั้นเพื่อให้มีความเป็นระเบียบเรียบร้อยในการปฏิบัติเกี่ยวกับวิธีการใช้บัตรข้างต้น ทางกลุ่มบริษัท อโรม่า จึงเห็นสมควรกำหนด และใคร่ขอความร่วมมือมายังพนักงานที่ได้รับสิทธิทุกท่าน ในการปฏิบัติอย่างเคร่งครัด ดังนี้
					</p>
					
					<ol style="margin: 10px 0;">
						<li>ชื่อบัตร “บัตรน้ำมัน Synergy Card / Fleet Card” คือ บัตรที่ใช้สำหรับเติมน้ำมันรถยนต์ โดยไม่ต้องชำระเงินสด สำหรับการใช้บริการ ณ สถานีบริการน้ำมันที่บริษัทกำหนด เท่านั้น</li>
						<li>บัตรนี้ผู้มีสิทธิใช้เฉพาะพนักงานกลุ่มบริษัท อโรม่า ซึ่งได้รับมอบหมายจากบริษัท หรือผู้บังคับบัญชาต้นสังกัด เท่านั้น</li>
						<li>วิธีการใช้บัตร และการตั้งเรื่องเบิกเงิน/เคลียร์เงินทดรองจ่ายบัตรน้ำมัน (Synergy Card / Fleet Card) ทุกครั้งที่ใช้บริการ ให้ผู้ใช้เก็บใบเสร็จ/ใบกำกับภาษี เพื่อเป็นหลักฐานประกอบ
						 การเขียนรายงานการใช้บัตรน้ำมันประจำเดือนส่งบัญชีและการเงินเป็นรายเดือนไป หรือการปฏิบัติอื่นใดเพิ่มเติมให้เป็นไปตามระเบียบการเงินกำหนด</li>
						<li>พนักงานผู้ที่ได้รับสิทธิจะต้องใช้บัตรฯ เฉพาะในงานของกลุ่มบริษัท อโรม่า ซึ่งได้รับมอบหมายงานจากบริษัท หรือผู้บังคับบัญชาต้นสังกัด เท่านั้น หากพบว่าการใช้บัตรฯ ไม่เป็นไปตามวัตถุประสงค์ข้างต้น
						 พนักงานผู้ที่ใช้บัตรฯ จะต้องรับผิดชอบค่าใช้จ่ายที่เกิดขึ้นเองทั้งหมด</li>
						<li>กรณีพนักงานผู้ได้รับสิทธิใช้บัตรฯ และได้ใช้บัตรเกินวงเงินที่ได้รับอนุมัติ เนื่องจากการใช้ในงานตามข้อ 4. ให้พนักงานผู้ใช้บัตรฯนั้นๆ สำรองเงินจ่ายไปก่อน แล้วจึงนำเอกสารใบเสร็จรับเงินและหลักฐาน
						 การปฏิบัติงานมาตั้งเรื่องเบิกจากแผนกการเงิน โดยต้องผ่านการพิจารณาอนุมัติจากกรรมการบริหารแล้วเท่านั้น จึงสามารถเบิกเงินได้</li>
						<li>บริษัทฯ ของดการจ่ายเงินค่าเดินทาง/ค่าน้ำมันและค่าสึกหรอ ซึ่งได้จ่ายในบัญชีเงินเดือน รวมทั้งการตั้งเรื่องเบิกค่าพาหนะ/เดินทางจากทางแผนกการเงิน (กิโลเมตรละ 4 บาท หรือเบิกจ่ายตามบิลใบเสร็จ) 
						 สำหรับพนักงานผู้ได้รับสิทธิในการใช้บัตรฯ นับตั้งแต่วันที่พนักงานลงชื่อรับบัตรฯ เป็นต้นไป</li>
						<li>บริษัทฯ ขอสงวนไว้ซึ่งสิทธิที่จะยกเลิก แก้ไข เปลี่ยนแปลงวิธีการใช้บัตร วงเงินในบัตร พนักงานผู้ได้รับสิทธิการใช้บัตรได้ตามเห็นสมควร โดยจะแจ้งให้ทราบเป็นคราวๆ ไป</li>
					</ol>
					
					<p><br>
						ทั้งนี้ ให้ปฏิบัติอย่างเคร่งครัด ตั้งแต่วันที่ลงนามรับบัตรน้ำมัน เป็นต้นไป<br><br>
						
						จึงประกาศมาเพื่อทราบโดยทั่วกัน และปฏิบัติอย่างเคร่งครัด<br><br>
						
						ประกาศ ณ วันที่ 10 มีนาคม 2550
					</p>
					
					<div class="blog-details-bt">
						<div class="btn-group">
							<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
						</div>
					</div>
				
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-8 col-xs-12 p-fix">
				<aside>
					<div class="sidebar-news-list">
						<h4>CATEGORY</h4>
						<ul>
							<li>
								<a href="HRPolicy_BusinessEthicsHR.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบทรัพยากรมนุษย์</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAS.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบเกี่ยวกับทรัพย์สิน</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAC.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบเกี่ยวกับบัญชี การเงิน งบประมาณ จัดซื้อ</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsIT.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบเกี่ยวกับเทคโนโลยีสารสนเทศ</a>
							</li>
							<li>
								<a href="Benefits1_1.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบอื่นๆ</a>
							</li>
						</ul>
					</div>
				</aside>
				<aside>
					<div class="sidebar-news-list">
						<h4>Related Links</h4>
						<ul>
							<li>
								<a href="HRPolicy_BusinessEthicsAS1.aspx" class="tran3s">
									ระเบียบการใช้บัตรน้ำมัน Synergy Card Fleet Card บัตรน้ำมัน
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAS2.aspx" class="tran3s">
									ระเบียบเรื่องการเบิก และใช้ทรัพย์สินบริษัท
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAS3.aspx" class="tran3s">
									ระเบียบการใช้โทรศัพท์เคลื่อนที่ (APPLE IPHONE 7128)
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAS4.aspx" class="tran3s">
									ระเบียบการใช้โทรศัพท์เคลื่อนที่ (Samsung Galaxy A9 Pro)
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAS5.aspx" class="tran3s">
									ระเบียบการใช้รถที่บริษัทเช่าให้พนักงานใช้ปฏิบัติงาน
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAS6.aspx" class="tran3s">
									ระเบียบเงินประกันชุดฟอร์มพนักงาน
								</a>
							</li>
						</ul>
					</div>
				</aside>
			</div>
		</div>
		
	</div>
</article>

</asp:Content>
