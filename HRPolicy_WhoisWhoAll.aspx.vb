﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.UI.Control
Imports System.Drawing.Imaging
Imports System.Data


Public Class HRPolicy_WhoisWhoAll
    Inherits System.Web.UI.Page
    Dim username, ID As String
    Dim sql As String
    Dim dt, dt1, dt2, dt3, dtt, dtRole As New DataTable
    Dim DB_HR As New Connect_HR
    Private sms As New PKMsg("")

    Dim objConn As New SqlConnection
    Dim objCmd As SqlCommand
    Dim strConnString, strSQL As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim strConnString As String
        'strConnString = "Server=localhost;UID=sa;PASSWORD=;database=mydatabase;Max Pool Size=400;Connect Timeout=600;"
        strConnString = "Server=10.0.4.20;Uid=armth;PASSWORD=Kb5r#Ge9Z3M*mQ;database=DB_HR;Max Pool Size=400;Connect Timeout=600;"
        objConn = New SqlConnection(strConnString)
        objConn.Open()


        If Not IsPostBack Then
            Dim strSQL As String
            strSQL = "SELECT top 10 * FROM VW_Employee_HR where departments_name = 'After Sales Service Department' ORDER BY departments_name,orderby_level ASC"

            Dim dtReader As SqlDataReader
            objCmd = New SqlCommand(strSQL, objConn)
            dtReader = objCmd.ExecuteReader()

            '*** BindData to GridView ***'
            myGridView.DataSource = dtReader
            myGridView.DataBind()

            dtReader.Close()
            dtReader = Nothing

        Else
            loadimg()
        End If



    End Sub

    Protected Sub loadimg()

        'strConnString = "Server=10.0.4.20;Uid=sa;PASSWORD=Quax_005;database=DB_HR;Max Pool Size=400;Connect Timeout=600;"
        'objConn.ConnectionString = strConnString
        'objConn.Open()

        ''*** DataTable ***'
        'Dim dtAdapter As SqlDataAdapter
        'Dim dt As New DataTable
        'strSQL = "SELECT top 10 * FROM TB_Employee WHERE Employee_ID = @Employee_ID "
        'dtAdapter = New SqlDataAdapter(strSQL, objConn)
        'objCmd = dtAdapter.SelectCommand
        'objCmd.Parameters.Add("@Employee_ID", SqlDbType.NVarChar).Value = "1159067" 'Request.QueryString("Employee_ID")
        'dtAdapter.Fill(dt)

        'If dt.Rows.Count > 0 Then
        '    Response.ContentType = dt.Rows(0)("()").ToString()
        '    Response.BinaryWrite(dt.Rows(0)("Image"))
        'End If

        'dt = Nothing

        Dim strSQL As String
        strSQL = "SELECT * FROM VW_Employee_HR where departments_name = '" & drpDepartment.SelectedValue & "'  ORDER BY departments_name,orderby_level ASC"

        Dim dtReader As SqlDataReader
        objCmd = New SqlCommand(strSQL, objConn)
        dtReader = objCmd.ExecuteReader()

        '*** BindData to GridView ***'
        myGridView.DataSource = dtReader
        myGridView.DataBind()

        dtReader.Close()
        dtReader = Nothing

    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        objConn.Close()
        objConn = Nothing
    End Sub


    Protected Sub myGridView_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles myGridView.RowDataBound
        '*** FilesID ***'
        Dim lblID As Label = DirectCast(e.Row.FindControl("lblEmployee_ID"), Label)

        If Not IsNothing(lblID) Then
            lblID.Text = e.Row.DataItem("employee_id")
        End If

        '*** Name ***'
        Dim lblImage_name As Label = DirectCast(e.Row.FindControl("lblfullname"), Label)
        If Not IsNothing(lblImage_name) Then
            lblImage_name.Text = e.Row.DataItem("fullname")
        End If

        '*** Picture ***'
        Dim ImgPic As Image = DirectCast(e.Row.FindControl("image"), Image)
        If Not IsNothing(ImgPic) Then
            ImgPic.ImageUrl = "ViewImg.aspx?type=Employee&ID=" & e.Row.DataItem("employee_id")
        End If

        '*** Name ***'
        Dim lblEmployee_Nickname As Label = DirectCast(e.Row.FindControl("lblEmployee_Nickname"), Label)
        If Not IsNothing(lblEmployee_Nickname) Then
            lblEmployee_Nickname.Text = e.Row.DataItem("Employee_Nickname")
        End If

        '*** Name ***'
        Dim lblposition As Label = DirectCast(e.Row.FindControl("lblposition"), Label)
        If Not IsNothing(lblposition) Then
            lblposition.Text = e.Row.DataItem("position")
        End If

        ''*** Name ***'
        'Dim lbldepartments_name As Label = DirectCast(e.Row.FindControl("lbldepartments_name"), Label)
        'If Not IsNothing(lbldepartments_name) Then
        '    lbldepartments_name.Text = e.Row.DataItem("departments_name")
        'End If

        '*** Name ***'
        Dim lbldepartment_name As Label = DirectCast(e.Row.FindControl("lbldepartment_name"), Label)
        If Not IsNothing(lbldepartment_name) Then
            lbldepartment_name.Text = e.Row.DataItem("departments_name")
        End If

        '*** Name ***'
        Dim lblEmail As Label = DirectCast(e.Row.FindControl("lblEmail"), Label)
        If Not IsNothing(lblEmail) Then
            lblEmail.Text = e.Row.DataItem("Email")
        End If

        '*** Name ***'
        Dim lblExt As Label = DirectCast(e.Row.FindControl("lblExt"), Label)
        If Not IsNothing(lblExt) Then
            lblExt.Text = e.Row.DataItem("Ext")
        End If


        ''*** Hyperlink ***'
        'Dim hplEdit As HyperLink = DirectCast(e.Row.FindControl("hplEdit"), HyperLink)
        'If Not IsNothing(hplEdit) Then
        '    hplEdit.Text = "Edit"
        '    hplEdit.NavigateUrl = "Edit.aspx?ID=" & e.Row.DataItem("ID")
        'End If

        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    'getting username from particular row
        '    Dim ID As String = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ID"))
        '    Dim lnkbtnresult As LinkButton = DirectCast(e.Row.FindControl("lnkdelete"), LinkButton)
        '    lnkbtnresult.Attributes.Add("onclick", "javascript:return ConfirmationBox('" & ID & "')")
        'End If
    End Sub



    Protected Sub drpDepartment_Init(sender As Object, e As EventArgs) Handles drpDepartment.Init
        Dim sqlCon As New SqlConnection(DB_HR.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "SELECT  departments_name FROM VW_Employee_HR where departments_name is not null group by departments_name "

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_HR.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpDepartment.Items.Clear()
        drpDepartment.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpDepartment.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub backButton_Click(sender As Object, e As EventArgs) Handles backButton.Click

    End Sub

End Class