﻿Imports System.Drawing
Imports System.IO
Imports System.Data.OleDb
Imports System.Data.SqlClient

Public Class A_EmployeeImport
    Inherits System.Web.UI.Page
    Dim DB_HR As New Connect_HR
    Dim db_Leave As New Connect_Leave
    Dim dt, dt1, dt2, dt3, dtt, dtRole As New DataTable
    Dim username As String
    Dim DeliveryNo As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ID = Request.QueryString("id")

        If Not IsPostBack Then


        End If

    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        If Attach1.Value = "" Then
            Me.Label3.Text = "Please choose file to import..!!"
            Me.Label3.ForeColor = Color.Red
            Me.Label3.Font.Bold = True
            Me.lblMissItem.Visible = False
            Exit Sub
        Else
            Me.lblMissItem.Visible = False
            Me.lstMissItem.Items.Clear()

            UploadXLS()

        End If
    End Sub

    '---- 2
    Private Sub UploadXLS()
        Dim sFile As HttpPostedFile = Me.Attach1.PostedFile

        ' Dim sSavePath As String = Server.MapPath("~" + HttpRuntime.AppDomainAppVirtualPath + "importData")
        Dim sSavePath As String = "E:\Import Employee Data"

        Dim sSaveFilePath As String = ""
        If Not Directory.Exists(sSavePath) Then
            Directory.CreateDirectory(sSavePath)
        End If
        Dim strFileName = System.IO.Path.GetFileName(sFile.FileName)

        DeliveryNo = strFileName.Substring(0, strFileName.IndexOf(".")) 'strFileName

        sSaveFilePath = sSavePath & "\" & strFileName
        If strFileName <> "" Then
            Me.Label3.Text = strFileName
            Me.Label3.ForeColor = Color.Teal
            sFile.SaveAs(sSaveFilePath)
            sSaveFilePath = sSaveFilePath.Replace("/", "\")
            GetXLS(sSaveFilePath, DeliveryNo)
        End If
    End Sub

    '----3
    Private Sub GetXLS(ByVal sFilename As String, ByVal DeliveryNo As String)
        Dim xls03 As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sFilename & " ;Extended Properties=""Excel 8.0;HDR=YES;"""
        Dim xls07 As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & sFilename & " ;Extended Properties=""Excel 8.0;HDR=YES;"""
        Dim strConn As String = ""
        Dim x As Boolean = False
        Dim s As Boolean = False
        Dim cItem As Boolean = True
        Dim sSQLCheckItem As String = ""
        If Right(Me.Attach1.Value, 1) = "x" Then
            strConn = xls07
        ElseIf Right(Me.Attach1.Value, 1) = "s" Then
            strConn = xls03
        End If

        Dim connExcel As New OleDbConnection(strConn)
        Dim cmdExcel As New OleDbCommand
        Dim oda As New OleDbDataAdapter
        Dim dt1 As New DataTable
        cmdExcel.Connection = connExcel


        Dim constring As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & sFilename & ";Extended Properties=""Excel 12.0;HDR=YES;"""
        Dim con As New OleDbConnection(constring & "")
        con.Open()

        REM: Get the name of First Sheet
        connExcel.Open()
        Dim dtExcelSchema As DataTable
        ' dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
        dtExcelSchema = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)

        Dim SheetName As String = ""
        Me.Label3.Text = ""
        For i As Integer = 0 To dtExcelSchema.Rows.Count - 1
            SheetName = UCase(Trim(dtExcelSchema.Rows(i)("TABLE_NAME").ToString()))
            Me.Label3.Text = Me.Label3.Text & SheetName & "^"
            If SheetName = "SHEET1$" Then s = True
        Next
        Me.Label3.Text = Me.Label3.Text & ":: s = " & s.ToString
        If s = False Then
            Me.Label3.Text = "Not found 'Sheet1'"
            Me.Label3.ForeColor = Color.Red
            Exit Sub
        End If

        REM: Read Data from First Sheet
        If connExcel.State = ConnectionState.Closed Then connExcel.Open()
        cmdExcel.CommandText = "SELECT * From [sheet1$]"
        oda.SelectCommand = cmdExcel
        oda.Fill(dt1)
        connExcel.Close()
        For i As Integer = 0 To dt1.Rows.Count - 1
            sSQLCheckItem = sSQLCheckItem & "'" & dt1.Rows(i)(0) & "'"

            If i < dt1.Rows.Count - 1 Then
                sSQLCheckItem = sSQLCheckItem & ", "
            End If
        Next

        REM: Get data from [Items]
        Dim SQLADP As SqlDataAdapter
        Dim dt3 As New DataTable
        Dim sCon As New SqlConnection(DB_HR.sqlCon)
        Dim sSQL As String = ""
        Dim sqlDT2 As String
        sSQL = "select  rtrim(Employee_ID) Employee_ID from  TB_Employee where 1=1"
        sSQL = sSQL & "AND Employee_ID IN ("
        sSQL = sSQL & sSQLCheckItem & ") "
        sSQL = sSQL & "order by Employee_ID"
        SQLADP = New SqlDataAdapter(sSQL, sCon)
        'SQLADP.Fill(dt3)
        dt3 = DB_HR.GetDataTable(sSQL)


        'Me.Table2.Rows.Clear()
        'cItem = CheckItemCodes(dt1, dt3)
        'If cItem = False Then
        '    Me.Label3.Text = "No Employee ID, Please check list below"
        '    Me.Label3.ForeColor = Color.Red
        '    'Me.lblMissItem.Text = "No items, Please check list below"
        '    'btnAddItem.Visible = True
        '    Exit Sub
        'End If

        REM: Get data from [TB_ItemWH]
        Dim dt2 As New DataTable


        'sqlDT2 = "SELECT [DeliveryNo]+[ItemCode]+[Pallet]+[ItemTypeImport]+CAST( Qty AS nvarchar(20) )  as itemImport, Qty  FROM [TB_ItemImportFile] with (nolock)"
        sqlDT2 = "SELECT Employee_ID  FROM [TB_Employee] with (nolock)"
        dt2 = DB_HR.GetDataTable(sqlDT2)
        ImportDT(dt1, dt2, DeliveryNo)


    End Sub

    '---4
    Private Sub ImportDT(ByVal dt1 As DataTable, ByVal dt2 As DataTable, ByVal DeliveryNo As String)

        Dim site, Employee_ID, NameTitle, fullname, Employee_Name, Employee_LastName, Employee_Nickname, branch As String
        Dim departments_id, department_id, EmpLevel, position_id, position, SEX, birthday As String
        Dim ID_Card, adr, Tel, Ext, Email, Manager_id, StartDate, Status, EmpLevel_ApproveLeave, position_id_ApproveLeave As String


        Dim u As Integer = 0
        Dim n As Integer = 0
        Dim foundRows() As DataRow
        Dim bUpdate As Boolean = False
        Dim sExpression As String = ""
        Me.Label3.Text = "ImportData"
        Me.Label3.ForeColor = Color.Teal

        For i As Integer = 0 To dt1.Rows.Count - 1
            Employee_ID = checkIsDBNull(UCase(Trim(dt1.Rows(i)(0))))
            'DeliveryNo = DeliveryNo
            site = checkIsDBNull(UCase(Trim(dt1.Rows(i)(1))))
            NameTitle = checkIsDBNull(UCase(Trim(dt1.Rows(i)(2))))
            fullname = checkIsDBNull(UCase(Trim(dt1.Rows(i)(3))))

            Employee_Name = checkIsDBNull(UCase(Trim(dt1.Rows(i)(4))))
            Employee_LastName = checkIsDBNull(UCase(Trim(dt1.Rows(i)(5))))
            Employee_Nickname = checkIsDBNull(UCase(Trim(dt1.Rows(i)(6))))
            branch = checkIsDBNull(UCase(Trim(dt1.Rows(i)(7))))
            departments_id = checkIsDBNull(UCase(Trim(dt1.Rows(i)(8))))
            department_id = checkIsDBNull(UCase(Trim(dt1.Rows(i)(9))))
            EmpLevel = checkIsDBNull(UCase(Trim(dt1.Rows(i)(10))))
            position_id = checkIsDBNull(UCase(Trim(dt1.Rows(i)(11))))
            position = checkIsDBNull(UCase(Trim(dt1.Rows(i)(12))))
            SEX = checkIsDBNull(UCase(Trim(dt1.Rows(i)(13))))
            birthday = checkIsDBNull(UCase(Trim(dt1.Rows(i)(14))))
            ID_Card = checkIsDBNull(UCase(Trim(dt1.Rows(i)(15))))
            adr = checkIsDBNull(UCase(Trim(dt1.Rows(i)(16))))
            Tel = checkIsDBNull(UCase(Trim(dt1.Rows(i)(17))))
            Ext = checkIsDBNull(UCase(Trim(dt1.Rows(i)(18))))
            Email = checkIsDBNull(UCase(Trim(dt1.Rows(i)(19))))
            Manager_id = checkIsDBNull(UCase(Trim(dt1.Rows(i)(20))))
            StartDate = checkIsDBNull(UCase(Trim(dt1.Rows(i)(21))))
            Status = "Active"
            EmpLevel_ApproveLeave = checkIsDBNull(UCase(Trim(dt1.Rows(i)(10))))
            position_id_ApproveLeave = checkIsDBNull(UCase(Trim(dt1.Rows(i)(11))))

            'branch = checkIsDBNull(dt1.Rows(i)("ชื่อ"))

            Dim sData As String = Employee_ID & "^" & site & "^" & NameTitle & "^" & fullname & "^" & Employee_Name & "^" & Employee_LastName & "^" & Employee_Nickname & "^" & branch & "^" & departments_id & "^" & department_id & "^" & EmpLevel & "^" & position_id & "^" & position & "^" & SEX & "^" & DateTime.Parse(birthday).ToString("yyyy-MM-dd") & "^" & ID_Card & "^" & adr & "^" & Tel & "^" & Ext & "^" & Email & "^" & Manager_id & "^" & DateTime.Parse(StartDate).ToString("yyyy-MM-dd") & "^" & Status & "^" & EmpLevel_ApproveLeave & "^" & position_id_ApproveLeave
            sExpression = "Employee_ID='" & Employee_ID & "'"
            foundRows = dt2.Select(sExpression)

            If foundRows.Length <= 0 Then
                InsertData(sData)
                InsertDataLeave(sData)
                n = n + 1
            Else
                'UpdateData(sData)  'ตัด Stock
                'InsertIssueStock(sData)
                u = u + 1
            End If

        Next
        '  Me.Label3.Text = "COMPLETE..!!! Update: " & u & ",, Insert: " & n
        Me.Label3.Text = "COMPLETE..!!! Insert: " & n & "   duplicate: " & u
        Me.Label3.ForeColor = Color.RoyalBlue
    End Sub

    Private Sub InsertData(ByVal sData As String)
        Dim sID As Integer = 0
        Dim sqlCMD As New SqlCommand
        Dim arrData() As String = sData.Split("^")
        Dim sSQL As String = ""
        Dim sCon As New SqlConnection(DB_HR.sqlCon)


        sSQL = "INSERT INTO TB_Employee("
        sSQL = sSQL & "Employee_ID ,site, NameTitle, fullname, Employee_Name, Employee_LastName, Employee_Nickname, branch"
        sSQL = sSQL & ",departments_id, department_id, EmpLevel, position_id, position, SEX, birthday"
        sSQL = sSQL & ",ID_Card, adr, Tel, Ext, Email, Manager_id, StartDate, Status, EmpLevel_ApproveLeave, position_id_ApproveLeave"
        sSQL = sSQL & ")"

        sSQL = sSQL & "VALUES("
        sSQL = sSQL & "'" & arrData(0) & "','" & arrData(1) & "','" & arrData(2) & "','" & arrData(3) & "','" & arrData(4) & "','" & arrData(5) & "','" & arrData(6) & "','" & arrData(7) & "','" & arrData(8) & "','" & arrData(9) & "'"
        sSQL = sSQL & ",'" & arrData(10) & "','" & arrData(11) & "','" & arrData(12) & "','" & arrData(13) & "','" & arrData(14) & "','" & arrData(15) & "','" & arrData(16) & "','" & arrData(17) & "'"
        sSQL = sSQL & ",'" & arrData(18) & "','" & arrData(19) & "','" & arrData(20) & "','" & arrData(21) & "','" & arrData(22) & "','" & arrData(23) & "','" & arrData(24) & "'"
        sSQL = sSQL & ")"

        dtt = DB_HR.GetDataTable(sSQL)

    End Sub

    Private Sub InsertDataLeave(ByVal sData As String)
        Dim sID As Integer = 0
        Dim sqlCMD As New SqlCommand
        Dim arrData() As String = sData.Split("^")
        Dim sSQL As String = ""
        Dim sCon As New SqlConnection(db_Leave.sqlCon)


        sSQL = "INSERT INTO TB_Employee("
        sSQL = sSQL & "Employee_ID ,site, NameTitle, fullname, Employee_Name, Employee_LastName, Employee_Nickname, branch"
        sSQL = sSQL & ",departments_id, department_id, EmpLevel, position_id, position, SEX, birthday"
        sSQL = sSQL & ",ID_Card, adr, Tel, Ext, Email, Manager_id, StartDate, Status, EmpLevel_ApproveLeave, position_id_ApproveLeave"
        sSQL = sSQL & ")"

        sSQL = sSQL & "VALUES("
        sSQL = sSQL & "'" & arrData(0) & "','" & arrData(1) & "','" & arrData(2) & "','" & arrData(3) & "','" & arrData(4) & "','" & arrData(5) & "','" & arrData(6) & "','" & arrData(7) & "','" & arrData(8) & "','" & arrData(9) & "'"
        sSQL = sSQL & ",'" & arrData(10) & "','" & arrData(11) & "','" & arrData(12) & "','" & arrData(13) & "','" & arrData(14) & "','" & arrData(15) & "','" & arrData(16) & "','" & arrData(17) & "'"
        sSQL = sSQL & ",'" & arrData(18) & "','" & arrData(19) & "','" & arrData(20) & "','" & arrData(21) & "','" & arrData(22) & "','" & arrData(23) & "','" & arrData(24) & "'"
        sSQL = sSQL & ")"

        dtt = db_Leave.GetDataTable(sSQL)

    End Sub

    Private Function checkIsDBNull(ByVal FieldValue As Object, Optional ByVal sDefaultValue As String = "")
        If IsDBNull(FieldValue) Then
            FieldValue = ""
            If sDefaultValue <> "" Then
                FieldValue = sDefaultValue
            End If
        Else
            FieldValue = Trim(FieldValue)
        End If
        Return FieldValue
    End Function


    Private Function CheckRepeatXLS(ByVal dtXLS As DataTable)
        Dim dtItem As DataTable = dtXLS
        Dim x As String = ""
        Dim a As Boolean = False
        Dim arrReturn As New ArrayList
        Dim sitemcode1, sitemcode2 As String
        Dim sData As String
        Dim arrCheck As New ArrayList
        DrawTable("Line^Employee_ID")
        For i As Integer = 0 To dtXLS.Rows.Count - 1
            sitemcode1 = checkIsDBNull(dtXLS.Rows(i)(0))
            x = ""
            Dim sExpression As String = "Employee_ID = '" & sitemcode1 & "'"
            Dim foundRows() As DataRow = dtItem.Select(sExpression)
            If (foundRows.Length > 1) Then
                sData = i + 2 & "^" & sitemcode1
                arrCheck.Add(sData)
            End If
        Next
        If arrCheck.Count = 0 Then
            a = True
        Else
            a = False
            For i As Integer = 0 To arrCheck.Count - 1
                DrawTable(arrCheck.Item(i))
            Next
            Me.lblMissItem.Visible = True
        End If
        Return a
    End Function

    Private Function DrawTable(ByVal arrData As String)
        Dim myRow As New TableRow
        Dim sRow() As String = arrData.Split("^")
        For i As Integer = 0 To UBound(sRow)
            Dim myCell As New TableCell
            With myCell
                .Text = sRow(i)
                .Font.Bold = True
            End With
            myRow.Cells.Add(myCell)
        Next
        Me.Table2.Rows.Add(myRow)
    End Function

    Private Function CheckItemCodes(ByVal dtXLS As DataTable, ByVal dtItem As DataTable)
        Dim x As String = ""
        Dim a As Boolean = False
        Dim arrReturn As New ArrayList
        Dim sitemcode1, sitemcode2 As String
        Dim sData As String
        Dim arrCheck As New ArrayList
        DrawTable("Line^Employee_ID")
        For i As Integer = 0 To dtXLS.Rows.Count - 1
            sitemcode1 = checkIsDBNull(dtXLS.Rows(i)(0))
            x = ""
            Dim sExpression As String = "Employee_ID = '" & sitemcode1 & "'"
            Dim foundRows() As DataRow = dtItem.Select(sExpression)
            If (foundRows.Length <= 0) Then
                sData = i + 2 & "^" & sitemcode1
                arrCheck.Add(sData)
            End If
        Next
        If arrCheck.Count = 0 Then
            a = True
        Else
            a = False
            For i As Integer = 0 To arrCheck.Count - 1
                DrawTable(arrCheck.Item(i))
            Next
            Me.lblMissItem.Visible = True
        End If
        Return a
    End Function

End Class