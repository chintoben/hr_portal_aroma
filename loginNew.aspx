﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="loginNew.aspx.vb" Inherits="Aroma_HRPortal.loginNew" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="UTF-8">
  <title>HR Portal</title>
  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">

  <link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900'>
<link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Montserrat:400,700'>
<link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>

      <link rel="stylesheet" href="cssLogin/style.css">

  
</head>
<body>
    <form id="form1" runat="server">
    <div>
    

    
<div class="container">
  <div class="info">
<%--    <h1>Flat Login Form</h1><span>Made with <i class="fa fa-heart"></i> by <a href="http://andytran.me">Andy Tran</a></span>--%>
     <h1>HR Portal</h1><span>Login </span>
  </div>
</div>
<div class="form">
  <div><img src="Images/logo-aroma.png" width="120" /></div>
   <br />
  <form class="login-form">
    <%--<input type="text" placeholder="username"/>
    <input type="password" placeholder="password"/>--%>

      <asp:TextBox ID="txtUser" runat="server" placeholder="username"
                  ></asp:TextBox>

      <asp:TextBox ID="txtPass" runat="server"   placeholder="password"
                     TextMode="Password"></asp:TextBox>

    <%--<button>login</button>--%>

          <asp:Button ID="btnLogin" runat="server" Text="Log in" 
                    BackColor="#990000" Font-Bold="True" ForeColor="White" 
                    ToolTip="Log in เพื่อเข้าสู่ระบบ" Width="100px" Font-Names="Tahoma" 
                    Font-Size="Medium" />


    <p class="message">Copyright © 2018, Aroma Group All Rights Reserved</p>

   
  </form>
</div>


<video id="video" autoplay="autoplay" loop="loop" poster="polina.jpg">
  <source src="http://andytran.me/A%20peaceful%20nature%20timelapse%20video.mp4" type="video/mp4"/>
</video>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

  <script  src="jsLogin/index.js"></script>





    </div>
    </form>
</body>
</html>
