﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_BrandStory9.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_BrandStory9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<article class="blog-details-page">
	<div class="container">
		
		<div class="post-heading">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
                <li>/</li>
                <li><a href="HRPolicy_BrandStory.aspx">ประวัติความเป็นมา โครงสร้างการบริหาร</a></li>
            </ul>
			<h2>Company Profile Video</h2>
		</div> <!-- /.post-heading -->
		
		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix">
				<div class="blog-details-post-wrapper">
				
					
					
            <table algin="center">
            <tr>
                <td>	
			    <h4>Version ภาษาไทย</h4>

                </td>
            <td></td>
            <td>&nbsp;</td>
            <td>

			    &nbsp;</td>
            </tr>
             <tr>
            <td>
             <video width="700" height="400" controls>
              <source src="video\Aroma Group VDO TH Version - 09082017.mp4" type="video/mp4">
            </video>
            </td>
            <td></td>
            <td>&nbsp;</td>
            <td>
               
            </td>
            </tr>
            
             <tr>
            <td>
                &nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>
               
                &nbsp;</td>
            </tr>
            
             <tr>
            <td>

			<h4>Version ภาษาอังกฤษ</h4>




					
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>
                &nbsp;</td>
            </tr>
            
             <tr>
            <td>
                 <video width=700" height="400" controls>
  <source src="video\Aroma Group VDO ENG Version - 09082017.mp4" type="video/ogg">
</video></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>
                &nbsp;</td>
            </tr>
            
            </table>


<br />

			
					<div class="blog-details-bt">
						<div class="btn-group">
							<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
						</div>
					</div>
				
				</div>
			</div>
			
		</div>
		
	</div>
</article>

</asp:Content>
