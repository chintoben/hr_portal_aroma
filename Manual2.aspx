﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="Manual2.aspx.vb" Inherits="Aroma_HRPortal.Manual2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<article class="blog-details-page">
	<div class="container">
		
		<div class="post-heading">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
                <li>/</li>
                <li><a href="HRPolicy_BrandStory.aspx">คู่มือการใช้งานระบบต่างๆ</a></li>
            </ul>
			<h2>วิดิโอ แนะนำการใช้งานระบบประเมินผลงาน (PMS Online)</h2>
		</div> <!-- /.post-heading -->
		
		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix">
				<div class="blog-details-post-wrapper">
				
					

             <h3>วิธีการกำหนดแผนงาน/เป้าหมาย (KPI)</h3>

             <video width="700" height="400" controls>
              <source src="video\How-to-PMS-Online.mp4" type="video/mp4">

             
            </video>

                 <br/> <br/><br/>
			
				
                     <h3>วิธีการ ประเมินผลการปฏิบัติงาน (กลางปีและปลายปี)</h3>

                       <video width="700" height="400" controls>
              <source src="video\PMS End_1080p.mp4" type="video/mp4">

             
            </video>

                    <p style="padding: 5px 0 0;">
				  		<ul class="p">
							<li><a href="Document/Manual/HR/คู่มือฉบับย่อ PMS Online.pdf" target="_blank">คู่มือฉบับย่อ PMS Online</a></li>
                        </ul>
					</p>

                

                    	<div class="blog-details-bt">
						<div class="btn-group">
							<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
                    	</div>
					</div>

				
				</div>
			</div>
			
		</div>
		
	</div>
</article>
</asp:Content>
