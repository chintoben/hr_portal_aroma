﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_Holiday4.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_Holiday4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/hr.jpg" alt=""/></div>
<!-- end banner -->

<article class="blog-details-page">
	<div class="container">
		
		<div class="post-heading">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
                <li>/</li>
                <li><a href="HRPolicy_Holidays.aspx">วันหยุดประจำปีบริษัท</a></li>
            </ul>
			<h2>วันหยุดประเพณี ประจำปี พ.ศ. 2563</h2>
		</div> <!-- /.post-heading -->
		
		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix">
				<div class="blog-details-post-wrapper">
				
					<img src="new/images/banner/hr-policy/img25.jpg" alt=""/>
					
					<%--<h4>บริษัท เค.วี.เอ็น. อิมปอร์ต เอกซ์ปอร์ต (1991) จำกัด  / บริษัท ไลอ้อน ทรี-สตาร์ จำกัด</h4>--%>

					<h4>ทางบริษัทฯ ได้กำหนดวันหยุดประจำปี พ.ศ. 2563 ดังนี้</h4>
					<p>
						<table class="table">
						  <tbody>
							<tr>
							  <td width="2%">1.</td>
							  <td width="15%">วันพุธที่ </td>
							  <td width="15%">1 มกราคม</td>
							  <td width="68%">วันขึ้นปีใหม่</td>
							</tr>
							<tr>
							  <td>2.</td>
							  <td>วันจันทร์ที่</td>
							  <td>10 กุมภาพันธ์</td>
							  <td>ชดเชยวันมาฆบูชา (วันเสาร์ที่ 8 กุมภาพันธ์ 2563)</td>
							</tr>
							<tr>
							  <td>3.</td>
							  <td>วันจันทร์ที่</td>
							  <td>6  เมษายน</td>
							  <td>วันพระบาทสมเด็จพระพุทธยอดฟ้าจุฬาโลกมหาราช  และวันที่ระลึกมหาจักรีบรมราชวงศ์  </td>
							</tr>
							<tr>
							  <td>4.</td>
							  <td>วันจันทร์ที่</td>
							  <td>13 เมษายน</td>
							  <td>วันสงกรานต์</td>
							</tr>
							<tr>
							  <td>5.</td>
							  <td>วันอังคารที่</td>
							  <td>14 เมษายน</td>
							  <td>วันสงกรานต์</td>
							</tr>
							<tr>
							  <td>6.</td>
							  <td>วันพุธที่</td>
							  <td>15 เมษายน</td>
							  <td>วันสงกรานต์</td>
							</tr>
							<tr>
							  <td>7.</td>
							  <td>วันศุกร์ที่</td>
							  <td>1  พฤษภาคม</td>
							  <td>วันแรงงานแห่งชาติ</td>
							</tr>
							<tr>
							  <td>8.</td>
							  <td>วันจันทร์ที่</td>
							  <td>4 พฤษภาคม</td>
							  <td>วันฉัตรมงคล</td>
							</tr>
							<tr>
							  <td>9.</td>
							  <td>วันพุธที่</td>
							  <td>6  พฤษภาคม</td>
							  <td>วันวิสาขบูชา</td>
							</tr>
							<tr>
							  <td>10.</td>
							  <td>วันพุธที่</td>
							  <td>3  มิถุนายน</td>
							  <td>วันเฉลิมพระชนมพรรษาสมเด็จพระนางเจ้าสุทิดา พัชรสุธาพิมลลักษณ พระบรมราชินี</td>
							</tr>
							<tr>
							  <td>11.</td>
							  <td>วันจันทร์ที่</td>
							  <td>6 กรกฎาคม</td>
							  <td>ชดเชยวันอาสาฬหบูชา (วันอาทิตย์ที่ 5 กรกฎาคม 2563) </td>
							</tr>
							<tr>
							  <td>12.</td>
							  <td>วันอังคารที่</td>
							  <td>28 กรกฎาคม</td>
							  <td>วันเฉลิมพระชนมพรรษา  พระบาทสมเด็จพระเจ้าอยู่หัว </td>
							</tr>
							<tr>
							  <td>13.</td>
							  <td>วันพุธที่</td>
							  <td>12 สิงหาคม</td>
							  <td>วันเฉลิมพระชนมพรรษา สมเด็จพระนางเจ้าสิริกิติ์  พระบรมราชินีนาถ พระบรมราชชนนีพันปีหลวง  และวันแม่แห่งชาติ </td>
							</tr>
							<tr>
							  <td>14.</td>
							  <td>วันอังคารที่</td>
							  <td>13 ตุลาคม</td>
							  <td>วันคล้ายวันสวรรคต  พระบาทสมเด็จพระบรมชนกาธิเบศร มหาภูมิพลอดุลยเดชมหาราช  บรมนาถบพิตร  </td>
							</tr>
							<tr>
							  <td>15.</td>
							  <td>วันศุกร์ที่</td>
							  <td>23 ตุลาคม</td>
							  <td>วันปิยมหาราช</td>
							</tr>

                            <tr>
							  <td>16.</td>
							  <td>วันจันทร์ที่</td>
							  <td>7  ธันวาคม</td>
							  <td>ชดเชยวันคล้ายวันเฉลิมพระชนมพรรษาพระบาทสมเด็จพระบรมชนกาธิเบศร มหาภูมิพลอดุลยเดชมหาราช  บรมนาถบพิตร วันชาติ  และวันพ่อแห่งชาติ  (วันเสาร์ที่ 5 ธันวาคม 2563) </td>
							</tr>
                            <tr>
							  <td>17.</td>
							  <td>วันพฤหัสบดีที่</td>
							  <td>10  ธันวาคม </td>
							  <td>วันพระราชทานรัฐธรรมนูญ</td>
							</tr>
                            <tr>
							  <td>18.</td>
							  <td>วันพฤหัสบดีที่</td>
							  <td>31 ธันวาคม</td>
							  <td>วันสิ้นปี</td>
							</tr>

						  </tbody>
						</table>
					</p>
					
					<p>
						จึงประกาศมาเพื่อทราบโดยทั่วกัน<br><br>

						ประกาศ ณ วันที่ 5 พฤศจิกายน 2562<br><br>	
		 			 
						หมายเหตุ : หากมีการประกาศเปลี่ยนแปลงวันหยุดตามราชประเพณีหรือวันสำคัญต่างๆ บริษัทฯ จะมีการพิจารณาเปลี่ยนแปลงและ แจ้งให้ทราบอีกครั้ง
					</p>
					
					<div class="blog-details-bt">
						<div class="btn-group">
							<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
						</div>
					</div>
				
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-8 col-xs-12 p-fix">
				<aside>
					<div class="sidebar-news-list">
						<h4>CATEGORY</h4>
						<ul>
							<li>
								<a href="HRPolicy_Holiday1.aspx" class="tran3s">วันหยุดประเพณี ประจำปี พ.ศ. 2560</a>
							</li>
							<li>
								<a href="HRPolicy_Holiday2.aspx" class="tran3s">วันหยุดประเพณี ประจำปี พ.ศ. 2561</a>
							</li>
                            <li>
								<a href="HRPolicy_Holiday3.aspx" class="tran3s">วันหยุดประเพณี ประจำปี พ.ศ. 2562</a>
							</li>
                             <li>
								<a href="HRPolicy_Holiday4.aspx" class="tran3s">วันหยุดประเพณี ประจำปี พ.ศ. 2563</a>
							</li>
                              <li>
								<a href="HRPolicy_Holiday5.aspx" class="tran3s">วันหยุดประเพณี ประจำปี พ.ศ. 2564</a>
							</li>
						</ul>
					</div>
				</aside>
			</div>
		</div>
		
	</div>
</article>

</asp:Content>
