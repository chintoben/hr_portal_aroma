﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_Authorize.Master" CodeBehind="A_IndexActivityMain.aspx.vb" Inherits="Aroma_HRPortal.A_IndexActivityMain" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<br />
<br />

                 <div class="col-lg-12">
                <h3>
                   Activity&Semina Calenda (ปฏิทินกิจกรรม/สัมนา)
                </h3>
                
                </div>

   <div class="col-lg-12">
                                    <asp:Button ID="btnNew" runat="server" Text="New" 
                    BackColor="#990000" Font-Bold="True" ForeColor="White" Height="30px" 
                    ToolTip="New" Width="100px" Font-Names="Tahoma" Font-Size="Medium" />
                  </div>    

<asp:GridView ID="GridView1" runat="server" AllowPaging="True"  
                                                            
            AutoGenerateColumns="False" DataKeyNames="IndexID" DataSourceID="SqlDataSource1" 
                                                            Font-Bold="False" 
            Font-Names="Tahoma" Font-Size="Small" ShowFooter="True" 
                                                            Width="100%" 
            CellPadding="1" BackColor="White" BorderColor="White" 
                                                           
                                                            GridLines="None" 
            Height="157px" PageSize="12" BorderWidth="15px">
                                                            <FooterStyle BackColor="gray" ForeColor="#333333" />
                                                            <PagerStyle BackColor="gray" ForeColor="black" HorizontalAlign="Center" />
                                                            <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="black" />
                                                            <HeaderStyle BackColor="gray" Font-Bold="true" ForeColor="black" />
                                                            <RowStyle BackColor="White" ForeColor="#333333" />
                                                                
                                                            <Columns> 
                                                                
                                                                <%--<asp:BoundField DataField="Employee_ID" HeaderText="ID" ReadOnly="true"  
                                                                    SortExpression="Employee_ID" InsertVisible="False" >
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                --%>
                                                                
                                                                <asp:HyperLinkField DataTextField="IndexID" 
                                                                    DataNavigateUrlFields="IndexID" DataNavigateUrlFormatString="~/A_IndexActivity.aspx?IndexID={0}"
                                                                        HeaderText="ID" ItemStyle-Width = "20" >
                                                                     <ItemStyle Width="50px"></ItemStyle>
                                                                </asp:HyperLinkField>
                                                                 
                                                                <asp:BoundField DataField="TxtSubject" HeaderText="Subject" 
                                                                    SortExpression="TxtSubject" >
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="TxtDetail" HeaderText="Detail" 
                                                                    SortExpression="TxtDetail" ReadOnly="True" >
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>

                                                                 <asp:BoundField DataField="image_name" HeaderText="image_name" 
                                                                    SortExpression="image_name" ReadOnly="True" >
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                               
                                                              <%--  <asp:BoundField DataField="CreateBy" HeaderText="CreateBy" 
                                                                    SortExpression="CreateBy" ReadOnly="True" >
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>--%>

                                                                 <asp:BoundField DataField="CreateDate" HeaderText="CreateDate" 
                                                                    SortExpression="CreateDate" ReadOnly="True" >
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                
                                                            <%--    <asp:TemplateField HeaderText="Active" SortExpression="status">
                                                                    <EditItemTemplate>
                                                                        <asp:DropDownList ID="Drop2" runat="server" 
                                                                            SelectedValue='<%# Bind("status") %>' Height="23px" Width="82px">
                                                                            <asp:ListItem Value="Active">Active</asp:ListItem>
                                                                            <asp:ListItem Value="InActive">InActive</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </EditItemTemplate>

                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chk" runat="server" 
                                                                            Checked='<%# If(Eval("status")= "Active", True, False) %>'  Enabled="False" />
                                                                    </ItemTemplate>

                                                                    <HeaderStyle HorizontalAlign="Left" />

                                                                 </asp:TemplateField>  --%>
                                                             
                                                                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" 
                                                                   
                                                                    DeleteImageUrl="~/Images/icon_delete.ico" 
                                                                    EditImageUrl="~/Images/icon_edit.ico" 
                                                                    ButtonType="Image" 
                                                                    CancelImageUrl="~/Images/icon_close.ico" 
                                                                    UpdateImageUrl="~/Images/icon_Save.ico" >
                                                                         <HeaderStyle HorizontalAlign="Left" />
                                                                   </asp:CommandField>
                                                                   
                                                            </Columns>
                                                              <SortedAscendingCellStyle BackColor="#F8FAFA" />
                                                            <SortedAscendingHeaderStyle BackColor="#246B61" />
                                                            <SortedDescendingCellStyle BackColor="#D4DFE1" />
                                                            <SortedDescendingHeaderStyle BackColor="#15524A" />
                                                        </asp:GridView>



          <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:HRConnectionString %>" 
        DeleteCommand="DELETE FROM [TB_IndexHome] where [IndexID] = @IndexID" 
        SelectCommand="SELECT [IndexID],[TxtSubject],left([TxtDetail],100)+'...' as [TxtDetail],[image_name] , isnull([Status],'Active') ,[CreateBy] ,[CreateDate] FROM [dbo].[TB_IndexHome]  where [type] = 'Activity'  order by indexid desc" 
        UpdateCommand="UPDATE [TB_IndexHome] set  TxtSubject=@TxtSubject  where [IndexID] = @IndexID"
        >
        
        <UpdateParameters>      
            <asp:Parameter Name="status" />       
        </UpdateParameters>
        
    </asp:SqlDataSource>



</asp:Content>
