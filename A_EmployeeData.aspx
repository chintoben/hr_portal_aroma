﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_Authorize.Master" CodeBehind="A_EmployeeData.aspx.vb" Inherits="Aroma_HRPortal.A_EmployeeData" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            width: 15px;
        }
        .style2
        {
        }
        .style3
        {
        }
        .style6
        {
            width: 167px;
        }
        .style8
        {
        }
        .style10
        {
            width: 167px;
            height: 26px;
        }
        .style11
        {
            height: 26px;
        }
        .style12
        {
            width: 15px;
            height: 26px;
        }
        .style14
        {
            width: 403px;
            height: 26px;
        }
        .fn_ContentTital
        {}
        .style15
        {
            width: 208px;
            height: 26px;
        }
        .style16
        {
            width: 205px;
            height: 26px;
        }
        .style18
        {
            width: 209px;
            height: 20px;
        }
        .style21
        {
            width: 403px;
            height: 20px;
        }
        .style22
        {
            width: 15px;
            height: 20px;
        }
        .style23
        {
            width: 209px;
            height: 26px;
        }
        .style25
        {
            height: 26px;
        }
        .style26
        {
            width: 105px;
            height: 20px;
        }
        .style27
        {
            width: 105px;
            height: 26px;
        }
        .style28
        {
            width: 105px;
        }
        .fn_Content
        {}
        .style29
        {
            width: 167px;
            height: 27px;
        }
        .style30
        {
            height: 27px;
        }
        .style31
        {
            width: 15px;
            height: 27px;
        }
        .style32
        {
            width: 403px;
            height: 27px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.min.js"></script>

                    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.16/jquery-ui.min.js"></script>

                    <link type="text/css" rel="Stylesheet" href="http://ajax.microsoft.com/ajax/jquery.ui/1.8.16/themes/redmond/jquery-ui.css" />

                    <script type="text/javascript">
                        //ประกาศตัวแปรเพื่อรับใหม่
                        $jq = $.noConflict();
                        $jq().ready(

                 function () {

                     $jq('#<%=txtsDate.ClientID%>').datepicker(
                     {
                         showOn: 'button',
                         buttonText: 'Show Date',
                         buttonImageOnly: true,
                         buttonImage: 'http://jqueryui.com/resources/demos/datepicker/images/calendar.gif',
                         dateFormat: 'dd/mm/yy',
                         constrainInput: true
                     }
                     );

                     $jq('#<%=txteDate.ClientID%>').datepicker(
                     {
                         showOn: 'button',
                         buttonText: 'Show Date',
                         buttonImageOnly: true,
                         buttonImage: 'http://jqueryui.com/resources/demos/datepicker/images/calendar.gif',
                         dateFormat: 'dd/mm/yy',
                         constrainInput: true
                     }
                     );


                     $jq('#<%=txtBirthday.ClientID%>').datepicker(
                     {
                         showOn: 'button',
                         buttonText: 'Show Date',
                         buttonImageOnly: true,
                         buttonImage: 'http://jqueryui.com/resources/demos/datepicker/images/calendar.gif',
                         dateFormat: 'dd/mm/yy',
                         constrainInput: true
                     }
                     );

                 });
       
                    </script>


                     <script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
                    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/css/select2.min.css" rel="stylesheet" />
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/js/select2.min.js" type="text/javascript"></script>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $("#drpBranch").select2({
                            });
                        });
                    </script>
                    
                     <script type="text/javascript">
                         $(document).ready(function () {
                             $("#drpDepartment").select2({
                             });
                         });
                    </script>

                   <script type="text/javascript">
                       $(document).ready(function () {
                           $("#drpDepartments").select2({
                           });
                       });
                    </script>
        <br />
        <br />
         <br />


          <div class="btn-group">
                                 <asp:button id="backButton" runat="server" text="Back" class="btn btn-primary"
OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>

   </div>

   <div class="col-lg-12">
                <h3>
                   Employee Data
                </h3>
                
                </div>

  
  <table class="TBBody" style="font-family: tahoma; font-size: small">
          
           <%--<asp:ImageButton ID="Cal1" runat="server" Height="21px" 
                                                  ImageUrl="images\dt_02.gif" Width="24px" />--%>

            <tr>
                <td align="right" class="style10">
                    <asp:Label ID="Label50" runat="server" CssClass="fn_ContentTital" 
                        Text="*" Font-Bold="True" ForeColor="Red"></asp:Label>
                    <asp:Label ID="Label18" runat="server" 
                        Text="Site :" Font-Names="Tahoma"></asp:Label>
                </td>
                <td align="left" valign="middle" class="style16">
                    <asp:DropDownList ID="drpSite" runat="server" Width="150px" 
                        style="margin-bottom: 0px" Height="25px">
                        <asp:ListItem>kvn-office</asp:ListItem>
                        <asp:ListItem>kvn-factory</asp:ListItem>
                        <asp:ListItem>aff</asp:ListItem>
                        <asp:ListItem>be rich</asp:ListItem>
                        <asp:ListItem>fg</asp:ListItem>
                        <asp:ListItem>lion</asp:ListItem>
                        <asp:ListItem>ubp</asp:ListItem>
                        <asp:ListItem>SWC</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td align="right" class="style27">
                    </td>
                <td align="right" class="style11">
                <asp:Label ID="Label3" runat="server" CssClass="fn_ContentTital" 
                        Text="*" Font-Bold="True" ForeColor="Red"></asp:Label>
                 <asp:Label ID="Label2" runat="server" 
                        Text="Work Place :" Font-Names="Tahoma"></asp:Label>
                </td>
                <td align="left" valign="middle" class="style11">
                    <asp:DropDownList ID="drpWorkplace" runat="server" Width="320px" 
                        ClientIDMode="Static" Font-Size="Small">
                    </asp:DropDownList>
                </td>
                <td align="left" valign="middle" class="style11">
                    &nbsp;
                </td>
                <td align="left" class="style8" rowspan="6">
                    <asp:Image ID="img" runat="server" ImageUrl="~/Images/Employee/noImage.jpg" 
                        Height="179px" Width="163px" BorderWidth="1px" />
                    </td>
                <td align="left" class="style12">
                    &nbsp;
                </td>
            </tr>

            <tr>
                <td align="right" class="style10">
                    <asp:Label ID="Label37" runat="server" CssClass="fn_ContentTital" 
                        Text="*" Font-Bold="True" ForeColor="Red"></asp:Label>
                    <asp:Label ID="Label38" runat="server" CssClass="fn_ContentTital" 
                        Text="รหัสพนักงาน :"></asp:Label>
                </td>
                <td align="left" valign="middle" class="style15">
                    <asp:TextBox ID="txtID" runat="server" CssClass="fn_Content" Width="200px"></asp:TextBox>
                </td>
                <td align="left" class="style27">

         <%--<asp:ImageButton ID="Cal1" runat="server" Height="21px" 
                                                  ImageUrl="images\dt_02.gif" Width="24px" />--%>&nbsp;&nbsp;</td>
                <td align="right" class="style11">
                    <asp:Label ID="Label51" runat="server" CssClass="fn_ContentTital" 
                        Text="เลขประจำตัวประชาชน :" Width="130px"></asp:Label>
                </td>
                <td align="left" valign="middle" class="style11">
                    <asp:TextBox ID="txtIDcard" runat="server" CssClass="fn_Content" Width="200px"></asp:TextBox>
                </td>
                <td align="left" valign="middle" class="style11">
                    &nbsp;</td>
                <td align="left" class="style12">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="right" class="style10">
                    <asp:Label ID="Label52" runat="server" CssClass="fn_ContentTital" 
                        Text="คำนำหน้าชื่อ :"></asp:Label>
                </td>
                <td align="left" valign="middle" class="style11">
                    <asp:TextBox ID="txtTitle" runat="server" CssClass="fn_Content" Width="200px"></asp:TextBox>
                </td>
                <td align="left" valign="middle" class="style27">
                    &nbsp;</td>
                <td align="right" class="style11">
                    &nbsp;</td>
                <td align="left" valign="middle" class="style11">
                    &nbsp;</td>
                <td align="left" valign="middle" class="style11">
                    &nbsp;</td>
                <td align="left" class="style12">
                    &nbsp;</td>
            </tr>
            
            <tr>
                <td align="right" class="style10">
                    <asp:Label ID="Label39" runat="server" CssClass="fn_ContentTital" 
                        Text="*" Font-Bold="True" ForeColor="Red"></asp:Label>
                    <asp:Label ID="Label11" runat="server" CssClass="fn_ContentTital" 
                        Text="ชื่อ - นามสกุล :"></asp:Label>
                </td>
                <td align="left" valign="middle" class="style11" colspan="2">
                    <asp:TextBox ID="txtFullName" runat="server" CssClass="fn_Content" 
                        Width="200px"></asp:TextBox>
                </td>
                <td align="right" class="style11">
                    <asp:Label ID="Label58" runat="server" CssClass="fn_ContentTital" 
                        Text="*" Font-Bold="True" ForeColor="Red"></asp:Label>
                    <asp:Label ID="Label57" runat="server" CssClass="fn_ContentTital" 
                        Text="ชื่อ - นามสกุล (EN) :"></asp:Label>
                </td>
                <td align="left" valign="middle" class="style11">
                    <asp:TextBox ID="txtFullNameEN" runat="server" CssClass="fn_Content" 
                        Width="200px"></asp:TextBox>
                </td>
                <td align="left" valign="middle" class="style11">
                    &nbsp;</td>
                <td align="left" class="style12">
                    &nbsp;
                </td>
            </tr>
            
            
            <tr>
                <td align="right" class="style10">
                    <asp:Label ID="Label36" runat="server" CssClass="fn_ContentTital" 
                        Text="*" Font-Bold="True" ForeColor="Red"></asp:Label>
                    <asp:Label ID="Label21" runat="server" CssClass="fn_ContentTital" 
                        Text="ชื่อ :"></asp:Label>
                </td>
                <td align="left" valign="middle" class="style11" colspan="2">
                    <asp:TextBox ID="txtName" runat="server" CssClass="fn_Content" Width="200px"></asp:TextBox>
                </td>
                <td align="right" class="style11">
                    <asp:Label ID="Label55" runat="server" CssClass="fn_ContentTital" 
                        Text="ชื่อ (EN) :"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style11">
                    <asp:TextBox ID="txtNameEN" runat="server" CssClass="fn_Content" Width="200px"></asp:TextBox>
                    </td>
                <td align="left" valign="middle" class="style11">
                    &nbsp;</td>
                <td align="left" class="style12">
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right" class="style10">
                    <asp:Label ID="Label40" runat="server" CssClass="fn_ContentTital" 
                        Text="*" Font-Bold="True" ForeColor="Red"></asp:Label>
                    <asp:Label ID="Label22" runat="server" CssClass="fn_ContentTital" 
                        Text="นามสกุล :"></asp:Label>
                </td>
                <td align="left" valign="middle" class="style11" colspan="2">
                    <asp:TextBox ID="txtSurName" runat="server" CssClass="fn_Content" Width="200px"></asp:TextBox>
                </td>
                <td align="right" class="style11">
                    <asp:Label ID="Label56" runat="server" CssClass="fn_ContentTital" 
                        Text="นามสกุล (EN) :"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style11">
                    <asp:TextBox ID="txtSurNameEN" runat="server" CssClass="fn_Content" 
                        Width="200px"></asp:TextBox>
                    </td>
                <td align="left" valign="middle" class="style11">
                    &nbsp;</td>
                <td align="left" class="style12">
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right" class="style10">
                    <asp:Label ID="Label23" runat="server" CssClass="fn_ContentTital" 
                        Text="ชื่อเล่น :"></asp:Label>
                </td>
                <td align="left" valign="middle" class="style11" colspan="2">
                    <asp:TextBox ID="txtNickName" runat="server" CssClass="fn_Content" 
                        Width="200px"></asp:TextBox>
                </td>
                <td align="right" class="style11">
                    &nbsp;</td>
                <td align="left" valign="middle" class="style11">
                    &nbsp;</td>
                <td align="left" valign="middle" class="style11">
                    &nbsp;</td>
                <td align="left" class="style8">
                            <%--<asp:FileUpload ID="FileUpload1" runat="server" CssClass="sample" 
                                style="font-family: Tahoma" Width="275px" />--%>
                                <%--ปิดเพราะจะอัพโหลดเป็นแบบไฟล์ของพี่วิทย์ น้ำ 04/08/2021--%>
                    </td>
                <td align="left" class="style12">
                            <asp:TextBox ID="txtPic" runat="server" BorderColor="White" BorderWidth="0px" 
                                Height="0px" Width="93px"></asp:TextBox>
                            </td>
            </tr>
            
            <tr>
                <td align="right" class="style6">
                    <asp:Label ID="Label41" runat="server" CssClass="fn_ContentTital" 
                        Text="*" Font-Bold="True" ForeColor="Red"></asp:Label>
                    <asp:Label ID="Label24" runat="server" CssClass="fn_ContentTital" 
                        Text="ฝ่าย :"></asp:Label>
                </td>
                <td align="left" valign="middle" class="style3" colspan="2">
                    <asp:DropDownList ID="drpDepartments" runat="server" Width="320px" 
                        ClientIDMode="Static" Font-Size="Small">
                    </asp:DropDownList>
                </td>
                <td align="right">
                    <asp:Label ID="Label9" runat="server" CssClass="fn_ContentTital" 
                        Text="สาขา :"></asp:Label>
                </td>
                <td align="left" valign="middle">
                    <asp:DropDownList ID="drpBranch" runat="server" Width="200px" 
                         ClientIDMode="Static" Font-Size="Small">
                    </asp:DropDownList>

                   
                                 </td>
                <td align="left" valign="middle">
                            <asp:TextBox ID="txtPic0" runat="server" BorderColor="White" BorderWidth="0px" 
                                Height="0px" Width="55px"></asp:TextBox>
                    </td>
                <td align="left" class="style8">
                    &nbsp;</td>
                <td align="left" class="style1">
                </td>
            </tr>
            <tr>
                <td align="right" class="style29">
                    <asp:Label ID="Label42" runat="server" CssClass="fn_ContentTital" 
                        Text="*" Font-Bold="True" ForeColor="Red"></asp:Label>
                    <asp:Label ID="Label15" runat="server" CssClass="fn_ContentTital" 
                        Text="แผนก :"></asp:Label>
                </td>
                <td align="left" valign="middle" class="style30" colspan="2">
                    <asp:DropDownList ID="drpDepartment" runat="server" Width="320px" 
                        ClientIDMode="Static" Font-Size="Small">
                    </asp:DropDownList>
                </td>
                <td align="left" class="style30">
                    &nbsp;</td>
                <td align="left" class="style30">
                    &nbsp;</td>
                <td align="left" valign="middle" class="style30">
                    &nbsp;</td>
                <td align="left" class="style8">
                    <asp:Label ID="Label59" runat="server" CssClass="fn_ContentTital" 
                        Text="ระบบ PMS - online" Font-Bold="True"></asp:Label>
                    </td>
                <td align="left" class="style8">
                            &nbsp;</td>
                <td align="left" class="style31">
                </td>
            </tr>
          
            <tr>
                <td align="right" class="style29">
                    <asp:Label ID="Label43" runat="server" CssClass="fn_ContentTital" 
                        Text="*" Font-Bold="True" ForeColor="Red"></asp:Label>
                    <asp:Label ID="Label31" runat="server" CssClass="fn_ContentTital" 
                        Text="รหัสผู้จัดการ :"></asp:Label>
                </td>
                <td align="left" valign="middle" class="style30" colspan="2">
                    <asp:TextBox ID="txtManagerid" runat="server" CssClass="fn_Content" 
                        Width="200px"></asp:TextBox>
                </td>
                <td align="right" class="style30">
                    <asp:Label ID="Label64" runat="server" CssClass="fn_ContentTital" 
                        Text="รหัสผู้จัดการ PMS :"></asp:Label>
                    </td>
                <td align="left" class="style30">
                    <asp:TextBox ID="txtManagerid_pms" runat="server" CssClass="fn_Content" 
                        Width="200px"></asp:TextBox>
                    </td>
                <td align="left" valign="middle" class="style30">
                    </td>
                <td align="left" class="style8">
                    <asp:RadioButtonList ID="rdoPMS" runat="server" 
                        RepeatColumns="2" Width="150px" Height="25px">
                        <asp:ListItem Value="Yes">ใช้</asp:ListItem>
                        <asp:ListItem Value="No">ไม่ใช้</asp:ListItem>
                    </asp:RadioButtonList>
                    </td>
                <td align="left" class="style32">
                            &nbsp;</td>
                <td align="left" class="style31">
                    </td>
            </tr>
          
            <tr>
                <td align="right" class="style10">
                    <asp:Label ID="Label44" runat="server" CssClass="fn_ContentTital" 
                        Text="*" Font-Bold="True" ForeColor="Red"></asp:Label>
                    <asp:Label ID="Label29" runat="server" CssClass="fn_ContentTital" 
                        Text="ระดับพนักงาน :"></asp:Label>
                </td>
                <td align="left" valign="middle" class="style11" colspan="2">
                    <asp:TextBox ID="txtEmpLevel" runat="server" CssClass="fn_Content" 
                        Width="100px"></asp:TextBox>
                    <asp:Label ID="Label60" runat="server" CssClass="fn_ContentTital" 
                        Text="(ตัวอย่าง A,B,C,D)" Font-Size="X-Small"></asp:Label>
                </td>
                <td align="right" class="style11">
                    <asp:Label ID="Label45" runat="server" CssClass="fn_ContentTital" 
                        Text="*" Font-Bold="True" ForeColor="Red"></asp:Label>
                    <asp:Label ID="Label12" runat="server" CssClass="fn_ContentTital" 
                        Text="ระดับตำแหน่ง :"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style11">
                    <asp:TextBox ID="txtPositionID" runat="server" CssClass="fn_Content" 
                        Width="100px"></asp:TextBox>
                    <asp:Label ID="Label61" runat="server" CssClass="fn_ContentTital" 
                        Text="(ตัวอย่าง D2,D3)" Font-Size="X-Small"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style11">
                    &nbsp;</td>
                <td align="left" class="style8">
                    <asp:Label ID="Label32" runat="server" CssClass="fn_ContentTital" 
                        Text="สำหรับการ Approve Leave Online " Font-Bold="True"></asp:Label>
                    </td>
                <td align="left" class="style12">
                    &nbsp;</td>
            </tr>
          
            <tr>
                <td align="right" class="style10">
                    <asp:Label ID="Label54" runat="server" CssClass="fn_ContentTital" 
                        Text="ตำแหน่ง :" Width="100px"></asp:Label>
                </td>
                <td align="left" valign="middle" class="style11" colspan="2">
                    <asp:TextBox ID="txtPosition" runat="server" CssClass="fn_Content" 
                        Width="200px"></asp:TextBox>
                </td>
                <td align="right" class="style11">
                    <asp:Label ID="Label30" runat="server" CssClass="fn_ContentTital" 
                        Text="ตำแหน่ง (EN) :" Width="100px"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style11">
                    <asp:TextBox ID="txtPositionEN" runat="server" CssClass="fn_Content" 
                        Width="200px"></asp:TextBox>
                    </td>
                <td align="left" valign="middle" class="style11">
                    &nbsp;</td>
                <td align="left" class="style8" colspan="2">
                    <asp:Label ID="Label46" runat="server" CssClass="fn_ContentTital" 
                        Text="*" Font-Bold="True" ForeColor="Red"></asp:Label>
                    <asp:Label ID="Label33" runat="server" CssClass="fn_ContentTital" 
                        Text="ระดับพนักงาน :" Width="100px"></asp:Label>
                    <asp:TextBox ID="txtEmpLevel_Leave" runat="server" CssClass="fn_Content" 
                        Width="100px"></asp:TextBox>
                    <asp:Label ID="Label62" runat="server" CssClass="fn_ContentTital" 
                        Text="(ตัวอย่าง A,B,C,D)" Font-Size="X-Small"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right" class="style10">
                    <asp:Label ID="Label13" runat="server" CssClass="fn_ContentTital" 
                        Text="เพศ :" ToolTip="วันที่เกิด"></asp:Label>
                </td>
                <td align="left" valign="middle" class="style11">
                    <asp:RadioButtonList ID="rdoSex" runat="server" 
                        RepeatColumns="2" Width="188px" Height="25px">
                        <asp:ListItem>Male</asp:ListItem>
                        <asp:ListItem>Female</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td align="left" class="style27">
                    &nbsp;</td>
                <td align="right" class="style11">
                    <asp:Label ID="Label48" runat="server" CssClass="fn_ContentTital" 
                        Text="*" Font-Bold="True" ForeColor="Red"></asp:Label>
                    <asp:Label ID="Label14" runat="server" CssClass="fn_ContentTital" 
                        Text="Birthday :" ToolTip="เดือนเกิด"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style11">
                    <asp:TextBox ID="txtBirthday" runat="server" Width="150px"></asp:TextBox>
                    <asp:Label ID="Label1" runat="server" CssClass="fn_ContentTital" 
                        Text="(ตัวอย่าง 31/01/1990)" Font-Size="X-Small"></asp:Label>
                </td>
                <td align="left" valign="middle" class="style11">
                    &nbsp;</td>
                <td align="left" class="style8" colspan="2">
                    <asp:Label ID="Label47" runat="server" CssClass="fn_ContentTital" 
                        Text="*" Font-Bold="True" ForeColor="Red"></asp:Label>
                    <asp:Label ID="Label34" runat="server" CssClass="fn_ContentTital" 
                        Text="ระดับตำแหน่ง :" Width="100px"></asp:Label>
                    <asp:TextBox ID="txtPositionID_Leave" runat="server" CssClass="fn_Content" 
                        Width="100px"></asp:TextBox>
                    <asp:Label ID="Label63" runat="server" CssClass="fn_ContentTital" 
                        Text="(ตัวอย่าง D2,D3,D4)" Font-Size="X-Small"></asp:Label>
                </td>
            </tr>
            
            <tr>
                <td align="right" class="style6">
                    <asp:Label ID="Label25" runat="server" CssClass="fn_ContentTital" 
                        Text="ที่อยู่ :" ToolTip="เดือนเกิด" Width="160px"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style2" colspan="2">
                    <asp:TextBox ID="txtAddress" runat="server" CssClass="fn_Content" Width="300px" 
                        Height="39px" TextMode="MultiLine"></asp:TextBox>
                </td>
                <td align="right">
                    &nbsp;</td>
                <td align="left" valign="middle">
                    </td>
                <td align="left" valign="middle">
                    &nbsp;</td>
                <td align="left" class="style8">
                    <asp:Label ID="Label35" runat="server" CssClass="fn_ContentTital" 
                        Text="ตรวจสอบเงื่อนไข :" Width="111px"></asp:Label>
                    <asp:RadioButtonList ID="rdoLeaveCheck" runat="server" 
                        RepeatColumns="2" Width="188px" Height="25px">
                        <asp:ListItem>check</asp:ListItem>
                        <asp:ListItem>unchecked</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td align="left" class="style1">
                    </td>
            </tr>
            <tr>
                <td align="right" class="style6">
                    <asp:Label ID="Label26" runat="server" CssClass="fn_ContentTital" 
                        Text="เบอร์โทรศัพท์ :" ToolTip="เดือนเกิด"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style2">
                    <asp:TextBox ID="txtTel" runat="server" Width="200px"></asp:TextBox>
                </td>
                <td align="right" class="style28">
                    &nbsp;</td>
                <td align="right">
                    <asp:Label ID="Label27" runat="server" CssClass="fn_ContentTital" 
                        Text="เบอร์ต่อ :" ToolTip="เดือนเกิด"></asp:Label>
                </td>
                <td align="left" valign="middle">
                    <asp:TextBox ID="txtExt" runat="server" Width="200px"></asp:TextBox>
                    </td>
                <td align="left" valign="middle">
                    &nbsp;</td>
                <td align="left" class="style8">
                    <asp:Label ID="Label53" runat="server" CssClass="fn_ContentTital" 
                        Text="การอนุมัติถึงระดับ B2 :"></asp:Label>
                    </td>
                <td align="left" class="style1">
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right" class="style6">
                    <asp:Label ID="Label28" runat="server" CssClass="fn_ContentTital" 
                        Text="Email :" ToolTip="เดือนเกิด"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style2" colspan="2">
                    <asp:TextBox ID="txtEmail" runat="server" Width="300px"></asp:TextBox>
                </td>
                <td align="right">
                    &nbsp;</td>
                <td align="left" valign="middle">
                    &nbsp;</td>
                <td align="left" valign="middle">
                    &nbsp;</td>
                <td align="left" class="style8">
                    <asp:RadioButtonList ID="rdoCheckB2" runat="server" 
                        RepeatColumns="2" Width="188px" Height="25px">
                        <asp:ListItem>check</asp:ListItem>
                        <asp:ListItem>unchecked</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td align="left" class="style1">
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right" class="style10">
                    <asp:Label ID="Label49" runat="server" CssClass="fn_ContentTital" 
                        Text="*" Font-Bold="True" ForeColor="Red"></asp:Label>
                    <asp:Label ID="Label17" runat="server" CssClass="fn_ContentTital" 
                        Text="วันที่เริ่มทำงาน :"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style11">
                    <asp:TextBox ID="txtsDate" runat="server" Width="150px"></asp:TextBox>
                    <asp:Label ID="Label4" runat="server" CssClass="fn_ContentTital" 
                        Text="(ตัวอย่าง31/01/1990)" Font-Size="X-Small"></asp:Label>
                </td>
                <td align="right" class="style27">
                    </td>
                <td align="right" class="style11">
                    <asp:Label ID="Label19" runat="server" 
                        Text="สิ้นสุด :" Width="87px"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style11">
                    <asp:TextBox ID="txteDate" runat="server" Width="150px"></asp:TextBox>
                    <asp:Label ID="Label5" runat="server" CssClass="fn_ContentTital" 
                        Text="(ตัวอย่าง 31/01/1990)" Font-Size="X-Small"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style11">
                    &nbsp;</td>
                <td align="left" class="style14">
                    &nbsp;</td>
                <td align="left" class="style12">
                    </td>
            </tr>
            <tr>
                <td align="right" class="style6">
                    <asp:Label ID="Label20" runat="server" CssClass="fn_ContentTital" 
                        Text="สถานะ :" Width="150px"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style2">
                    <asp:RadioButtonList ID="rdoStatus" runat="server" 
                        RepeatColumns="2" Width="188px" Height="25px">
                        <asp:ListItem>Active</asp:ListItem>
                        <asp:ListItem>InActive</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td align="left" class="style28">
                    &nbsp;</td>
                <td align="right">
                            &nbsp;</td>
                <td align="left" valign="middle">
                    &nbsp;</td>
                <td align="left" valign="middle">
                    &nbsp;</td>
                <td align="left" class="style8">
                    &nbsp;</td>
                <td align="left" class="style1">
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right" class="fh5co-spacer-xxs">
                    </td>
                <td align="left" valign="middle" class="style18">
                    </td>
                <td align="left" class="style26">
                    </td>
                <td align="right" class="fh5co-spacer-xxs">
                            </td>
                <td align="left" valign="middle" class="fh5co-spacer-xxs">
                    </td>
                <td align="left" valign="middle" class="fh5co-spacer-xxs">
                    </td>
                <td align="left" class="style21">
                    </td>
                <td align="left" class="style22">
                    </td>
            </tr>
            <tr>
                <td align="right" class="style6">
                    &nbsp;</td>
                <td align="left" valign="middle" class="style2">
                    <asp:Button ID="btnSave" runat="server" Text="Save" 
                    BackColor="#990000" Font-Bold="True" ForeColor="White" Height="30px" 
                    ToolTip="บันทึกข้อมูล" Width="100px" Font-Names="Tahoma" Font-Size="Medium" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
                    BackColor="#990000" Font-Bold="True" ForeColor="White" Height="30px" 
                    ToolTip="ยกเลิก" Width="100px" Font-Names="Tahoma" Font-Size="Medium" />
                </td>
                <td align="left" class="style28">
                    &nbsp;</td>
                <td align="right">
                            &nbsp;</td>
                <td align="left" valign="middle">
                    &nbsp;</td>
                <td align="left" valign="middle">
                    &nbsp;</td>
                <td align="left" class="style8">
                    &nbsp;</td>
                <td align="left" class="style1">
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right" class="style6">
                    &nbsp;</td>
                <td align="left" valign="middle" class="style2">
                    &nbsp;</td>
                <td align="left" class="style28">
                    &nbsp;</td>
                <td align="right">
                            &nbsp;</td>
                <td align="left" valign="middle">
                    &nbsp;</td>
                <td align="left" valign="middle">
                    &nbsp;</td>
                <td align="left" class="style8">
                    &nbsp;</td>
                <td align="left" class="style1">
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right" class="style6">
                    &nbsp;</td>
                <td align="left" valign="middle" class="style2">
                    &nbsp;</td>
                <td align="left" class="style28">
                    &nbsp;</td>
                <td align="right">
                            &nbsp;</td>
                <td align="left" valign="middle">
                    &nbsp;</td>
                <td align="left" valign="middle">
                    &nbsp;</td>
                <td align="left" class="style8">
                    &nbsp;</td>
                <td align="left" class="style1">
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right" class="style6">
                    &nbsp;</td>
                <td align="left" valign="middle" class="style2">
                    &nbsp;</td>
                <td align="left" class="style28">
                    &nbsp;</td>
                <td align="right">
                            &nbsp;</td>
                <td align="left" valign="middle">
                    &nbsp;</td>
                <td align="left" valign="middle">
                    &nbsp;</td>
                <td align="left" class="style8">
                    &nbsp;</td>
                <td align="left" class="style1">
                    &nbsp;</td>
            </tr>
        </table>

</asp:Content>
