﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_Login.Master" CodeBehind="Login.aspx.vb" Inherits="Aroma_HRPortal.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    .style1
    {
        width: 12px;
    }
</style>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
   <div class="row" style="margin-top:30px; text-align:center">
    <img src="Images/logotrans.png" 
           style="margin-right:5px; height: 45px; width: 134px;" /> <span style="font-weight:600; font-size:22px">Authorize HR Portal </span>
    <hr />
    
    
    <table cellpadding="5" style="margin-left:37%">
        
       
       <%-- <tr>
            <td><span id="lblMsg" class="error"></span></td>
        </tr>--%>
        <tr><td>&nbsp;</td>
            <td style="text-align:right">
                &nbsp;</td>
            <td style="text-align:right" class="style1">
                &nbsp;</td>
        </tr>
        <tr><td>&nbsp;</td>
            <td style="text-align:right">
                </td>
            <td style="text-align:right" class="style1">
                &nbsp;</td>
        </tr>
        <tr><td>
            <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Names="Tahoma" 
                ForeColor="Black" Text="Username :  " Width="120px"></asp:Label>
            </td>
            <td style="text-align:right">
                <asp:TextBox ID="TextBox1" runat="server" Width="150px" Height="30px"></asp:TextBox>
                </td>
            <td style="text-align:right" class="style1">
                &nbsp;</td>
        </tr>
        <tr><td>
            <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Tahoma" 
                ForeColor="Black" Text="Password :" Width="120px"></asp:Label>
            </td>
            <td style="text-align:right">
                <asp:TextBox ID="TextBox2" runat="server" TextMode="Password" Width="150px" 
                    Height="30px"></asp:TextBox>
                </td>
            <td style="text-align:right" class="style1">
                &nbsp;</td>
        </tr>
        <tr><td></td>
            <td style="text-align:left">
                &nbsp;<asp:Button ID="btnLogin" runat="server" Text="Log in" 
                    BackColor="#990000" Font-Bold="True" ForeColor="White" Height="30px" 
                    ToolTip="Log in เพื่อเข้าสู่ระบบ" Width="100px" Font-Names="Tahoma" 
                    Font-Size="Medium" />
            </td>
            <td style="text-align:right" class="style1">
                &nbsp;</td>
        </tr>
    </table>



</div>
<div id="divUrl" data-get-url-verify="@Url.Action("VerifyAccountAdmin", "Account")"></div>

    </form>

</asp:Content>

