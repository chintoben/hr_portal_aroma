﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="Flow_IT.aspx.vb" Inherits="Aroma_HRPortal.Flow_IT" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/development-training/banner.jpg" alt=""/></div>
<!-- end banner -->

<section id="about-us" class="blog-category">
<div class="container">
	<div class="theme-title tt-edit category-title">
        <ul>
            <li><a href="index_hr.aspx">HOME</a></li>
            <li>/</li>
            <li><a href="Training1.aspx">หลักสูตรพัฒนาและฝึกอบรม</a></li>
        </ul>
		<h2>สมรรถนะ (COMPETENCY)</h2>
	</div> <!-- /.theme-title -->

	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-6 div-empty"></div>
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="far icon-045-skills"></i>
				</div>
				<p><a href="Document/DevelopmentAndTraining/Training_Competency/1.การพัฒนาเน้นสมรรถนะ (Competency-Based Development).pdf" target="_blank" class="tran3s">การพัฒนาเน้นสมรรถนะ<br>(Competency-Based<br>Development)</a></p>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="far icon-119-businessman-discussing-a-progress-report"></i>
				</div>
				<p><a href="Document/DevelopmentAndTraining/Training_Competency/2.แนวทางการพัฒนาสมรรถนะ (Our Solutions Competency Development).pdf" target="_blank" class="tran3s">แนวทางการพัฒนาสมรรถนะ<br>(Our Solutions<br>Competency Development)</a></p>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6 div-empty"></div>
	</div> <!-- /.row -->
	<div class="blog-category-bt">
		<div class="btn-group">
			<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
		</div>
	</div>
</div> <!-- /.container -->
</section>

</asp:Content>
