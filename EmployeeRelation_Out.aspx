﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="EmployeeRelation_Out.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_Contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/hr.jpg" alt=""/></div>
<!-- end banner -->

<section id="team-section">
	<div class="container">
		<div class="theme-title">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
            </ul>
			<h2>พนักงานลาออก</h2>
			<!--p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p-->
		</div> <!-- /.theme-title -->

		<div class="clear-fix team-member-wrapper">
		
			<div class="float-left">
				<div class="employee_out">
					<div class="img">
						<img src="Images/HR/1.ศิริพร-นิลกำแหง.jpg" alt=""/>
					</div>
					<div class="employee_name">
						<h5>คุณศิริพร  นิลกำแหง (น้อง)</h5>
						<p>ผู้อำนวยการฝ่ายทรัพยากรมนุษย์</p>
						<div>ทำงานวันสุดท้าย : 2/2/2561</div>
					</div>
				</div>
			</div>

			<div class="float-left">
				<div class="employee_out">
					<div class="img">
						<img src="Images/HR/1.ศิริพร-นิลกำแหง.jpg" alt=""/>
					</div>
					<div class="employee_name">
						<h5>คุณศิริพร  นิลกำแหง (น้อง)</h5>
						<p>ผู้อำนวยการฝ่ายทรัพยากรมนุษย์</p>
						<div>ทำงานวันสุดท้าย : 2/2/2561</div>
					</div>
				</div>
			</div>
			
			<div class="float-left">
				<div class="employee_out">
					<div class="img">
						<img src="Images/HR/1.ศิริพร-นิลกำแหง.jpg" alt=""/>
					</div>
					<div class="employee_name">
						<h5>คุณศิริพร  นิลกำแหง (น้อง)</h5>
						<p>ผู้อำนวยการฝ่ายทรัพยากรมนุษย์</p>
						<div>ทำงานวันสุดท้าย : 2/2/2561</div>
					</div>
				</div>
			</div>
			
			<div class="float-left">
				<div class="employee_out">
					<div class="img">
						<img src="Images/HR/1.ศิริพร-นิลกำแหง.jpg" alt=""/>
					</div>
					<div class="employee_name">
						<h5>คุณศิริพร  นิลกำแหง (น้อง)</h5>
						<p>ผู้อำนวยการฝ่ายทรัพยากรมนุษย์</p>
						<div>ทำงานวันสุดท้าย : 2/2/2561</div>
					</div>
				</div>
			</div>
			
			<div class="float-left">
				<div class="employee_out">
					<div class="img">
						<img src="Images/HR/1.ศิริพร-นิลกำแหง.jpg" alt=""/>
					</div>
					<div class="employee_name">
						<h5>คุณศิริพร  นิลกำแหง (น้อง)</h5>
						<p>ผู้อำนวยการฝ่ายทรัพยากรมนุษย์</p>
						<div>ทำงานวันสุดท้าย : 2/2/2561</div>
					</div>
				</div>
			</div>
			
		</div> <!-- /.team-member-wrapper -->
	</div> <!-- /.conatiner -->
</section>
 
</asp:Content>

