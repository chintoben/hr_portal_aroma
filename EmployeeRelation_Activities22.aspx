﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="EmployeeRelation_Activities22.aspx.vb" Inherits="Aroma_HRPortal.EmployeeRelation_Activities22" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



  <script type = "text/javascript">
      function SetTarget() {
          document.forms[0].target = "_blank";
      }
</script>

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/Activities.jpg" 
        alt="" /></div>
<!-- end banner -->

<section id="news-section">
	<div class="container">
	
	<div class="theme-title">
        <ul>
        	<li><a href="index_hr.aspx">HOME</a></li>
        </ul>
		<h2>กิจกรรมต่างๆ</h2>
     <%--   <div>
            <select>
                <option>พ.ศ. 2560</option>
                <option>พ.ศ. 2561</option>
            </select>

            <select>
                <option>มกราคม</option>
                <option>กุมภาพันธ์</option>
                <option>มีนาคม</option>
                <option>เมษายน</option>
                <option>พฤษภาคม</option>
                <option>มิถุนายน</option>
                <option>กรกฎาคม</option>
                <option>สิงหาคม</option>
                <option>กันยายน</option>
                <option>ตุลาคม</option>
                <option>พฤศจิกายน</option>
                <option>ธันวามคม</option>
            </select>
        </div>--%>
        <br />
		<!--p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p-->
	
    </div> 
    <!-- /.theme-title -->
	
		<div class="clear-fix news-wrapper">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="single-price-table">
					<!--i class="fas fa-external-link-alt"></i-->
					<img src="new/images/Activities/RED%20CARPET%20PARTY(ปาร์ตี้%20พรมแดง)%202018.jpg" 
                        alt=""/>
                    
				<%--	<h4>Playback: Akufo-Addo speaks to business community</h4>--%>
                    <asp:Label ID="lblHeader1" runat="server" 
                        Text="&quot;RED CARPET PARTY(ปาร์ตี้ พรมแดง)&quot; 2018" Font-Bold="True"></asp:Label>
					<div class="news-date">
                        <asp:Label ID="lblDate1" runat="server" 
                        Text="28 Feb, 2018"></asp:Label>
                    
                    </div>
					<p>
						  <asp:Label ID="lblDetail1" runat="server" 
                        Text=" กลับมาอีกครั้ง! กับปาร์ตี้สุดมัมส์ RED CARPET PARTY(ปาร์ตี้ พรมแดง) @ The PUBLIC Restaurant & Bar (เกษตร-นวมินทร์ )"></asp:Label>
					</p>
					<%--<a href="EmployeeRelation_ActivitiesDetail.aspx">Read more</a>--%>
                    <asp:Button ID="Button1" runat="server" Text="Read more"  OnClientClick = "SetTarget();"
                        class="btn btn-primary"/>
                
						
				</div>
			</div>
			
		</div>
	</div>
</section>
 
</asp:Content>
