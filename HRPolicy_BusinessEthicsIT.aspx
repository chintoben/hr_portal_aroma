﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_BusinessEthicsIT1.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_BusinessEthicsIT1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/hr.jpg" alt=""/></div>
<!-- end banner -->

<section id="about-us" class="blog-category">
<div class="container">
	<div class="theme-title tt-edit category-title">
        <ul>
            <li><a href="index_hr.aspx">HOME</a></li>
            <li>/</li>
            <li><a href="HRPolicy_Announce.aspx">ระเบียบต่างๆ</a></li>
        </ul>
		<h2>ประกาศ คำสั่ง ระเบียบเกี่ยวกับเทคโนโลยีสารสนเทศ</h2>
	</div> <!-- /.theme-title -->

	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-6 div-empty"></div>
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-005-network-1"></i>
				</div>
				<p><a href="HRPolicy_BusinessEthicsIT1.aspx" class="tran3s">ระเบียบการใช้คอมพิวเตอร์ และระบบเครือข่าย</a></p>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-001-technology"></i>
				</div>
				<p><a href="HRPolicy_BusinessEthicsIT2.aspx" class="tran3s">นโยบายการใช้งานระบบสารสนเทศของ Aroma Group</a></p>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6 div-empty"></div>
	</div> <!-- /.row -->
	<div class="blog-category-bt">
		<div class="btn-group">
			<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
		</div>
	</div>
</div> <!-- /.container -->
</section>

</asp:Content>