﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="DownloadHR.aspx.vb" Inherits="Aroma_HRPortal.DownloadHR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 
    <!-- banner -->
<div class="base-banner"><img src="new/images/banner/form/banner.jpg" alt=""/></div>
<!-- end banner -->

<article class="blog-details-page">
	<div class="container">
		
		<div class="post-heading">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
            </ul>
			<h2>แบบฟอร์ม HR</h2>
		</div> <!-- /.post-heading -->
		
		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix" style="float: none; margin: 0 auto;">
				<div class="blog-details-post-wrapper">
				
					<img src="new/images/banner/form/img1.jpg" alt="Image">

					<p style="padding: 30px 0 0;">
				  		<ul class="p">
							<li><a href="Document/Download/HR/1.1ใบขออัตรากำลังคน.pdf" target="_blank">ใบขออัตรากำลังคน</a></li>
							<li><a href="Document/Download/HR/2.1ขออนุมัติการทำงานล่วงเวลา-ทำงานในวันหยุด (พนง.สนญ.).pdf" target="_blank">ขออนุมัติการทำงานล่วงเวลา-ทำงานในวันหยุด (พนง.สนญ.)</a></li>
							<li><a href="Document/Download/HR/2.2ขอเบิกสวัสดิการ-สิ่งของจากฝ่ายทรัพยากรมนุษย์-ดำเนินการในเรื่องอื่นๆ.pdf" target="_blank">ขอเบิกสวัสดิการ-สิ่งของจากฝ่ายทรัพยากรมนุษย์-ดำเนินการในเรื่องอื่นๆ</a></li>
							<li><a href="Document/Download/HR/2.3แบบฟอร์มขออนุมัติการเดินทาง (Pre Trip Requisition Form).pdf" target="_blank">แบบฟอร์มขออนุมัติการเดินทาง (Pre Trip Requisition Form)</a></li>
							
                            <%--<li><a href="Document/Download/HR/2.4ใบรายงานค่าใช้จ่ายการเดินทาง เบี้ยเลี้ยง ที่พัก.PDF" target="_blank">ใบรายงานค่าใช้จ่ายการเดินทาง เบี้ยเลี้ยง ที่พัก</a></li>--%>
                            <li><a href="Document/Download/HR/ใบเบิกค่าใช้จ่ายการเดินทาง FR-TR-550101 Rev.Mar.2017.pdf" target="_blank">ใบรายงานค่าใช้จ่ายการเดินทาง เบี้ยเลี้ยง ที่พัก</a></li>

							<li><a href="Document/Download/HR/2.5ขอเอกสารสำคัญจากบริษัท.pdf" target="_blank">ขอเอกสารสำคัญจากบริษัท</a></li>
							<li><a href="Document/Download/HR/3.1หนังสือคำสั่งลงโทษทางวินัย (พนักงานลงชื่อรับคำสั่ง).pdf" target="_blank">หนังสือคำสั่งลงโทษทางวินัย (พนักงานลงชื่อรับคำสั่ง)</a></li>
							<li><a href="Document/Download/HR/3.2หนังสือคำสั่งลงโทษทางวินัย (พนักงานไม่ลงชื่อรับคำสั่ง).pdf" target="_blank">หนังสือคำสั่งลงโทษทางวินัย (พนักงานไม่ลงชื่อรับคำสั่ง)</a></li>
							<li><a href="Document/Download/HR/3.หนังสือมอบอำนาจดำเนินการต่างๆ ต่อกลุ่มบริษัท อโรม่า.pdf" target="_blank">หนังสือมอบอำนาจดำเนินการต่างๆ ต่อกลุ่มบริษัท อโรม่า</a></li>
							<li><a href="Document/Download/HR/4.1ขออนุมัติส่งพนักงานเข้ารับการพัฒนาและฝึกอบรมบุคลากร.pdf" target="_blank">ขออนุมัติส่งพนักงานเข้ารับการพัฒนาและฝึกอบรมบุคลากร</a></li>


				  		</ul>
					</p>
				
				</div>
			</div>
		</div>
		
	</div>
</article>

</asp:Content>
