﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_WhoisWho18.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_Contact" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/HR.jpg" alt=""/></div>
<!-- end banner -->

<section id="team-section">
	<div class="container">
		<div class="theme-title">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
                <li>/</li>
                <li><a href="HRPolicy_WhoisWho.aspx">ติดต่อฝ่าย-แผนกต่างๆ</a></li>
            </ul>
			<h2>รายชื่อติดต่อ-บัญชีบริหารและระบบงาน (audit)</h2>
			<!--p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p-->
		</div> <!-- /.theme-title -->

		<div class="clear-fix team-member-wrapper">
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who2019/15.audit/คุณศจีรัตน์%20%20ธนภัทรเวโรจน์.JPG" 
                            alt="" height="150"/>
						<div class="opacity tran4s">
							<h4>คุณศจีรัตน์  ธนภัทรเวโรจน์ (รัตน์)</h4>
							<span>ผู้จัดการฝ่าย</span>
							<p>
								เบอร์ต่อภายใน : 151<br />
                    			<%--โทรศัพท์มือถือ : -<br />--%>
                    			Email : <a href="mailto:sajeerattan@aromathailand.com">sajeerattan@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณศจีรัตน์  ธนภัทรเวโรจน์ (รัตน์)</h5>
						<p>ผู้จัดการฝ่าย</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->
            
            <div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who2019/15.audit/คุณปวริศา%20%20คลังทรัพย์.jpg" 
                            alt="" height="150"/>
						<div class="opacity tran4s">
							<h4>คุณปวริศา คลังทรัพย์ (ใจ)</h4>
							<span>ผู้จัดการแผนก</span>
							<p>
								เบอร์ต่อภายใน : 151<br />
                    			<%--โทรศัพท์มือถือ : -<br />--%>
                    			Email : <a href="mailto:pawarisak@aromathailand.com">pawarisak@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณปวริศา คลังทรัพย์ (ใจ)</h5>
						<p>ผู้จัดการแผนก</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->
			
		</div><!-- /.team-member-wrapper -->
		
		<div class="blog-category-bt">
			<div class="btn-group">
				<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
			</div>
		</div>
		
	</div> <!-- /.conatiner -->
</section>
 
</asp:Content>
