﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="Flow_AS.aspx.vb" Inherits="Aroma_HRPortal.Flow_AS" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/flow/banner.jpg" alt=""/></div>
<!-- end banner -->

<article class="blog-details-page">
	<div class="container">
		
		<div class="post-heading">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
            </ul>
			<h2>หน่วยงาน HR</h2>
		</div> <!-- /.post-heading -->
		
		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix" style="float: none; margin: 0 auto;">
				<div class="blog-details-post-wrapper">
				
					<img src="new/images/banner/benefit/hr_portal2Flow_HR.jpg" alt="Image">

					<p style="padding: 30px 0 0;">
						<ul class="p">
							<%--<li><a href="#" target="_blank"></a></li>
--%>



					<li><a href="Document/6Flow/HR/1.ขั้นตอนการสรรหาและคัดเลือกพนักงาน.pdf"  target="_blank">1.ขั้นตอนการสรรหาและคัดเลือกพนักงาน-Recruitment&Selection Process Nov.01,2017.pdf</a></li>

                    <li>	<a href="Document/6Flow/HR/2.ขั้นตอนการสอบสวนโทษทางวินัย-เรื่องขาด ลา มาสาย Nov.01,2017.pdf"  target="_blank">2.ขั้นตอนการสอบสวนโทษทางวินัย-เรื่องขาด ลา มาสาย Nov.01,2017.pdf</a></li>

                    <li>	<a href="Document/6Flow/HR/3.ขั้นตอนการสอบสวนโทษทางวินัย-เรื่องทุจริต ยักยอก เลิกจ้าง Nov.01,2017.pdf"  target="_blank">3.ขั้นตอนการสอบสวนโทษทางวินัย-เรื่องทุจริต ยักยอก เลิกจ้าง Nov.01,2017.pdf</a></li>

                    <li>	<a href="Document/6Flow/HR/4.ขั้นตอนการลาออก Nov.01,2017.pdf"  target="_blank">4.ขั้นตอนการลาออก Nov.01,2017.pdf</a></li>

                    <li>	<a href="Document/6Flow/HR/5.ขั้นตอนการดำเนินงานพัฒนาฝึกอบรมพนักงาน-จัดภายใน Nov.01,2017.pdf"  target="_blank">5.ขั้นตอนการดำเนินงานพัฒนาฝึกอบรมพนักงาน-จัดภายใน Nov.01,2017.pdf</a></li>

                    <li>	<a href="Document/6Flow/HR/6.ขั้นตอนการดำเนินงานพัฒนาฝึกอบรมพนักงาน-ส่งอบรมภายนอก Nov.01,2017.pdf"  target="_blank">6.ขั้นตอนการดำเนินงานพัฒนาฝึกอบรมพนักงาน-ส่งอบรมภายนอก Nov.01,2017.pdf</a></li>

                    <li>	<a href="Document/6Flow/HR/Flow training Evaluation.pdf"  target="_blank">7.ขั้นตอนการใช้งานระบบ Traning development</a></li>
                    
                    <li>	<a href="Document/6Flow/HR/Flow training Evaluation2.pdf"  target="_blank">8.ขั้นตอนการประเมินผลการฝึกอบรม</a></li>                   


						</ul>
					</p>
				
				</div>
			</div>
		</div>
		
	</div>
</article>

</asp:Content>
