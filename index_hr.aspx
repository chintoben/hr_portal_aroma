﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="index_hr.aspx.vb" Inherits="Aroma_HRPortal.index_hr" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<style>
	.swiper-container {
		width: 100%;
		height: 100%;
	}
	.swiper-slide {
		text-align: center;
		background: #fff;
	}
	.swiper-slide img 
	{
		width: 100%;
	}
	.swiper-pagination-bullet-active {
		opacity: 1;
		background: #af2f2f;
	}
	.swiper-container-horizontal > .swiper-pagination-bullets .swiper-pagination-bullet {
		margin: 0 5px;
	}
	.swiper-pagination-fraction, .swiper-pagination-custom, .swiper-container-horizontal > .swiper-pagination-bullets {
		bottom: 20px;
	}
	.swiper-pagination-bullet {
		width: 10px;
		height: 10px;
	}
	.swiper-button-prev, .swiper-button-next {
	  position: absolute;
	  top: 50%;
	  width: 32px;
	  height: 44px;
	  -moz-background-size: 32px 44px;
	  -webkit-background-size: 32px 44px;
	  background-size: 32px 44px;
	}
	.swiper-button-prev, .swiper-container-rtl .swiper-button-next {
		background-image: url(new/images/index/arrow-left.png);
		left: 20px;
		right: auto;
	}
	.swiper-button-next, .swiper-container-rtl .swiper-button-prev {
		background-image: url(new/images/index/arrow-right.png);
		right: 20px;
		left: auto;
	}
</style>

<!-- banner -->
<!-- Swiper -->
<div class="swiper-container">
    <div class="swiper-wrapper">

        <div class="swiper-slide">
            <asp:Image ID="Image1" runat="server" width="100%" title="คลิ๊กเพื่อดูรายละเอียด"/>
        </div>

        <div class="swiper-slide">
            <asp:Image ID="Image2" runat="server" width="100%" title="คลิ๊กเพื่อดูรายละเอียด"/>
        </div>

        <div class="swiper-slide">
            <asp:Image ID="Image3" runat="server" width="100%" title="คลิ๊กเพื่อดูรายละเอียด"/>
        </div>

        <div class="swiper-slide">
            <asp:Image ID="Image4" runat="server" width="100%" title="คลิ๊กเพื่อดูรายละเอียด"/>
        </div>


    </div>
    <!-- Add Pagination -->
	<div class="swiper-pagination"></div>
	<!-- Add Arrows -->
	<div class="swiper-button-next"></div>
	<div class="swiper-button-prev"></div>
</div>
<!-- end banner -->

<%--🎉(h)appy (b)irthday🎉

(a)(i)(r)(:)คุณวรางคณา วงศ์วารี
(b)(e)(e) (:) คุณนงนภา วงศ์วารี 
..................................................
🙏พวกเราชาวอโรม่า  ขออวยพรเนื่องในวันเกิด 
🙏ขอให้นายหญิงทั้ง 2 มีความสุข สุขภาพแข็งแรง และนำพาพวกเรา ไปสู่ความสำเร็จ ❤️🎁🎂

#HBD Social Distancing Style--%>
<%--
<section id="hr-policy">
	<div class="container">
		<div class="theme-title">

            <h1>🌹น้อมรำลึกผู้วางรากฐานแห่งธุรกิจกาแฟ อโรม่ากรุ๊ป🌹</h1></br>

            <h3>📢 บรรยากาศความประทับใจ Aroma Founder's Day 🌹 วันที่ 25-6-64</h3></br>
            <video width="1000" height="600" controls>
              <source src="video/Aroma_Founders_Day_2021xxx.mp4" type="video/mp4">
            </video>


        </div>
	</div>
</section>--%>

<%--<section id="hr-policy">
	<div class="container">
		<div class="theme-title">

            <h1>♥️Happiness Sharing Project #8♥️</h1></br>

            <h3>📢 บรรยากาศความประทับใจ วันที่ 📍25-6-64</h3></br>
            <video width="1000" height="600" controls>
              <source src="video/aromaFounday2.MP4xxx" type="video/mp4">
            </video>


        </div>
	</div>
</section>--%>

<section id="hr-policy6">
	<div class="container">
		<div class="theme-title">
            <h1>#CEOTalk EP.6 : ในไตรมาสหน้า..เราจะมีอะไรใหม่ๆน่าตื่นเต้นกันนะ ??</h1></br>
            <h3>สารดิจิตอลจาก CEO คุณกิจจา วงศ์วารี</h3></br>
            <video width="1000" height="600" controls>
              <source src="video/CEOTalkEP6_720p.mp4" type="video/mp4">
            </video>
        </div>
	</div>
</section>

<section id="hr-policy5">
	<div class="container">
		<div class="theme-title">
            <h1>#CEOTalk EP.5 : ข่าวดี เดือนแห่งความรัก</h1></br>
            <h3>ส่งความรัก แบบดิจิตอลจาก CEO คุณกิจจา วงศ์วารี</h3></br>
            <video width="1000" height="600" controls>
              <source src="video/CEOTalkEP5.MP4" type="video/mp4">
            </video>
        </div>
	</div>
</section>

<%--<section id="hr-policy4">
	<div class="container">
		<div class="theme-title">
            <h1>📢📢📢 #CEOTalk EP.4</h1></br>
            <h3>#เราจะแข็งแกร่งกว่าปีที่แล้ว เราจะแข็งแกร่งกว่าก่อนโควิท</h3></br>
             <h4>สารดิจิตอลจาก CEO คุณกิจจา วงศ์วารี</h4>
            <video width="1000" height="600" controls>
              <source src="video/video-C03004-F11.mp4" type="video/mp4">
            </video>
        </div>
	</div>
</section>--%>


<%--<section id="hr-policy2">
	<div class="container">
		<div class="theme-title">

            <h1>Merry Chrismas 2020</h1></br>

            <h3>#ของขวัญจากซานต้าครอส</h3></br>

            <video width="1000" height="600" controls>
              <source src="video/Merry Christmas 2020 - 631230.mp4" type="video/mp4">
            </video>


        </div>
	</div>
</section>--%>

<section id="hr-policy3">
	<div class="container">
		<div class="theme-title">

            <h1>Check In Online</h1></br>

            <h3>"เปลี่ยน" การใช้เครื่องบันทึกเวลา สู่ยุคดิจิทัล</h3></br>
            <video width="1000" height="600" controls>
              <source src="video/VDO-Checkin-Online.MP4" type="video/mp4">
            </video>


        </div>
	</div>
</section>




<section id="hr-policy">
	<div class="container">
		<div class="theme-title">


        </div>
	</div>
</section>


<section id="hr-policy">
	<div class="container">
		<div class="theme-title">
			<h2>WELCOME TO HR INFORMATION SYSTEM</h2>
			<p>"กว่า 60 ปีที่ อโรม่า กรุ๊ป ส่งต่อวัฒนธรรมกันจากรุ่นสู่รุ่นอย่างเข้มแข็งตลอดมา พนักงานทุกคนจึงล้วนมีบุคลิกที่รักความท้าทาย เต็มไปด้วยความสร้างสรรค์ มีความรักในอาชีพ รักการพัฒนาศักยภาพของตนเองอยู่เสมอ ทั้งยังมีความอดทน และไม่ย่อท้อต่ออุปสรรค ในขณะเดียวกันทุกคนต่างมีส่วนร่วมในการผสานความตั้งใจของทุกภาคส่วนเพื่อผลักดันองค์กรให้เดินไปข้างหน้าอย่างยั่งยืน"</p>
		</div>
	</div>
	<div class="container-fluid">
		<!-- menu img -->
		<div class="size-menu-img">
			<div class="menu-img-l">
				<div class="img">
					<img src="new/images/index/project1.jpg" alt="Image"  >
					<a href="HRPolicy_Announce.aspx" class="opacity tran4s"><i class="fa fa-link" aria-hidden="true"></i></a>
				</div>
			</div>
		   <div class="menu-img-l">
				<div class="img">
					<img src="new/images/index/project2.jpg" alt="Image">
					<a href="EmployeeRelation_Activities.aspx" class="opacity tran4s"><i class="fa fa-link" aria-hidden="true"></i></a>
				</div>
			</div>
			<div class="menu-img-l">
				<div class="img">
					<img src="new/images/index/project3.jpg" alt="Image">
					<a href="EmployeeRelation_Public.aspx" class="opacity tran4s"><i class="fa fa-link" aria-hidden="true"></i></a>
				</div>
			</div>
			<div class="menu-img-l">
				<div class="img">
					<img src="new/images/index/project4.jpg" alt="Image">
					<a href="#" class="opacity tran4s"><i class="fa fa-link" aria-hidden="true"></i></a>
				</div>
			</div>
			<div class="clear-fix"></div>
		</div>
		<!-- end menu img -->
	</div>
</section>






		
<!--
=====================================================
	Team Section
=====================================================
-->

<%--
<section id="team-section" class="bg-team-section">
	<div class="container">
	
	<div class="box-bd-ne">
		
	
			<div class="theme-title">
				<h2>BIRTHDAY THIS MONTH</h2>
				<p class="mh-bd-ne">have a Happy Birthday, request have strong health, there is the progress in the work and have one’s hopes fulfilled for what, wish every the points<br>
				สุขสันต์วันเกิด ขอให้สุขภาพแข็งแรง มีความเจริญก้าวหน้าในหน้าที่การงานและสมหวังในสิ่งที่ปรารถนาทุกประการ</p>
			</div> 


			<div class="clear-fix team-member-wrapper">
				
           

				<div class="theme-title">
     
					<iframe id="I1" runat="server" name="I1" src="index_Birthday.aspx" 
                        scrolling="no" frameborder="0" 
                        style="width: 350px; height:350px; border:0px; overflow:hidden;"></iframe>
      
				</div>	

			</div>
		<div class="clear-fix"></div>
		
	</div>
		
	</div>
</section> --%>

<div id="pricing-section">
	<div class="container">
		<div class="clear-fix">

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="single-price-table">
					<!--i class="fas fa-user"></i-->
					<img src="new/images/index/blog2.jpg" alt=""/>
					<h4>Career Opportunities</h4>
					<!--p>
						Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero sit amet.
					</p-->
					<a href="http://www.aromathailand.com/2015/career.php" target="_blank">Read more</a>
				</div>
			</div>

			<%--<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="single-price-table">
					<!--i class="fas fa-external-link-alt"></i-->
					<img src="new/images/index/blog1.jpg" alt=""/>
					<h4>Application For Aroma Group <br /> ระบบภายใน อโรม่า กรุ๊ป</h4>
					<!--p>
						Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero sit amet.
					</p-->
					<a href="http://101.109.248.9:81/Aroma" target="_blank">Read more</a>
				</div>
			</div>--%>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="single-price-table">
					<!--i class="fas fa-external-link-alt"></i-->
					<img src="new/images/index/blog1.jpg" alt=""/>
					<h4>Application For Aroma Group</h4>
					<p>
						ระบบภายใน อโรม่า กรุ๊ป
					</p>
					<%--<a href="http://leaveonline.aromathailandapp.com/Leave_Login.aspx" target="_blank">Read more</a>--%>
                    <a href="https://armapplication.com/Aroma/" target="_blank">Read more</a>
				</div>
			</div>
			
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="single-price-table">
					<!--i class="fas fa-book"></i-->
					<img src="new/images/index/blog3.jpg" alt=""/>
					<h4>Instruction Manual </h4>
					<p>
						คู่มือการใช้งานระบบต่างๆ
					</p>
					<a href="Manual.aspx">Read more</a>
                   <%-- <a href="Document/Manual/คู่มือการใช้งาน HR Portal.pdf" target="_blank" >Read more</a>--%>
				</div>
			</div>

		</div>
	</div>
</div>
        
<center>
    <asp:Label ID="lblCount" runat="server" Text="Label"></asp:Label>
</center>

<script type='text/javascript' src='https://www.siamecohost.com/member/gcounter/graphcount.php?page=http://10.0.4.20/hr_portal/&style=30&maxdigits=10'></script>

<!-- Swiper JS -->
<script src="new/js/swiper.min.js"></script>

<!-- Initialize Swiper -->
<script>
    var swiper = new Swiper('.swiper-container', {
		slidesPerView: 1,
		loop: true,
		autoplay: {
        	delay: 3000,
        	disableOnInteraction: false,
      	},
		pagination: {
			el: '.swiper-pagination',
		},
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
    });
</script>

</asp:Content>
