﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="Benefits.aspx.vb" Inherits="Aroma_HRPortal.Benefits1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<style>
	.swiper-container {
		width: 100%;
		height: 100%;
	}
	.swiper-slide {
		text-align: center;
		background: #fff;
	}
	.swiper-slide img 
	{
		width: 100%;
	}
	.swiper-pagination-bullet-active {
		opacity: 1;
		background: #af2f2f;
	}
</style>

<!-- banner -->
<!-- Swiper -->
<div class="swiper-container swiper1">
    <div class="swiper-wrapper">
        <div class="swiper-slide" data-hash="slide1"><img src="new/images/banner/benefit/banner1.jpg" /></div>
        <div class="swiper-slide" data-hash="slide2">Slide 2</div>
        <div class="swiper-slide" data-hash="slide3">Slide 3</div>
    </div>
    <!-- Add Pagination -->
   	<div class="swiper-pagination swiper-pagination1"></div>
    <!-- Add Arrows -->
    <!--div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div-->
</div>
<!-- end banner -->

<!-- content -->
<div class="container">
<div class="row">
    <div class="base-content">
    	
    	<div class="base-content-head">สวัสดิการพนักงาน</div>
		<div class="base-head-bot-line"></div>
   		<div class="box-benefits">
   			<div class="box-circle">
            <a href="">
   				<div class="circle">
   					<div class="circle-icon"><i class="fa fa-usd" aria-hidden="true"></i></div>
   					<div class="circle-txt">
   						กองทุนสำรองเลี้ยงชีพ<br>
   						มอบเกียรติบัตร<br>
   						เงินรางวัล
   					</div>
   				</div>
            </a>
   			</div>
   			<div class="box-circle">
             <a href="">
   				<div class="circle">
   					<div class="circle-icon"><i class="fa fa-usd" aria-hidden="true"></i></div>
   					<div class="circle-txt">
						เงินช่วยเหลืองาน<br>
						และโอกาสพิเศษ<br>
						ต่างๆ
   					</div>
   				</div>
            </a>
   			</div>
   			<div class="box-circle">
             <a href="">
   				<div class="circle">
   					<div class="circle-icon"><i class="fa fa-usd" aria-hidden="true"></i></div>
   					<div class="circle-txt">
   						เจ็บป่วย ประกันชีวิต<br>
   						สุขภาพ อุบัติเหตุ
   					</div>
   				</div>
            </a>
   			</div>
   			<div class="box-circle">
             <a href="">
   				<div class="circle">
   					<div class="circle-icon"><i class="fa fa-usd" aria-hidden="true"></i></div>
   					<div class="circle-txt">
   						ปฏิบัติงานนอกสถานที่<br>
   						(ในประเทศ)
   					</div>
   				</div>
            </a>
   			</div>
   			<div class="box-circle">
             <a href="">
   				<div class="circle">
   					<div class="circle-icon"><i class="fa fa-usd" aria-hidden="true"></i></div>
   					<div class="circle-txt">
   						ปฏิบัติงานนอกสถานที่<br>
   						(ต่างประเทศ)
   					</div>
   				</div>
            </a>
   			</div>
   			<div class="box-circle">
             <a href="">
   				<div class="circle">
   					<div class="circle-icon"><i class="fa fa-usd" aria-hidden="true"></i></div>
   					<div class="circle-txt">
   						ปฏิบัติงาน<br>
   						EXHIBITION - EVENT<br>
   						(ในประเทศ)
   					</div>
   				</div>
            </a>
   			</div>
   			<div class="box-circle">
             <a href="">
   				<div class="circle">
   					<div class="circle-icon"><i class="fa fa-usd" aria-hidden="true"></i></div>
   					<div class="circle-txt">
						ค่าเลี้ยงรับรอง
   					</div>
   				</div>
            </a>
   			</div>
   			<div class="box-circle">
             <a href="">
   				<div class="circle">
   					<div class="circle-icon"><i class="fa fa-usd" aria-hidden="true"></i></div>
   					<div class="circle-txt">
   						เงินกู้
   					</div>
   				</div>
            </a>
   			</div>
   			<div class="box-circle">
            <a href="">
   				<div class="circle">
   					<div class="circle-icon"><i class="fa fa-usd" aria-hidden="true"></i></div>
   					<div class="circle-txt">
   						ส่วนลดสินค้า<br>
   						สำหรับพนักงาน
   					</div>
   				</div>
            </a>
   			</div>
   			<div class="box-circle">
             <a href="">
   				<div class="circle">
   					<div class="circle-icon"><i class="fa fa-usd" aria-hidden="true"></i></div>
   					<div class="circle-txt">
   						เบี้ยขยัน
   					</div>
   				</div>
            </a>
   			</div>
   			<div class="box-circle">
             <a href="">
   				<div class="circle">
   					<div class="circle-icon"><i class="fa fa-usd" aria-hidden="true"></i></div>
   					<div class="circle-txt">
   						ตรวจสุขภาพ
   					</div>
   				</div>
            </a>
   			</div>
   			<div class="box-circle">
             <a href="">
   				<div class="circle">
   					<div class="circle-icon"><i class="fa fa-usd" aria-hidden="true"></i></div>
   					<div class="circle-txt">
   						สวัสดิการอื่นๆ
   					</div>
   				</div>
            </a>
   			</div>
   			<div class="clear"></div>
   		</div>
    	
	</div>
</div>
</div>

<!-- Swiper JS -->
<script src="new/js/swiper.min.js"></script>

<!-- Initialize Swiper -->
<script>
	var swiper1 = new Swiper('.swiper1', {
		spaceBetween: 30,
		pagination: {
        el: '.swiper-pagination1',
        clickable: true,
      },
    });
</script> 

</asp:Content>
