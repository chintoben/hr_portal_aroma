﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="Calendar_Link.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_Contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner">
<%--<img src="new/images/banner/benefit/banner1.jpg" />--%>
</div>
<!-- end banner -->



<section id="about-us" class="blog-category">
<div class="container">
	<div class="theme-title tt-edit category-title">
        <ul>
            <li><a href="index_hr.aspx">HOME</a></li>
            <li>/</li>
            <li><a href="Calendar_link.aspx">Training Calendar</a></li>
        </ul>
		<h2>กำหนดหมวดหลักสูตรฝึกอบรม</h2>
	</div> <!-- /.theme-title -->

	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-6 div-empty"></div>
		
<%--        <div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="far icon-045-skills"></i>
				</div>
				<p><a href="Document/DevelopmentAndTraining/Training_Competency/1.การพัฒนาเน้นสมรรถนะ (Competency-Based Development).pdf" target="_blank" class="tran3s">การพัฒนาเน้นสมรรถนะ<br>(Competency-Based<br>Development)</a></p>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="far icon-119-businessman-discussing-a-progress-report"></i>
				</div>
				<p><a href="Document/DevelopmentAndTraining/Training_Competency/2.แนวทางการพัฒนาสมรรถนะ (Our Solutions Competency Development).pdf" target="_blank" class="tran3s">แนวทางการพัฒนาสมรรถนะ<br>(Our Solutions<br>Competency Development)</a></p>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6 div-empty"></div>--%>

        <table>
        <tr>
            <td><h5>1.</h5></td>
            <td>&nbsp;</td>
            <td><h5>ทรัพยากรมนุษย์ , อาคาร / สถานที่ / ต้อนรับส่วนหน้า</h5></td>
            <td>
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Calendar/1.jpg" 
                    Width="80px" />
            </td>
        </tr>

          <tr>
            <td><h5>2.</h5></td>
            <td>&nbsp;</td>
            <td><h5>กฎหมาย</h5></td>
            <td>
                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/Calendar/2.jpg" 
                    Width="80px" />
            </td>
        </tr>

          <tr>
            <td><h5>3.</h5></td>
            <td>&nbsp;</td>
            <td><h5>ออกแบบ</h5></td>
            <td>
                <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/Calendar/3.jpg" 
                    Width="80px" />
            </td>
        </tr>

          <tr>
            <td><h5>4.</h5></td>
            <td>&nbsp;</td>
            <td><h5>งบประมาณ , ตรวจสอบ , Audit</h5></td>
            <td>
                <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/Calendar/4.jpg" 
                    Width="80px" />
            </td>
        </tr>

          <tr>
            <td><h5>5.</h5></td>
            <td>&nbsp;</td>
            <td><h5>บัญชี - การเงิน</h5></td>
            <td>
                <asp:Image ID="Image6" runat="server" ImageUrl="~/Images/Calendar/5.jpg" 
                    Width="80px" />
            </td>
        </tr>

          <tr>
            <td><h5>6.</h5></td>
            <td>&nbsp;</td>
            <td><h5>จัดซื้อ - จัดจ้าง</h5></td>
            <td>
                <asp:Image ID="Image7" runat="server" ImageUrl="~/Images/Calendar/6.jpg" 
                    Width="80px" />
            </td>
        </tr>

          <tr>
            <td><h5>7.</h5></td>
            <td>&nbsp;</td>
            <td><h5>ขาย</h5></td>
            <td>
                <asp:Image ID="Image8" runat="server" ImageUrl="~/Images/Calendar/7.jpg" 
                    Width="80px" />
            </td>
        </tr>

          <tr>
            <td><h5>8.</h5></td>
            <td>&nbsp;</td>
            <td><h5>การตลาด , ผลิตภัณฑ์ , กราฟฟิค</h5></td>
            <td>
                <asp:Image ID="Image9" runat="server" ImageUrl="~/Images/Calendar/8.jpg" 
                    Width="80px" />
            </td>
        </tr>

          <tr>
            <td><h5>9.</h5></td>
            <td>&nbsp;</td>
            <td><h5>ซัพพลายเชน , คลังสินค้า</h5></td>
            <td>
                <asp:Image ID="Image10" runat="server" ImageUrl="~/Images/Calendar/9.jpg" 
                    Width="80px" />
            </td>
        </tr>

        <tr>
            <td><h5>10.</h5></td>
            <td>&nbsp;</td>
            <td><h5>เทคโนโลยีสารสนเทศ</h5></td>
            <td>
                <asp:Image ID="Image5" runat="server" ImageUrl="~/Images/Calendar/10.jpg" 
                    Width="80px" />
            </td>
        </tr>

        <tr>
            <td><h5>11.</h5></td>
            <td>&nbsp;</td>
            <td><h5>ทั่วไป</h5></td>
            <td>
                <asp:Image ID="Image11" runat="server" ImageUrl="~/Images/Calendar/11.jpg" 
                    Width="80px" />
            </td>
        </tr>

      
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>

      
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><h2>
                <asp:Image ID="Image12" runat="server" ImageUrl="~/Images/Calendar/Calendar-icon.png" 
                    Width="80px" />
                <a href="https://calendar.google.com/calendar/r?tab=cc" target="_blank"><span>Training Calendar </span></a></h2></td>
            <td>
                &nbsp;</td>
        </tr>

      
        </table>


	</div> <!-- /.row -->
	<div class="blog-category-bt">
		<div class="btn-group">
			<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="https://calendar.google.com/calendar/r?tab=cc"></asp:button>
		</div>
	</div>
</div> <!-- /.container -->
</section>



</asp:Content>