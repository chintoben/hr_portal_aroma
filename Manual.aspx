﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="Manual.aspx.vb" Inherits="Aroma_HRPortal.Manual" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<!-- banner -->
<div class="base-banner">
<img src="new/images/banner/banner manual.jpg" 
        alt="" width="100%" />
</div>
<!-- end banner -->

<section id="about-us" class="blog-category">
<div class="container">
	<div class="theme-title tt-edit category-title">
        <ul>
            <li><a href="index_hr.aspx">HOME</a></li>
        </ul>
		<h2>คู่มือการใช้งานระบบต่างๆ</h2>
		<!--p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p-->
	</div> <!-- /.theme-title -->

	

       <div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix" style="float: none; margin: 0 auto;">
				
                
                <div class="blog-details-post-wrapper">
				
					<h4>เอกสาร นโยบายการใช้งานระบบสารสนเทศ</h4>

					<p style="padding: 5px 0 0;">
				  		<ul class="p">
							<li><a href="Document/Manual/MIS/ข้อปฏิบัติสำหรับผู้ใช้งานเครือข่ายคอมพิวเตอร์  Rev.1.pdf" target="_blank">ข้อปฏิบัติสำหรับผู้ใช้งานเครือข่ายคอมพิวเตอร์  Rev.1</a></li>
		                    <li><a href="Document/Manual/MIS/นโยบายการใช้ระบบสารสนเทศของ Aroma group  Rev.1.pdf" target="_blank">นโยบายการใช้ระบบสารสนเทศของ Aroma group  Rev.1</a></li>
                            <li><a href="Document/Manual/MIS/มาตรฐานของซอฟแวร์ที่ใช้ในองค์กร Rev.1.pdf" target="_blank">มาตรฐานของซอฟแวร์ที่ใช้ในองค์กร Rev.1</a></li>
				  		</ul>
					</p>
				
				</div>

                <div class="blog-details-post-wrapper">
				
					<h4>คู่มือการใช้งานระบบสารสนเทศ Aromagroup</h4>

					<p style="padding: 5px 0 0;">
				  		<ul class="p">
							<li><a href="Document/Manual/MIS/ขั้นตอนการ Set lock print ปริ้นเตอร์ RICOH.pdf" target="_blank">ขั้นตอนการ Set lock print ปริ้นเตอร์ RICOH</a></li>
		                    <li><a href="Document/Manual/MIS/ขั้นตอนการ Set Scan เครื่องปริ้นเตอร์ Samsung.pdf" target="_blank">ขั้นตอนการ Set Scan เครื่องปริ้นเตอร์ Samsung</a></li>
                            <li><a href="Document/Manual/MIS/ขั้นตอนการ เปลี่ยน Password เข้าใช้งานระบบ Computer ของ Aromagroup.pdf" target="_blank">ขั้นตอนการ เปลี่ยน Password เข้าใช้งานระบบ Computer ของ Aromagroup</a></li>
				  		    <li><a href="Document/Manual/MIS/ขั้นตอนการใช้งาน Email ของบริษัท.pdf" target="_blank">ขั้นตอนการใช้งาน Email ของบริษัท</a></li>
                        
                        </ul>
					</p>
				
				</div>

          <%--      <br />--%>

                 <div class="blog-details-post-wrapper">
				
					<h4>คู่มือการใช้งานระบบของ  HR</h4>

					<p style="padding: 5px 0 0;">
				  		<ul class="p">
							<li><a href="Document/Manual/คู่มือการใช้งาน HR Portal.pdf" target="_blank">คู่มือการใช้งาน HR Portal</a></li>

                            <li><a href="Document/Manual/PMS.pdf" target="_blank">คู่มือการใช้งานระบบประเมินผลออนไลน์ (PMS)</a></li>

                           <%-- <li><a href="Document/Manual/คู๋มือ PMS.pdf" target="_blank">วิดิโอ แนะนำการใช้งานระบบประเมินผลออนไลน์ (PMS)</a></li>--%>
                            <a href="Manual2.aspx" target="_blank">-  วิดิโอ แนะนำการใช้งานระบบประเมินผลออนไลน์ (PMS)</a>
                        </ul>
					</p>
				
				</div>

              

			</div>
		</div>


</div> <!-- /.container -->
</section>
 
</asp:Content>
