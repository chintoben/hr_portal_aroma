﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="Benefits8_1.aspx.vb" Inherits="Aroma_HRPortal.Benefits8_1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/benefit/banner1.jpg" /></div>
<!-- end banner -->

<!--
=====================================================
	Blog Page Details
=====================================================
-->
<article class="blog-details-page">
	<div class="container">
		
		<div class="post-heading">
            <ul>
			    <li><a href="index_hr.aspx">HOME</a></li>
			    <li>/</li>
			    <li><a href="Benefits_Employee.aspx">ระเบียบสวัสดิการ</a></li>
                <li>/</li>
                <li><a href="Benefits8.aspx">เงินกู้</a></li>
			</ul>
			<h2>สินเชื่อสวัสดิการพนักงาน (MOU ธนาคารธนชาต)</h2>
		</div> <!-- /.post-heading -->
		
		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix">
				<div class="blog-details-post-wrapper">
				
					<img src="new/images/banner/benefit/img9.jpg" alt="Image">
					
					<h4>เกณฑ์พิจารณา</h4>
					<p>
						<ol>
							<li>วงเงินกู้
								<ul class="a">
									<li>อนุมัติวงเงินกู้ตั้งแต่ 20,000 – 1,000,000 บาท ทั้งนี้ขึ้นอยู่กับคุณสมบัติของผู้กู้</li>
									<li>ธนาคารขอสงวนสิทธิ์ในการอนุมัติให้เงินกู้ตามความเหมาะสม</li>
									<li>ระยะเวลาการกู้ 12 – 60 เดือน</li>
								</ul>
							</li>
							<li>หลักประกัน ไม่ต้องมีหลักทรัพย์ และไม่ต้องมีบุคคลค้ำประกัน</li>
							<li>อัตราดอกเบี้ยเงินกู้ (ลดต้นลดดอก)	MRR + 4.625% ต่อปี</li>
							<li>อัตราค่าธรรมเนียมจัดการสินเชื่อ 0.5% ของวงเงินที่ได้รับอนุมัติ ขั้นต่ำ 100 บาท สูงสุดไม่เกิน 500 บาท</li>
							<li>อัตราดอกเบี้ยสินเชื่อกรณีผิดนัด คิดอัตราดอกเบี้ยสูงสุดกรณีผิดนัดชำระหนี้ (เป็นไปตามประกาศธนาคาร)</li>
							<li>อัตราดอกเบี้ยกรณีผู้กู้ลาออกหรือ พ้นสภาพจากการเป็นพนักงานหรือลูกจ้างประจำของบริษัท คิดอัตราดอกเบี้ยสูงสุดกรณีปกติตามประกาศธนาคาร (เป็นไปตามประกาศธนาคาร)</li>
							<li>ค่าใช้จ่ายในการติดตามทวงถามหนี้	200 บาท / รอบบัญชี</li>
							<li>คุณสมบัติผู้กู้
								<ul class="a">
									<li>ต้องเป็นพนักงานหรือลูกจ้างประจำของบริษัท ที่ลงนามใน MOU เข้าร่วมโครงการกับทางธนาคาร</li>
									<li>ปัจจุบันต้องไม่ถือบัญชีสินเชื่อเงินสด T-Loan /บัตรสินเชื่อหมุนเวียน Flash Card /SCIB Credit / SCIB Salary Plus ของธนาคาร</li>
									<li>อายุของผู้กู้ 20-55 ปีบริบูรณ์ หรือ ตาม MOU</li>
									<li>รายได้ประจำไม่ต่ำกว่า 7,000 บาท (รายได้ประจำ หมายถึง เงินเดือนประจำรวมค่าครองชีพและรายได้อื่นๆ ทั้งนี้ต้องเป็นรายได้ประจำที่จ่ายเข้าบัญชีเงินฝากธนาคารใดธนาคารหนึ่งหรือตาม MOU)</li>
									<li>อายุการทำงานไม่น้อยกว่า 1 ปี</li>
								</ul>
							</li>
							<li>เอกสารประกอบการสมัคร
								<ul class="a">
									<li>สำเนาบัตรประจำตัวประชาชน</li>
									<li>สำเนาทะเบียนบ้าน</li>
									<li>สลิปเงินเดือน เดือนล่าสุด (ต้นฉบับ) หรือ ต้นฉบับหนังสือรับรองเงินเดือน และ Statement แสดงรายการไม่น้อยกว่า 3 เดือน</li>
									<li>สำเนาสมุดบัญชีหน้าแรก (หน้าที่แสดงเลขที่บัญชี และชื่อบัญชี)</li>
								</ul>
							</li>
							<li>การโอนเงินกู้ ธนาคารจะโอนเงินกู้ที่ได้รับอนุมัติเข้าบัญชีเงินฝากของ ผู้กู้หรือบริษัทสินค้าที่เกี่ยวข้องตามสัญญาข้อตกลง</li>
							<li>การชำระคืนเงินกู้ ผู้กู้จะต้องทำหนังสือยินยอมให้หักเงินเดือนเพื่อชำระหนี้เงินกู้ตามโครงการให้ไว้แก่บริษัทต้นสังกัดของผู้กู้และบริษัทจะเป็นผู้ดำเนินการหักเงินผ่านบัญชีเงินเดือนของผู้กู้ รวบรวมส่งให้กับธนาคาร
	 ทุกเดือนเท่ากับจำนวนเงินที่ผู้กู้ต้องผ่อนชำระกับธนาคาร หรือในกรณีที่ผู้กู้มีบัญชีเงินเดือน ( Payroll) เปิดไว้กับธนาคาร ผู้กู้ต้องดำเนินการทำหนังสือยินยอมให้หักเงินเดือนจากบัญชีเงินเดือน
	 ดังกล่าว เท่ากับจำนวนเงินที่ผู้กู้ต้องผ่อนชำระกับธนาคาร เพื่อชำระหนี้เงินกู้ตามโครงการให้ไว้แก่ธนาคารและในกรณีที่ผู้กู้ต้องการชำระเพิ่มเติม สามารถชำระได้ที่สาขาของธนาคารธนชาตทุกสาขา</li>
						</ol>
					</p>
					
					<h4>ขั้นตอนการดำเนินการ</h4>
					<p>
						<ol>
							<li>พนักงานที่สนใจ ติดต่อขอใบสมัครสินเชื่อได้ี่ที่ฝ่ายทรัพยากรมนุษย์้ทุกวันที่ทำการ</li>
							<li>ส่งใบสมัคร(เขียนรายละเอียดให้ครบถ้วน) พร้อมแนบเอกสารการสมัครที่ฝ่ายทรัพยากรมนุษย์ (โปรดเตรียมเอกสารให้พร้อมด้วยตนเอง)</li>
							<li>ฝ่ายบทรัพยากรมนุษย์รับรองสถานะการเป็นพนักงานในใบสมัคร และส่งธนาคารทุกๆ 2 อาทิตย์</li>
							<li>ผู้ขอสินเชื่อทราบผลการพิจารณาภายใน 7 วันทำการ นับจากวันที่ธนาคารได้รับเอกสาร</li>
							<li>ธนาคารขอสงวนสิทธิ์ในการอนุมัติให้เงินกู้ตามความเหมาะสม</li>
						</ol>
					</p>
					
					<div class="blog-details-bt">
						<div class="btn-group">
							<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
						</div>
					</div>
				
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-8 col-xs-12 p-fix">
				<aside>
					<div class="sidebar-news-list">
						<h4>CATEGORY</h4>
						<ul>
							<li>
								<a href="Benefits1.aspx" class="tran3s">กองทุนสำรองเลี้ยงชีพ มอบเกียรติบัตรเงินรางวัล</a>
							</li>
							<li>
								<a href="Benefits2_1.aspx" class="tran3s">เงินช่วยเหลืองาน และโอกาสพิเศษต่างๆ</a>
							</li>
							<li>
								<a href="Benefits15.aspx" class="tran3s">เจ็บป่วย ประกันชีวิต สุขภาพ อุบัติเหตุ</a>
							</li>
							<li>
								<a href="Benefits5_1.aspx" class="tran3s">ปฏิบัติงานนอกสถานที่ (ในประเทศ)</a>
							</li>
							<li>
								<a href="Benefits3_1.aspx" class="tran3s">ปฏิบัติงานนอกสถานที่ (ต่างประเทศ)</a>
							</li>
							<li>
								<a href="Benefits4_1.aspx" class="tran3s">ปฏิบัติงาน EXHIBITION - EVENT (ในประเทศ)</a>
							</li>
							<li>
								<a href="Benefits7_1.aspx" class="tran3s">ค่าเลี้ยงรับรอง</a>
							</li>
							<li>
								<a href="Benefits8.aspx" class="tran3s">เงินกู้</a>
							</li>
							<li>
								<a href="Benefits9_1.aspx" class="tran3s">ส่วนลดสินค้า สำหรับพนักงาน</a>
							</li>
							<li>
								<a href="Benefits10_1.aspx" class="tran3s">เบี้ยขยัน</a>
							</li>
							<li>
								<a href="Benefits11_1.aspx" class="tran3s">ตรวจสุขภาพ</a>
							</li>
							<li>
								<a href="#" class="tran3s">สวัสดิการอื่นๆ</a>
							</li>
						</ul>
					</div>
				</aside>
				<aside>
					<div class="sidebar-news-list">
						<h4>Related Links</h4>
						<ul>
							<li>
								<a href="Benefits8_1.aspx" class="tran3s">
									สินเชื่อสวัสดิการพนักงาน (MOU ธนาคารธนชาต)
								</a>
							</li>
							<li>
								<a href="Benefits8_2.aspx" class="tran3s">
									สินเชื่อสวัสดิการพนักงาน (MOU ธนาคารกรุงศรีอยุธยา) (1)
								</a>
							</li>
							<li>
								<a href="Benefits8_3.aspx" class="tran3s">
									สินเชื่อสวัสดิการพนักงาน (MOU ธนาคารออมสิน)
								</a>
							</li>
							<li>
								<a href="Benefits8_4.aspx" class="tran3s">
									สวัสดิการเงินกู้บริษัท
								</a>
							</li>
						</ul>
					</div>
				</aside>
				<aside>
					<div class="sidebar-news-list">
						<h4>PDF Links</h4>
						<ul class="p">
							<li>
								<a href="Document/4Benefits/ขั้นตอนการดำเนินการกู้สินเชื่อ ธ.ออมสิน.pdf" class="tran3s">
								ขั้นตอนการดำเนินการกู้สินเชื่อ ธ.ออมสิน</a>
							</li>
							<li>
								<a href="Document/4Benefits/สินเชื่อสวัสดิการพนักงานร่วมกับสถาบันการเงิน.pdf" class="tran3s">
								สินเชื่อสวัสดิการพนักงานร่วมกับสถาบันการเงิน</a>
							</li>
						</ul>
					</div>
				</aside>
			</div>
		</div>
		
	</div>
</article>

</asp:Content>
