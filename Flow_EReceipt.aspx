﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="Flow_EReceipt.aspx.vb" Inherits="Aroma_HRPortal.Flow_EReceipt" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/flow/banner.jpg" alt=""/></div>
<!-- end banner -->

<article class="blog-details-page">
	<div class="container">
		
		<div class="post-heading">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
            </ul>
			<h2>Flow การทำงานในส่วนของการออก e-receipt </h2>
		</div> <!-- /.post-heading -->
		
		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix" style="float: none; margin: 0 auto;">
				<div class="blog-details-post-wrapper">
				    
					<%--<img src="new/images/banner/benefit/hr_portal2Flow_IT.jpg" alt="Image">--%>

					<p style="padding: 30px 0 0;">
				        <ul class="p">
							<li><a href="Document/6Flow/E-Receipt/Flowลูกค้า8OL.pdf" target="_blank">Flow ลูกค้า 8OL</a></li>
                            <li><a href="Document/6Flow/E-Receipt/Flowลูกค้า8LZ8SH (ต้องการใบกำกับภาษี).pdf" target="_blank">Flow ลูกค้า 8LZ,8SH (ต้องการใบกำกับภาษี)</a></li>
                            <li><a href="Document/6Flow/E-Receipt/Flowลูกค้า8LZ8SH (ไม่ต้องการใบกำกับภาษี).pdf" target="_blank">Flow ลูกค้า 8LZ,8SH (ไม่ต้องการใบกำกับภาษี)</a></li>
                            <li><a href="Document/6Flow/E-Receipt/Flowลูกค้า8LZ8SH (แจ้งต้องการใบกำกับภาษีภายหลังได้รับสินค้า).pdf" target="_blank">Flow ลูกค้า 8LZ,8SH (แจ้งต้องการใบกำกับภาษีภายหลังได้รับสินค้า)</a></li>
                            <li><a href="Document/6Flow/E-Receipt/Flowลูกค้าต้องการแก้ไขเอกสาร.pdf" target="_blank">Flow ลูกค้าต้องการแก้ไขเอกสาร</a></li>
                            <li><a href="Document/6Flow/E-Receipt/Flowส่งสินค้าไม่ครบ ทำใบลดหนี้.pdf" target="_blank">Flow ส่งสินค้าไม่ครบ ทำใบลดหนี้</a></li>
                            <li><a href="Document/6Flow/E-Receipt/Flowการคืนสินค้า.pdf" target="_blank">Flow การคืนสินค้า</a></li>

				        </ul>
					</p>
				
				</div>
			</div>
		</div>
		
	</div>
</article>

</asp:Content>
