﻿Public Class A_IndexNewsMain
    Inherits System.Web.UI.Page
    Dim Role As String



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Role = Request.QueryString("Role")
    End Sub

    Protected Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click
        Server.Transfer("A_IndexNews.aspx?Role=" & Role)
    End Sub
End Class