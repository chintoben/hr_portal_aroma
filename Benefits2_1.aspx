﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="Benefits2_1.aspx.vb" Inherits="Aroma_HRPortal.Benefits2_1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/benefit/banner1.jpg" /></div>
<!-- end banner -->

<!--
=====================================================
	Blog Page Details
=====================================================
-->
<article class="blog-details-page">
	<div class="container">
		
		<div class="post-heading">
            <ul>
			    <li><a href="index_hr.aspx">HOME</a></li>
			    <li>/</li>
			    <li><a href="Benefits_Employee.aspx">ระเบียบสวัสดิการ</a></li>
			</ul>
			<h2>เงินช่วยเหลืองาน และโอกาสพิเศษต่างๆ</h2>
		</div> <!-- /.post-heading -->
		
		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix">
				<div class="blog-details-post-wrapper">
				
					<img src="new/images/banner/benefit/img3.jpg" alt="Image">

					<h4>
						บริษัท เค.วี.เอ็น. อิมปอร์ต เอกซ์ปอร์ต (1991) จำกัด/บริษัท ไลอ้อน ทรี-สตาร์ จำกัด
					</h4>
					<h4>เงินช่วยเหลือการสมรส (กรณีจดทะเบียนถูกต้องตามกฏหมาย หรืออยู่กินฉันสามีภรรยา โดยจัดพิธีตามประเพณีอย่างเป็นทางการ)</h4>
					<p>
						<table class="table">
						  <tbody>
							<tr>
							  <td>พนักงานระดับ A จำนวนเงิน</td>
							  <td>5,000 บาท</td>
							</tr>
							<tr>
							  <td>พนักงานระดับ B จำนวนเงิน</td>
							  <td>4,000 บาท</td>
							</tr>
							<tr>
							  <td>พนักงานระดับ C จำนวนเงิน</td>
							  <td>3,000 บาท</td>
							</tr>
							<tr>
							  <td>พนักงานระดับ D จำนวนเงิน</td>
							  <td>2,000 บาท</td>
							</tr>
						  </tbody>
						</table>
					</p>
					
					<h4>เกณฑ์พิจารณา</h4>
					<p>
						<ol>
							<li>เบิกได้ไม่เกิน 1 ครั้งตลอดอายุงาน</li>
							<li>กรณีเป็นพนักงานในกลุ่มบริษัท ทั้งพนักงานชายกับหญิงของบริษัท จะได้รับช่วยเหลือเพียงหนึ่งสิทธิ</li>
							<li>พนักงานต่างระดับกัน ให้เบิกได้ตามอัตราระดับพนักงานที่สูงกว่า</li>
						</ol>
					</p>
					
					<h4>เอกสารประกอบการเบิกจ่าย</h4>
					<p>
						<ol>
							<li>เขียนใบเบิกสวัสดิการ</li>
							<li>การ์ดเชิญ และรูปถ่ายตอนทำพิธีแต่งงาน</li>
							<li>ทำเรื่องเบิกจ่ายภายใน 30 วัน นับจากวันสมรส</li>
						</ol>
					</p>
					
					<h4>เงินช่วยเหลือค่าคลอดบุตร (กรณีจดทะเบียนถูกต้องตามกฏหมาย หรืออยู่กินฉันสามีภรรยา)</h4>
					<p>
						<table class="table">
						  <tbody>
							<tr>
							  <td>พนักงานระดับ A จำนวนเงิน</td>
							  <td>5,000 บาท</td>
							</tr>
							<tr>
							  <td>พนักงานระดับ B จำนวนเงิน</td>
							  <td>4,000 บาท</td>
							</tr>
							<tr>
							  <td>พนักงานระดับ C จำนวนเงิน</td>
							  <td>3,000 บาท</td>
							</tr>
							<tr>
							  <td>พนักงานระดับ D จำนวนเงิน</td>
							  <td>2,000 บาท</td>
							</tr>
						  </tbody>
						</table>
					</p>
					
					<h4>เกณฑ์พิจารณา</h4>
					<p>
						<ol>
							<li>เบิกได้ไม่เกิน 2 ครั้งตลอดอายุงาน</li>
							<li>กรณีเป็นพนักงานในกลุ่มบริษัท ทั้งพนักงานชายกับหญิงของบริษัท จะได้รับช่วยเหลือเพียงหนึ่งสิทธิ</li>
							<li>พนักงานต่างระดับกัน ให้เบิกได้ตามอัตราระดับพนักงานที่สูงกว่า</li>
						</ol>
					</p>
					
					<h4>เอกสารประกอบการเบิกจ่าย</h4>
					<p>
						<ol>
							<li>เขียนใบเบิกสวัสดิการ</li>
							<li>สำเนาใบสูติบัตรบุตร (ชายต้องมีการรับรองเป็นบิดาบุตรในใบสูติบัตร)</li>
							<li>ทำเรื่องเบิกจ่ายภายใน 120 วัน นับจากวันที่คลอด</li>
						</ol>
					</p>
					
					<h4>เงินช่วยเหลือการอุปสมบท/บวชชี (สำหรับพนักงานอายุการทำงาน 1 ปี ขึ้นไป)</h4>
					<p>
						<table class="table">
						  <tbody>
							<tr>
							  <td>พนักงานระดับ A จำนวนเงิน</td>
							  <td>5,000 บาท</td>
							</tr>
							<tr>
							  <td>พนักงานระดับ B จำนวนเงิน</td>
							  <td>4,000 บาท</td>
							</tr>
							<tr>
							  <td>พนักงานระดับ C จำนวนเงิน</td>
							  <td>3,000 บาท</td>
							</tr>
							<tr>
							  <td>พนักงานระดับ D จำนวนเงิน</td>
							  <td>2,000 บาท</td>
							</tr>
						  </tbody>
						</table>
					</p>
					
					<h4>เกณฑ์พิจารณา</h4>
					<p>
						<ol>
							<li>เบิกได้ไม่เกิน 1 ครั้งตลอดอายุงาน</li>
							<li>กำหนดวันลาเป็นไปตามระเบียบข้อบังคับบริษัทกำหนด</li>
							<li>พนักงานชายอุปสมบทเป็นพระ (บรรพชาเป็นเณร ไม่มีสิทธิเบิก)</li>
							<li>พนักงานหญิงบวชชี (กรณีชีพราหมณ์ วิปัสสนา ไม่มีสิทธิเบิก)</li>
						</ol>
					</p>
					
					<h4>เอกสารประกอบการเบิกจ่าย</h4>
					<p>
						<ol>
							<li>เขียนใบเบิกสวัสดิการ</li>
							<li>การ์ดเชิญ และรูปถ่ายตอนทำพิธีอุปสมบท/บวชชี</li>
							<li>ทำเรื่องเบิกจ่ายภายใน 30 วัน นับจากวันลาสิกขา</li>
						</ol>
					</p>
					
					<h4>เงินช่วยเหลืองานฌาปนกิจศพ</h4>
					<ol>
						<li>กรณีพนักงาน เสียชีวิต</li>
						<p>
							<table class="table">
							  <tbody>
								<tr>
								  <td>พนักงานระดับ A จำนวนเงิน</td>
								  <td>8,000 บาท</td>
								</tr>
								<tr>
								  <td>พนักงานระดับ B จำนวนเงิน</td>
								  <td>7,000 บาท</td>
								</tr>
								<tr>
								  <td>พนักงานระดับ C จำนวนเงิน</td>
								  <td>6,000 บาท</td>
								</tr>
								<tr>
								  <td>พนักงานระดับ D จำนวนเงิน</td>
								  <td>5,000 บาท</td>
								</tr>
							  </tbody>
							</table>
						</p>
					
						<li>กรณีบิดา/มารดา / กรณีสามี/ภรรยา (จดทะเบียนสมรส/อยู่กินฉันสามีภรรยา)/กรณีบุตร (บุตรชอบด้วยกฏหมาย/จดทะเบียนรับรองเป็นบุตร) เสียชีวิต</li>
						<p>
							<table class="table">
							  <tbody>
								<tr>
								  <td>พนักงานระดับ A จำนวนเงิน</td>
								  <td>5,000 บาท</td>
								</tr>
								<tr>
								  <td>พนักงานระดับ B จำนวนเงิน</td>
								  <td>4,000 บาท</td>
								</tr>
								<tr>
								  <td>พนักงานระดับ C จำนวนเงิน</td>
								  <td>3,000 บาท</td>
								</tr>
								<tr>
								  <td>พนักงานระดับ D จำนวนเงิน</td>
								  <td>2,000 บาท</td>
								</tr>
							  </tbody>
							</table>
						</p>
					
						<li>พวงหรีด/สิ่งของอื่นคารวะศพ (เบิกจ่ายตามจริง แต่ไม่เกินยอดเงินกำหนด)</li>
						<p>
							<table class="table">
							  <tbody>
								<tr>
								  <td>พนักงานระดับ A จำนวนเงิน</td>
								  <td>1,500 บาท</td>
								</tr>
								<tr>
								  <td>พนักงานระดับ B จำนวนเงิน</td>
								  <td>1,500 บาท</td>
								</tr>
								<tr>
								  <td>พนักงานระดับ C จำนวนเงิน</td>
								  <td>1,000 บาท</td>
								</tr>
								<tr>
								  <td>พนักงานระดับ D จำนวนเงิน</td>
								  <td>1,000 บาท</td>
								</tr>
							  </tbody>
							</table>
						</p>
					
					<h4>เกณฑ์พิจารณา</h4>
					<p>
						<ol>
							<li>กรณีเกิดเหตุให้รีบแจ้งฝ่ายทรัพยากรมนุษย์ทันที</li>
							<li>กรณีพนักงาน จะช่วยเหลือเป็นรายคน</li>
							<li>กรณีครอบครัวพนักงาน หากเป็นพนักงานของบริษัท 2 คนขึ้นไป จะได้รับช่วยเหลือเพียงหนึ่งสิทธิ</li>
							<li>พนักงานต่างระดับกัน ให้เบิกได้ตามอัตราระดับพนักงานที่สูงกว่า</li>
						</ol>
					</p>
					
					<h4>เอกสารประกอบการเบิกจ่าย</h4>
					<p>
						<ol>
							<li>สำเนาใบมรณบัตร</li>
							<li>ใบเสร็จรับเงินค่าพวงหรีด/สิ่งของอื่นๆ (ฉบับจริง)</li>
							<li>ทางฝ่ายทรัพยากรมนุษย์ทำเรื่องเบิกจ่ายให้ในช่วงงาน หรือพนักงานทำเรื่องเบิกจ่ายภายใน 30 วัน นับจากวันเสียชีวิต (แล้วแต่กรณี)</li>
						</ol>
					</p>
					
					<h4>ของเยี่ยม เนื่องจากเกิดอุบัติเหตุในการทำงาน (แพทย์แผนปัจจุบัน ชั้น 1 สั่งหยุดงานตั้งแต่ 3 วันขึ้นไป)</h4>
					<p>
						<table class="table">
						  <tbody>
							<tr>
							  <td>พนักงานระดับ A จำนวนเงิน</td>
							  <td>1,500 บาท</td>
							</tr>
							<tr>
							  <td>พนักงานระดับ B จำนวนเงิน</td>
							  <td>1,500 บาท</td>
							</tr>
							<tr>
							  <td>พนักงานระดับ C จำนวนเงิน</td>
							  <td>1,000 บาท</td>
							</tr>
							<tr>
							  <td>พนักงานระดับ D จำนวนเงิน</td>
							  <td>1,000 บาท</td>
							</tr>
						  </tbody>
						</table>
					</p>
					
					<h4>เกณฑ์พิจารณา</h4>
					<p>
						<ol>
							<li>กรณีเกิดเหตุให้รีบแจ้งฝ่ายทรัพยากรมนุษย์ทันที</li>
							<li>เบิกค่าใช้จ่ายตามจริง แต่ไม่เกินอัตรากำหนดต่อครั้ง</li>
						</ol>
					</p>
					
					<h4>เอกสารประกอบการเบิกจ่าย</h4>
					<p>
						<ol>
							<li>ใบรับรองแพทย์ (ฉบับจริง)</li>
							<li>ใบเสร็จรับเงินค่าของเยี่ยม (ฉบับจริง)</li>
							<li>ทำเรื่องเบิกจ่ายภายใน 30 วันนับจากวันเกิดเหตุ</li>
							<li>ฝ่ายทรัพยากรมนุษย์ออกเยี่ยม หรือตัวแทนต้นสังกัด แล้วแต่กรณีพื้นที่</li>
						</ol>
					</p>
					
					<h4>สิทธิการลงทุน (สำหรับพนักงานอายุการทำงานครบ 3 ปีบริบูรณ์ ขึ้นไป เพื่อสร้างรายได้/อาชีพเสริมแก่พนักงาน และครอบครัวพนักงาน) พนักงานทุกระดับ (A, B, C, D)</h4>
					
					<h4>เกณฑ์พิจารณา</h4>
					<p>
						<ol>
							<li>มูลค่าการลงทุน ตั้งแต่ 120,000 บาทขึ้นไป (หลังจากหักส่วนลดถ้ามี)</li>
							<li>ผ่อนชำระผ่านระบบบัญชีเงินเดือน (Pay Roll) ได้ไม่เกิน 12 งวด โดยปลอดดอกเบี้ย</li>
							<li>ประสงค์ลงทุนเพิ่ม ให้ผ่อนชำระหนี้ลงทุนเดิมหมดก่อน ถึงจะผ่อนชำระใหม่ได้</li>
						</ol>
					</p>
					
					<h4>เอกสารประกอบการเบิกจ่าย</h4>
					<p>
						<ol>
							<li>เขียนใบเบิกสวัสดิการ</li>
							<li>แนบสัญญารายละเอียดสิทธิลงทุน</li>
							<li>พนักงานผู้ผ่อนชำระ มีรายได้หลังหักค่าใช้จ่ายเพียงพอต่อการผ่อนชำระต่อเดือน</li>
							<li>ฝ่จัดหาผู้ค้ำประกันในระดับพนักงานเดียวกัน หรือสูงกว่า จำนวน 1 ราย โดยมีรายได้หลังหักค่าใช้จ่ายเพียงพอต่อการผ่อนชำระต่อเดือน</li>
							<li>แนบสำเนาบัตรประชาชนผู้ผ่อนชำระ และผู้ค้ำประกัน คนละ 1 ฉบับ</li>
							<li>ทำสัญญาผ่อนชำระสินค้า และทำสัญญาค้ำประกัน</li>
							<li>กรณีลาออก/พ้นสภาพพนักงาน จะต้องชำระจำนวนเงินที่คงค้างเต็มจำนวนคืนบริษัททันที หรือภายในวันทำงานสุดท้าย</li>
						</ol>
					</p>
					
					<h4>หมายเหตุ</h4>
					<p>
						<ol>
							<li>รายการสวัสดิการข้างต้น พนักงานที่ได้รับสิทธิจะต้องผ่านการทดลองงานและได้บรรจุเป็นพนักงานบริษัทแล้วเท่านั้น ยกเว้นบางสวัสดิการให้เป็นไปตามหลักเกณฑ์/เงื่อนไขสวัสดิการนั้นๆ กำหนด</li>
							<li>ระดับพนักงาน A คือ ผู้ช่วยรองกรรมการผู้จัดการขึ้นไป, ระดับ B คือ ผู้จัดการฝ่าย - ผู้จัดการทั่วไป, ระดับ C คือ หัวหน้าส่วน - ผู้จัดการแผนก, ระดับ D คือ พนักงาน - หัวหน้างาน</li>
							<li>การเบิกจ่าย พนักงานจะต้องดำเนินการเขียนใบเบิก พร้อมแนบเอกสารหลักฐานประกอบการเบิกทุกครั้ง ถึงจะได้รับการพิจารณาอนุมัติเบิกจ่าย</li>
							<li>กำหนดตั้งเรื่องเบิกจ่ายผ่านฝ่ายทรัพยากรมนุษย์ตรวจสอบ และส่งต่อฝ่ายบัญชีและการเงินทำจ่าย ตามเวลากำหนดข้างต้น มิฉะนั้นจะถือว่าพนักงานสละสิทธิ โดยไม่มีข้อโต้แย้งแต่อย่างใดทั้งสิ้น</li>
							<li>การเบิกสวัสดิการพนักงาน ที่ไม่สามารถใช้สิทธิเบิกได้ มีกรณีต่างๆ ดังนี้ ทำร้ายร่างกายตน การใช้สารเสพติดตามกฏหมายสารเสพติด การกระทำการใดๆ เพื่อความสวยงามโดยไม่มีข้อบ่งชี้ทางการแพทย์ การรักษาที่อยู่ ระหว่างการค้นคว้าทดลอง การรักษาภาวะมีบุตรยาก การตรวจเนื้อเยื่อเพื่อการผ่าตัดเปลี่ยนอวัยวะ การผ่าตัดเปลี่ยนอวัยวะ การเปลี่ยนเพศ การผสมเทียม ทันตกรรม และแว่นตา เป็นต้น</li>
							<li>ผู้มีอำนาจอนุมัติเบิกจ่ายรายการสวัสดิการข้างต้น ได้แก่กรรมการบริหาร หรือผู้ที่ได้รับมอบหมาย</li>
							<li>กรณีตรวจสอบพบว่าผู้บังคับบัญชาต้นสังกัดลงนามรับรองผิดพลาดจากการเหตุการณ์จริง/เดินทางจริง ส่วนที่ผิดพลาดผู้ลงนามรับรองนั้นๆ จะต้องเป็นผู้จ่ายเงินคืนให้บริษัท</li>
							<li>บริษัทขอสงวนไว้ซึ่งสิทธิที่จะยกเลิก แก้ไข เพิ่มเติม เปลี่ยนแปลงอัตราข้างต้นได้ตามเห็นสมควร</li>
							<li>ทั้งนี้ให้มีผลตั้งแต่วันที่ 1 ตุลาคม 2560 เป็นต้นไป จนกว่าจะมีประกาศเปลี่ยนแปลง</li>
						</ol>
					</p>
					
					<div class="blog-details-bt">
						<div class="btn-group">
							<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
						</div>
					</div>
				
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-8 col-xs-12 p-fix">
				<aside>
					<div class="sidebar-news-list">
						<h4>CATEGORY</h4>
						<ul>
							<li>
								<a href="Benefits1.aspx" class="tran3s">กองทุนสำรองเลี้ยงชีพ มอบเกียรติบัตรเงินรางวัล</a>
							</li>
							<li>
								<a href="Benefits2_1.aspx" class="tran3s">เงินช่วยเหลืองาน และโอกาสพิเศษต่างๆ</a>
							</li>
							<li>
								<a href="Benefits15.aspx" class="tran3s">เจ็บป่วย ประกันชีวิต สุขภาพ อุบัติเหตุ</a>
							</li>
							<li>
								<a href="Benefits5_1.aspx" class="tran3s">ปฏิบัติงานนอกสถานที่ (ในประเทศ)</a>
							</li>
							<li>
								<a href="Benefits3_1.aspx" class="tran3s">ปฏิบัติงานนอกสถานที่ (ต่างประเทศ)</a>
							</li>
							<li>
								<a href="Benefits4_1.aspx" class="tran3s">ปฏิบัติงาน EXHIBITION - EVENT (ในประเทศ)</a>
							</li>
							<li>
								<a href="Benefits7_1.aspx" class="tran3s">ค่าเลี้ยงรับรอง</a>
							</li>
							<li>
								<a href="Benefits8.aspx" class="tran3s">เงินกู้</a>
							</li>
							<li>
								<a href="Benefits9_1.aspx" class="tran3s">ส่วนลดสินค้า สำหรับพนักงาน</a>
							</li>
							<li>
								<a href="Benefits10_1.aspx" class="tran3s">เบี้ยขยัน</a>
							</li>
							<li>
								<a href="Benefits11_1.aspx" class="tran3s">ตรวจสุขภาพ</a>
							</li>
							<li>
								<a href="#" class="tran3s">สวัสดิการอื่นๆ</a>
							</li>
						</ul>
					</div>
				</aside>
			</div>
		</div>
		
	</div>
</article>

</asp:Content>
