﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="index_hr.aspx.vb" Inherits="Aroma_HRPortal.index_hr" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<style>
	.swiper-container {
		width: 100%;
		height: 100%;
	}
	.swiper-slide {
		text-align: center;
		background: #fff;
	}
	.swiper-slide img 
	{
		width: 100%;
	}
	.swiper-pagination-bullet-active {
		opacity: 1;
		background: #af2f2f;
	}
	.swiper-button-next, .swiper-button-prev {
		width: 32px;
		background-size: 32px;
		top: 45%;
	}
	.swiper-button-prev, .swiper-container-rtl .swiper-button-next {
		background-image: url(new/images/arrow-l.png);
		left: 20px;
		right: auto;
	}
	.swiper-button-next, .swiper-container-rtl .swiper-button-prev {
		background-image: url(new/images/arrow-r.png);
		right: 20px;
		left: auto;
	}
</style>

<!-- banner -->
<!-- Swiper -->
<div class="swiper-container swiper1">
    <div class="swiper-wrapper">
        <div class="swiper-slide"><asp:Image ID="Image1" runat="server" width="100%" /></div>
        <div class="swiper-slide"><asp:Image ID="Image2" runat="server" width="100%" /></div>
        <div class="swiper-slide"><asp:Image ID="Image3" runat="server" width="100%" /></div>
        <div class="swiper-slide"><asp:Image ID="Image4" runat="server" width="100%" /></div>
        <div class="swiper-slide"><asp:Image ID="Image5" runat="server" width="100%" /></div>
    </div>
    <!-- Add Pagination -->
   	<div class="swiper-pagination swiper-pagination1"></div>
    <!-- Add Arrows -->
    <!--div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div-->
</div>
<!-- end banner -->

<section id="hr-policy">
	<div class="container">
		<div class="theme-title">
			<h2>WELCOME TO HR DEPARTMENT SYSTEM</h2>
			<p>"วันนี้ Aroma Group ก้าวเข้าสู่ยุคใหม่ที่จะสร้างแรงบันดาลใจให้แก่ทุกคน เรามาออกเดินทางเพื่อค้นหาสิ่งใหม่และนำพาทุกความสำเร็จมาสู่ทุกคนด้วยกัน ผมพร้อมแล้ว คุณ..พร้อมหรือยัง" คุณกิจจา วงศ์วารี</p>
		</div>
	</div>
	<div class="container-fluid">
		<!-- menu img -->
		<div class="size-menu-img">
			<div class="menu-img-l">
				<div class="img">
					<img src="new/images/index/project1.jpg" alt="Image">
					<a href="HRPolicy_BrandStory.aspx" class="opacity tran4s"><i class="fa fa-link" aria-hidden="true"></i></a>
				</div>
			</div>
		   <div class="menu-img-l">
				<div class="img">
					<img src="new/images/index/project2.jpg" alt="Image">
					<a href="HRPolicy_BusinessEthics.aspx" class="opacity tran4s"><i class="fa fa-link" aria-hidden="true"></i></a>
				</div>
			</div>
			<div class="menu-img-l">
				<div class="img">
					<img src="new/images/index/project3.jpg" alt="Image">
					<a href="HRPolicy_Announce.aspx" class="opacity tran4s"><i class="fa fa-link" aria-hidden="true"></i></a>
				</div>
			</div>
			<div class="menu-img-l">
				<div class="img">
					<img src="new/images/index/project4.jpg" alt="Image">
					<a href="HRPolicy_Orientation.aspx" class="opacity tran4s"><i class="fa fa-link" aria-hidden="true"></i></a>
				</div>
			</div>
			<!--div class="menu-img-l">
				<div class="img">
					<img src="new/images/index/project5.jpg" alt="Image">
					<a href="HRPolicy_Employee.aspx" class="opacity tran4s"><i class="fa fa-link" aria-hidden="true"></i></a>
				</div>
			</div>
		   <div class="menu-img-l">
				<div class="img">
					<img src="new/images/index/project6.jpg" alt="Image">
					<a href="HRPolicy_Holidays.aspx" class="opacity tran4s"><i class="fa fa-link" aria-hidden="true"></i></a>
				</div>
			</div>
			<div class="menu-img-l">
				<div class="img">
					<img src="new/images/index/project8.jpg" alt="Image">
					<a href="HRPolicy_Contact.aspx" class="opacity tran4s"><i class="fa fa-link" aria-hidden="true"></i></a>
				</div>
			</div>
			<div class="menu-img-l">
				<div class="img">
					<img src="new/images/index/project9.jpg" alt="Image">
					<a href="HRPolicy_WhoisWho.aspx" class="opacity tran4s"><i class="fa fa-link" aria-hidden="true"></i></a>
				</div>
			</div-->
			<div class="clear-fix"></div>
		</div>
		<!-- end menu img -->
	</div>
</section>
		
<!--
=====================================================
	Team Section
=====================================================
-->
<section id="team-section">
	<div class="container">
		<div class="theme-title">
			<h2>BIRTHDAY THIS WEEK</h2>
			<p>have a Happy Birthday, request have strong health, there is the progress in the work and have one’s hopes fulfilled for what, wish every the points<br>
			สุขสันต์วันเกิด ขอให้สุขภาพแข็งแรง มีความเจริญก้าวหน้าในหน้าที่การงานและสมหวังในสิ่งที่ปรา รถนาทุกประการ</p>
		</div> <!-- /.theme-title -->

		<div class="clear-fix team-member-wrapper">
			<div class="float-left">
				<iframe id="I1" runat="server" name="I1" src="index_Birthday.aspx" border=0 scrolling="no" frameborder="0" style = "width: 100%; border:0px; overflow:hidden;"></iframe>
			</div> <!-- /float-left -->

			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/team/2.jpg" alt="Image">
						<div class="opacity tran4s">
							<h3>อมร มุ้งอ้อมกลาง</h3>
							<span>Media Partner</span>
							<p>On the other hand, We denounce ut with righteo indignation and dislike men who are so beguiled and demoralized.</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>อมร มุ้งอ้อมกลาง</h5>
						<p>Media Partner</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->

			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/team/3.jpg" alt="Image">
						<div class="opacity tran4s">
							<h3>ขวัญใจ ดีจริงจริง</h3>
							<span>Graphic Design</span>
							<p>On the other hand, We denounce ut with righteo indignation and dislike men who are so beguiled and demoralized.</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>ขวัญใจ ดีจริงจริง</h5>
						<p>Graphic Design</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->
		</div> <!-- /.team-member-wrapper -->
	</div> <!-- /.conatiner -->
</section> <!-- /#team-section -->
			
<!--
=====================================================
	Team Section
=====================================================
-->
<section id="team-section" class="bg-team-section">
	<div class="container">
		<div class="theme-title">
			<h2>NEW EMPLOYEE</h2>
			<p>Message from CEO<br>กิจจา วงศ์วารี<br>กรรมการบริหารบริษัท อโรม่า กรุ๊ป<br><br>

            จากประสบการณ์ของ อโรม่า กรุ๊ป ในเส้นทางสายกาแฟของเรา เราได้สั่งสมความรู้ไม่ว่าจะเป็นทั้งภาคทฤษฎีและภาคปฏิบัตินั้นมีมากมายผมถือว่าเป็นหัวใจสำคัญที่ทำให้ อโรม่า กรุ๊ป ยืนหยัดอย่างเข้มแข็ง ในฐานะผู้นำทางธุรกิจกาแฟจนถึงทุกวันนี้<br><br>

            ผมมองว่า ‘บริษัทที่สร้างกำไรมากที่สุด อาจไม่ใช่บริษัทที่เจริญที่สุด’ ถ้าคุณต้องการเจริญเติบโตจะต้องไม่เอารัดเอาเปรียบใคร คำว่า Business ในมุมมองของผมแปลว่า ‘การอยู่ร่วมกัน’ ต้องอยู่แบบไม่เอาเปรียบนะครับ เราอยู่ได้ เขาอยู่ได้ แต่ถ้าคุณมองว่า Business คือ กำไร มันจะเกิดการแข่งขันทันที ยิ่งแข่งก็ยิ่งแพ้ ไม่มีใครชนะ ทุกคนต้องอยู่ร่วมกันให้ได้ เป็นพันธมิตรกัน แล้วกำไรจะตามมาเอง<br><br>
 
            เป้าหมายการทำธุรกิจของเราอย่างหนึ่งก็คือ ทำให้ทุกคนมีชีวิตความเป็นอยู่ที่ดีกว่าเดิม เราต้องทำให้เขาเติบโตให้ได้ เพราะเราก็ต้องการเติบโตเช่นกันครับ ถ้าเขามีรายได้ดีขึ้นก็จะอยู่กับเราไปเรื่อยๆ บริษัทของเรายินดีสนับสนุนเต็มที่อย่างเวลาไปออกบูธขายกาแฟในงานแสดงสินค้า ต้องมีค่าเช่าพื้นที่แต่ทางบริษัทพร้อมออกค่าใช้จ่ายส่วนนี้ให้นะ ถามว่าเราได้อะไร? กาแฟของเราขายได้ไงครับ แบรนด์เราจะเป็นที่รู้จักมากขึ้น เราออกค่าเช่า ส่วนเขาซื้อกาแฟเราไปขาย การทำธุรกิจมันต้องแบ่งกันกิน อย่าอ้วนอยู่คนเดียวเพราะสุดท้ายแล้วเขาจะไปต่อไม่ไหว เราต้องโตไปด้วยกัน<br><br>
 
            ทุกวันนี้รายได้หลักของบริษัทมาจากตลาดกาแฟภายในประเทศ แต่ผมยังมีแผนการตลาดที่จะพัฒนาบริษัทให้เติบโตแบบไม่หยุดยั้ง ส่งเสริมให้ไปไกลระดับต่างประเทศด้วย ส่วนก้าวต่อไปในระยะเวลาอันใกล้คือการขยายเอาท์เลตให้ครอบคลุมทั่วประเทศ สร้างการรับรู้มากขึ้น เพื่อให้คนมาร่วมเป็นพันธมิตรกับ Aroma Shop และที่สำคัญก็คือ อยากให้คนไทยดื่มเอสเปรสโซ่เป็น เพราะจะได้ดื่มด่ำรสชาติกาแฟที่แท้จริง</p>
		</div> <!-- /.theme-title -->

		<div class="clear-fix team-member-wrapper">
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/team/1.jpg" alt="Image">
						<div class="opacity tran4s">
							<h3>คุณศิริพร นิลกำแหง (น้อง)</h3>
							<span>อำนวยการฝ่ายทรัพยากรมนุษย์</span>
							<p>
								เบอร์ต่อภายใน : 404<br>
								โทรศัพท์มือถือ : -<br>
								Email : <a href="mailto:siripornn@aromathailand.com">siripornn@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณศิริพร นิลกำแหง (น้อง)</h5>
						<p>ผู้อำนวยการฝ่ายทรัพยากรมนุษย์</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->

			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/team/2.jpg" alt="Image">
						<div class="opacity tran4s">
							<h3>คุณกันตพัฒน์ ตังพิพัฒน์ชัย (ชัช)</h3>
							<span>ผู้อำนวยการฝ่ายทรัพยากรมนุษย์</span>
							<p>
								เบอร์ต่อภายใน : 400<br>
								โทรศัพท์มือถือ : 085-4802750<br>
								Email : <a href="mailto:kantapatt@aromathailand.com">kantapatt@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณกันตพัฒน์ ตังพิพัฒน์ชัย (ชัช)</h5>
						<p>ผู้อำนวยการฝ่ายทรัพยากรมนุษย์</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->

			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/team/3.jpg" alt="Image">
						<div class="opacity tran4s">
							<h3>คุณภนัญญา ศรีทวี</h3>
							<span>ผู้จัดการฝ่ายทรัพยากรมนุษย์</span>
							<p>
								เบอร์ต่อภายใน : 416<br>
								โทรศัพท์มือถือ : -<br>
								Email : <a href="mailto:phananyas@aromathailand.com">phananyas@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณภนัญญา ศรีทวี</h5>
						<p>ผู้จัดการฝ่ายทรัพยากรมนุษย์</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->
		</div> <!-- /.team-member-wrapper -->
	</div> <!-- /.conatiner -->
</section> <!-- /#team-section -->

<div id="pricing-section">
	<div class="container">
		<div class="clear-fix">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="single-price-table">
					<!--i class="fas fa-external-link-alt"></i-->
					<img src="new/images/index/blog1.jpg" alt=""/>
					<h4>Leave online & Time Attendance</h4>
					<!--p>
						Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero sit amet.
					</p-->
					<a href="http://leaveonline.aromathailandapp.com/Leave_Login.aspx">Read more</a>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="single-price-table">
					<!--i class="fas fa-user"></i-->
					<img src="new/images/index/blog2.jpg" alt=""/>
					<h4>Career Opportunities</h4>
					<!--p>
						Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero sit amet.
					</p-->
					<a href="http://www.aromathailand.com/2015/career.php">Read more</a>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="single-price-table">
					<!--i class="fas fa-book"></i-->
					<img src="new/images/index/blog3.jpg" alt=""/>
					<h4>Instruction Manual</h4>
					<!--p>
						Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero sit amet.
					</p-->
					<a href="Document/คู่มือการใช้งาน HR Portal.pdf">Read more</a>
				</div>
			</div>
		</div>
	</div>
</div>
        
<!-- Swiper JS -->
<script src="new/js/swiper.min.js"></script>

<!-- Initialize Swiper -->
<script>
	var swiper1 = new Swiper('.swiper1', {
		pagination: {
        el: '.swiper-pagination1',
        clickable: true,
      },
    });
	var swiper2 = new Swiper('.swiper2', {
		slidesPerView: 2,
      	spaceBetween: 30,
		navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
    var swiper3 = new Swiper('.swiper3', {
		slidesPerView: 2,
		spaceBetween: 30,
		navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
</script>  

</asp:Content>
