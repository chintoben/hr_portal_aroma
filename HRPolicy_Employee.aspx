﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_Employee.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_Employee" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/hr.jpg" alt=""/></div>
<!-- end banner -->

<article class="blog-details-page">
	<div class="container">
		
		<div class="post-heading">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
            </ul>
			<h2>กลุ่มระดับพนักงาน หน้าที่งาน ปรับตำแหน่ง</h2>
		</div> <!-- /.post-heading -->
		
		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix" style="float: none; margin: 0 auto;">
				<div class="blog-details-post-wrapper">
				
					<img src="new/images/banner/hr-policy/img23.jpg" alt=""/>

					<h4>กลุ่มระดับตำแหน่งพนักงาน</h4>
					<p>
						<ul class="p">
							<li><a href="Document/2Orientation/Employee/1.1กลุ่มระดับ และตำแหน่งพนักงาน update Sep.01,2017.pdf"  target="_blank">กลุ่มระดับ และตำแหน่งพนักงาน update Sep.01,2017</a>
							</li>
						</ul>
					</p>
					
					<h4>ขอบเขตหน้าที่ความรับผิดชอบและคุณสมบัติแต่ละตำแหน่งงาน</h4>
					<p>
						<ul class="p">
							<li><a href="Document/2Orientation/Employee/2.1ขอบเขตความรับผิดชอบและคุณสมบัติ (ผู้จัดการทั่วไป General Manager).pdf"  target="_blank">ขอบเขตความรับผิดชอบและคุณสมบัติ (ผู้จัดการทั่วไป General Manager)</a></li>
							<li><a href="Document/2Orientation/Employee/2.1ขอบเขตความรับผิดชอบและคุณสมบัติ (ผู้อำนวยการฝ่าย  (Director Department).pdf"  target="_blank">ขอบเขตความรับผิดชอบและคุณสมบัติ (ผู้อำนวยการฝ่าย  (Director Department)</a></li>
							<li><a href="Document/2Orientation/Employee/2.3ขอบเขตความรับผิดชอบและคุณสมบัติ (ผู้จัดการฝ่าย Department Manager).pdf"  target="_blank">ขอบเขตความรับผิดชอบและคุณสมบัติ (ผู้จัดการฝ่าย Department Manager)</a></li>
							<li><a href="Document/2Orientation/Employee/2.4ขอบเขตความรับผิดชอบและคุณสมบัติ (ผู้จัดการแผนก Division Manager).pdf"  target="_blank">ขอบเขตความรับผิดชอบและคุณสมบัติ (ผู้จัดการแผนก Division Manager)</a></li>
							<li><a href="Document/2Orientation/Employee/2.5ขอบเขตความรับผิดชอบและคุณสมบัติ (หัวหน้าแผนก Division Chief).pdf"  target="_blank">ขอบเขตความรับผิดชอบและคุณสมบัติ (หัวหน้าแผนก Division Chief)</a></li>
							<li><a href="Document/2Orientation/Employee/2.6ขอบเขตความรับผิดชอบและคุณสมบัติ (หัวหน้าส่วน Section Chief).pdf"  target="_blank">ขอบเขตความรับผิดชอบและคุณสมบัติ (หัวหน้าส่วน Section Chief)</a></li>
							<li><a href="Document/2Orientation/Employee/2.7ขอบเขตความรับผิดชอบและคุณสมบัติ (หัวหน้างาน Supervisor).pdf"  target="_blank">ขอบเขตความรับผิดชอบและคุณสมบัติ (หัวหน้างาน Supervisor)</a></li>
							<li><a href="Document/2Orientation/Employee/2.8ขอบเขตความรับผิดชอบและคุณสมบัติ (เจ้าหน้าที่อาวุโส Senior Officer).pdf"  target="_blank">ขอบเขตความรับผิดชอบและคุณสมบัติ (เจ้าหน้าที่อาวุโส Senior Officer)</a></li>
							<li><a href="Document/2Orientation/Employee/2.9ขอบเขตความรับผิดชอบและคุณสมบัติ (เจ้าหน้าที่ Officer).pdf"  target="_blank">ขอบเขตความรับผิดชอบและคุณสมบัติ (เจ้าหน้าที่ Officer)</a></li>
							<li><a href="Document/2Orientation/Employee/2.10ขอบเขตความรับผิดชอบและคุณสมบัติ (พนักงาน Staff).pdf"  target="_blank">ขอบเขตความรับผิดชอบและคุณสมบัติ (พนักงาน Staff)</a></li>
						</ul>
					</p>
					
					<h4>ขอบเขตหน้าที่ความรับผิดชอบและคุณสมบัติแต่ละตำแหน่งงาน</h4>
					<p>
						<ul class="p">
							<li><a href="Document/2Orientation/Employee/3.1หลักเกณฑ์การพิจารณาปรับตำแหน่งงาน อโรม่า กรุ๊ป.pdf"  target="_blank">หลักเกณฑ์การพิจารณาปรับตำแหน่งงาน อโรม่า กรุ๊ป</a></li>
							<li><a href="Document/2Orientation/Employee/3.2หลักเกณฑ์การพิจารณาปรับตำแหน่งงานเป็น ผู้จัดการทั่วไป-General Manager (B3).pdf"  target="_blank">หลักเกณฑ์การพิจารณาปรับตำแหน่งงานเป็น ผู้จัดการทั่วไป-General Manager (B3)</a></li>
							<li><a href="Document/2Orientation/Employee/3.3หลักเกณฑ์การพิจารณาปรับตำแหน่งงานเป็น ผู้อำนวยการฝ่าย-Director Department (B2).pdf"  target="_blank">หลักเกณฑ์การพิจารณาปรับตำแหน่งงานเป็น ผู้อำนวยการฝ่าย-Director Department (B2)</a></li>
							<li><a href="Document/2Orientation/Employee/3.4หลักเกณฑ์การพิจารณาปรับตำแหน่งงานเป็น ผู้จัดการฝ่าย-Department Manager (B1).pdf"  target="_blank">หลักเกณฑ์การพิจารณาปรับตำแหน่งงานเป็น ผู้จัดการฝ่าย-Department Manager (B1)</a></li>
							<li><a href="Document/2Orientation/Employee/3.5หลักเกณฑ์การพิจารณาปรับตำแหน่งงานเป็น ผู้จัดการแผนก-Division Manager (C3).pdf"  target="_blank">หลักเกณฑ์การพิจารณาปรับตำแหน่งงานเป็น ผู้จัดการแผนก-Division Manager (C3)</a></li>
							<li><a href="Document/2Orientation/Employee/3.6หลักเกณฑ์การพิจารณาปรับตำแหน่งงานเป็น หัวหน้าแผนก-Division Chief (C2).pdf"  target="_blank">หลักเกณฑ์การพิจารณาปรับตำแหน่งงานเป็น หัวหน้าแผนก-Division Chief (C2)</a></li>
							<li><a href="Document/2Orientation/Employee/3.7หลักเกณฑ์การพิจารณาปรับตำแหน่งงานเป็น หัวหน้าส่วน-Section Chief (C1).pdf"  target="_blank">หลักเกณฑ์การพิจารณาปรับตำแหน่งงานเป็น หัวหน้าส่วน-Section Chief (C1)</a></li>
							<li><a href="Document/2Orientation/Employee/3.8หลักเกณฑ์การพิจารณาปรับตำแหน่งงานเป็น หัวหน้างาน-Supervisor (D4).pdf"  target="_blank">หลักเกณฑ์การพิจารณาปรับตำแหน่งงานเป็น หัวหน้างาน-Supervisor (D4)</a></li>
							<li><a href="Document/2Orientation/Employee/3.9หลักเกณฑ์การพิจารณาปรับตำแหน่งงานเป็น เจ้าหน้าที่อาวุโส-Seior Officer (D3).pdf"  target="_blank">หลักเกณฑ์การพิจารณาปรับตำแหน่งงานเป็น เจ้าหน้าที่อาวุโส-Seior Officer (D3)</a></li>
							<li><a href="Document/2Orientation/Employee/3.10หลักเกณฑ์การพิจารณาปรับตำแหน่งงานเป็น เจ้าหน้าที่-Officer (D2).pdf"  target="_blank">หลักเกณฑ์การพิจารณาปรับตำแหน่งงานเป็น เจ้าหน้าที่-Officer (D2)</a></li>
							<li><a href="Document/2Orientation/Employee/3.11หลักเกณฑ์การพิจารณาปรับตำแหน่งงานเป็น พนักงานอาวุโส-Senior Staff (D1).pdf"  target="_blank">หลักเกณฑ์การพิจารณาปรับตำแหน่งงานเป็น พนักงานอาวุโส-Senior Staff (D1)</a></li>
						</ul>
					</p>
				
				</div>
			</div>
		</div>
		
	</div>
</article>
    
<!-- Swiper JS -->
<script src="new/js/swiper.min.js"></script>

<!-- Initialize Swiper -->
<script>
	var swiper1 = new Swiper('.swiper1', {
		spaceBetween: 30,
		pagination: {
        el: '.swiper-pagination1',
        clickable: true,
      },
    });
</script>

</asp:Content>
