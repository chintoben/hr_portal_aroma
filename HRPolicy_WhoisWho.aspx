﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_WhoisWho.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_WhoisWho" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/HR.jpg" alt=""/></div>
<!-- end banner -->

<article class="blog-details-page">
	<div class="container">
		
		<div class="post-heading">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
            </ul>
			<h2>ติดต่อฝ่าย-แผนกต่างๆ</h2>
		</div> <!-- /.post-heading -->
		
		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix" style="float: none; margin: 0 auto;">
				<div class="blog-details-post-wrapper">
				
					<img src="new/images/banner/hr-policy/img26.jpg" alt=""/>

					<p style="padding: 30px 0 0;">
						<ul class="n">
							<li><a href="HRPolicy_WhoisWho1.aspx">รายชื่อติดต่อ-สำนักคณะกรรมการ, เลขานุการ (Board Director)</a></li>
							<li><a href="HRPolicy_WhoisWho2.aspx">รายชื่อติดต่อ-ขาย Aroma Shop</a></li>
                            <li><a href="HRPolicy_WhoisWho9.aspx">รายชื่อติดต่อ-การตลาด (Marketing)</a></li>
							<li><a href="HRPolicy_WhoisWho3.aspx">รายชื่อติดต่อ-ขายโรงแรม&โมเดิร์นเทรด (Horeca&Moderntrade)</a></li>
                            <li><a href="HRPolicy_WhoisWho11.aspx">รายชื่อติดต่อ-สถาบันพัฒนาธุรกิจกาแฟ (ACA)</a></li>
                            <li><a href="HRPolicy_WhoisWho12.aspx">รายชื่อติดต่อ-ไลอ้อน ทรี-สตาร์ (Lion 3 star)</a></li>
                            <li><a href="HRPolicy_WhoisWho4.aspx">รายชื่อติดต่อ-กลุ่มลูกค่าองค์กร (Corporate Account)</a></li>
                            <li><a href="HRPolicy_WhoisWho5.aspx">รายชื่อติดต่อ-ชาวดอย (Chao Doi)</a></li>
                            <li><a href="HRPolicy_WhoisWho22.aspx">รายชื่อติดต่อ-เทคโนโลยีสารสนเทศ (MIS)</a></li>
                            <li><a href="HRPolicy_WhoisWho20.aspx">รายชื่อติดต่อ-โรงงานบางปะกง (Plant)</a></li>
                            <li><a href="HRPolicy_WhoisWho19.aspx">รายชื่อติดต่อ- Supply Chain</a></li>
                            <li><a href="HRPolicy_WhoisWho13.aspx">รายชื่อติดต่อ-การเงิน (Finance)</a></li>
                            <li><a href="HRPolicy_WhoisWho14.aspx">รายชื่อติดต่อ-บัญชี (Accounting)</a></li>
                            <li><a href="HRPolicy_WhoisWho17.aspx">รายชื่อติดต่อ-งบประมาณและตรวจติดตาม (Budget)</a></li>
                            <li><a href="HRPolicy_WhoisWho18.aspx">รายชื่อติดต่อ-บัญชีบริหารและระบบงาน (audit)</a></li>
                            <li><a href="HRPolicy_WhoisWho6.aspx">รายชื่อติดต่อ-พัฒนาธุรกิจระหว่างประเทศ (Inter Business Development)</a></li>
                            <li><a href="HRPolicy_WhoisWho8.aspx">รายชื่อติดต่อ-ออกแบบตกแต่งภายใน (interior design)</a></li>
                            <li><a href="HRPolicy_WhoisWho10.aspx">รายชื่อติดต่อ-ผลิตภัณฑ์ (Product)</a></li>
                            <li><a href="HRPolicy_WhoisWho15.aspx">รายชื่อติดต่อ-จัดซื้อ (Purchasing)</a></li>
                            <li><a href="HRPolicy_WhoisWho16.aspx">รายชื่อติดต่อ-จัดซื้อต่างประเทศ (import)</a></li>

                            <li><a href="HRPolicy_WhoisWho21.aspx">รายชื่อติดต่อ-วิจัยและพัฒนา (RD)</a></li>

                           	<li><a href="HRPolicy_WhoisWho23.aspx">รายชื่อติดต่อ-กฏหมาย (Legal)</a></li>
                           	<li><a href="HRPolicy_WhoisWho24.aspx">รายชื่อติดต่อ-อาคารสถานที่ (Building)</a></li>

                            <br />

                            <li><a href="HRPolicy_WhoisWhoAll.aspx">รายชื่อติดต่อ-พนักงานทั้งหมด Aroma Group</a></li>

                            <%--<li><a href="HRPolicy_WhoisWho7.aspx">รายชื่อติดต่อ-พัฒนาธุรกิจ (Business Development)</a></li>--%>
                    

                        
						</ul>
					</p>
				
				</div>
			</div>
		</div>
		
	</div>
</article>

</asp:Content>
