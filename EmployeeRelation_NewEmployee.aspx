﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="EmployeeRelation_NewEmployee.aspx.vb" Inherits="Aroma_HRPortal.EmployeeRelation_NewEmployee" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



<!-- banner -->
<div class="base-banner">
<img src="new/images/banner/Activities.jpg" 
        alt="" />
</div>
<!-- end banner -->

<section id="about-us" class="blog-category">
<div class="container">
	<div class="theme-title tt-edit category-title">
        <ul>
            <li><a href="index_hr.aspx">HOME</a></li>
        </ul>
		<h2>พนักงานเข้าใหม่</h2>
		<!--p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p-->
	</div> <!-- /.theme-title -->

        <div class="row">

		<%--<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-046-megaphone"></i>
				</div>
				<h3><a href="Benefits1_1.aspx" class="tran3s">ประกาศพนักงานเข้าใหม่ (ทุกบริษัท) ประจำเดือน พฤษภาคม-มิถุนายน 2563</a></h3>
				<!--p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p-->
				<a href="Document/3EmployeeLevel/ประกาศรายชื่อพนักงานเข้าใหม่ (ทุกบริษัท) ประจำเดือนมิถุนายน 2563.pdf" target="_blank" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-046-megaphone"></i>
				</div>
				<h3><a href="Benefits1_1.aspx" class="tran3s">ประกาศพนักงานเข้าใหม่ (ทุกบริษัท) ประจำเดือน เมษายน 2563</a></h3>
				<!--p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p-->
				<a href="Document/3EmployeeLevel/ประกาศรายชื่อพนักงานเข้าใหม่ (ทุกบริษัท) ประจำเดือน เมษายน 2563.pdf" target="_blank" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
        <div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-046-megaphone"></i>
				</div>
				<h3><a href="Benefits1_1.aspx" class="tran3s">ประกาศพนักงานเข้าใหม่ (ทุกบริษัท) ประจำเดือน มีนาคม 2563</a></h3>
				<!--p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p-->
				<a href="Document/3EmployeeLevel/ประกาศรายชื่อพนักงานเข้าใหม่ (ทุกบริษัท) ประจำเดือน มีนาคม 2563.pdf" target="_blank" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->--%>


        <div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div style="border: 1px solid gray;box-shadow: 4px 4px 3px gray;"> 
					<video width="99%" controls>
                        <source src="video/vdoHR1_1.mp4" type="video/mp4">
                    </video>
                </div>
                <div style="margin-top:-15px;">&nbsp;</div>
                <h4>ประกาศพนักงานเข้าใหม่[ทุกบริษัท]</h4>
                <h4>ประจำเดือน พฤษภาคม - มิถุนายน 2564</h4>
	

				<!--p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p-->
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->

        <div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div style="border: 1px solid gray;box-shadow: 4px 4px 3px gray;"> 
					<video width="99%" controls>
                        <source src="video/NewEmployee_July2.mp4" type="video/mp4">
                    </video>
                </div>
                <div style="margin-top:-15px;">&nbsp;</div>
                <h4>ประกาศพนักงานเข้าใหม่[ทุกบริษัท]</h4>
                <h4>ประจำเดือน มิถุนายน-กรกฏาคม 2564</h4>
			</div> 
		</div> 
        <div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div style="border: 1px solid gray;box-shadow: 4px 4px 3px gray;"> 
					<video width="99%" controls>
                        <source src="video/แนะนำมาพนักงาน082021.mp4" type="video/mp4">
                    </video>
                </div>
                <div style="margin-top:-15px;">&nbsp;</div>
                <h4>ประกาศพนักงานเข้าใหม่[ทุกบริษัท]</h4>
                <h4>ประจำเดือน กรกฏาคม-สิงหาคม 2564</h4>
			</div> 
		</div> 
        
	</div> <!-- /.row -->
	</div> <!-- /.container -->
</section>
  
<!-- Swiper JS -->
<script src="new/js/swiper.min.js"></script>

<!-- Initialize Swiper -->
<script>
	var swiper1 = new Swiper('.swiper1', {
		spaceBetween: 30,
		pagination: {
        el: '.swiper-pagination1',
        clickable: true,
      },
    });
</script>

</asp:Content>
