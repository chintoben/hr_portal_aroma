﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_Announce.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_Announce" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!--
=====================================================
	Theme Inner page Banner
=====================================================
-->
<!--section class="inner-page-banner" style="background: url(new/images/banner/benefit/banner1.jpg) no-repeat center;">
<div class="opacity">
	<div class="container">
		<h1>HR POLICY</h1>
		<ul>
			<li><a href="index.html">Home</a></li>
			<li>/</li>
			<li>Blog Details</li>
		</ul>
	</div> <!-- /.container -->
<!--/div> <!-- /.opacity -->
<!--/section--> <!-- /.inner-page-banner -->

<style>
	.swiper-container {
		width: 100%;
		height: 100%;
	}
	.swiper-slide {
		text-align: center;
		background: #fff;
	}
	.swiper-slide img 
	{
		width: 100%;
	}
	.swiper-pagination-bullet-active {
		opacity: 1;
		background: #af2f2f;
	}
</style>

<!-- banner -->
<!-- Swiper -->
<div class="swiper-container swiper1">
    <div class="swiper-wrapper">
        <div class="swiper-slide" data-hash="slide1"><img src="new/images/banner/hr-policy/banner.jpg" /></div>
        <div class="swiper-slide" data-hash="slide1"><img src="new/images/banner/hr-policy/banner.jpg" /></div>
    </div>
    <!-- Add Pagination -->
   	<div class="swiper-pagination swiper-pagination1"></div>
    <!-- Add Arrows -->
    <!--div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div-->
</div>
<!-- end banner -->

<section id="about-us">
<div class="container">
	<div class="theme-title tt-edit">
		<h2>ประวัติความเป็นมา โครงสร้างการบริหาร</h2>
		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p>
	</div> <!-- /.theme-title -->

	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas fa-pencil-alt"></i>
				</div>
				<h3><a href="http://www.aromathailand.com/2015/index.php" target="_blank" class="tran3s">ประวัติความเป็นมา โครงสร้างการบริหาร</a></h3>
				<p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
				<a href="http://www.aromathailand.com/2015/index.php" target="_blank" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas fa-pencil-alt"></i>
				</div>
				<h3><a href="http://www.aromathailand.com/2015/index.php" target="_blank" class="tran3s">ข้อบังคับเกี่ยวกับการทำงาน</a></h3>
				<p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
				<a href="http://www.aromathailand.com/2015/index.php" target="_blank" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas fa-pencil-alt"></i>
				</div>
				<h3><a href="http://www.aromathailand.com/2015/index.php" target="_blank" class="tran3s">ระเบียบต่างๆ</a></h3>
				<p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
				<a href="http://www.aromathailand.com/2015/index.php" target="_blank" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas fa-pencil-alt"></i>
				</div>
				<h3><a href="http://www.aromathailand.com/2015/index.php" target="_blank" class="tran3s">คู่มือพนักงานใหม่</a></h3>
				<p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
				<a href="http://www.aromathailand.com/2015/index.php" target="_blank" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="clear"></div>
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas fa-pencil-alt"></i>
				</div>
				<h3><a href="http://www.aromathailand.com/2015/index.php" target="_blank" class="tran3s">กลุ่มระดับพนักงาน หน้าที่งาน ปรับตำแหน่ง</a></h3>
				<p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
				<a href="http://www.aromathailand.com/2015/index.php" target="_blank" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas fa-pencil-alt"></i>
				</div>
				<h3><a href="http://www.aromathailand.com/2015/index.php" target="_blank" class="tran3s">วันหยุดประจำปีบริษัท</a></h3>
				<p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
				<a href="http://www.aromathailand.com/2015/index.php" target="_blank" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas fa-pencil-alt"></i>
				</div>
				<h3><a href="HRPolicy_Contact.aspx" class="tran3s">ติดต่อ HR</a></h3>
				<p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
				<a href="http://www.aromathailand.com/2015/index.php" target="_blank" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas fa-pencil-alt"></i>
				</div>
				<h3><a href="HRPolicy_WhoisWho.aspx" class="tran3s">ติดต่อฝ่าย-แผนกต่างๆ</a></h3>
				<p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
				<a href="http://www.aromathailand.com/2015/index.php" target="_blank" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
	</div> <!-- /.row -->
</div> <!-- /.container -->
</section> <!-- /#about-us -->
    
<!-- Swiper JS -->
<script src="new/js/swiper.min.js"></script>

<!-- Initialize Swiper -->
<script>
	var swiper1 = new Swiper('.swiper1', {
		spaceBetween: 30,
		pagination: {
        el: '.swiper-pagination1',
        clickable: true,
      },
    });
</script>

</asp:Content>
