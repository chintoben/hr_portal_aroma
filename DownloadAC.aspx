﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="DownloadAC.aspx.vb" Inherits="Aroma_HRPortal.DownloadAC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 
<!-- banner -->
<div class="base-banner"><img src="new/images/banner/form/banner.jpg" alt=""/></div>
<!-- end banner -->

<article class="blog-details-page">
	<div class="container">
		
		<div class="post-heading">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
            </ul>
			<h2>แบบฟอร์ม บัญชี-การเงิน</h2>
		</div> <!-- /.post-heading -->
		
		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix" style="float: none; margin: 0 auto;">
				<div class="blog-details-post-wrapper">
				
					<img src="new/images/banner/form/img2.jpg" alt="Image">

					<p style="padding: 30px 0 0;">
						<ul class="p">
							<li><a href="Document/Download/AC/1.ใบเบิกเงินสำรองจ่าย.pdf" target="_blank">ใบเบิกเงินสำรองจ่าย</a></li>
                    		<li><a href="Document/Download/AC/2.ใบสำคัญจ่ายเงินสดย่อย KVN.pdf" target="_blank">ใบสำคัญจ่ายเงินสดย่อย KVN</a></li>
                    		<li><a href="Document/Download/AC/3.ใบสำคัญจ่ายเงินสดย่อย LION.pdf" target="_blank">ใบสำคัญจ่ายเงินสดย่อย LION</a></li>

                            <li><a href="Document/Download/AC/ใบรับรองแทนใบเสร็จรับเงิน11.6.2018.pdf" target="_blank">ใบรับรองแทนใบเสร็จรับเงิน KVN&LION เริ่ม มี.ค.61</a></li>
                            <li><a href="Document/Download/AC/ใบสำคัญรับเงิน.pdf" target="_blank">ใบสำคัญรับเงิน KVN&LION เริ่ม มี.ค.61</a></li>

                   			<%--<li><a href="Document/Download/AC/4.ใบรับเงิน (กรณีใบเสร็จไม่ถูกต้อง-ไม่มีใบเสร็จ).pdf" target="_blank">ใบรับเงิน (กรณีใบเสร็จไม่ถูกต้อง-ไม่มีใบเสร็จ)</a></li>--%>

						</ul>
					</p>
				
				</div>
			</div>
		</div>
		
	</div>
</article>

</asp:Content>
