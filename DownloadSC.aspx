﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="DownloadSC.aspx.vb" Inherits="Aroma_HRPortal.DownloadSC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 
<!-- banner -->
<div class="base-banner"><img src="new/images/banner/form/banner.jpg" alt=""/></div>
<!-- end banner -->

<article class="blog-details-page">
	<div class="container">
		
		<div class="post-heading">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
            </ul>
			<h2>แบบฟอร์ม จัดซื้อ</h2>
		</div> <!-- /.post-heading -->
		
		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix" style="float: none; margin: 0 auto;">
				<div class="blog-details-post-wrapper">
				
					<img src="new/images/banner/form/img3.jpg" alt="Image">

					<p style="padding: 30px 0 0;">
						<ul class="p">
							<li><a href="Document/Download/SC/1.ใบขอให้ดำเนินการ (กรณีต้องการให้จัดซื้อจัดหาสินค้าใหม่).pdf" target="_blank">ใบขอให้ดำเนินการ (กรณีต้องการให้จัดซื้อจัดหาสินค้าใหม่)</a></li>
                            <li><a href="Document/Download/SC/PR form.pdf" target="_blank">ใบขอซื้อ (PURCHASE REQUISTION)</a></li>

						</ul>
					</p>
				
				</div>
			</div>
		</div>
		
	</div>
</article>

</asp:Content>
