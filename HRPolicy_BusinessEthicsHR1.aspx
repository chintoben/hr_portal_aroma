﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_BusinessEthicsHR1.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_BusinessEthicsHR1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<style>
	.swiper-container {
		width: 100%;
		height: 100%;
	}
	.swiper-slide {
		text-align: center;
		background: #fff;
	}
	.swiper-slide img 
	{
		width: 100%;
	}
	.swiper-pagination-bullet-active {
		opacity: 1;
		background: #af2f2f;
	}
</style>

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/hr.jpg" alt=""/></div>
<!-- end banner -->

<!--
=====================================================
	Blog Page Details
=====================================================
-->
<article class="blog-details-page">
	<div class="container">
		
		<div class="post-heading">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
                <li>/</li>
                <li><a href="HRPolicy_Announce.aspx">ระเบียบต่างๆ</a></li>
                <li>/</li>
                <li><a href="HRPolicy_BusinessEthicsHR.aspx">ประกาศ คำสั่ง ระเบียบทรัพยากรมนุษย์</a></li></li>
            </ul>
			<h2>การคล้อง ติดบัตรพนักงาน</h2>
		</div> <!-- /.post-heading -->
		
		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix">
				<div class="blog-details-post-wrapper">
				
					<img src="new/images/banner/hr-policy/img5.jpg" alt=""/>
					
         		    <p style="padding: 30px 0 0;">
         		    	อ้างถึงประกาศเรื่อง การคล้อง/ติดบัตรประจำตัวพนักงาน ที่ H100/049/49 ลงวันที่ 7 กันยายน 2549 ทางกลุ่มบริษัท อโรม่า จึงเห็นสมควรออกประกาศฉบับใหม่ และใคร่ขอความร่วมมือ มายังพนักงานทุกท่านในการปฏิบัติอย่างเคร่งครัด เพื่อให้เกิดภาพลักษณ์ที่ดี รวมถึงการแสดงตนเพื่อความปลอดภัยในที่ทำงาน และความเป็นระเบียบเรียบร้อยในการปฏิบัติงาน รวมทั้งขอยกเลิกประกาศฉบับดังกล่าวข้างต้น แต่ให้ถือใช้ประกาศฉบับนี้แทน ดังนี้
         		    </p>
					<p>
						<ol style="margin: 10px 0;">
							<li>ให้พนักงานทุกท่าน คล้อง/ติดบัตรประจำตัวพนักงานตลอดเวลาปฏิบัติงาน และทุกวันที่ทำงาน</li>
							<li>บัตรพนักงาน และสายคล้องบัตร ซึ่งทางกลุ่มบริษัทฯ จัดทำและแจกให้เป็นครั้งแรก ทางบริษัทจะเป็นผู้ออกค่าใช้จ่ายให้ทั้งสิ้น แต่พนักงานจะต้องจ่ายเงินประกันค่าบัตรและสายคล้องเป็นจำนวนเงิน 
							 100 บาท โดยจะได้รับคืนเมื่อพ้นสภาพการเป็นพนักงาน และได้ส่งคืนบัตรพนักงาน พร้อมสายคล้องบัตร ให้กับฝ่ายทรัพยากรมนุษย์ครบถ้วนแล้ว</li>
							<li>กรณีบัตรพนักงานสูญหาย ชำรุด ทางพนักงานจะต้องเป็นผู้ออกค่าใช้จ่ายเองทั้งสิ้น (ทั้งบัตรแบบพลาสติกและแบบกระดาษ หรือสายคล้องบัตร มีค่าใช้จ่ายคิดเป็นจำนวนเงินอย่างละ 50 บาท 
							 ซึ่งค่าใช้จ่ายอาจเปลี่ยนแปลงได้โดยอยู่ในดุลยพินิจของบริษัท)</li>
							<li>กรณีกลุ่มบริษัทฯ มีนโยบายเปลี่ยนบัตร/สายคล้องแบบใหม่ พนักงานจะต้องติดต่อเปลี่ยนและส่งคืนแก่บริษัทในทันทีที่ได้รับแจ้ง กรณีหากพนักงานท่านใดมิได้ส่งคืน/เปลี่ยน พนักงานผู้นั้นๆ จะต้องถูกปรับ
							 เป็นจำนวนเงินตามข้อ 3.ตามแต่กรณี</li>
							<li>กรณีตัวแทนบริษัทฯ (กรรมการบริหาร, ผู้บังคับบัญชาต้นสังกัด, ฝ่ายทรัพยากรมนุษย์) พบว่าพนักงานท่านใดเพิกเฉย ละเลย จงใจฝ่าฝืนในการปฏิบัติ พนักงานท่านนั้นๆ อาจถูกดำเนินการทางวินัย</li>
						</ol>
					</p>
					
					<h4>ตามระเบียบของบริษัทฯ รวมทั้งจะมีผลต่อการพิจารณาปรับเงินเดือน และโบนัสประจำปีนั้นๆ ดังนี้</h4>
					<h4>กรณีลืมคล้อง/ติดบัตร หรือลืมบัตรไว้ที่บ้านและที่อื่นๆ</h4>
					<p>
						<ol>
							<li>กรณีที่ 1 ลืม 1 ครั้ง/เดือน จะถูกปรับเป็นเงินครั้งละ 50 บาท/วัน (หักผ่านเงินเดือน) และฝ่ายทรัพยากรมนุษย์จะออกบัตรพนักงานชั่วคราวให้คล้องชั่วคราว และมีแบบฟอร์มลงชื่อรับทราบ</li>
							<li>กรณีที่ 2 ลืม 2-4 ครั้ง/เดือน จะถูกปรับเป็นเงินครั้งละ 50 บาท/วัน (หักผ่านเงินเดือน) และฝ่ายทรัพยากรมนุษย์จะออกบัตรพนักงานชั่วคราวให้คล้องชั่วคราว กรณีลืมครบ 4 ครั้ง/เดือน
							 จะถูกเตือนด้วยวาจา (บันทึกเป็นหนังสือ)</li>
							<li>กรณีที่ 3 ลืม 5–7 ครั้ง/เดือน จะถูกปรับเป็นเงินครั้งละ 50 บาท/วัน (หักผ่านเงินเดือน) และฝ่ายทรัพยากรมนุษย์จะออกบัตรพนักงานชั่วคราวให้คล้องชั่วคราว กรณีลืมครบ 7 ครั้ง/เดือน 
							 จะถูกเตือนเป็นหนังสือ</li>
							<li>กรณีที่ 4 ลืม 8 ครั้ง/เดือนเป็นต้นไป จะถูกปรับเป็นเงินครั้งละ 100 บาท/วัน (หักผ่านเงินเดือน) และถูกเตือนเป็นหนังสือ</li>
						</ol>
					</p>
					
					<h4>กรณีเพิกเฉย จงใจฝ่าฝืนการปฏิบัติภายในปีนั้นๆ</h4>
					<p>
						<ol>
							<li>ครั้งที่ 1 จะถูกปรับเป็นเงิน 100 บาท/วัน (หักผ่านเงินเดือน) และถูกเตือนด้วยวาจา (บันทึกเป็นหนังสือ)</li>
							<li>ครั้งที่ 2 จะถูกปรับเป็นเงิน 100 บาท/วัน (หักผ่านเงินเดือน) และถูกเตือนเป็นหนังสือ</li>
							<li>ครั้งที่ 3 เป็นต้นไป จะถูกปรับเป็นเงิน 100 บาท/วัน (หักผ่านเงินเดือน) และถูกเตือนเป็นหนังสือ รวมทั้งถูกปรับลดคะแนนประเมินและอัตราพิจารณาปรับเงินเดือนและโบนัสประจำปีลง 25%
							 จากอัตราที่ได้ในปีนั้นๆ</li>
						</ol>
					</p>
					<p><br>
						จึงประกาศมาเพื่อทราบโดยทั่วกัน และปฏิบัติอย่างเคร่งครัด ตั้งแต่วันที่ 15 พฤษภาคม 2551 เป็นต้นไป<br><br>
						ประกาศ ณ วันที่ 9 พฤษภาคม 2551	
					</p>
					
					<div class="blog-details-bt">
						<div class="btn-group">
							<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
						</div>
					</div>
				
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-8 col-xs-12 p-fix">
				<aside>
					<div class="sidebar-news-list">
						<h4>CATEGORY</h4>
						<ul>
							<li>
								<a href="HRPolicy_BusinessEthicsHR.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบทรัพยากรมนุษย์</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAS.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบเกี่ยวกับทรัพย์สิน</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAC.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบเกี่ยวกับบัญชี การเงิน งบประมาณ จัดซื้อ</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsIT.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบเกี่ยวกับเทคโนโลยีสารสนเทศ</a>
							</li>
							<li>
								<a href="Benefits1_1.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบอื่นๆ</a>
							</li>
						</ul>
					</div>
				</aside>
				<aside>
					<div class="sidebar-news-list">
						<h4>Related Links</h4>
						<ul>
							<li>
								<a href="HRPolicy_BusinessEthicsHR1.aspx" class="tran3s">
									การคล้อง ติดบัตรพนักงาน
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR2.aspx" class="tran3s">
									การลาพักผ่อนประจำปี
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR3.aspx" class="tran3s">
									การแต่งกายพนักงานขาย (พีซี)
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR4.aspx" class="tran3s">
									การลงนามอนุมัติเกินอำนาจดำเนินการ หรือความเสียหายจากการปฏิบัติหน้าที่
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR5.aspx" class="tran3s">
									ระเบียบบ้านพักพนักงาน
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR6.aspx" class="tran3s">
									ระเบียบการลงโทษพนักงานทุจริต
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR7.aspx" class="tran3s">
									ทดลองจัดวันเวลาทำงานและวันหยุด (เป็นกรณีพิเศษ) ทำงานวันจันทร์ - ศุกร์
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR8.aspx" class="tran3s">
									ระเบียบการแต่งกายพนักงานประจำสำนักงาน
								</a>
							</li>
						</ul>
					</div>
				</aside>
			</div>
		</div>
		
	</div>
</article>
    
<!-- Swiper JS -->
<script src="new/js/swiper.min.js"></script>

<!-- Initialize Swiper -->
<script>
	var swiper1 = new Swiper('.swiper1', {
		spaceBetween: 30,
		pagination: {
        el: '.swiper-pagination1',
        clickable: true,
      },
    });
</script>

</asp:Content>
