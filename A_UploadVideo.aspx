﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="A_UploadVideo.aspx.vb" Inherits="Aroma_HRPortal.A_UploadVideo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">

<%--    https://www.w3schools.com/tags/tryit.asp?filename=tryhtml5_video--%>

  
  <video width="320" height="240" controls>
  <source src="video\Aroma Group VDO TH Version - 09082017.mp4" type="video/mp4">
<%--  <source src="video\Aroma Group VDO ENG Version - 09082017.mp4" type="video/ogg">--%>
  Your browser does not support the video tag.
</video>

  <video width="400" height="300" controls>
<%--  <source src="video\Aroma Group VDO TH Version - 09082017.mp4" type="video/mp4">--%>
  <source src="video\Aroma Group VDO ENG Version - 09082017.mp4" type="video/ogg">
  Your browser does not support the video tag.
</video>

<p><strong>Note:</strong> The video tag is not supported in Internet Explorer 8 and earlier versions.</p>

    </form>
</body>
</html>
