﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="Benefits8_2.aspx.vb" Inherits="Aroma_HRPortal.Benefits8_2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/benefit/banner1.jpg" /></div>
<!-- end banner -->

<!--
=====================================================
	Blog Page Details
=====================================================
-->
<article class="blog-details-page">
	<div class="container">
		
		<div class="post-heading">
            <ul>
			    <li><a href="index_hr.aspx">HOME</a></li>
			    <li>/</li>
			    <li><a href="Benefits_Employee.aspx">ระเบียบสวัสดิการ</a></li>
                <li>/</li>
                <li><a href="Benefits8.aspx">เงินกู้</a></li>
			</ul>
			<h2>สินเชื่อสวัสดิการพนักงาน (MOU ธนาคารกรุงศรีอยุธยา) (1)</h2>
		</div> <!-- /.post-heading -->
		
		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix">
				<div class="blog-details-post-wrapper">
				
					<img src="new/images/banner/benefit/img10.jpg" alt="Image">

					<h4>เกณฑ์พิจารณา</h4>
					<p>
						<ol>
							<li>ไม่ต้องมีหลักทรัพย์ หรือผู้ค้ำประกัน</li>
							<li>อนุมัติสูงสุด 5 เท่าของรายได้ วงเงินอนุมัติ 30,000 - 1,000,000 บาท</li>
							<li>ระยะเวลาการกู้ 12 - 60 เดือน</li>
							<li>อัตราดอกเบี้ยแบบลดต้นลดดอก MLR + 6% ต่อปี</li>
							<li>ค่าธรรมเนียมการยื่นกู้และการจัดทำนิติกรรมสัญญา 1,000 บาท</li>
						</ol>
					</p>
					
					<h4>คุณสมบัติผู้สมัคร</h4>
					<p>
						<ol>
							<li>พนักงานประจำของบริษัทที่เข้าร่วมโครงการ</li>
							<li>อายุผู้กู้ ุ 20-60 ปี (อายุผู้กู้รวมระยะเวลาผ่อนชำระต้องไม่เกิน 60 ปี)</li>
							<li>อายุงาน ต้องผ่านการทดลองงาน โดยอายุงานปัจจุบันไม่ต่ำกว่า 1 ปี และอายุงานรวมต้องไม่ต่ำกว่า 2 ปี</li>
							<li>รายได้ขั้นต้น 8,000 บาท และมีเงินเดือนพื้นฐาน 6,000 บาท</li>
						</ol>
					</p>
					
					<h4>การชำระคืน</h4>
					<p>
						หน่วยงานต้นสังกัดของผู้กู้ หักเงินเดือนผู้กู้นำส่งธนาคาร/ หักบัญชีอัตโนมัติกรณีที่ใช้บริการจ่ายเงินเดือนกับธนาคาร
					</p>
					
					<h4>เอกสารประกอบการสมัคร</h4>
					<p>
						<ol>
							<li>สำเนาบัตรประชาชน</li>
							<li>สำเนาทะเบียนบ้าน</li>
							<li>สลิปเงินเดือนฉบับล่าสุด/หนังสือรับรองเงินเดือนที่มี ระบุเงินเดือน ตำแหน่ง และอายุงาน</li>
							<li>สำเนาบัญชีเงินฝากที่มีเงินเดือนเข้าย้อนหลัง 3 เดือน</li>
							<li>เพิ่มเอกสารประกอบที่สำคัญอื่นๆ ถ้ามี เช่น ใบทะเบียนสมรส/หย่า, ใบเปลี่ยนชื่อ/ นามสกุล เป็นต้น</li>
						</ol>
					</p>
					
					<h4>ขั้นตอนการดำเนินการ</h4>
					<p>
						<ol>
							<li>พนักงานที่สนใจ ติดต่อขอใบสมัครสินเชื่อได้ี่ที่ฝ่ายทรัพยากรมนุษย์้ทุกวันที่ทำการ</li>
							<li>ส่งใบสมัคร(เขียนรายละเอียดให้ครบถ้วน) พร้อมแนบเอกสารการสมัครที่ฝ่ายทรัพยากรมนุษย์ (โปรดเตรียมเอกสารให้พร้อมด้วยตนเอง)</li>
							<li>ฝ่ายบทรัพยากรมนุษย์รับรองสถานะการเป็นพนักงานในใบสมัคร และส่งธนาคารทุกๆ 2 อาทิตย์</li>
							<li>ผู้ขอสินเชื่อทราบผลการพิจารณาภายใน 7 วันทำการ นับจากวันที่ธนาคารได้รับเอกสาร</li>
							<li>ธนาคารขอสงวนสิทธิ์ในการอนุมัติให้เงินกู้ตามความเหมาะสม</li>
						</ol>
					</p>
					
					<div class="blog-details-bt">
						<div class="btn-group">
							<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
						</div>
					</div>
				
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-8 col-xs-12 p-fix">
				<aside>
					<div class="sidebar-news-list">
						<h4>CATEGORY</h4>
						<ul>
							<li>
								<a href="Benefits1.aspx" class="tran3s">กองทุนสำรองเลี้ยงชีพ มอบเกียรติบัตรเงินรางวัล</a>
							</li>
							<li>
								<a href="Benefits2_1.aspx" class="tran3s">เงินช่วยเหลืองาน และโอกาสพิเศษต่างๆ</a>
							</li>
							<li>
								<a href="Benefits15.aspx" class="tran3s">เจ็บป่วย ประกันชีวิต สุขภาพ อุบัติเหตุ</a>
							</li>
							<li>
								<a href="Benefits5_1.aspx" class="tran3s">ปฏิบัติงานนอกสถานที่ (ในประเทศ)</a>
							</li>
							<li>
								<a href="Benefits3_1.aspx" class="tran3s">ปฏิบัติงานนอกสถานที่ (ต่างประเทศ)</a>
							</li>
							<li>
								<a href="Benefits4_1.aspx" class="tran3s">ปฏิบัติงาน EXHIBITION - EVENT (ในประเทศ)</a>
							</li>
							<li>
								<a href="Benefits7_1.aspx" class="tran3s">ค่าเลี้ยงรับรอง</a>
							</li>
							<li>
								<a href="Benefits8.aspx" class="tran3s">เงินกู้</a>
							</li>
							<li>
								<a href="Benefits9_1.aspx" class="tran3s">ส่วนลดสินค้า สำหรับพนักงาน</a>
							</li>
							<li>
								<a href="Benefits10_1.aspx" class="tran3s">เบี้ยขยัน</a>
							</li>
							<li>
								<a href="Benefits11_1.aspx" class="tran3s">ตรวจสุขภาพ</a>
							</li>
							<li>
								<a href="#" class="tran3s">สวัสดิการอื่นๆ</a>
							</li>
						</ul>
					</div>
				</aside>
				<aside>
					<div class="sidebar-news-list">
						<h4>Related Links</h4>
						<ul>
							<li>
								<a href="Benefits8_1.aspx" class="tran3s">
									สินเชื่อสวัสดิการพนักงาน (MOU ธนาคารธนชาต)
								</a>
							</li>
							<li>
								<a href="Benefits8_2.aspx" class="tran3s">
									สินเชื่อสวัสดิการพนักงาน (MOU ธนาคารกรุงศรีอยุธยา) (1)
								</a>
							</li>
							<li>
								<a href="Benefits8_3.aspx" class="tran3s">
									สินเชื่อสวัสดิการพนักงาน (MOU ธนาคารออมสิน)
								</a>
							</li>
							<li>
								<a href="Benefits8_4.aspx" class="tran3s">
									สวัสดิการเงินกู้บริษัท
								</a>
							</li>
						</ul>
					</div>
				</aside>
				<aside>
					<div class="sidebar-news-list">
						<h4>PDF Links</h4>
						<ul class="p">
							<li>
								<a href="Document/4Benefits/ขั้นตอนการดำเนินการกู้สินเชื่อ ธ.ออมสิน.pdf" class="tran3s">
								ขั้นตอนการดำเนินการกู้สินเชื่อ ธ.ออมสิน</a>
							</li>
							<li>
								<a href="Document/4Benefits/สินเชื่อสวัสดิการพนักงานร่วมกับสถาบันการเงิน.pdf" class="tran3s">
								สินเชื่อสวัสดิการพนักงานร่วมกับสถาบันการเงิน</a>
							</li>
						</ul>
					</div>
				</aside>
			</div>
		</div>
		
	</div>
</article>

</asp:Content>
