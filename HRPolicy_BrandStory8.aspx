﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_BrandStory8.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_BrandStory8" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<br />
<br />
<br />
<br />
<br />

  <div class="btn-group">
                                 <asp:button id="backButton" runat="server" text="Back" class="btn btn-primary"
OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>

   </div>

     <div class="container">
        <center>
        <h3>
           CSR
        </h3>
        </center>
       
        <div class="border">
        </div>
        <div class="row">

            <pre>
            <font size = "3" >

        อโรม่า กรุ๊ป มีความมุ่งมั่นที่จะเป็นผู้นำในธุรกิจกาแฟไทยอย่างยั่งยืน ด้วยการพัฒนาธุรกิจให้เติบโตในทุกมิติ ครอบคลุมด้านสังคม สิ่งแวดล้อม อาชีวอนามัย 
    และความปลอดภัย โดยการบูรณาการแนวคิด Corporate Social Responsibility (CSR)  ให้เป็นส่วนหนึ่งในนโยบายการทำงาน ซึ่งผู้บริหาร 
    และพนักงานทุกคนต่างมีส่วนร่วมสนับสนุนผลักดัน และปฏิบัติงานให้สอดคล้องกับนโยบาย ดังต่อไปนี้ 
 
    จรรยาบรรณในการดำเนินธุรกิจ 
        บริษัทได้สานต่อเจตนารมณ์ของผู้ก่อตั้ง “คุณประยุทธ วงศ์วารี” ที่ส่งต่อวิสัยทัศน์อันเป็นปัจจัยแห่งความสำเร็จ นั่นคือ ความซื่อสัตย์ ไม่เอาเปรียบ ไม่หาประโยชน์ให้ตน 
    และมีความโปร่งใสในการปฏิบัติต่อลูกค้า ผู้บริโภค คู่แข่งทางการค้า และพันธมิตรทางธุรกิจ  รวมทั้งส่งเสริมเกษตรกรไทยผู้ปลูกกาแฟด้วยการรับซื้อกาแฟในราคาที่เป็นธรรม   
 
    สิทธิมนุษยชน 
        บริษัทยึดปฏิบัติตามหลักการด้านสิทธิมนุษยชนที่ระบุไว้ตามกฎหมาย รวมทั้งที่เป็นมาตรฐานในระดับสากล เช่น United Nations Universal 
    Declaration of Human Rights: UNUDHR) และ UN Framework and Guiding Principles on Business and Human Rights (Ruggie Framework) 
 
    สิ่งแวดล้อม 
        บริษัทมีการกำหนด และวางแผนนโยบายด้านสิ่งแวดล้อมอย่างเป็นระบบ และได้รับมาตรฐานรับรอง ISO 14001 ซึ่งเป็นมาตรฐานรับรองระบบการจัดการสิ่งแวดล้อม 
    ISO 22000 มาตรฐานระดับสากลที่เกี่ยวข้องกับระบบการบริหารจัดการด้านความปลอดภัยของอาหาร เทียบเท่ากับ ISO 9000 + HACCP และมาตรฐานรับรอง GMP 
    จากสำนักงานคณะกรรมการอาหารและยาในระดับดีมาก 
 
    สังคม 
        การทำประโยชน์ให้แก่สังคม เปรียบเสมือนหัวใจในการดำเนินธุรกิจขององค์กร  ตลอดมาบริษัทได้ให้การสนับสนุน และจัดกิจกรรมต่างๆ เพื่อส่งเสริมการบำเพ็ญ
    สาธารณะประโยชน์ และการแบ่งปันให้กับสังคม ชุมชน และเยาวชน โดยได้รับความร่วมมือจากองค์กรที่ไม่แสวงผลกำไร ทั้งนี้บริษัทยังให้การสนับสนุนบุคลากร 
    และพันธมิตรทางธุรกิจในการเป็นจิตอาสา สร้างสรรค์กิจกรรมเพื่อการแบ่งปันภายใต้ชื่อโครงการ “ Aroma Happiness Sharing Project” พร้อมด้วยความตั้งใจ
    ที่จะส่งต่อแรงบันดาลใจต่อไปไม่สิ้นสุด
 





					
            </font>
            </pre>

        </div>
    </div>

</asp:Content>
