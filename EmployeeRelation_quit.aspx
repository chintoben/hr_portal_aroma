﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="EmployeeRelation_quit.aspx.vb" Inherits="Aroma_HRPortal.EmployeeRelation_quit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<!-- banner -->
<div class="base-banner">
<img src="new/images/banner/Activities.jpg" 
        alt="" />
</div>
<!-- end banner -->

<section id="about-us" class="blog-category">
<div class="container">
	<div class="theme-title tt-edit category-title">
        <ul>
            <li><a href="index_hr.aspx">HOME</a></li>
        </ul>
		<h2>พนักงานลาออก</h2>
		<!--p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p-->
	</div> <!-- /.theme-title -->

        <div class="row">

		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-046-megaphone"></i>
				</div>
				<h3><a href="Benefits1_1.aspx" class="tran3s">ประกาศพนักงานลาออก (ทุกบริษัท) ประจำเดือน พฤษภาคม-มิถุนายน 2563</a></h3>
				<!--p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p-->
				<a href="Document/3EmployeeLevel/ประกาศพนักงานลาออก (ทุกบริษัท) ประจำเดือนมิถุนายน 2563.pdf" target="_blank" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-046-megaphone"></i>
				</div>
				<h3><a href="Benefits1_1.aspx" class="tran3s">ประกาศพนักงานลาออก (ทุกบริษัท) ประจำเดือน เมษายน 2563</a></h3>
				<!--p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p-->
				<a href="Document/3EmployeeLevel/ประกาศพนักงานลาออก (ทุกบริษัท) ประจำเดือนเมษายน 2563.pdf" target="_blank" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
        <div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-046-megaphone"></i>
				</div>
				<h3><a href="Benefits1_1.aspx" class="tran3s">ประกาศพนักงานลาออก (ทุกบริษัท) ประจำเดือน มีนาคม 2563</a></h3>
				<!--p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p-->
				<a href="Document/3EmployeeLevel/ประกาศพนักงานลาออก (ทุกบริษัท) ประจำเดือน มีนาคม 2563.pdf" target="_blank" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->

        
	</div> <!-- /.row -->
	</div> <!-- /.container -->
</section>
  
<!-- Swiper JS -->
<script src="new/js/swiper.min.js"></script>

<!-- Initialize Swiper -->
<script>
	var swiper1 = new Swiper('.swiper1', {
		spaceBetween: 30,
		pagination: {
        el: '.swiper-pagination1',
        clickable: true,
      },
    });
</script>

</asp:Content>