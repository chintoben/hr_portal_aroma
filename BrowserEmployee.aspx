﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BrowserEmployee.aspx.vb" Inherits="Aroma_HRPortal.BrowserEmployee" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BrowseDataProvince</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	    <style type="text/css">
            .style1
            {
                height: 344px;
            }
            .style2
            {
                width: 953px;
            }
        </style>
</head>
<body>
    <form method="post" runat="server" ID="Form1" class="style2">
			<TABLE style="WIDTH: 560px; HEIGHT: 56px">
				<tr>
					<td style="WIDTH: 112px; HEIGHT: 17px"><asp:imagebutton id="SerachB" Runat="server" 
                            ImageUrl="~/Images/Search.gif"></asp:imagebutton></td>
					<td style="WIDTH: 112px; HEIGHT: 17px"><asp:imagebutton id="ClearB" Runat="server" 
                            ImageUrl="~/Images/Reset.gif"></asp:imagebutton></td>
					<td style="WIDTH: 52px; HEIGHT: 17px"></td>
					<td style="HEIGHT: 18px"><FONT face="Tahoma"></FONT></td>
				</tr>
				<tr>
					<td style="WIDTH: 112px"><asp:label id="label1" Runat="server" Font-Size="XX-Small" Width="104px" Font-Bold="True">Code</asp:label></td>
					<td style="WIDTH: 112px"><asp:textbox id="txtCode" Runat="server" Width="112px" OnTextChanged="txtCode_TextChanged"></asp:textbox></td>
					<td style="WIDTH: 52px"><asp:label id="Label2" Runat="server" Font-Size="XX-Small" Font-Bold="True">Name</asp:label></td>
					<td><asp:textbox id="TxtName" Runat="server" Width="256px" OnTextChanged="txtName_TextChanged"></asp:textbox></td>
				</tr>
			</TABLE>
			<table class="style1">
				<tr>
					<td>
					
					 <asp:datagrid id="Grid1" Runat="server" Width="560px" DataKeyField="code" OnPageIndexChanged="Grid_paging"
							AllowPaging="True" PageSize="20" AutoGenerateColumns="False" OnItemCommand="Sec_Index" 
                            Height="16px">
							<PagerStyle Mode="NumericPages"></PagerStyle>
							<AlternatingItemStyle BackColor="#C0FFFF"></AlternatingItemStyle>
							<ItemStyle Font-Size="XX-Small" Font-Bold="True" BackColor="PowderBlue"></ItemStyle>
							<HeaderStyle Font-Size="X-Small" Font-Bold="True" BackColor="#E0E0E0"></HeaderStyle>
							<Columns>
								<asp:ButtonColumn HeaderText="code" CommandName="Click_Code" DataTextField="code"></asp:ButtonColumn>
								<asp:BoundColumn DataField="description" HeaderText="description"></asp:BoundColumn>
							</Columns>
						</asp:datagrid>
						
				
						
						</td>
				</tr>
			</table>
			<input id="control" type="hidden" name="control" runat="server">
		</form>
</body>
</html>
