﻿Imports System.Data.OleDb
Imports System.IO
Imports System.Data.SqlClient

Public Class A_EmployeeData
    Inherits System.Web.UI.Page
    Dim dtt As New DataTable
    Private sms As New PKMsg("")
    Dim sqlCheck, sql As String
    Dim DB_HR As New Connect_HR
    Dim db_Leave As New Connect_Leave
    Dim dt, dt2 As New DataTable
    Dim Cmd As OleDbCommand
    Dim Dr As OleDbDataReader
    Dim mode As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        mode = Request.QueryString("mode")
        ID = Request.QueryString("id")

        If Not IsPostBack Then

            If mode = "new" Then

            Else
                LoadData()
            End If
        End If

    End Sub


    Sub LoadData()

        sql = " select emp.site, emp.workplace_id,"
        sql += " emp.Employee_ID, isnull(emp.NameTitle,'') as NameTitle ,isnull(emp.fullname,'') as fullname ,isnull(emp.[Employee_Name],'') as Employee_Name,isnull(emp.[Employee_LastName],'') as Employee_LastName,isnull(emp.[Employee_Nickname],'') as Employee_Nickname"
        sql += " , isnull(emp.branch,'') as branch "
        'sql += " , isnull(emp.departments_id,'')+' : '+isnull(deps.departments_name ,'') as departments ,  isnull(emp.department_id,'')+' : '+isnull(dep.department_name ,'') as department"
        sql += " , isnull(emp.departments_id,'') as departments ,  isnull(emp.department_id,'') as department"
        sql += " , isnull([EmpLevel],'') as EmpLevel,isnull([position_id],'') as position_id, isnull([position],'') as position , isnull([SEX],'Male') as SEX "
        sql += " , isnull([EmpLevel_ApproveLeave],'') as EmpLevel_ApproveLeave,isnull([position_id_ApproveLeave],'') as position_id_ApproveLeave "

        sql += " , convert(varchar,isnull(emp.birthday,'01/01/1900'),103) as birthday  "
        sql += " , isnull([adr],'') as adr, isnull([Tel],'') as Tel, isnull([Ext],'') as Ext , isnull([Email],'') as Email, isnull(Manager_id,'') as Manager_id , isnull(Manager_id_pms,'') as Manager_id_pms "
        sql += " , convert(varchar,isnull(emp.StartDate,'01/01/1900'),103) as StartDate , convert(varchar,isnull(emp.EndDate,'01/01/1900'),103) as EndDate  "
        sql += " , emp.image , isnull(emp.Status,'InActive') as Status , isnull(emp.LeaveLock,'check') as LeaveLock ,isnull(ID_Card,'') as ID_Card "
        sql += " , isnull(emp.CheckB2,'') as CheckB2 ,isnull([Employee_NameEN],'') as Employee_NameEN,isnull([Employee_LastNameEN],'') as Employee_LastNameEN,isnull([positionEN],'') as positionEN,isnull([fullnameEN],'') as fullnameEN"
        sql += " , isnull(pms,'No') as pms "

        sql += " from [TB_Employee] emp "
        sql += "  left outer join [TB_departments] deps on deps.departments_id = emp.departments_id   "
        sql += "  left outer join [TB_department] dep on dep.department_id = emp.department_id "
        sql += " where emp.[Employee_ID] = '" & ID & "' "

        dt = DB_HR.GetDataTable(sql)

        If dt.Rows.Count > 0 Then
            drpSite.Text = dt.Rows(0)("site")
            drpWorkplace.Text = dt.Rows(0)("workplace_id")
            txtID.Text = dt.Rows(0)("Employee_ID")
            txtTitle.Text = dt.Rows(0)("NameTitle")
            txtFullName.Text = dt.Rows(0)("fullname")
            drpBranch.Text = dt.Rows(0)("branch")
            drpDepartments.SelectedValue = dt.Rows(0)("departments")
            drpDepartment.SelectedValue = dt.Rows(0)("department")
            txtEmpLevel.Text = dt.Rows(0)("EmpLevel")
            txtPositionID.Text = dt.Rows(0)("position_id")
            txtEmpLevel_Leave.Text = dt.Rows(0)("EmpLevel_ApproveLeave")
            txtPositionID_Leave.Text = dt.Rows(0)("position_id_ApproveLeave")
            txtPosition.Text = dt.Rows(0)("position")
            txtAddress.Text = dt.Rows(0)("adr")
            txtEmail.Text = dt.Rows(0)("Email")
            txtTel.Text = dt.Rows(0)("Tel")
            txtExt.Text = dt.Rows(0)("Ext")
            txtManagerid.Text = dt.Rows(0)("Manager_id")
            txtManagerid_pms.Text = dt.Rows(0)("Manager_id_pms")
            txtName.Text = dt.Rows(0)("Employee_Name")
            txtSurName.Text = dt.Rows(0)("Employee_LastName")
            txtNickName.Text = dt.Rows(0)("Employee_Nickname")
            txtIDcard.Text = dt.Rows(0)("ID_Card")

            txtNameEN.Text = dt.Rows(0)("Employee_NameEN")
            txtSurNameEN.Text = dt.Rows(0)("Employee_LastNameEN")
            txtPositionEN.Text = dt.Rows(0)("positionEN")
            txtFullNameEN.Text = dt.Rows(0)("fullnameEN")

            If dt.Rows(0)("birthday") = "01/01/1900' Then" Then
                txtBirthday.Text = ""
            Else
                txtBirthday.Text = dt.Rows(0)("birthday")
            End If

            If dt.Rows(0)("StartDate") = "01/01/1900' Then" Then
                txtsDate.Text = ""
            Else
                txtsDate.Text = dt.Rows(0)("StartDate")
            End If

            If dt.Rows(0)("EndDate") = "01/01/1900" Then
                txteDate.Text = ""
            Else
                txteDate.Text = dt.Rows(0)("EndDate")
            End If

            If dt.Rows(0)("Status") = "Active" Then
                rdoStatus.Items.FindByValue("Active").Selected = True
            Else
                rdoStatus.Items.FindByValue("InActive").Selected = True
            End If


            If dt.Rows(0)("LeaveLock") = "check" Then
                rdoLeaveCheck.Items.FindByValue("check").Selected = True
            Else
                rdoLeaveCheck.Items.FindByValue("unchecked").Selected = True
            End If

            If dt.Rows(0)("CheckB2") = "check" Then
                rdoCheckB2.Items.FindByValue("check").Selected = True
            Else
                rdoCheckB2.Items.FindByValue("unchecked").Selected = True
            End If

            If dt.Rows(0)("Sex") = "Male" Then
                rdoSex.Items.FindByValue("Male").Selected = True
            Else
                rdoSex.Items.FindByValue("Female").Selected = True
            End If

            If dt.Rows(0)("pms") = "Yes" Then
                rdoPMS.Items.FindByValue("Yes").Selected = True
            Else
                rdoPMS.Items.FindByValue("No").Selected = True
            End If

            If Not IsDBNull(dt.Rows(0)("image")) Then
                img.ImageUrl = ReadImage(dt.Rows(0)("Employee_ID"))
            End If

        End If

    End Sub

    Function ReadImage(ByVal strID As String)
        Return ("Readimage.aspx?ImageID=" & strID)
    End Function

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        If mode = "new" Then
            Call insert()
            'Call insertHost()
            'If FileUpload1.FileName <> "" Then
            '    Call insertIMG()
            '    insertLogo()  ' LOGO
            'End If

            'Call LoadData()
            sms.Msg = "บันทึกข้อมูลเรียบร้อยแล้วค่ะ"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        Else
            'If FileUpload1.FileName <> "" Then
            '    Call insertIMG()
            '    insertLogo()  ' LOGO

            '    Call insertIMG_2()
            '    insertLogo_2()  ' LOGO

            'End If
            'น้ำปิด 04/08/2021

            Call Update()
            Call UpdateHost()

            'Call LoadData()
            sms.Msg = "บันทึกข้อมูลเรียบร้อยแล้วค่ะ"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        End If

    End Sub

    Sub insertIMG()
        Dim CurrentFileName As String
        'CurrentFileName = FileUpload1.FileName

        'If (Path.GetExtension(CurrentFileName).ToLower <> ".jpg") Then
        '    sms.Msg = "You choose the file dishonestly !!! , Please choose be file .jpg"
        '    Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        '    Exit Sub
        'End If

        'If FileUpload1.PostedFile.ContentLength > 131072 Then
        '    sms.Msg = "The size of big too file , the size of the file must 128 KB not exceed!!!"
        '    Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        '    Exit Sub
        'End If

        Dim CurrentPath As String = Server.MapPath("~/Temp/")

        'FileUpload1.PostedFile.SaveAs(CurrentPath & FileUpload1.FileName)
        'FileUpload1.PostedFile.SaveAs("c:\Temp\" & FileUpload1.FileName)
        'img.ImageUrl = CurrentPath & FileUpload1.FileName
        'txtPic.Text = "c:\Temp\" & FileUpload1.FileName

    End Sub

    Sub insertLogo()
        Dim db As New Connect_HR
        Dim sql As String
        Dim tr As SqlTransaction
        Dim sqlcon As New SqlConnection(DB_HR.sqlCon)

        If sqlcon.State = ConnectionState.Closed Then
            sqlcon = New SqlConnection(DB_HR.sqlCon)
            sqlcon.Open()
        End If

        tr = sqlcon.BeginTransaction

        Try
            Dim cmd As SqlCommand = New SqlCommand(sql, sqlcon, tr)
            If txtPic.Text <> "" Then
                Dim FileN As FileStream
                Dim Ln As Int32
                Dim oBinaryReader As BinaryReader
                Dim oImgByteArray As Byte()

                FileN = New FileStream(txtPic.Text, FileMode.Open, FileAccess.Read)
                oBinaryReader = New BinaryReader(FileN)
                oImgByteArray = oBinaryReader.ReadBytes(CInt(FileN.Length))
                Ln = CInt(FileN.Length)
                oBinaryReader.Close()
                FileN.Close()

                sql = "update [TB_Employee] set image_name=@FF1,image=@FF2 where employee_id  = '" & txtID.Text & "'"
                Dim cmd3 As SqlCommand = New SqlCommand(sql, sqlcon, tr)
                cmd3.Parameters.Add("@FF1", SqlDbType.VarChar, 0).Value = txtID.Text + Path.GetExtension(txtPic.Text).ToLower
                cmd3.Parameters.Add("@FF2", SqlDbType.Image, 0).Value = oImgByteArray
                cmd3.CommandTimeout = 0
                cmd3.ExecuteNonQuery()
            End If

            tr.Commit()
            sqlcon.Close()
        Catch ex As Exception
            tr.Rollback()
            sqlcon.Close()
            Exit Sub
        End Try

    End Sub


    Sub insertIMG_2()
        Dim CurrentFileName As String
        'CurrentFileName = FileUpload1.FileName

        'If (Path.GetExtension(CurrentFileName).ToLower <> ".jpg") Then
        '    sms.Msg = "You choose the file dishonestly !!! , Please choose be file .jpg"
        '    Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        '    Exit Sub
        'End If

        'If FileUpload1.PostedFile.ContentLength > 131072 Then
        '    sms.Msg = "The size of big too file , the size of the file must 128 KB not exceed!!!"
        '    Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        '    Exit Sub
        'End If

        Dim CurrentPath As String = Server.MapPath("~/Temp/")

        'FileUpload1.PostedFile.SaveAs(CurrentPath & FileUpload1.FileName)
        'FileUpload1.PostedFile.SaveAs("c:\Temp\" & FileUpload1.FileName)
        'img.ImageUrl = CurrentPath & FileUpload1.FileName
        'txtPic.Text = "c:\Temp\" & FileUpload1.FileName

    End Sub

    Sub insertLogo_2()
        Dim db As New Connect_Leave
        Dim sql As String
        Dim tr As SqlTransaction
        Dim sqlcon As New SqlConnection(db_Leave.sqlCon)

        If sqlcon.State = ConnectionState.Closed Then
            sqlcon = New SqlConnection(db_Leave.sqlCon)
            sqlcon.Open()
        End If

        tr = sqlcon.BeginTransaction

        Try
            Dim cmd As SqlCommand = New SqlCommand(sql, sqlcon, tr)
            If txtPic.Text <> "" Then
                Dim FileN As FileStream
                Dim Ln As Int32
                Dim oBinaryReader As BinaryReader
                Dim oImgByteArray As Byte()

                FileN = New FileStream(txtPic.Text, FileMode.Open, FileAccess.Read)
                oBinaryReader = New BinaryReader(FileN)
                oImgByteArray = oBinaryReader.ReadBytes(CInt(FileN.Length))
                Ln = CInt(FileN.Length)
                oBinaryReader.Close()
                FileN.Close()

                sql = "update [TB_Employee] set image_name=@FF1,image=@FF2 where employee_id  = '" & txtID.Text & "'"
                Dim cmd3 As SqlCommand = New SqlCommand(sql, sqlcon, tr)
                cmd3.Parameters.Add("@FF1", SqlDbType.VarChar, 0).Value = txtID.Text + Path.GetExtension(txtPic.Text).ToLower
                cmd3.Parameters.Add("@FF2", SqlDbType.Image, 0).Value = oImgByteArray
                cmd3.CommandTimeout = 0
                cmd3.ExecuteNonQuery()
            End If

            tr.Commit()
            sqlcon.Close()
        Catch ex As Exception
            tr.Rollback()
            sqlcon.Close()
            Exit Sub
        End Try

    End Sub

    Sub Update()
        Dim x As Integer
        Dim DB_HR As New Connect_HR
        Dim Cn As New SqlConnection(DB_HR.sqlCon)
        Dim Status, Sex, LeaveLock, checkB2, pms As String
        Dim sqlcon As New OleDbConnection(DB_HR.cnPAStr)
        Dim sqlcom As New OleDbCommand

        'Dim date1 As String = txtDate1.Text 'SearchDataSub("SC_Subcontract", "convert(datetime,'" & txtDate1.Text & "',103)", "")

        If txtName.Text = "" Or txtFullName.Text = "" Or txtSurName.Text = "" Or txtID.Text = "" Or txtEmpLevel.Text = "" Or txtEmpLevel_Leave.Text = "" Or txtPositionID.Text = "" Or txtPositionID_Leave.Text = "" Or txtBirthday.Text = "" Or txtManagerid.Text = "" Or txtsDate.Text = "" Or drpWorkplace.SelectedValue = "" Then
            sms.Msg = "กรุณาระบุข้อมูลสำคัญ (*) ให้ครบถ้วน"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        End If


        Dim sdate As String
        If txtsDate.Text = "" Then
            sdate = "01/01/1900"
        Else
            sdate = SearchDataSub("TB_Employee", "convert(datetime,'" & txtsDate.Text & "',103)", "") ' txtDate1.Text
        End If

        Dim edate As String
        If txteDate.Text = "" Then
            edate = "01/01/1900"
        Else
            edate = SearchDataSub("TB_Employee", "convert(datetime,'" & txteDate.Text & "',103)", "") ' txtDate1.Text
        End If

        If rdoStatus.Items.FindByValue("Active").Selected = True Then
            Status = "Active"
        ElseIf rdoStatus.Items.FindByValue("InActive").Selected = True Then
            Status = "InActive"
        Else
            Status = "InActive"
        End If

        If rdoLeaveCheck.Items.FindByValue("check").Selected = True Then
            LeaveLock = "check"
        ElseIf rdoLeaveCheck.Items.FindByValue("unchecked").Selected = True Then
            LeaveLock = "unchecked"
        Else
            LeaveLock = "unchecked"
        End If

        If rdoCheckB2.Items.FindByValue("check").Selected = True Then
            checkB2 = "check"
        ElseIf rdoLeaveCheck.Items.FindByValue("unchecked").Selected = True Then
            checkB2 = "unchecked"
        Else
            checkB2 = "unchecked"
        End If

        If rdoSex.Items.FindByValue("Male").Selected = True Then
            Sex = "Male"
        ElseIf rdoSex.Items.FindByValue("Female").Selected = True Then
            Sex = "Female"
        Else
            Sex = "Female"
        End If

        If rdoPMS.Items.FindByValue("Yes").Selected = True Then
            pms = "Yes"
        Else
            pms = "No"
        End If

        Dim sSql As String = "UPDATE [TB_Employee] SET "
        sSql += " fullname ='" & txtFullName.Text & "'"
        sSql += " ,fullnameEN ='" & txtFullNameEN.Text & "'"

        sSql += " ,site ='" & drpSite.SelectedValue & "'"
        sSql += " ,workplace_id ='" & drpWorkplace.SelectedValue & "'"
        sSql += " , NameTitle ='" & txtTitle.Text & "'"
        sSql += " , Employee_Name ='" & txtName.Text & "'"
        sSql += " , Employee_LastName ='" & txtSurName.Text & "'"
        sSql += " , Employee_Nickname ='" & txtNickName.Text & "'"
        sSql += " , branch ='" & drpBranch.Text & "'"

        sSql += " , departments_id ='" & drpDepartments.SelectedValue & "'"
        sSql += " , department_id ='" & drpDepartment.SelectedValue & "'"

        sSql += " , EmpLevel ='" & txtEmpLevel.Text & "'"
        sSql += " , position_id ='" & txtPositionID.Text & "'"
        sSql += " , EmpLevel_ApproveLeave ='" & Trim(txtEmpLevel_Leave.Text) & "'"
        sSql += " , position_id_ApproveLeave ='" & Trim(txtPositionID_Leave.Text) & "'"

        sSql += " , position ='" & txtPosition.Text & "'"
        sSql += " , [SEX] ='" & Sex & "'"
        sSql += " , birthday ='" & CDate(txtBirthday.Text).ToString("yyyy-MM-dd") & "'"
        sSql += " , adr ='" & txtAddress.Text & "'"
        sSql += " , Tel ='" & txtTel.Text & "'"
        sSql += " , Ext ='" & txtExt.Text & "'"
        sSql += " , Email ='" & txtEmail.Text & "'"
        sSql += " , Manager_id ='" & txtManagerid.Text & "'"
        sSql += " , Manager_id_pms ='" & txtManagerid_pms.Text & "'"
        sSql += " , StartDate ='" & CDate(sdate).ToString("yyyy-MM-dd") & "'"
        sSql += " , EndDate ='" & CDate(edate).ToString("yyyy-MM-dd") & "'"
        sSql += " , [Status] ='" & Status & "'"
        sSql += " , [LeaveLock] ='" & LeaveLock & "'"
        sSql += " , [checkB2] ='" & checkB2 & "'"
        sSql += " , [ID_Card] ='" & txtIDcard.Text & "'"

        sSql += " , [Employee_NameEN] ='" & txtNameEN.Text & "'"
        sSql += " , [Employee_LastNameEN] ='" & txtSurNameEN.Text & "'"
        sSql += " , [positionEN] ='" & txtPositionEN.Text & "'"

        sSql += " , [pms] = '" & pms & "'"

        sSql += " Where Employee_ID = '" & txtID.Text & "' "


        If sqlcon.State = ConnectionState.Closed Then
            sqlcon = New OleDbConnection(DB_HR.cnPAStr)
            sqlcon.Open()
        End If


        With sqlcom
            .Connection = sqlcon
            .CommandType = CommandType.Text
            .CommandText = sSql
            .ExecuteNonQuery()
        End With



    End Sub

    Sub UpdateHost()
        Dim x As Integer
        Dim db_Leave As New Connect_Leave
        Dim Cn As New SqlConnection(db_Leave.sqlCon)
        Dim Status, Sex, LeaveLock, checkB2, pms As String
        Dim sqlcon As New OleDbConnection(db_Leave.cnPAStr)
        Dim sqlcom As New OleDbCommand

        If txtName.Text = "" Or txtFullName.Text = "" Or txtSurName.Text = "" Or txtID.Text = "" Or txtEmpLevel.Text = "" Or txtEmpLevel_Leave.Text = "" Or txtPositionID.Text = "" Or txtPositionID_Leave.Text = "" Or txtBirthday.Text = "" Or txtManagerid.Text = "" Or txtsDate.Text = "" Then
            sms.Msg = "กรุณาระบุข้อมูลสำคัญ (*) ให้ครบถ้วน"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        End If

        'Dim date1 As String = txtDate1.Text 'SearchDataSub("SC_Subcontract", "convert(datetime,'" & txtDate1.Text & "',103)", "")
        Dim sdate As String
        If txtsDate.Text = "" Then
            sdate = "01/01/1900"
        Else
            sdate = SearchDataSub("TB_Employee", "convert(datetime,'" & txtsDate.Text & "',103)", "") ' txtDate1.Text
        End If

        Dim edate As String
        If txteDate.Text = "" Then
            edate = "01/01/1900"
        Else
            edate = SearchDataSub("TB_Employee", "convert(datetime,'" & txteDate.Text & "',103)", "") ' txtDate1.Text
        End If

        If rdoStatus.Items.FindByValue("Active").Selected = True Then
            Status = "Active"
        ElseIf rdoStatus.Items.FindByValue("InActive").Selected = True Then
            Status = "InActive"
        Else
            Status = "InActive"
        End If

        If rdoLeaveCheck.Items.FindByValue("check").Selected = True Then
            LeaveLock = "check"
        ElseIf rdoLeaveCheck.Items.FindByValue("unchecked").Selected = True Then
            LeaveLock = "unchecked"
        Else
            LeaveLock = "unchecked"
        End If

        If rdoCheckB2.Items.FindByValue("check").Selected = True Then
            checkB2 = "check"
        ElseIf rdoLeaveCheck.Items.FindByValue("unchecked").Selected = True Then
            checkB2 = "unchecked"
        Else
            checkB2 = "unchecked"
        End If

        If rdoSex.Items.FindByValue("Male").Selected = True Then
            Sex = "Male"
        ElseIf rdoSex.Items.FindByValue("Female").Selected = True Then
            Sex = "Female"
        Else
            Sex = "Female"
        End If


        If rdoPMS.Items.FindByValue("Yes").Selected = True Then
            pms = "Yes"
        Else
            pms = "No"
        End If

        Dim sSql As String = "UPDATE [TB_Employee] SET "
        sSql += " fullname ='" & txtFullName.Text & "'"
        sSql += " ,site ='" & drpSite.SelectedValue & "'"
        sSql += " ,workplace_id ='" & drpWorkplace.SelectedValue & "'"
        sSql += " ,fullnameEN ='" & txtFullNameEN.Text & "'"
        sSql += " , NameTitle ='" & txtTitle.Text & "'"
        sSql += " , Employee_Name ='" & txtName.Text & "'"
        sSql += " , Employee_LastName ='" & txtSurName.Text & "'"
        sSql += " , Employee_Nickname ='" & txtNickName.Text & "'"
        sSql += " , branch ='" & drpBranch.Text & "'"

        sSql += " , departments_id ='" & drpDepartments.SelectedValue & "'"
        sSql += " , department_id ='" & drpDepartment.SelectedValue & "'"

        sSql += " , EmpLevel ='" & txtEmpLevel.Text & "'"
        sSql += " , position_id ='" & txtPositionID.Text & "'"
        sSql += " , EmpLevel_ApproveLeave ='" & Trim(txtEmpLevel_Leave.Text) & "'"
        sSql += " , position_id_ApproveLeave ='" & Trim(txtPositionID_Leave.Text) & "'"

        sSql += " , position ='" & txtPosition.Text & "'"
        sSql += " , [SEX] ='" & Sex & "'"
        sSql += " , birthday ='" & CDate(txtBirthday.Text).ToString("yyyy-MM-dd") & "'"
        sSql += " , adr ='" & txtAddress.Text & "'"
        sSql += " , Tel ='" & txtTel.Text & "'"
        sSql += " , Ext ='" & txtExt.Text & "'"
        sSql += " , Email ='" & txtEmail.Text & "'"
        sSql += " , Manager_id ='" & txtManagerid.Text & "'"
        sSql += " , Manager_id_pms ='" & txtManagerid_pms.Text & "'"
        sSql += " , StartDate ='" & CDate(sdate).ToString("yyyy-MM-dd") & "'"
        sSql += " , EndDate ='" & CDate(edate).ToString("yyyy-MM-dd") & "'"
        sSql += " , [Status] ='" & Status & "'"
        sSql += " , [LeaveLock] ='" & LeaveLock & "'"
        sSql += " , [checkB2] ='" & checkB2 & "'"
        sSql += " , [ID_Card] ='" & txtIDcard.Text & "'"

        sSql += " , [Employee_NameEN] ='" & txtNameEN.Text & "'"
        sSql += " , [Employee_LastNameEN] ='" & txtSurNameEN.Text & "'"
        sSql += " , [positionEN] ='" & txtPositionEN.Text & "'"

        sSql += " , [pms] = '" & pms & "'"

        sSql += " Where Employee_ID = '" & txtID.Text & "' "


        If sqlcon.State = ConnectionState.Closed Then
            sqlcon = New OleDbConnection(db_Leave.cnPAStr)
            sqlcon.Open()
        End If


        With sqlcom
            .Connection = sqlcon
            .CommandType = CommandType.Text
            .CommandText = sSql
            .ExecuteNonQuery()
        End With


    End Sub

    Sub insert()
        Dim Status, Sex, LeaveLock, pms As String
        Dim sqlcheck As String

        sqlcheck = " select * "
        sqlcheck += " from [TB_Employee] "
        sqlcheck += " where [Employee_ID] Like '" & txtID.Text & "' "

        If txtName.Text = "" Or txtFullName.Text = "" Or txtSurName.Text = "" Or txtID.Text = "" Or txtEmpLevel.Text = "" Or txtEmpLevel_Leave.Text = "" Or txtPositionID.Text = "" Or txtPositionID_Leave.Text = "" Or txtBirthday.Text = "" Or txtManagerid.Text = "" Or txtsDate.Text = "" Or txtManagerid_pms.Text = "" Then
            sms.Msg = "กรุณาระบุข้อมูลสำคัญ (*) ให้ครบถ้วน"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        End If

        dt = DB_HR.GetDataTable(sqlcheck)
        If dt.Rows.Count > 0 Then
            sms.Msg = "Employee ID นี้มีอยู่แล้ว!!"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If

        Dim bdate As String
        If txtBirthday.Text = "" Then
            bdate = "01/01/1900"
        Else
            bdate = SearchDataSub("TB_Employee", "convert(datetime,'" & txtBirthday.Text & "',103)", "") ' txtDate1.Text
        End If

        Dim sdate As String
        If txtsDate.Text = "" Then
            sdate = "01/01/1900"
        Else
            sdate = SearchDataSub("TB_Employee", "convert(datetime,'" & txtsDate.Text & "',103)", "") ' txtDate1.Text
        End If

        Dim edate As String
        If txteDate.Text = "" Then
            edate = "01/01/1900"
        Else
            edate = SearchDataSub("TB_Employee", "convert(datetime,'" & txteDate.Text & "',103)", "") ' txtDate1.Text
        End If

        If rdoStatus.Items.FindByValue("Active").Selected = True Then
            Status = "Active"
        ElseIf rdoStatus.Items.FindByValue("InActive").Selected = True Then
            Status = "InActive"
        Else
            Status = "InActive"
        End If

        If rdoLeaveCheck.Items.FindByValue("check").Selected = True Then
            LeaveLock = "check"
        ElseIf rdoLeaveCheck.Items.FindByValue("unchecked").Selected = True Then
            LeaveLock = "unchecked"
        Else
            LeaveLock = "unchecked"
        End If

        If rdoSex.Items.FindByValue("Male").Selected = True Then
            Sex = "Male"
        ElseIf rdoSex.Items.FindByValue("Female").Selected = True Then
            Sex = "Female"
        Else
            Sex = "Female"
        End If

        If rdoPMS.Items.FindByValue("Yes").Selected = True Then
            pms = "Yes"
        Else
            pms = "No"
        End If


        Dim sql As String = "INSERT INTO [TB_Employee] (site ,workplace_id  ,Employee_ID	,fullname	,Employee_Name ,NameTitle"
        sql &= " ,Employee_LastName	,Employee_Nickname	,branch	,departments_id	,department_id	,EmpLevel, EmpLevel_ApproveLeave ,position_id_ApproveLeave "
        sql &= " ,position_id	,position	,SEX	,birthday "
        sql &= " ,adr	,Tel	,Ext	,Email	,Manager_id "
        sql &= " ,StartDate	,EndDate	,Status	,LeaveLock ,ID_Card ,Employee_NameEN,Employee_LastNameEN,positionEN,fullnameEN,PMS,Manager_id_pms "
        sql &= " )"


        sql &= "VALUES ('" & drpSite.Text & "','" & drpWorkplace.Text & "','" & txtID.Text & "','" & txtFullName.Text & "','" & txtName.Text & "','" & txtTitle.Text & "'"
        sql &= " ,'" & txtSurName.Text & "' , '" & txtNickName.Text & "','" & drpBranch.SelectedValue & "' , '" & drpDepartments.SelectedValue & "', '" & drpDepartment.SelectedValue & "', '" & Trim(txtEmpLevel.Text) & "'  "
        sql &= " ,'" & Trim(txtEmpLevel_Leave.Text) & "' ,'" & Trim(txtPositionID_Leave.Text) & "'"
        sql &= " ,'" & txtPositionID.Text & "' , '" & txtPosition.Text & "','" & Sex & "','" & CDate(bdate).ToString("yyyy-MM-dd") & "' "
        sql &= " ,'" & txtAddress.Text & "' , '" & txtTel.Text & "','" & txtExt.Text & "','" & txtEmail.Text & "','" & txtManagerid.Text & "' "
        sql &= " , '" & CDate(sdate).ToString("yyyy-MM-dd") & "', '" & CDate(edate).ToString("yyyy-MM-dd") & "', '" & Status & "', '" & LeaveLock & "', '" & txtIDcard.Text & "' , '" & txtNameEN.Text & "', '" & txtSurNameEN.Text & "' , '" & txtPositionEN.Text & "', '" & txtFullNameEN.Text & "', '" & pms & "', '" & txtManagerid_pms.Text & "') "
        dtt = DB_HR.GetDataTable(sql)

        dtt = db_Leave.GetDataTable(sql)

    End Sub


    Private Function SearchDataSub(ByVal Tb As String, ByVal Fld As String, Optional ByVal Cond As String = "")
        Dim sql As String
        Dim Sqladp As SqlDataAdapter
        Dim DataSearch As New DataSet
        Dim dtrow As DataRow
        Dim SCon As New SqlConnection(DB_HR.sqlCon)
        sql = "select top 1 " & Fld & " from " & Tb
        If Cond <> "" Then
            sql = sql & " where " & Cond
        End If
        Sqladp = New SqlDataAdapter(sql, SCon)
        Sqladp.Fill(DataSearch, "data1")
        If DataSearch.Tables("data1").Rows.Count <> 0 Then  'Not IsDBNull(DataSearch.Tables("data1").Rows(0)) Then
            dtrow = DataSearch.Tables("data1").Rows(0)
            SearchDataSub = dtrow.Item(0)
        Else
            SearchDataSub = ""
        End If
        SCon.Close()
    End Function

    Protected Sub drpBranch_Init(sender As Object, e As EventArgs) Handles drpBranch.Init
        Dim sqlCon As New SqlConnection(DB_HR.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select branch as id,branch from TB_Site  group by branch order by id "

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_HR.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpBranch.Items.Clear()
        drpBranch.Items.Add(New ListItem("", 1))
        While Rs.Read
            'drpProvince.Items.Add(New ListItem(Str(Rs.GetInt32(0)) + " " + Rs.GetString(1), Rs.GetInt32(0)))
            drpBranch.Items.Add(New ListItem(Rs.GetString(1), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpDepartment_Init(sender As Object, e As EventArgs) Handles drpDepartment.Init
        Dim sqlCon As New SqlConnection(DB_HR.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [department_id] as id,[department_name] from [TB_department]  where Stat ='Active' "

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_HR.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpDepartment.Items.Clear()
        drpDepartment.Items.Add(New ListItem("", 1))
        While Rs.Read
            drpDepartment.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
            'drpDepartment.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(1), Rs.GetString(0)))
            'drpDepartment.Items.Add(New ListItem(Rs.GetString(1), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Server.Transfer("A_Employee.aspx")
    End Sub

    Protected Sub drpDepartments_Init(sender As Object, e As EventArgs) Handles drpDepartments.Init
        Dim sqlCon As New SqlConnection(DB_HR.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [departments_id] as id,[departments_name] from [TB_departments] "

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_HR.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpDepartments.Items.Clear()
        drpDepartments.Items.Add(New ListItem("", 1))
        While Rs.Read
            'drpProvince.Items.Add(New ListItem(Str(Rs.GetInt32(0)) + " " + Rs.GetString(1), Rs.GetInt32(0)))
            'drpDepartments.Items.Add(New ListItem(Rs.GetString(1), Rs.GetString(0)))
            drpDepartments.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
        End While
    End Sub


    Protected Sub txtEmpLevel_TextChanged(sender As Object, e As EventArgs) Handles txtEmpLevel.TextChanged
        ' txtEmpLevel_Leave.Text = txtEmpLevel.Text
    End Sub

    Protected Sub txtPositionID_TextChanged(sender As Object, e As EventArgs) Handles txtPositionID.TextChanged
        ' txtPositionID_Leave.Text = txtPositionID.Text
    End Sub

    Protected Sub drpWorkplace_Init(sender As Object, e As EventArgs) Handles drpWorkplace.Init
        Dim sqlCon As New SqlConnection(DB_HR.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [workplace_id] as id,[workplace_name] from [TB_Workplace]  where active ='y' order by workplace_id asc"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_HR.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With

        'drpWorkplace.Items.Clear()
        'drpWorkplace.Items.Add(New ListItem("", 1))
        While Rs.Read
            drpWorkplace.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
            'drpDepartment.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(1), Rs.GetString(0)))
            'drpDepartment.Items.Add(New ListItem(Rs.GetString(1), Rs.GetString(0)))
        End While
    End Sub
End Class