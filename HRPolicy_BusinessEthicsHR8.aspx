﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_BusinessEthicsHR8.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_BusinessEthicsHR8" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<style>
	.swiper-container {
		width: 100%;
		height: 100%;
	}
	.swiper-slide {
		text-align: center;
		background: #fff;
	}
	.swiper-slide img 
	{
		width: 100%;
	}
	.swiper-pagination-bullet-active {
		opacity: 1;
		background: #af2f2f;
	}
</style>

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/hr.jpg" alt=""/></div>
<!-- end banner -->

<article class="blog-details-page">
	<div class="container">
		
		<div class="post-heading">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
                <li>/</li>
                <li><a href="HRPolicy_Announce.aspx">ระเบียบต่างๆ</a></li>
                <li>/</li>
                <li><a href="HRPolicy_BusinessEthicsHR.aspx">ประกาศ คำสั่ง ระเบียบทรัพยากรมนุษย์</a></li></li>
            </ul>
			<h2>ระเบียบการแต่งกายพนักงานประจำสำนักงาน</h2>
		</div> <!-- /.post-heading -->
		
		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix">
				<div class="blog-details-post-wrapper">
				
					<img src="new/images/banner/hr-policy/img12.jpg" alt="Image">
         		    
         		     <p style="padding: 30px 0 0">
         		    	เพื่อให้เกิดภาพลักษณ์ที่ดี และมีความเป็นระเบียบเรียบร้อยในการปฏิบัติงาน ทาง อโรม่า กรุ๊ป และบริษัทในเครือ จึงเห็นสมควรกำหนด และขอความร่วมมือมายังพนักงานประจำสำนักงานทุกท่าน ในการปฏิบัติอย่างเคร่งครัดเรื่อง การแต่งกายทุกวันที่มาปฏิบัติงาน ดังนี้<br><br>
         		    	
         		    	ประกาศฉบับนี้ไม่บังคับใช้พนักงานซึ่งปฏิบัติงานประจำสายการผลิต โรงงานบางปะกง งานปฏิบัติการ/หน้าร้าน งานบริการลูกค้า (ช่างเทคนิค) สาขาทั้งกรุงเทพ- ปริมณฑล และต่างจังหวัด งานแนะนำสินค้า (พีซี) เนื่องจากกำหนดให้สวมใส่ชุดฟอร์มทุกวันที่ปฏิบัติงาน
					</p>
					
					<ol>
						<li>ทรงผม
							<ul class="a">
								<li>พนักงานชาย ให้ไว้ผมทรงสั้น รองทรง รวมทั้งจัดทรงให้ดูแล้วสุภาพเรียบร้อย หากยาวให้มัดจัดเก็บทรงให้เรียบร้อย</li>
 								<li>พนักงานหญิง หากผมยาวให้จัดทรงให้ดูแล้วสุภาพเรียบร้อย</li>
							</ul>
						</li>
						<li>บัตรพนักงาน
							<ul class="a">
								<li>บัตรพนักงานทั้งพนักงานชายและหญิง ให้คล้องบัตรพนักงานทุกวันที่ทำงาน และตลอดเวลาปฏิบัติงาน</li>
							</ul>
						</li>
						<li>การแต่งกาย ทำงานวันจันทร์ – วันพฤหัสบดี
							<ul class="a">
								<li>เสื้อ
									<ul class="b">
										<li>พนักงานชาย ให้ใส่เสื้อเชิ้ต หรือเสื้อโปโล (คอปก) โดยต้องสวมใส่ไว้ข้างในกางเกงตลอดเวลาปฏิบัติงาน</li>
										<li>พนักงานหญิง ให้ใส่เสื้อเชิ้ต หรือเสื้อโปโล (คอปก) หรือเสื้อสุภาพสตรีทำงานที่สุภาพ งดใส่เสื้อเกาะอก สายเดี่ยว</li>
									</ul>
								</li>
								<li>กางเกง/กระโปรง
									<ul class="b">
										<li>พนักงานชาย ให้สวมกางเกงขายาวผ้าทำงาน งดสวมกางเกงยีนส์</li>
										<li>พนักงานหญิง ให้สวมกระโปรง หรือกางเกงขายาวแบบผ้าทำงาน งดสวมกระโปรง/กางเกงยีนส์ รวมถึงงดสวม กระโปรงสั้น (กระโปรงสั้น คือ ชายกระโปรงสั้นนับจากเหนือเข่าเกิน
										 หนึ่งฝ่ามือ หรือเกิน 3 นิ้ว)</li>
										<li>ทั้งพนักงานชาย และหญิง งดสวมกางเกงหรือกระโปรง โดยจงใจใส่เอวต่ำให้เห็นบ็อกเซอร์ หรือเอวต่ำแล้วเห็นขอบชั้นใน เป็นต้น</li>
										<li>พนักงานช่างเทคนิค คลังสินค้า จัดส่ง โดยอนุโลมให้สวมกางเกงยีนส์สีเข้มมาปฏิบัติงานได้ อันเนื่องจากลักษณะงาน และพนักงานฝ่ายขายและการตลาด กรณีต้องไปติดตั้ง/เก็บบูท งานแสดงสินค้าต่างๆ เท่านั้น</li>
									</ul>
								</li>
								<li>รองเท้า
									<ul class="b">
										<li>พนักงานชาย ให้สวมรองเท้าหนัง หรือผ้าใบ</li>
										<li>พนักงานหญิง ให้สวมรองเท้าคัทชู หรือรองเท้าผ้าใบ (ตามความเหมาะสม) หรือรองเท้าสุภาพสตรีทำงาน</li>
										<li>งดสวมรองเท้าแตะ</li>
									</ul>								
								</li>
							</ul>
						</li>
						<li>การแต่งกาย ทำงานวันศุกร์
							<ul class="a">
								<li>กรณีพนักงานใส่เสื้อ อโรม่า กรุ๊ป เสื้อแบรนด์สินค้าบริษัท โดยอนุโลมให้พนักงานสวมกับกางเกงขายาว/กระโปรงยีนส์สุภาพสีเข้มได้ ยกเว้นพนักงานท่านใดใส่เสื้อ อโรม่า กรุ๊ป ไปพบ/ติดต่อลูกค้าให้พนักงานสวมกางเกงขายาว/กระโปรงผ้า ทำงานสุภาพสีเข้มเท่านั้น รวมทั้งสวมรองเท้าหนัง หรือคัทชูหุ้มส้น หรือรองเท้าสุภาพสตรีรัดส้นให้เรียบร้อย (งดยีนส์ และรองเท้าผ้าใบ)</li>
								<li>กรณีพนักงานไม่ได้ใส่เสื้อ อโรม่า กรุ๊ป เสื้อแบรนด์สินค้าบริษัท ให้พนักงานแต่งกายเช่นเดียวกันกับวันจันทร์ – พฤหัสบดี</li>
								<li>งดใส่เสื้อเกาะอก สายเดี่ยว</li>
								<li>งดสวมกระโปรงสั้น</li>
								<li>งดสวมกางเกงหรือกระโปรง โดยจงใจใส่เอวต่ำให้เห็นบ็อกเซอร์ หรือเอวต่ำแล้วเห็นขอบชั้นใน</li>
								<li>รองเท้า
									<ul class="a">
										<li>พนักงานชาย ให้สวมรองเท้าหนัง หรือผ้าใบ</li>
										<li>พนักงานหญิง ให้สวมรองเท้าคัทชู หรือรองเท้าผ้าใบ (ตามความเหมาะสม) หรือรองเท้าสุภาพสตรีทำงาน</li>
										<li>งดสวมรองเท้าแตะ</li>
									</ul>
								</li>
							</ul>							
						</li>
					</ol>
					
					<p><br>
						ทั้งนี้ หากพบว่าพนักงานท่านใดจงใจ ฝ่าฝืน เพิกเฉย ละเลยไม่ให้ความร่วมมือปฏิบัติตามระเบียบดังกล่าว โดยไม่มีเหตุผลอันควร ทางบริษัทขอกำหนดให้พนักงานจะต้องเปลี่ยนและสวมใส่ชุดตามแบบที่บริษัทจัดเตรียมไว้ให้ โดยไม่มีข้อโต้แย้งแต่อย่างใดทั้งสิ้น และมีความจำเป็นอย่างยิ่งที่จะต้องลงโทษตามระเบียบวินัยขั้นตอนบริษัท ซึ่งจะมีผลต่อการประเมินผลปรับเงินเดือน/โบนัสประจำปีด้วย<br><br>

						พร้อมกันนี้ ทางบริษัทฯ ขอชื่นชมพนักงานท่านที่แต่งกายถูกต้องอยู่ ณ ปัจจุบันนี้ มา ณ โอกาสนี้ด้วย<br><br>

						ระเบียบนี้ ให้ถือปฏิบัติตั้งแต่บัดนี้เป็นต้นไป<br><br>

						จึงประกาศมาเพื่อทราบ และขอความร่วมมือปฏิบัติอย่างเคร่งครัดโดยทั่วกัน<br><br>

						ประกาศ ณ วันที่ 21 ตุลาคม 2556
					</p>
					
					<div class="blog-details-bt">
						<div class="btn-group">
							<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
						</div>
					</div>
				
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-8 col-xs-12 p-fix">
				<aside>
					<div class="sidebar-news-list">
						<h4>CATEGORY</h4>
						<ul>
							<li>
								<a href="HRPolicy_BusinessEthicsHR.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบทรัพยากรมนุษย์</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAS.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบเกี่ยวกับทรัพย์สิน</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAC.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบเกี่ยวกับบัญชี การเงิน งบประมาณ จัดซื้อ</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsIT.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบเกี่ยวกับเทคโนโลยีสารสนเทศ</a>
							</li>
							<li>
								<a href="Benefits1_1.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบอื่นๆ</a>
							</li>
						</ul>
					</div>
				</aside>
				<aside>
					<div class="sidebar-news-list">
						<h4>Related Links</h4>
						<ul>
							<li>
								<a href="HRPolicy_BusinessEthicsHR1.aspx" class="tran3s">
									การคล้อง ติดบัตรพนักงาน
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR2.aspx" class="tran3s">
									การลาพักผ่อนประจำปี
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR3.aspx" class="tran3s">
									การแต่งกายพนักงานขาย (พีซี)
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR4.aspx" class="tran3s">
									การลงนามอนุมัติเกินอำนาจดำเนินการ หรือความเสียหายจากการปฏิบัติหน้าที่
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR5.aspx" class="tran3s">
									ระเบียบบ้านพักพนักงาน
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR6.aspx" class="tran3s">
									ระเบียบการลงโทษพนักงานทุจริต
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR7.aspx" class="tran3s">
									ทดลองจัดวันเวลาทำงานและวันหยุด (เป็นกรณีพิเศษ) ทำงานวันจันทร์ - ศุกร์
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR8.aspx" class="tran3s">
									ระเบียบการแต่งกายพนักงานประจำสำนักงาน
								</a>
							</li>
						</ul>
					</div>
				</aside>
			</div>
		</div>
		
	</div>
</article>
    
<!-- Swiper JS -->
<script src="new/js/swiper.min.js"></script>

<!-- Initialize Swiper -->
<script>
	var swiper1 = new Swiper('.swiper1', {
		spaceBetween: 30,
		pagination: {
        el: '.swiper-pagination1',
        clickable: true,
      },
    });
</script>

</asp:Content>
