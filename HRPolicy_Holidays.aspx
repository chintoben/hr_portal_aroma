﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_Holidays.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_Holidays" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/hr.jpg" alt=""/></div>
<!-- end banner -->

<section id="about-us" class="blog-category">
<div class="container">
	<div class="theme-title tt-edit category-title">
        <ul>
            <li><a href="index_hr.aspx">HOME</a></li>
        </ul>
		<h2>วันหยุดประจำปีบริษัท</h2>

		<!--p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p-->
	</div> <!-- /.theme-title -->

	<%--<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-6 div-empty"></div>
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-022-sun-umbrella"></i>
				</div>
				<h3><a href="HRPolicy_Holiday1.aspx" class="tran3s">วันหยุดประเพณี ประจำปี<br>พ.ศ. 2560</a></h3>
				<!--p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p-->
				<a href="HRPolicy_Holiday1.aspx" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-077-summer"></i>
				</div>
				<h3><a href="HRPolicy_Holiday2.aspx" class="tran3s">วันหยุดประเพณี ประจำปี<br>พ.ศ. 2561</a></h3>
				<!--p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p-->
				<a href="HRPolicy_Holiday2.aspx" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
       
        <div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-022-sun-umbrella"></i>
				</div>
				<h3><a href="HRPolicy_Holiday3.aspx" class="tran3s">วันหยุดประเพณี ประจำปี<br>พ.ศ. 2562</a></h3>
				<!--p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p-->
				<a href="HRPolicy_Holiday1.aspx" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->

        <div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-022-sun-umbrella"></i>
				</div>
				<h3><a href="HRPolicy_Holiday3.aspx" class="tran3s">วันหยุดประเพณี ประจำปี<br>พ.ศ. 2562</a></h3>
				<!--p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p-->
				<a href="HRPolicy_Holiday1.aspx" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->

		 <div class="col-lg-3 col-md-3 col-sm-6 div-empty"></div>
	</div> <!-- /.row -->--%>


    	<div class="row">
<%--		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-022-sun-umbrella"></i>
				</div>
				<h3><a href="HRPolicy_Holiday1.aspx" class="tran3s">วันหยุดประเพณี ประจำปี<br>พ.ศ. 2560</a></h3>
				<!--p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p-->
				<a href="HRPolicy_Holiday1.aspx" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->--%>
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-077-summer"></i>
				</div>
				<h3><a href="HRPolicy_Holiday2.aspx" class="tran3s">วันหยุดประเพณี ประจำปี<br>พ.ศ. 2561</a></h3>
				<!--p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p-->
				<a href="HRPolicy_Holiday2.aspx" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-022-sun-umbrella"></i>
				</div>
				<h3><a href="HRPolicy_Holiday3.aspx" class="tran3s">วันหยุดประเพณี ประจำปี<br>พ.ศ. 2562</a></h3>
				<!--p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p-->
				<a href="HRPolicy_Holiday3.aspx" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->

        	<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-077-summer"></i>
				</div>
				<h3><a href="HRPolicy_Holiday4.aspx" class="tran3s">วันหยุดประเพณี ประจำปี<br>พ.ศ. 2563</a></h3>

               <%--<a href="Document/Holiday/ประกาศ วันหยุดประเพณี ประจำปี 2563.pdf"  target="_blank" class="more tran3s hvr-bounce-to-right">More Details</a>--%>
                <a href="HRPolicy_Holiday4.aspx" class="more tran3s hvr-bounce-to-right">More Details</a>

			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->

        <div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-077-summer"></i>
				</div>
				<h3><a href="HRPolicy_Holiday5.aspx" class="tran3s">วันหยุดประเพณี ประจำปี<br>พ.ศ. 2564</a></h3>

               <%--<a href="Document/Holiday/ประกาศ วันหยุดประเพณี ประจำปี 2563.pdf"  target="_blank" class="more tran3s hvr-bounce-to-right">More Details</a>--%>
                <a href="HRPolicy_Holiday5.aspx" class="more tran3s hvr-bounce-to-right">More Details</a>

			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->



		<%--<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-104-programmer"></i>
				</div>
				<h3><a href="HRPolicy_BusinessEthicsIT.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบ<br>เกี่ยวกับเทคโนโลยีสารสนเทศ</a></h3>
				<!--p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p-->
				<a href="HRPolicy_BusinessEthicsIT.aspx" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-046-megaphone"></i>
				</div>
				<h3><a href="Benefits1_1.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบอื่นๆ</a></h3>
				<!--p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p-->
				<a href="#" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->--%>

        
	</div> <!-- /.row -->

</div> <!-- /.container -->
</section>

</asp:Content>
