﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="Public_Relations.aspx.vb" Inherits="Aroma_HRPortal.Flow_IT" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/flow/banner.jpg" alt=""/></div>
<!-- end banner -->

<section id="news-section">
	<div class="container">
	
	<div class="theme-title">
        <ul>
        	<li><a href="index_hr.aspx">HOME</a></li>
        </ul>
		<h2>ประชาสัมพันธ์ๆ</h2>
		<!--p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p-->
	</div> <!-- /.theme-title -->
	
		<div class="clear-fix news-wrapper">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="single-price-table">
					<!--i class="fas fa-external-link-alt"></i-->
					<img src="new/images/index/blog1.jpg" alt=""/>
					<h4><a href="Public_RelationsDetail.aspx">Playback: Akufo-Addo speaks to business community</a></h4>
					<div class="news-date"><a href="Public_RelationsDetail.aspx">04 Feb, 2018</a></div>
					<p>
						Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero sit amet.
					</p>
					<a href="Public_RelationsDetail.aspx">Read more</a>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="single-price-table">
					<!--i class="fas fa-user"></i-->
					<img src="new/images/index/blog2.jpg" alt=""/>
					<h4><a href="Public_RelationsDetail.aspx">Career Opportunities</a></h4>
					<div class="news-date"><a href="Public_RelationsDetail.aspx">04 Feb, 2018</a></div>
					<p>
						Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero sit amet.
					</p>
					<a href="Public_RelationsDetail.aspx">Read more</a>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="single-price-table">
					<!--i class="fas fa-book"></i-->
					<img src="new/images/index/blog3.jpg" alt=""/>
					<h4><a href="Public_RelationsDetail.aspx">Instruction Manual</a></h4>
					<div class="news-date"><a href="Public_RelationsDetail.aspx">04 Feb, 2018</a></div>
					<p>
						Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero sit amet.
					</p>
					<a href="Document/คู่มือการใช้งาน HR Portal.pdf">Read more</a>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="single-price-table">
					<!--i class="fas fa-external-link-alt"></i-->
					<img src="new/images/index/blog1.jpg" alt=""/>
					<h4><a href="Public_RelationsDetail.aspx">Leave online & Time Attendance</a></h4>
					<div class="news-date"><a href="Public_RelationsDetail.aspx">04 Feb, 2018</a></div>
					<p>
						Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero sit amet.
					</p>
					<a href="http://leaveonline.aromathailandapp.com/Leave_Login.aspx">Read more</a>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="single-price-table">
					<!--i class="fas fa-user"></i-->
					<img src="new/images/index/blog2.jpg" alt=""/>
					<h4><a href="Public_RelationsDetail.aspx">Career Opportunities</a></h4>
					<div class="news-date"><a href="Public_RelationsDetail.aspx">04 Feb, 2018</a></div>
					<p>
						Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero sit amet.
					</p>
					<a href="Public_RelationsDetail.aspx">Read more</a>
				</div>
			</div>
		</div>
	</div>
</section>

</asp:Content>
