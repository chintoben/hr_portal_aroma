﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_EmployeeLevel.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_EmployeeLevel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <style>
.jumbotron {
  padding: 20px;
  margin-bottom: 15px;
  font-size: 19px;
  font-weight: 200;
  line-height: 1.0428571435;
  color: inherit;
  background-color: #eeeeee;
}

.jumbotron h1 {
  line-height: 1;
  color: inherit;
}

.jumbotron p {
  line-height: 1.2;
}

.container .jumbotron {
  border-radius: 4px;
}

@media screen and (min-width: 668px) {
  .jumbotron {
    padding-top: 30px;
    padding-bottom: 30px;
  }
  .container .jumbotron {
    padding-right: 50px;
    padding-left: 50px;
  }
  .jumbotron h1 {
    font-size: 53px;
  }
}

/* CSS reset */
body,div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,form,fieldset,input,textarea,p,blockquote,th,td { 
	margin:0;
	padding:0;
}
html,body {
	margin:0;
	padding:0;
	height: 100%;
}
table {
	border-collapse:collapse;
	border-spacing:0;
}
fieldset,img { 
	border:0;
}
address,caption,cite,code,dfn,th,var {
	font-style:normal;
	font-weight:normal;
}
ol,ul {
	list-style:none;
}
caption,th {
	text-align:left;
}
h1,h2,h3,h4,h5,h6 {
	font-size:100%;
	font-weight:normal;
}
q:before,q:after {
	content:'';
}
abbr,acronym { border:0;
}
section, header{
	display: block;
}

/* Header Style */
.freshdesignweb-top{
	line-height: 24px;
	font-size: 11px;
	background: rgba(0, 0, 0, 0.05);
	text-transform: uppercase;
	z-index: 9999;
	position: relative;
	box-shadow: 1px 0px 2px rgba(0,0,0,0.2);
}
.freshdesignweb-top a{
	padding: 0px 10px;
	letter-spacing: 1px;
	color: #333;
	text-shadow: 0px 1px 1px #fff;
	display: block;
	float: left;
}
.freshdesignweb-top a:hover{
	background: #fff;
}
.freshdesignweb-top span.right{
	float: right;
}
.freshdesignweb-top span.right a{
	float: left;
	display: block;
}
.freshdesignweb-demos{
    text-align:center;
	display: block;
	line-height: 30px;
	padding: 20px 0px;
}
.freshdesignweb-demos a{
    display: inline-block;
	margin: 0px 4px;
	padding: 0px 4px;
	color: #fff;
	line-height: 20px;	
	font-style: italic;
	font-size: 13px;
	border-radius: 3px;
	background: rgba(41,77,95,0.1);
	-webkit-transition: all 0.2s linear;
	-moz-transition: all 0.2s linear;
	-o-transition: all 0.2s linear;
	-ms-transition: all 0.2s linear;
	transition: all 0.2s linear;
}
.freshdesignweb-demos a:hover{
	background: rgba(41,77,95,0.3);
}
.freshdesignweb-demos a.current,
.freshdesignweb-demos a.current:hover{
	background: rgba(41,77,95,0.3);
}


        .style1
        {
            height: 38px;
        }


        .style2
        {
            height: 36px;
        }
        .style3
        {
            height: 38px;
            width: 148px;
        }
        .style4
        {
            height: 36px;
            width: 148px;
        }
        .style5
        {
            width: 148px;
        }


    </style>

    
<br />
<br />
<br />
<div class="jumbotron" style="text-align:center">
    <img src="Images/logotrans.png" 
        style="margin-top:20px; height: 82px; width: 249px;"/><h4>Aroma HR Portal</h4>
</div>

<div class="row">
    <table class="table" style="margin-left:15px; width:97.5%;">
        <thead class="thead" style="font-size:16px; font-weight:700">
            <tr>
                <th class="style3"></th>
                <th class="style1">กลุ่มระดับ และตำแหน่งพนักงาน</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row" class="style4"></th>
                <td class="style2"><i class="fa fa-file-pdf-o fa-lg" aria-hidden="true"></i>
                    <asp:Image ID="Image10" runat="server" ImageUrl="~/Images/pdf-icon.png" />
                    &nbsp;<a href="Document/3EmployeeLevel/3.1กลุ่มระดับ และตำแหน่งพนักงาน 20-07-2010.pdf"  target="_blank">1. กลุ่มระดับ และตำแหน่งพนักงาน 20-07-2010</a></td>
            </tr>
            <tr>
                <th scope="row" class="style5">&nbsp;</th>
                <td><i class="fa fa-file-pdf-o fa-lg" aria-hidden="true"></i>
                    <asp:Image ID="Image11" runat="server" ImageUrl="~/Images/pdf-icon.png" />
                    &nbsp;<a href="Document/3EmployeeLevel/3.2หลักเกณฑ์การปรับตำแหน่งพนักงาน 21-04-2015.pdf" target="_blank">2. หลักเกณฑ์การปรับตำแหน่งพนักงาน 21-04-2015</a></td>
              
            </tr>
        </tbody>
    </table> 
</div>

</asp:Content>
