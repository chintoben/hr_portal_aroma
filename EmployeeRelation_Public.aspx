﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="EmployeeRelation_Public.aspx.vb" Inherits="Aroma_HRPortal.EmployeeRelation_Public" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner">
<img src="new/images/banner/Activities.jpg" 
        alt="" />
</div>
<!-- end banner -->

<section id="news-section">
	<div class="container">
	
	<div class="theme-title">
        <ul>
        	<li><a href="index_hr.aspx">HOME</a></li>
        </ul>
		<h2>ประชาสัมพันธ์ต่างๆ</h2>
        <%--<a href="EmployeeRelation_ActivitiesDetail.aspx">Read more</a>--%>
        <br />
		<!--p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p-->
	</div> <!-- /.theme-title -->
	
		<div class="clear-fix news-wrapper">
			
            
            <%--  <asp:Button ID="Button2" runat="server" Text="Read more"  class="btn btn-primary"/>   --%>

            	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="single-price-table">
					<!--i class="fas fa-external-link-alt"></i-->
				<%--	<img src="new/images/Activities/RED%20CARPET%20PARTY(ปาร์ตี้%20พรมแดง)%202018.jpg" 
                        alt="" height="200" width="400"/>     --%>    
                        
                         <asp:Image ID="Image1" runat="server" width="400px" Height ="200px" />
                                 
				<%--	<h4>Playback: Akufo-Addo speaks to business community</h4>--%>
                    <asp:Label ID="lblHeader1" runat="server" 
                        Text="&quot;RED CARPET PARTY(ปาร์ตี้ พรมแดง)&quot; 2018" Font-Bold="True"></asp:Label>
					<div class="news-date">
                        <asp:Label ID="lblDate1" runat="server" 
                        Text="28 Feb, 2018"></asp:Label>
                    
                    </div>
					<p>
						  <asp:Label ID="lblDetail1" runat="server" 
                        Text=" กลับมาอีกครั้ง! กับปาร์ตี้สุดมัมส์ RED CARPET PARTY(ปาร์ตี้ พรมแดง) @ The PUBLIC Restaurant & Bar (เกษตร-นวมินทร์ )"></asp:Label>
					</p>
					<%--<a href="EmployeeRelation_ActivitiesDetail.aspx">Read more</a>--%>
                    <asp:Button ID="Button1" runat="server" Text="Read more"  OnClientClick = "SetTarget();"
                        class="btn btn-primary"/>		

                        &nbsp;<asp:Button ID="Button11" runat="server" Text="Images"  OnClientClick = "SetTarget();"
                        class="btn btn-primary"/>		

                        <asp:Label ID="Label1" runat="server" Text="Label" BackColor="White" 
                        ForeColor="White" Font-Size="Smaller"></asp:Label>
				  

                     
				</div>
			</div>

         <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="single-price-table">

                    <asp:Image ID="Image2" runat="server" width="400px" Height ="200px" />
                    <asp:Label ID="lblHeader2" runat="server" 
                        Text="Playback: Akufo-Addo speaks to business community" Font-Bold="True"></asp:Label>
					<div class="news-date">
                        <asp:Label ID="lblDate2" runat="server" 
                        Text="04 Feb, 2018"></asp:Label>                
                    </div>
					<p>
						<asp:Label ID="lblDetail2" runat="server" 
                        Text="Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero sit amet."></asp:Label>
					</p>
                  <%--  <asp:Button ID="Button2" runat="server" Text="Read more"  class="btn btn-primary"/>   --%>
                         <asp:Button ID="Button2" runat="server" Text="Read more"  OnClientClick = "SetTarget();"
                        class="btn btn-primary"/>		
                        &nbsp;<asp:Button ID="Button21" runat="server" Text="Images"  OnClientClick = "SetTarget();"
                        class="btn btn-primary"/>		

                        <br />
                           <asp:Label ID="Label2" runat="server" Text="Label" BackColor="White" 
                        ForeColor="White" Font-Size="Smaller"></asp:Label>         					
                          					
				</div>
			</div>

            	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="single-price-table">
					  <asp:Image ID="Image3" runat="server" width="400px" Height ="200px" />
                    <asp:Label ID="lblHeader3" runat="server" 
                        Text="Playback: Akufo-Addo speaks to business community" Font-Bold="True"></asp:Label>
					<div class="news-date">
                        <asp:Label ID="lblDate3" runat="server" 
                        Text="04 Feb, 2018"></asp:Label>                
                    </div>
					<p>
						<asp:Label ID="lblDetail3" runat="server" 
                        Text="Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero sit amet."></asp:Label>
					</p>
                    <asp:Button ID="Button3" runat="server" Text="Read more"  
                          class="btn btn-primary"  OnClientClick = "SetTarget();"/>   

                          &nbsp;<asp:Button ID="Button31" runat="server" Text="Images"  OnClientClick = "SetTarget();"
                        class="btn btn-primary"/>		

                          <br />
                             <asp:Label ID="Label3" runat="server" Text="Label" BackColor="White" 
                        ForeColor="White"></asp:Label>
                     
				</div>
			</div>
		
		

           
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="single-price-table">
					  <asp:Image ID="Image4" runat="server" width="400px" Height ="200px" />
                    <asp:Label ID="lblHeader4" runat="server" 
                        Text="Playback: Akufo-Addo speaks to business community" Font-Bold="True"></asp:Label>
					<div class="news-date">
                        <asp:Label ID="lblDate4" runat="server" 
                        Text="04 Feb, 2018"></asp:Label>                
                    </div>
					<p>
						<asp:Label ID="lblDetail4" runat="server" 
                        Text="Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero sit amet."></asp:Label>
					</p>
                    <asp:Button ID="Button4" runat="server" Text="Read more"  
                          class="btn btn-primary"  OnClientClick = "SetTarget();"/>   
                             &nbsp;<asp:Button ID="Button41" runat="server" Text="Images"  OnClientClick = "SetTarget();"
                        class="btn btn-primary"/>		

                             <asp:Label ID="Label4" runat="server" Text="Label" BackColor="White" 
                        ForeColor="White"></asp:Label>
                               
                     
                               
				</div>
			</div>

            	

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="single-price-table">
					  <asp:Image ID="Image5" runat="server" width="400px" Height ="200px" />
                    <asp:Label ID="lblHeader5" runat="server" 
                        Text="Playback: Akufo-Addo speaks to business community" Font-Bold="True"></asp:Label>
					<div class="news-date">
                        <asp:Label ID="lblDate5" runat="server" 
                        Text="04 Feb, 2018"></asp:Label>                
                    </div>
					<p>
						<asp:Label ID="lblDetail5" runat="server" 
                        Text="Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero sit amet."></asp:Label>
					</p>
                    <asp:Button ID="Button5" runat="server" Text="Read more"  
                          class="btn btn-primary"  OnClientClick = "SetTarget();"/>   
                             &nbsp;<asp:Button ID="Button51" runat="server" Text="Images"  OnClientClick = "SetTarget();"
                        class="btn btn-primary"/>		

                             <asp:Label ID="Label5" runat="server" Text="Label" BackColor="White" 
                        ForeColor="White"></asp:Label>
                               
                     
                               
				</div>
			</div>

            

			
			
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="single-price-table">
					  <asp:Image ID="Image6" runat="server" width="400px" Height ="200px" />
                    <asp:Label ID="lblHeader6" runat="server" 
                        Text="Playback: Akufo-Addo speaks to business community" Font-Bold="True"></asp:Label>
					<div class="news-date">
                        <asp:Label ID="lblDate6" runat="server" 
                        Text="04 Feb, 2018"></asp:Label>                
                    </div>
					<p>
						<asp:Label ID="lblDetail6" runat="server" 
                        Text="Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero sit amet."></asp:Label>
					</p>
                    <asp:Button ID="Button6" runat="server" Text="Read more"  
                          class="btn btn-primary"  OnClientClick = "SetTarget();"/>   

                             &nbsp;<asp:Button ID="Button61" runat="server" Text="Images"  OnClientClick = "SetTarget();"
                        class="btn btn-primary"/>		

                             <asp:Label ID="Label6" runat="server" Text="Label" BackColor="White" 
                        ForeColor="White"></asp:Label>
                               
                       
                               
				</div>
			</div>
		</div>

           <asp:Label ID="Label11" runat="server" Text="Label" BackColor="White" 
                        ForeColor="White" Font-Size="Smaller" Width="50px"></asp:Label>

                <asp:Label ID="Label21" runat="server" Text="Label" BackColor="White" 
                        ForeColor="White" Font-Size="Smaller"></asp:Label>    

                             <asp:Label ID="Label31" runat="server" Text="Label" BackColor="White" 
                        ForeColor="White"></asp:Label>
                               

        <asp:Label ID="Label41" runat="server" Text="Label" BackColor="White" 
                        ForeColor="White"></asp:Label>

        <asp:Label ID="Label51" runat="server" Text="Label" BackColor="White" 
                        ForeColor="White"></asp:Label>

                              <asp:Label ID="Label61" runat="server" Text="Label" BackColor="White" 
                        ForeColor="White"></asp:Label>
	</div>
</section>
</asp:Content>
