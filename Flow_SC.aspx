﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="Flow_SC.aspx.vb" Inherits="Aroma_HRPortal.Flow_SC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/flow/banner.jpg" alt=""/></div>
<!-- end banner -->

<article class="blog-details-page">
	<div class="container">
		
		<div class="post-heading">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
            </ul>
			<h2>หน่วยงาน Supply Chain</h2>
		</div> <!-- /.post-heading -->
		
		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix" style="float: none; margin: 0 auto;">
				<div class="blog-details-post-wrapper">
				     
					<img src="new/images/banner/benefit/hr_portal2Flow_SC.jpg" alt="Image">

					<p style="padding: 30px 0 0;">
						<ul class="p">
							<%--<li><a href="Document/6Flow/SC/SC1.jpg" target="_blank">Flow การสั่งซื้อสินค้า</a></li>
							<li><a href="Document/6Flow/SC/SC2.jpg" target="_blank">Flow สินค้าใหม่/Logo ใหม่/Packaging ใหม่</a></li>
							<li><a href="Document/6Flow/SC/SC3.jpg" target="_blank">Flow ใบจองสินค้ารายไตรมาส</a></li>--%>
                            <li><a href="Document/6Flow/SC/Flow จองสินค้านำเข้าด่วน Urgent ระหว่างไตรมาส.pdf" target="_blank">Flow จองสินค้านำเข้าด่วน Urgent ระหว่างไตรมาส</a></li>
                            <li><a href="Document/6Flow/SC/Flow จองสินค้ารายไตรมาส.pdf" target="_blank">Flow จองสินค้ารายไตรมาส</a></li>

                            <li><a href="Document/6Flow/SC/Purchasing Procedure.pdf" target="_blank">Purchasing Procedure</a></li>
                            <li><a href="Document/6Flow/SC/Order Placement Procedure.pdf" target="_blank">Order Placement Procedure</a></li>


						</ul>
					</p>
				
				</div>
			</div>
		</div>
		
	</div>
</article>


</asp:Content>
