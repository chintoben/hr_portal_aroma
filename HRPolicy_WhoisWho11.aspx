﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_WhoisWho11.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_Contact" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/HR.jpg" alt=""/></div>
<!-- end banner -->

<section id="team-section">
	<div class="container">
		<div class="theme-title">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
                <li>/</li>
                <li><a href="HRPolicy_WhoisWho.aspx">ติดต่อฝ่าย-แผนกต่างๆ</a></li>
            </ul>
			<h2>รายชื่อติดต่อ-สถาบันพัฒนาธุรกิจกาแฟ (ACA)</h2>
			<!--p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p-->
		</div> <!-- /.theme-title -->

		<div class="clear-fix team-member-wrapper">
			<%--<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who/ACA/1.คุณฐิติวัชร์ ลิมปิโรจนฤทธิ์.png" alt=""/>
						<div class="opacity tran4s">
							<h4>คุณฐิติวัชร์ ลิมปิโรจนฤทธิ์ (อาร์ต)</h4>
							<span>ผู้จัดการฝ่ายอาวุโส</span>
							<p>
								เบอร์ต่อภายใน : 02-9332353 ต่อ 114<br />
                    			
                    			Email : <a href="mailto:thitiwatl@aromathailand.com">thitiwatl@aromathailand.com</a>
							</p>
						</div>
					</div> 
					<div class="member-name">
						<h5>คุณฐิติวัชร์ ลิมปิโรจนฤทธิ์ (อาร์ต)</h5>
						<p>ผู้จัดการฝ่ายอาวุโส</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> 
				</div> 
			</div> --%>

			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who2019/5.aca/คุณศุภกร%20%20กระพันธ์แก้ว.jpg" alt="" 
                            height="150"/>
						<div class="opacity tran4s">
							<h4>คุณศุภกร กระพันธ์แก้ว (พี)</h4>
							<span>ผู้จัดการฝ่าย</span>
							<p>
								เบอร์ต่อภายใน : 02-9332353 ต่อ 114<br />
                    			<%--โทรศัพท์มือถือ : -<br />--%>
                    			Email : <a href="mailto:suphakron_trd@94coffee.com">suphakron_trd@94coffee.com</a>,  <a href="mailto:peerawut_kr@94coffee.com">peerawut_kr@94coffee.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณศุภกร กระพันธ์แก้ว (พี)</h5>
						<p>ผู้จัดการฝ่าย</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->
			
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who2019/5.aca/คุณพรลภัส%20%20ปัญญาเจริญสุข.jpg" alt="" 
                            height="150"/>
						<div class="opacity tran4s">
							<h4>คุณพรลภัส ปัญญาเจริญสุข (ภัส)</h4>
							<span>ผู้จัดการฝ่าย</span>
							<p>
								เบอร์ต่อภายใน : 02-9332353 ต่อ 114<br />
                    			<%--โทรศัพท์มือถือ : -<br />--%>
                    			Email : <a href="mailto:pronrapat_trd@94coffee.com">pronrapat_trd@94coffee.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณพรลภัส ปัญญาเจริญสุข (ภัส)</h5>
						<p>ผู้จัดการฝ่าย</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->
			
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who2019/5.aca/คุณวิษณุพงษ์%20%20ลิ่วประโคน.jpg" alt="" 
                            height="150"/>
						<div class="opacity tran4s">
							<h4>คุณวิษณุพงษ์ ลิ่วประโคน (อิน)</h4>
							<span>ผู้ช่วยผู้จัดการแผนก</span>
							<p>
								เบอร์ต่อภายใน : 02-9332353 ต่อ 114<br />
                    			<%--โทรศัพท์มือถือ : -<br />--%>
                    			Email : <a href="mailto:wissanupongl@aromathailand.com">wissanupongl@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณวิษณุพงษ์ ลิ่วประโคน (อิน)</h5>
						<p>ผู้ช่วยผู้จัดการแผนก</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->
			
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who2019/5.aca/คุณฐิติวัฒน์%20%20สหัสพันธ์.jpg" alt="" 
                            height="150"/>
						<div class="opacity tran4s">
							<h4>คุณฐิติวัฒน์ สหัสพันธ์ (จ๊อบ)</h4>
							<span>ผู้ช่วยผู้จัดการแผนก</span>
							<p>
								เบอร์ต่อภายใน : 02-9332353 ต่อ 114<br />
                    			<%--โทรศัพท์มือถือ : -<br />--%>
                    			Email : <a href="mailto:thitiwats@aromathailand.com">thitiwats@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณฐิติวัฒน์ สหัสพันธ์ (จ๊อบ)</h5>
						<p>ผู้ช่วยผู้จัดการแผนก</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->
			
		
		</div> <!-- /.team-member-wrapper -->
		
		<div class="blog-category-bt">
			<div class="btn-group">
				<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
			</div>
		</div>
		
	</div> <!-- /.conatiner -->
</section>
 
</asp:Content>
