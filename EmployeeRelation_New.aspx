﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="EmployeeRelation_New.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_Contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/hr.jpg" alt=""/></div>
<!-- end banner -->

<section id="team-section">
	<div class="container">
		<div class="theme-title">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
            </ul>
			<h2>พนักงานเข้าใหม่</h2>
			<!--p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p-->
		</div> <!-- /.theme-title -->

		<div class="clear-fix team-member-wrapper">
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="Images/HR/1.ศิริพร-นิลกำแหง.jpg" alt=""/>
						<div class="opacity tran4s">
							<h4>คุณศิริพร  นิลกำแหง (น้อง)</h4>
							<span>ผู้อำนวยการฝ่ายทรัพยากรมนุษย์</span>
							<p>
								เบอร์ต่อภายใน : 404<br />
                    			<%--โทรศัพท์มือถือ : -<br />--%>
                    			Email : <a href="mailto:siripornn@aromathailand.com">siripornn@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณศิริพร  นิลกำแหง (น้อง)</h5>
						<p>ผู้อำนวยการฝ่ายทรัพยากรมนุษย์</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->

			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="Images/HR/2.กันตพัฒน์-ตังพิพัฒน์ชัย-4.jpg" alt=""/>
						<div class="opacity tran4s">
							<h4>คุณกันตพัฒน์ ตังพิพัฒน์ชัย (ชัช)</h4>
							<span>ผู้จัดการฝ่ายทรัพยากรมนุษย์</span>
							<p>
								เบอร์ต่อภายใน : 400<br />
                    			โทรศัพท์มือถือ : 085-4802750<br />
                    			Email : <a href="mailto:kantapatt@aromathailand.com">kantapatt@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณกันตพัฒน์ ตังพิพัฒน์ชัย (ชัช)</h5>
						<p>ผู้จัดการฝ่ายทรัพยากรมนุษย์</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->

			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="Images/HR/1161004%20วรรณพร%20%20โลหะวิจารณ์.jpg" alt="" height="300" 
                            width="300"/>
						<div class="opacity tran4s">
							<h4>คุณวรรณพร  โลหะวิจารณ์ (ชมพู่)</h4>
							<span>ผู้จัดการฝ่ายพัฒนาองค์กร</span>
							<p>
								เบอร์ต่อภายใน : 416<br />
                    			<%--โทรศัพท์มือถือ : -<br />--%>
                    			Email : <a href="mailto:wannapornloh@aromathailand.com">wannapornloh@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณวรรณพร  โลหะวิจารณ์ (ชมพู่)</h5>
						<p>ผู้จัดการฝ่ายพัฒนาองค์กร</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->
			
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="Images/HR/4.จิรวัฒน์-หงอกสิมมา.jpg" alt=""/>
						<div class="opacity tran4s">
							<h4>คุณจิรวัฒน์ หงอกสิมมา (เอ)</h4>
							<span>ผู้จัดการแผนกสรรหาว่าจ้าง-จัดการงานสรรหาว่าจ้าง และผลประโยชน์ตอบแทน</span>
							<p>
								เบอร์ต่อภายใน : 514<br />
                        		<%--โทรศัพท์มือถือ : 081-6164697<br />--%>
                        		Email : <a href="mailto:jirawatn@aromathailand.com">jirawatn@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณจิรวัฒน์ หงอกสิมมา (เอ)</h5>
						<p>ผู้จัดการแผนกสรรหาว่าจ้าง-จัดการงานสรรหาว่าจ้าง<br />และผลประโยชน์ตอบแทน</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->
			
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="Images/HR/5.พิสมัย-อังโชคชัชวาล.jpg" alt=""/>
						<div class="opacity tran4s">
							<h4>คุณพิสมัย อังโชคชัชวาล (ไหม)</h4>
							<span>ผู้จัดการแผนก แผนกฝึกอบรมและแรงงานสัมพันธ์-จัดการงานแรงงานสัมพันธ์และฝึกอบรม</span>
							<p>
								เบอร์ต่อภายใน : 518<br />
                        		<%--โทรศัพท์มือถือ : 086-4085344<br />--%>
                        		Email : <a href="mailto:pissamaia@aromathailand.com">pissamaia@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณพิสมัย อังโชคชัชวาล (ไหม)</h5>
						<p>ผู้จัดการแผนก แผนกฝึกอบรมและแรงงานสัมพันธ์-จัดการงานแรงงานสัมพันธ์และฝึกอบรม</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->
			
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="Images/HR/6.ปราณี-บุญแท้แท้.jpg" alt=""/>
						<div class="opacity tran4s">
							<h4>คุณปราณี บุญแท้แท้ (ปลา)</h4>
							<span>เจ้าหน้าที่อาวุโส แผนกผลประโยชน์ตอบแทน-พนักงานสังกัด KVN</span>
							<p>
								เบอร์ต่อภายใน : 401<br />
                        		<%--โทรศัพท์มือถือ : -<br />--%>
                        		Email : <a href="mailto:praneeb@aromathailand.com">praneeb@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณปราณี บุญแท้แท้ (ปลา)</h5>
						<p>เจ้าหน้าที่อาวุโส แผนกผลประโยชน์ตอบแทน-พนักงานสังกัด KVN</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->
			
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="Images/HR/7.ปรียาภัทร์-ไชยเดช.jpg" alt=""/>
						<div class="opacity tran4s">
							<h4>คุณปรียาภัทร์ ไชยเดช (ออย)</h4>
							<span>เจ้าหน้าที่ แผนกผลประโยชน์ตอบแทน-พนักงานสังกัด AFF, UBP</span>
							<p>
								เบอร์ต่อภายใน : 402<br />
                        		<%--โทรศัพท์มือถือ : 085-0189583<br />--%>
                        		Email : <a href="mailto:preeyapatc@aromathailand.com">preeyapatc@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณปรียาภัทร์ ไชยเดช (ออย)</h5>
						<p>เจ้าหน้าที่ แผนกผลประโยชน์ตอบแทน-พนักงานสังกัด AFF, UBP</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->
			
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="Images/HR/8.เอนก-แซ่ฉั่ว.jpg" alt=""/>
						<div class="opacity tran4s">
							<h4>คุณเอนก แซ่ฉั่ว (ตูน)</h4>
							<span>เจ้าหน้าที่ แผนกผลประโยชน์ตอบแทน-พนักงานสังกัด FG, BE RICH</span>
							<p>
								เบอร์ต่อภายใน : 402<br />
                        		<%--โทรศัพท์มือถือ : 080-2050375<br />--%>
                        		Email : <a href="mailto:aneks@aromathailand.com">aneks@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณเอนก แซ่ฉั่ว (ตูน)</h5>
						<p>เจ้าหน้าที่ แผนกผลประโยชน์ตอบแทน-พนักงานสังกัด FG, BE RICH</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->
			
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="Images/HR/9.พงษ์พันธ์-กระแสร์.jpg" alt=""/>
						<div class="opacity tran4s">
							<h4>คุณพงษ์พันธ์ กระแสร์ (เอส)</h4>
							<span>เจ้าหน้าที่ แผนกฝึกอบรมและแรงงานสัมพันธ์-งานแรงงานสัมพันธ์</span>
							<p>
								เบอร์ต่อภายใน : 516<br />
                        		<%--โทรศัพท์มือถือ : 089-8100931<br />--%>
                        		Email : <a href="mailto:phongphunk@aromathailand.com">phongphunk@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณพงษ์พันธ์ กระแสร์ (เอส)</h5>
						<p>เจ้าหน้าที่ แผนกฝึกอบรมและแรงงานสัมพันธ์-งานแรงงานสัมพันธ์</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->
			
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="Images/HR/10.อัจฉรา-บุตรสิงห์.jpg" alt=""/>
						<div class="opacity tran4s">
							<h4>คุณอัจฉรา บุตรสิงห์ (ตูน)</h4>
							<span>เจ้าหน้าที่ แผนกฝึกอบรมและแรงงานสัมพันธ์-งานพัฒนาและฝึกอบรม</span>
							<p>
								เบอร์ต่อภายใน : 517<br />
                        		<%--โทรศัพท์มือถือ : 081-6164697<br />--%>
                        		Email : <a href="mailto:attcharab@aromathailand.com">attcharab@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณอัจฉรา บุตรสิงห์ (ตูน)</h5>
						<p>เจ้าหน้าที่ แผนกฝึกอบรมและแรงงานสัมพันธ์-งานพัฒนาและฝึกอบรม</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->
			
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="Images/HR/11.บริพัณฑ์-มโนธรรม.jpg" alt=""/>
						<div class="opacity tran4s">
							<h4>คุณบริพัณฑ์ มโนธรรม (เบิร์ด)</h4>
							<span>เจ้าหน้าที่สรรหาว่าจ้าง</span>
							<p>
								เบอร์ต่อภายใน : 514<br />
                        		<%--โทรศัพท์มือถือ : -<br />--%>
                        		Email : <a href="mailto:boriphanman@aromathailand.com">boriphanman@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณบริพัณฑ์ มโนธรรม (เบิร์ด)</h5>
						<p>เจ้าหน้าที่สรรหาว่าจ้าง</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->

            <div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="Images/HR/1161003 ศิรินทรา  มาลี 2.JPG" alt="" height="300" 
                            width="300"/>
						<div class="opacity tran4s">
							<h4>คุณศิรินทรา  มาลี (แนน)</h4>
							<span>เจ้าหน้าที่ผลประโยชน์ตอบแทน-พนักงานสังกัด  UBP , BE RICH</span>
							<p>
								เบอร์ต่อภายใน : 401<br />
                        		<%--โทรศัพท์มือถือ : -<br />--%>
                        		Email : <a href="mailto:sirinthramal@aromathailand.com">sirinthramal@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณศิรินทรา  มาลี (แนน)</h5>
						<p>เจ้าหน้าที่ผลประโยชน์ตอบแทน-พนักงานสังกัด  UBP , BE RICH</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->
			
		</div> <!-- /.team-member-wrapper -->
	</div> <!-- /.conatiner -->
</section>
 
</asp:Content>
