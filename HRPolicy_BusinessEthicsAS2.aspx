﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_BusinessEthicsAS2.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_BusinessEthicsAS2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/hr.jpg" alt=""/></div>
<!-- end banner -->

<article class="blog-details-page">
	<div class="container">
		
		<div class="post-heading">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
                <li>/</li>
                <li><a href="HRPolicy_Announce.aspx">ระเบียบต่างๆ</a></li>
                <li>/</li>
                <li><a href="HRPolicy_BusinessEthicsAS.aspx">ประกาศ คำสั่ง ระเบียบเกี่ยวกับทรัพย์สิน</a></li>
            </ul>
			<h2>ระเบียบเรื่องการเบิก และใช้ทรัพย์สินบริษัท</h2>
		</div> <!-- /.post-heading -->
		
		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix">
				<div class="blog-details-post-wrapper">
				
					<img src="new/images/banner/hr-policy/img14.jpg" alt=""/>
					
					<p style="padding: 30px 0 0;">
         		    	ตามที่บริษัทฯ ได้มีอุปกรณ์เครื่องมือ/ทรัพย์สินบริษัท เพื่ออำนวยความสะดวกในการปฏิบัติงานสำหรับพนักงานบางตำแหน่งงาน ดังนั้นเพื่อให้มีความเป็นระเบียบเรียบร้อยในการเบิกจ่าย และส่งมอบคืนทรัพย์สินบริษัท ทางกลุ่มบริษัท อโรม่า จึงเห็นสมควรกำหนด และใคร่ขอความร่วมมือมายังพนักงานทุกท่าน ในการปฏิบัติอย่างเคร่งครัด ดังนี้
					</p>
					
					<ol style="margin: 10px 0;">
						<li>พนักงานผู้มีสิทธิในการเบิกจ่าย/ส่งมอบคืน อุปกรณ์เครื่องมือ/ทรัพย์สินบริษัทฯ เฉพาะพนักงานกลุ่มบริษัท อโรม่า ซึ่งได้รับมอบหมายจากบริษัท หรือผู้บังคับบัญชาต้นสังกัด เท่านั้น</li>
						<li>พนักงานผู้ที่ได้รับสิทธิจะต้องใช้อุปกรณ์เครื่องมือ/ทรัพย์สินบริษัทฯ เฉพาะในงานของกลุ่มบริษัท อโรม่า ซึ่งได้รับมอบหมายงานจากบริษัท หรือผู้บังคับบัญชาต้นสังกัด เท่านั้น หากพบว่า
					 การใช้ไม่เป็นไปตามวัตถุประสงค์ข้าง ต้น บริษัทฯ ขอสงวนไว้ซึ่งสิทธิ ที่จะขอตัดสิทธิในการใช้อุปกรณ์เครื่องมือ/ทรัพย์สินบริษัทฯ รวมทั้งพนักงานจะต้องส่งมอบคืนบริษัทในสภาพ เรียบร้อยและครบถ้วนสมบูรณ์ทันที</li>
						<li>ตามข้อ 2.กรณีเกิดความเสียหายเนื่องจากการใช้ไม่เป็นไปตามวัตถุประสงค์ พนักงานผู้ที่ใช้จะต้องรับผิดชอบค่าใช้จ่ายที่เกิดขึ้นเองทั้งหมด</li>
						<li>การเบิกจ่าย/ส่งมอบคืนอุปกรณ์เครื่องมือ/ทรัพย์สินบริษัทฯ ทุกครั้ง พนักงานผู้ได้รับสิทธิจะต้องเป็นผู้ดำเนินการเขียนรายละเอียดลงในแบบฟอร์มเบิกจ่าย/ส่งคืนอุปกรณ์เครื่องมือ/ทรัพย์สิน ตามที่บริษัทกำหนดเป็นลายลักษณ์อักษรด้วยตนเอง เสร็จเสนออนุมัติต่อกรรมการบริหาร โดยผ่านผู้บังคับบัญชาต้นสังกัดเห็นชอบ ฝ่ายทรัพยากรมนุษย์ตรวจสอบ และเบิกจ่ายกับแผนกงาน ที่ได้รับมอบหมายในการรับผิดชอบดูแลทรัพย์สินนั้นๆ เช่น อุปกรณ์คอมพิวเตอร์เบิกที่แผนกเทคโนโลยีสารสนเทศ การเบิกจ่ายยานยนต์ที่แผนกยานยนต์ เป็นต้น</li>
						<li>พนักงานที่พ้นสภาพการเป็นพนักงาน หมดภาระหน้าที่รับผิดชอบปัจจุบัน หรือการใช้ไม่เป็นไปตามข้อ 2. พนักงานผู้ได้รับสิทธิใช้นั้นๆ จะต้องส่งมอบคืนอุปกรณ์เครื่องมือ/ทรัพย์สินบริษัท โดยจะต้องดำเนินการเขียนเอกสาร และเสนออนุมัติตามข้อ 4. เสร็จ ให้พนักงานส่งมอบคืนกับแผนกงานที่ได้รับมอบหมายในการรับผิดชอบดูแลทรัพย์สินนั้นๆ ในสภาพที่เรียบร้อย และครบถ้วนสมบูรณ์ในวันที่สิ้นสุดนั้นๆ กรณีส่งมอบคืนไม่ครบ ถ้วนสมบูรณ์ บริษัทฯ อาจจะพิจารณาเรียกร้องค่าใช้จ่ายเกี่ยวกับอุปกรณ์เครื่องมือ/ทรัพย์สินไม่ครบถ้วนสมบูรณ์เป็นกรณีๆ ไป	ตามความเหมาะสม</li>
						<li>บริษัทฯ ขอสงวนไว้ซึ่งสิทธิที่จะยกเลิก แก้ไข เปลี่ยนแปลงระเบียบการเบิกจ่าย/ส่งมอบคืน อุปกรณ์เครื่องมือ/ทรัพย์สินบริษัท พนักงานผู้ได้รับสิทธิได้ตามเห็นสมควร โดยจะแจ้งให้ทราบเป็นคราวๆ ไป</li>
					</ol>
					
					<p><br>
						ทั้งนี้ ให้ปฏิบัติอย่างเคร่งครัดนับตั้งแต่วันที่เบิกอุปกรณ์เครื่องมือ/ทรัพย์สินบริษัท เป็นต้นไป<br><br>

						จึงประกาศมาเพื่อทราบโดยทั่วกัน <br><br>
		
						ประกาศ ณ วันที่ 21 มีนาคม 2550 
					</p>
					
					<div class="blog-details-bt">
						<div class="btn-group">
							<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
						</div>
					</div>
				
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-8 col-xs-12 p-fix">
				<aside>
					<div class="sidebar-news-list">
						<h4>CATEGORY</h4>
						<ul>
							<li>
								<a href="HRPolicy_BusinessEthicsHR.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบทรัพยากรมนุษย์</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAS.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบเกี่ยวกับทรัพย์สิน</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAC.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบเกี่ยวกับบัญชี การเงิน งบประมาณ จัดซื้อ</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsIT.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบเกี่ยวกับเทคโนโลยีสารสนเทศ</a>
							</li>
							<li>
								<a href="Benefits1_1.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบอื่นๆ</a>
							</li>
						</ul>
					</div>
				</aside>
				<aside>
					<div class="sidebar-news-list">
						<h4>Related Links</h4>
						<ul>
							<li>
								<a href="HRPolicy_BusinessEthicsAS1.aspx" class="tran3s">
									ระเบียบการใช้บัตรน้ำมัน Synergy Card Fleet Card บัตรน้ำมัน
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAS2.aspx" class="tran3s">
									ระเบียบเรื่องการเบิก และใช้ทรัพย์สินบริษัท
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAS3.aspx" class="tran3s">
									ระเบียบการใช้โทรศัพท์เคลื่อนที่ (APPLE IPHONE 7128)
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAS4.aspx" class="tran3s">
									ระเบียบการใช้โทรศัพท์เคลื่อนที่ (Samsung Galaxy A9 Pro)
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAS5.aspx" class="tran3s">
									ระเบียบการใช้รถที่บริษัทเช่าให้พนักงานใช้ปฏิบัติงาน
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAS6.aspx" class="tran3s">
									ระเบียบเงินประกันชุดฟอร์มพนักงาน
								</a>
							</li>
						</ul>
					</div>
				</aside>
			</div>
		</div>
		
	</div>
</article>

</asp:Content>
