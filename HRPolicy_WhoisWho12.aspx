﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_WhoisWho12.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_Contact" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/HR.jpg" alt=""/></div>
<!-- end banner -->

<section id="team-section">
	<div class="container">
		<div class="theme-title">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
                <li>/</li>
                <li><a href="HRPolicy_WhoisWho.aspx">ติดต่อฝ่าย-แผนกต่างๆ</a></li>
            </ul>
			<h2>รายชื่อติดต่อ-ไลอ้อน ทรี-สตาร์ (Lion 3 star)</h2>
			<!--p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p-->
		</div> <!-- /.theme-title -->

		<div class="clear-fix team-member-wrapper">
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who2019/6.lion/คุณรวินันท์%20%20อัครพลกุลธร.JPG" alt="" 
                            height="150"/>
						<div class="opacity tran4s">
							<h4>คุณรวินันท์ อัครพลกุลธร (หมวย)</h4>
							<span>ผู้ช่วยกรรมการผู้จัดการ</span>
							<p>
								เบอร์ต่อภายใน : 141<br />
                    			<%--โทรศัพท์มือถือ : -<br />--%>
                    			Email : <a href="mailto:rawinana@aromathailand.com">rawinana@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณรวินันท์ อัครพลกุลธร (หมวย)</h5>
						<p>ผู้ช่วยกรรมการผู้จัดการ</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->

            <div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who2019/6.lion/คุณประเสริฐ%20%20ศิริวัฒนาโรจน์.jpg" alt="" 
                            height="150"/>
						<div class="opacity tran4s">
							<h4>คุณประเสริฐ ศิริวัฒนาโรจน์ (เปี๊ยก)</h4>
							<span>ผู้จัดการฝ่าย</span>
							<p>
								เบอร์ต่อภายใน : 02-9332354 ต่อ 501<br />
                    			<%--โทรศัพท์มือถือ : -<br />--%>
                    			Email : <a href="mailto:praserts@aromathailand.com">praserts@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณประเสริฐ ศิริวัฒนาโรจน์ (เปี๊ยก)</h5>
						<p>ผู้จัดการฝ่าย</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->
			
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who2019/6.lion/คุณวัชชนันท์%20%20ธนเจริญวัชร.jpg" alt="" 
                            height="150"/>
						<div class="opacity tran4s">
							<h4>คุณวัชชนันท์  ธนเจริญวัชร (ซัน)</h4>
							<span>ผู้จัดการฝ่าย</span>
							<p>
								เบอร์ต่อภายใน : -<br />
                    			<%--โทรศัพท์มือถือ : -<br />--%>
                    			Email : <a href="mailto:watchanuntan@aromathailand.com">watchanuntan@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณวัชชนันท์  ธนเจริญวัชร (ซัน)</h5>
						<p>ผู้จัดการฝ่าย</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->

            <div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who2019/6.lion/คุณชนวีร์%20%20จองเดิม.JPG" alt="" 
                            height="150"/>
						<div class="opacity tran4s">
							<h4>คุณชนวีร์ จองเดิม (วิว)</h4>
							<span>ผู้จัดการฝ่าย</span>
							<p>
								เบอร์ต่อภายใน : 02-9332354 ต่อ 113<br />
                    			<%--โทรศัพท์มือถือ : -<br />--%>
                    			Email : <a href="mailto:chonaweej@aromathailand.com">chonaweej@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณชนวีร์ จองเดิม (วิว)</h5>
						<p>ผู้จัดการฝ่าย</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->
			

			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who2019/6.lion/คุณสมใจ%20%20เพชรหนู.JPG" alt="" 
                            height="150"/>
						<div class="opacity tran4s">
							<h4>คุณสมใจ เพชรหนู (วี)</h4>
							<span>ผู้จัดการแผนก</span>
							<p>
								เบอร์ต่อภายใน : 02-9332354 ต่อ 306<br />
                    			<%--โทรศัพท์มือถือ : -<br />--%>
                    			Email : <a href="mailto:somjaip@aromathailand.com">somjaip@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณสมใจ เพชรหนู (วี)</h5>
						<p>ผู้จัดการแผนก</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->
			
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who2019/6.lion/คุณณัฐวรรธน์%20%20สิงหเศรษฐกิจ.JPG" alt="" 
                            height="150"/>
						<div class="opacity tran4s">
							<h4>คุณณัฐวรรธน์ สิงหเศรษฐกิจ (ก๊อต)</h4>
							<span>ผู้ช่วยผู้จัดการแผนก</span>
							<p>
								เบอร์ต่อภายใน : 02-9332354 ต่อ 307-308<br />
                    			<%--โทรศัพท์มือถือ : -<br />--%>
                    			Email : <a href="mailto:nattawats@aromathailand.com">nattawats@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณณัฐวรรธน์ สิงหเศรษฐกิจ (ก๊อต)</h5>
						<p>ผู้ช่วยผู้จัดการแผนก</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->

			
			<div class="float-left">
				<div class="single-team-member">
					<div class="img">
						<img src="new/images/who2019/6.lion/คุณสุพรรษา%20%20สุขสวัสดิ์.JPG" alt="" 
                            height="150"/>
						<div class="opacity tran4s">
							<h4>คุณสุพรรษา สุขสวัสดิ์ (ต้อย)</h4>
							<span>หัวหน้างาน</span>
							<p>
								เบอร์ต่อภายใน : 02-9332354 ต่อ 303<br />
                    			<%--โทรศัพท์มือถือ : -<br />--%>
                    			Email : <a href="mailto:supansas@aromathailand.com">supansas@aromathailand.com</a>
							</p>
						</div>
					</div> <!-- /.img -->
					<div class="member-name">
						<h5>คุณสุพรรษา สุขสวัสดิ์ (ต้อย)</h5>
						<p>หัวหน้างาน</p>
						<ul>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-pinterest"></i></a></li>
							<li><a href="#" class="tran3s round-border"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div> <!-- /.member-name -->
				</div> <!-- /.single-team-member -->
			</div> <!-- /float-left -->
			
			

		</div> <!-- /.team-member-wrapper -->
		
		<div class="blog-category-bt">
			<div class="btn-group">
				<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
			</div>
		</div>
		
	</div> <!-- /.conatiner -->
</section>
 
</asp:Content>
