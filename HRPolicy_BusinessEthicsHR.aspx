﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_BusinessEthicsHR1.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_BusinessEthicsHR1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<style>
	.swiper-container {
		width: 100%;
		height: 100%;
	}
	.swiper-slide {
		text-align: center;
		background: #fff;
	}
	.swiper-slide img 
	{
		width: 100%;
	}
	.swiper-pagination-bullet-active {
		opacity: 1;
		background: #af2f2f;
	}
</style>

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/hr.jpg" alt=""/></div>
<!-- end banner -->

<section id="about-us" class="blog-category">
<div class="container">
	<div class="theme-title tt-edit category-title">
        <ul>
            <li><a href="index_hr.aspx">HOME</a></li>
            <li>/</li>
            <li><a href="HRPolicy_Announce.aspx">ระเบียบต่างๆ</a></li>
        </ul>
		<h2>ประกาศ คำสั่ง ระเบียบทรัพยากรมนุษย์</h2>
	</div> <!-- /.theme-title -->

	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-085-id-card-2"></i>
				</div>
				<p><a href="HRPolicy_BusinessEthicsHR1.aspx" class="tran3s">การคล้อง ติดบัตรพนักงาน</a></p>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-021-transport-1"></i>
				</div>
				<p><a href="HRPolicy_BusinessEthicsHR2.aspx" class="tran3s">การลาพักผ่อนประจำปี</a></p>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-106-businessman"></i>
				</div>
				<p><a href="HRPolicy_BusinessEthicsHR3.aspx" class="tran3s">การแต่งกายพนักงานขาย (พีซี)</a></p>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-115-square"></i>
				</div>
				<p><a href="HRPolicy_BusinessEthicsHR4.aspx" class="tran3s">การลงนามอนุมัติเกินอำนาจดำเนินการ หรือความเสียหายจากการปฏิบัติหน้าที่</a></p>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-083-house"></i>
				</div>
				<p><a href="HRPolicy_BusinessEthicsHR5.aspx" class="tran3s">ระเบียบบ้านพักพนักงาน</a></p>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-096-criminal"></i>
				</div>
				<p><a href="HRPolicy_BusinessEthicsHR6.aspx" class="tran3s">ระเบียบการลงโทษพนักงานทุจริต</a></p>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-093-time-1"></i>
				</div>
				<p><a href="HRPolicy_BusinessEthicsHR7.aspx" class="tran3s">ทดลองจัดวันเวลาทำงาน และวันหยุด<br>(เป็นกรณีพิเศษ) ทำงานวันจันทร์ - ศุกร์</a></p>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-038-people-4"></i>
				</div>
				<p><a href="HRPolicy_BusinessEthicsHR8.aspx" class="tran3s">ระเบียบการแต่งกายพนักงาน<br>ประจำสำนักงาน</a></p>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->

        <div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-038-people-4"></i>
				</div>
				<p>
                <%--<a href="HRPolicy_BusinessEthicsHR5.aspx" class="tran3s">ประกาศแก้ไขเกษียณอายุ <br>มีผล 1 ม.ค.61</a>--%>

                  <a href="Document/HR Policy/Announce/ประกาศ 156-60 ประกาศเรื่อง แก้ไขการเกษียณอายุ KVN-LION.pdf" target="_blank" class="tran3s">ประกาศแก้ไขเกษียณอายุ <br>มีผล 1 ม.ค.61</a>

                </p>

              


			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->

        	<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-106-businessman"></i>
				</div>
				<p>
                <%--<a href="HRPolicy_BusinessEthicsHR3.aspx" class="tran3s">เกณฑ์พิจารณาต่อ/<br>ไม่ต่อเกษียณพนักงาน</a>--%>
                <a href="Document/HR Policy/Announce/ที่ H100-139-60 เรื่องขออนุมัติเกณฑ์พิจารณาต่อ-ไม่ต่อเกษียณพนักงาน  KVN-LION.pdf" target="_blank" class="tran3s">เกณฑ์พิจารณาต่อ/<br>ไม่ต่อเกษียณพนักงาน</a>
                
                </p>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->

        <div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-093-time-1"></i>
				</div>
				<p>
                
                <a href="Document/HRPolicy_BusinessEthicsHR/ประกาศ 088-56 ประกาศเรื่องระเบียบการมาทำงาน  และการมาทำงานสาย  (สำหรับพนักงานสำนักงาน) เริ่ม 21 ม.ค.57.pdf" target="_blank" class="tran3s">ประกาศ 088-56 ประกาศเรื่องระเบียบการมาทำงาน  และการมาทำงานสาย <br>(สำหรับพนักงานสำนักงาน) เริ่ม 21 ม.ค.57</a>
                <%--<a href="Document/HRPolicy_BusinessEthicsHR/ประกาศ 088-56 ประกาศเรื่องระเบียบการมาทำงาน  และการมาทำงานสาย  (สำหรับพนักงานสำนักงาน) เริ่ม 21 ม.ค.57.pdf" target="_blank">มาตรฐานของซอฟแวร์ที่ใช้ในองค์กร Rev.1</a>
                --%>
                </p>


			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->


	</div> <!-- /.row -->
	<div class="blog-category-bt">
		<div class="btn-group">
			<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
		</div>
	</div>
</div> <!-- /.container -->
</section>
    
<!-- Swiper JS -->
<script src="new/js/swiper.min.js"></script>

<!-- Initialize Swiper -->
<script>
	var swiper1 = new Swiper('.swiper1', {
		spaceBetween: 30,
		pagination: {
        el: '.swiper-pagination1',
        clickable: true,
      },
    });
</script>

</asp:Content>
