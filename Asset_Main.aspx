﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_Asset.Master" CodeBehind="Asset_Main.aspx.vb" Inherits="Aroma_HRPortal.Asset_Main" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 

    <style type="text/css">
        .style1
        {
        }
        .style2
        {}
        .fn_Content
        {}
    .style4
    {
        height: 20px;
        }
    .style5
    {
        }
    .style6
    {
        width: 22px;
    }
    .style7
    {
        height: 20px;
        width: 22px;
    }
        .style8
        {
            width: 196px;
            height: 29px;
        }
        .style16
        {
            width: 137px;
        }
        .style18
        {
            height: 20px;
            width: 94px;
        }
        .style20
        {
            width: 94px;
        }
        .style23
        {
            height: 20px;
        }
        </style>


</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 
                    <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.min.js"></script>
                    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.16/jquery-ui.min.js"></script>
                    <link type="text/css" rel="Stylesheet" href="http://ajax.microsoft.com/ajax/jquery.ui/1.8.16/themes/redmond/jquery-ui.css" />

                    <script type="text/javascript">
                        //ประกาศตัวแปรเพื่อรับใหม่
                        $jq = $.noConflict();
                        $jq().ready(
                 function () {
                     $jq('#<%=txtDate.ClientID%>').datepicker(
                     {
                         showOn: 'button',
                         buttonText: 'Show Date',
                         buttonImageOnly: true,
                         buttonImage: 'http://jqueryui.com/resources/demos/datepicker/images/calendar.gif',
                         dateFormat: 'dd/mm/yy',
                         constrainInput: true
                     }
                     );
                 });
        
                    </script>

                <script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
            <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/css/select2.min.css" rel="stylesheet" />
            <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/js/select2.min.js" type="text/javascript"></script>
            <script type="text/javascript">
                $(document).ready(function () {
                    $("#txtID").select2({
                    });
                });

                $(document).ready(function () {
                    $("#txtName").select2({
                    });
                });
            </script>

&nbsp;

<table class="TBBody" style="font-family: tahoma; font-size: small; width: 1283px;">
          
           

            <tr>
                <td align="right" class="style4">
                    <asp:Label ID="Label8" runat="server" CssClass="fn_ContentTital" 
                        Text="Employee ID :"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style1">


                    <asp:DropDownList ID="txtID" runat="server" Font-Size="Small" Width="250px" ClientIDMode="Static"
                        Font-Names="Tahoma" AutoPostBack="True">
                    </asp:DropDownList>


                    </td>
                <td align="left" valign="middle" class="style1">
                    &nbsp;</td>
                <td align="right" class="style7">
                    </td>
                <td align="right" class="style18">
                    <asp:Label ID="Label27" runat="server" CssClass="fn_ContentTital" 
                        Text="Name :"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style1">


                    <asp:DropDownList ID="txtName" runat="server" Font-Size="Small" Width="250px" ClientIDMode="Static"
                        Font-Names="Tahoma" AutoPostBack="True">
                    </asp:DropDownList>


                </td>
                <td align="left" class="style1">
                    &nbsp;
                </td>
            </tr>

          
      <%--      <tr>
                <td align="right" class="style4">
                    </td>
                
                 <td class="style1" colspan="3">
                     </td>
                
                <td align="right" class="style18">
                    </td>
                <td align="left" valign="middle" class="style1">
                    </td>
                <td align="left" class="style1">
                    </td>
            </tr>--%>
          
         
            <tr>
                <td align="right" class="style4">
                    <asp:Label ID="Label15" runat="server" CssClass="fn_ContentTital" 
                        Text="Department :"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style1">
                    <asp:TextBox ID="txtDept" runat="server" CssClass="fn_Content" Width="250px"></asp:TextBox>
                    </td>
                <td align="left" valign="middle" class="style1">
                    </td>
                <td align="left" class="style7">
                    </td>
                <td align="right" class="style18">
                    </td>
                <td align="left" valign="middle" class="style1">
                    <asp:Label ID="lblNum" runat="server" Text="0"></asp:Label>
                    </td>
                <td align="left" class="style1">
                    </td>
            </tr>
          
         
            <tr>
                <td align="right" class="style4">
                    <asp:Label ID="Label13" runat="server" CssClass="fn_ContentTital" 
                        Text="ประเภททรัพย์สิน :" ToolTip="ประเภททรัพย์สิน"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style4" colspan="2">
                    <asp:DropDownList ID="drpType" runat="server" Width="250px" AutoPostBack="True">
                    </asp:DropDownList>
                    </td>
                <td align="left" class="style7">
                    </td>
                <td align="right" class="style18">
                    </td>
                <td align="left" valign="middle" class="style4">
                    </td>
                <td align="left" class="style4">
                    </td>
            </tr>
          
         
            <tr>
                <td align="right" class="style4">
                    <asp:Label ID="Label24" runat="server" CssClass="fn_ContentTital" 
                        Text="ชื่ออุปกรณ์ :" ToolTip="ประเภททรัพย์สิน"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style4" colspan="2">
                    <asp:TextBox ID="txtAssetName" runat="server" Width="250px"></asp:TextBox>
                    </td>
                <td align="left" class="style7">
                    </td>
                <td align="right" class="style18">
                    </td>
                <td align="left" valign="middle" class="style4">
                    </td>
                <td align="left" class="style4">
                    </td>
            </tr>
          
         
         
            <tr>
                <td align="right" class="style4">
                    <asp:Label ID="Label19" runat="server" 
                        Text="Serial :" Width="87px"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style23" colspan="2">
                    <asp:TextBox ID="txtSerial" runat="server" Width="250px"></asp:TextBox>
                    </td>
                <td align="left" class="style7">
                    </td>
                <td align="right" class="style18">
                    </td>
                <td align="left" valign="middle" class="style1" rowspan="5">

                    <asp:Image ID="img" runat="server" ImageUrl="~/Images/Employee/noImage.jpg" 
                        Height="114px" Width="145px" BorderWidth="1px" />

                    </td>
                <td align="left" class="style23">
                    </td>
            </tr>
          
         
            <tr>
                <td align="right" class="style4">



                    <asp:Label ID="Label21" runat="server" 
                        Text="Asset :" Width="87px"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style23" colspan="2">

                    <asp:TextBox ID="txtAsset" runat="server" Width="250px"></asp:TextBox>
          
                

                    </td>
                <td align="left" class="style7">
                    </td>
                <td align="right" class="style18">



                    </td>
                <td align="left" class="style23">
                    </td>
            </tr>
          
         
            <tr>
                <td align="right" class="style4">



                    <asp:Label ID="Label25" runat="server" 
                        Text="MIS Asset :" Width="87px" Height="16px"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style23" colspan="2">

                    <asp:TextBox ID="txtMISAsset" runat="server" Width="250px"></asp:TextBox>
          
                

                    </td>
                <td align="left" class="style7">
                    </td>
                <td align="right" class="style18">



                    </td>
                <td align="left" class="style23">
                    </td>
            </tr>
          
         
            <tr>
                <td align="right" class="style4">



                    <asp:Label ID="Label26" runat="server" 
                        Text="Location :" Width="87px" Height="16px"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style23" colspan="2">

                    <asp:TextBox ID="txtLocation" runat="server" Width="250px"></asp:TextBox>
          
                

                    </td>
                <td align="left" class="style7">
                    &nbsp;</td>
                <td align="right" class="style18">



                    &nbsp;</td>
                <td align="left" class="style23">
                    &nbsp;</td>
            </tr>
          
         
            <tr>
                <td align="right" class="style4">



                    <asp:Label ID="lb" runat="server" 
                        Text="วันที่ซื้อมา :" Width="87px"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style1" colspan="2">

                    <asp:TextBox ID="txtDate" runat="server" Width="150px"></asp:TextBox>
          
                

                    </td>
                <td align="left" class="style7">
                    &nbsp;</td>
                <td align="right" class="style18">



                    &nbsp;</td>
                <td align="left" class="style1">
                    &nbsp;</td>
            </tr>
          
         
            <tr>
                <td align="right" class="style4">
                    <asp:Label ID="lb0" runat="server" 
                        Text="หมายเหตุ :" Width="87px"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style1" colspan="2" rowspan="2">
                    <asp:TextBox ID="txtRemark" runat="server" Width="346px" Height="47px" 
                        TextMode="MultiLine"></asp:TextBox>
          
                

                    </td>
                <td align="left" class="style7">
                    &nbsp;</td>
                <td align="right" class="style18">



                    <asp:Label ID="lb1" runat="server" 
                        Text="รูปภาพ : " Width="87px" Visible="False"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style1">

                            <asp:FileUpload ID="FileUpload1" runat="server" CssClass="sample" 
                                style="font-family: Tahoma" Width="275px" />

                    </td>
                <td align="left" class="style1">
                            <asp:TextBox ID="txtPic" runat="server" BorderColor="White" BorderWidth="0px" 
                                Height="0px" Width="93px"></asp:TextBox>
                    </td>
            </tr>
          
         
            <tr>
                <td align="right" class="style4">
                    &nbsp;</td>
                <td align="left" class="style7">
                    &nbsp;</td>
                <td align="right" class="style18">



                    &nbsp;</td>
                <td align="left" valign="middle" class="style1">

                    &nbsp;</td>
                <td align="left" class="style1">
                    &nbsp;</td>
            </tr>
        
         
            <tr>
                <td align="right" class="style4">
                    &nbsp;</td>
                <td align="left" valign="middle" class="style1" colspan="2">
                    &nbsp;</td>
                <td align="left" class="style7">
                    </td>
                <td align="right" class="style18">
                    &nbsp;</td>
                <td align="left" class="style1">

                            &nbsp;</td>
                <td align="left" class="style1">
                    </td>
            </tr>
            <%--    <tr>
                <td align="right" class="style4">
                    &nbsp;</td>
                <td align="left" valign="middle" class="style15" colspan="2">
                    &nbsp;</td>
                <td align="left" class="style7">
                    </td>
                <td align="right" class="style18">
                    </td>
                <td align="left" class="style15">
                    </td>
                <td align="left" class="style15">
                    </td>
            </tr>--%>
               
          
          
       
            <tr>
                <td align="left" class="style4" colspan="3">
                    <%--   <asp:Button ID="btnAdd" runat="server" Text="Add" 
                    BackColor="#990000" Font-Bold="True" ForeColor="White" Height="30px" 
                    ToolTip="add" Width="100px" Font-Names="Tahoma" Font-Size="Medium" />--%>
                    <asp:Button ID="btnBase" runat="server" Text="Save" 
                    BackColor="#990000" Font-Bold="True" ForeColor="White" Height="30px" 
                    ToolTip="Save" Width="100px" Font-Names="Tahoma" Font-Size="Medium" />
                    <asp:Button ID="btnClear" runat="server" Text="Clear" 
                    BackColor="#990000" Font-Bold="True" ForeColor="White" Height="30px" 
                    ToolTip="ยกเลิก" Width="100px" Font-Names="Tahoma" Font-Size="Medium" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
                    BackColor="#990000" Font-Bold="True" ForeColor="White" Height="30px" 
                    ToolTip="ยกเลิก" Width="100px" Font-Names="Tahoma" Font-Size="Medium" />
                    </td>
                <td align="left" class="style7">
                    </td>
                <td align="right" class="style18">
                    <asp:Label ID="Label20" runat="server" CssClass="fn_ContentTital" 
                        Text="รุ่น :" Width="94px" Visible="False"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style1">
                    <asp:TextBox ID="txtModel" runat="server" Width="250px" Visible="False"></asp:TextBox>
                    </td>
                <td align="left" class="style1">
                    </td>
                <td align="left" class="style1">
                    </td>
            </tr>
            <tr>
                <td align="right" class="style5" colspan="8">
                  
                    <asp:DataGrid ID="DataGrid4" runat="server" AllowSorting="True" 
                        AutoGenerateColumns="False" CellPadding="4" DataKeyField="num" Font-Size="Small" 
                        ForeColor="Black" GridLines="Horizontal" Width="100%" BackColor="White" 
                        BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px">
                        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                        <SelectedItemStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                        <Columns>
                            <asp:EditCommandColumn CancelText="Cancel" EditText="Select" UpdateText="Update" > 
                            </asp:EditCommandColumn>
                                                        
                            <asp:BoundColumn DataField="num" HeaderText="No."></asp:BoundColumn>
                            <asp:BoundColumn DataField="type" HeaderText="type"></asp:BoundColumn>
                            <asp:BoundColumn DataField="AssetName" HeaderText="Asset Name"></asp:BoundColumn>
               <%--             <asp:BoundColumn DataField="brand" HeaderText="brand"></asp:BoundColumn>
                            <asp:BoundColumn DataField="model" HeaderText="model"></asp:BoundColumn>--%>
                            <asp:BoundColumn DataField="Serial" HeaderText="Serial"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Asset" HeaderText="Asset"></asp:BoundColumn>
                            <asp:BoundColumn DataField="MIS_asset" HeaderText="MIS_asset"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Location" HeaderText="Location"></asp:BoundColumn>
                            <asp:BoundColumn DataField="datePur" HeaderText="date"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Remark" HeaderText="Remark"></asp:BoundColumn>
                            
                          <asp:ButtonColumn CommandName="Delete" Text="Delete"></asp:ButtonColumn>     
                        </Columns>
                        <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                    </asp:DataGrid>
                    <br />
                    <br />
                    </td>
            </tr>
            <tr>
                <td align="left" class="style16" valign="middle">
                    &nbsp;</td>
                &nbsp;<td align="left" valign="middle" class="style16">
                    &nbsp;</td>
                &nbsp;<td align="left" valign="middle" class="style2">
                    &nbsp;</td>
                <td align="left" class="style6">
                    &nbsp;</td>
                <td align="right" class="style20">
                            &nbsp;</td>
                <td align="left" valign="middle">
                    &nbsp;</td>
                <td align="left" class="style8">
                    &nbsp;</td>
                <td align="left" class="style1">
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="left" class="style16" valign="middle">
                    &nbsp;</td>
                &nbsp;<td align="left" valign="middle" class="style16">
                    &nbsp;</td>
                &nbsp;<td align="left" valign="middle" class="style2">
                    &nbsp;</td>
                <td align="left" class="style6">
                    &nbsp;</td>
                <td align="right" class="style20">
                            &nbsp;</td>
                <td align="left" valign="middle">
                    &nbsp;</td>
                <td align="left" class="style8">
                    &nbsp;</td>
                <td align="left" class="style1">
                    &nbsp;</td>
            </tr>
        </table>


    

</asp:Content>
