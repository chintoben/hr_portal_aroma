﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_Authorize.Master" CodeBehind="A_IndexActivity.aspx.vb" Inherits="Aroma_HRPortal.A_IndexActivity" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            height: 24px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<br />
<br />

                 <div class="col-lg-12">
                <h3>
                    Activity&Semina Calenda (ปฏิทินกิจกรรม/สัมนา)</h3>
                
                </div>


<table class="TBBody" style="font-family: tahoma; font-size: small; width: 1283px;" 
        cellpadding="1" cellspacing="2">
          
         
            <tr>
                <td align="right" class="style4">
                    &nbsp;</td>
                <td align="left" valign="middle" class="style14">


                    <asp:Label ID="Label16" runat="server" CssClass="fn_ContentTital" 
                        Text="ID :"></asp:Label>


                    <asp:Label ID="lblNum" runat="server" Text="0"></asp:Label>


                    </td>
                <td align="left" rowspan="9">


                 <asp:TextBox ID="txtPic" runat="server" BorderColor="White" BorderWidth="0px" 
                                Height="0px" Width="16px" Visible="False"></asp:TextBox>


                    <asp:Image ID="img" runat="server" ImageUrl="~/Images/Employee/noImage.jpg" 
                        Height="200px" Width="400px" BorderWidth="1px" />


                </td>
                <td align="left" valign="middle" class="style1">


                    &nbsp;</td>
                <td align="left" class="style11">
                    &nbsp;</td>
            </tr>

         
            <tr>
                <td align="right" class="style4">
                    <asp:Label ID="Label18" runat="server" CssClass="fn_ContentTital" 
                        Text="* " 
                        ToolTip="ประเภททรัพย์สิน" ForeColor="Red"></asp:Label>
                    <asp:Label ID="Label8" runat="server" CssClass="fn_ContentTital" 
                        Text="ชื่อหัวข้อ :"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style14">


                    <asp:TextBox ID="txtSubject" runat="server" CssClass="fn_Content" Width="95%"></asp:TextBox>


                    </td>
                <td align="left" valign="middle" class="style1">


                    &nbsp;</td>
                <td align="left" class="style11">
                    &nbsp;
                    

                </td>
            </tr>


            <tr>
                <td align="right" class="style4">



                    <asp:Label ID="Label20" runat="server" CssClass="fn_ContentTital" 
                        Text="วันที่ :" Width="150px"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style14">


                    <asp:TextBox ID="txtDate" runat="server" CssClass="fn_Content" Width="200px"></asp:TextBox>


                    </td>
                <td align="left" valign="middle" class="style1">


                    &nbsp;</td>
                <td align="left" class="style11">
                    &nbsp;</td>
            </tr>


            <tr>
                <td align="right" class="style4">



                    <asp:Label ID="Label15" runat="server" CssClass="fn_ContentTital" 
                        Text="รายละเอียด :" Width="150px"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style14">


                    <asp:TextBox ID="txtDetail" runat="server" Width="95%" Height="70px" 
                        TextMode="MultiLine"></asp:TextBox>
          
                

                    </td>
                <td align="left" valign="middle" class="style1">


                    &nbsp;</td>
                <td align="left" class="style11">

                    &nbsp;</td>
            </tr>

     

            <tr>
                <td align="right" class="style4">



                    <asp:Label ID="lb2" runat="server" 
                        Text="รูปภาพ : "></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style14">


                            <asp:FileUpload ID="FileUpload1" runat="server" CssClass="sample" 
                                style="font-family: Tahoma" Width="275px" />


                            <br />


                    </td>
                <td align="left" valign="middle" class="style1">


                    &nbsp;</td>
                <td align="left" class="style11">

                    &nbsp;</td>
            </tr>

          

         
         
            <tr>
                <td align="right" class="fh5co-spacer-xxs">
                    <asp:Label ID="Label21" runat="server" CssClass="fn_ContentTital" 
                        Text="Link รูปภาพกิจกรรม" Width="150px"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="fh5co-spacer-xxs">


                    <asp:TextBox ID="txtLinkImage" runat="server" CssClass="fn_Content" 
                        Width="95%"></asp:TextBox>


                    </td>
                <td align="left" valign="middle" class="fh5co-spacer-xxs">

                            </td>
                <td align="left" class="fh5co-spacer-xxs">
                    </td>
            </tr>
          
         
          
         
         
            <tr>
                <td align="right" class="fh5co-spacer-xxs">
                    &nbsp;</td>
                <td align="left" valign="middle" class="fh5co-spacer-xxs">


                    <asp:Label ID="Label22" runat="server" CssClass="fn_ContentTital" 
                        Text="* Upload ผ่าน google Drive"></asp:Label>


                    </td>
                <td align="left" valign="middle" class="fh5co-spacer-xxs">

                            &nbsp;</td>
                <td align="left" class="fh5co-spacer-xxs">
                    &nbsp;</td>
            </tr>
          
         
          
         
            <tr>
                <td align="right" class="style1">
                    <asp:Label ID="Label19" runat="server" CssClass="fn_ContentTital" 
                        Text="เอกสาร :" Width="150px"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style1">
                 <asp:FileUpload ID="FileUpload2" runat="server" CssClass="sample" 
                                style="font-family: Tahoma" Width="340px" Font-Size="Small" />
                    </td>
                <td align="left" valign="middle" class="style1">

                            &nbsp;</td>
                <td align="left" class="style1">
                    &nbsp;</td>
            </tr>
          
         
            <tr>
                <td align="right" class="style4">
                    &nbsp;</td>
                <td align="left" valign="middle" class="style16">

                <asp:DataGrid ID="DataGrid1" runat="server" AllowSorting="True" 
                                AutoGenerateColumns="False" BorderColor="Aqua" CssClass="style98" 
                                DataKeyField="Att_ID" Font-Size="Small" Width="445px" 
                        Height="16px" PageSize="1">
                                <Columns>
                                    <asp:BoundColumn DataField="Att_ID" HeaderText="Id "></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Att_Filename" HeaderText="File Name "></asp:BoundColumn>
                                    <asp:ButtonColumn CommandName="Edit" Text="Download"></asp:ButtonColumn>
                                    <asp:ButtonColumn CommandName="Delete" Text="Delete"></asp:ButtonColumn>
                                </Columns>
                                <HeaderStyle BackColor="Aqua" BorderColor="Black" Font-Bold="True" 
                                    Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" 
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="Black" Wrap="False" />
                </asp:DataGrid>

                    <br />
                    </td>
                <td align="right" class="style4">
                    &nbsp;</td>
                <td align="left" valign="middle" class="style4">
                    </td>
                <td align="left" class="style12">
                    </td>
            </tr>
                 
         
            <tr>
                <td align="right" class="style2">
                    </td>
                <td align="left" valign="middle" class="style14">
                    <asp:Button ID="btnSave" runat="server" Text="Save" 
                    BackColor="#990000" Font-Bold="True" ForeColor="White" Height="30px" 
                    ToolTip="Save" Width="100px" Font-Names="Tahoma" Font-Size="Medium" />
                    <asp:Button ID="btnClear" runat="server" Text="Clear" 
                    BackColor="#990000" Font-Bold="True" ForeColor="White" Height="30px" 
                    ToolTip="ยกเลิก" Width="100px" Font-Names="Tahoma" Font-Size="Medium" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
                    BackColor="#990000" Font-Bold="True" ForeColor="White" Height="30px" 
                    ToolTip="ยกเลิก" Width="100px" Font-Names="Tahoma" Font-Size="Medium" />
                    </td>
                <td align="right">
                    </td>
                <td align="left" valign="middle" class="style2">
                    </td>
                <td align="left" class="style11">
                    </td>
            </tr>
                 
         
           
         
            <tr>
                <td align="right" class="style2">
                    &nbsp;</td>
                <td align="left" valign="middle" class="style14">
                    &nbsp;</td>
                <td align="right">
                    &nbsp;</td>
                <td align="left" valign="middle" class="style2">
                    &nbsp;</td>
                <td align="left" class="style11">
                    &nbsp;</td>
            </tr>
                 
         
           
         
            <tr>
                <td align="right" class="style2">
                    &nbsp;</td>
                <td align="left" valign="middle" class="style14">
                    &nbsp;</td>
                <td align="right">
                    &nbsp;</td>
                <td align="left" valign="middle" class="style2">
                    &nbsp;</td>
                <td align="left" class="style11">
                    &nbsp;</td>
            </tr>
                 
         
           
         
            <tr>
                <td align="right" class="style2">
                    &nbsp;</td>
                <td align="left" valign="middle" class="style14">
                    &nbsp;</td>
                <td align="right">
                    &nbsp;</td>
                <td align="left" valign="middle" class="style2">
                    &nbsp;</td>
                <td align="left" class="style11">
                    &nbsp;</td>
            </tr>
                 
         
           
         
            <tr>
                <td align="right" class="style2">
                    &nbsp;</td>
                <td align="left" valign="middle" class="style14">
                    &nbsp;</td>
                <td align="right">
                    &nbsp;</td>
                <td align="left" valign="middle" class="style2">
                    &nbsp;</td>
                <td align="left" class="style11">
                    &nbsp;</td>
            </tr>
                 
         
    
          
         
           
         
         
          
                    
        </table>




</asp:Content>
