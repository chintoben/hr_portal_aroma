﻿Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Data
Imports System.IO
Imports System.Configuration
Imports System.Collections
Imports System.Web.UI.Page
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
'Imports ICSharpCode.SharpZipLib.Zip
Imports System.Web.UI

Public Class A_IndexBanner
    Inherits System.Web.UI.Page
    Dim DB_HR As New Connect_HR
    Private sms As New PKMsg("")
    Dim dtEmp4 As New DataTable
    Dim sql, sqlCheck, ImageName As String
    Dim sql2 As String
    Dim dt, dtt, dt2 As New DataTable
    Dim dtEmp As New DataTable
    Dim status, Role As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Role = Request.QueryString("Role")

        If Not Page.IsPostBack Then
            loadGridview()
        End If


    End Sub


    Private Sub loadGridview(Optional ByVal sCond As String = "")

        sql = " SELECT IndexID,Type,TxtSubject,TxtDetail ,image ,image_name,Status ,CreateBy ,CreateDate	"
        sql += " FROM TB_IndexHome where type = 'Banner' "

        sql += " order by IndexID desc "

        dt = DB_HR.GetDataTable(sql)
        If dt.Rows.Count > 0 Then
            DataGrid4.Visible = True
            DataGrid4.DataSource = dt
            DataGrid4.DataBind()
        Else
            DataGrid4.Visible = False
        End If

        Session("selCond") = sCond

    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        ImageName = FileUpload1.FileName

        If txtSubject.Text = "" Then
            sms.Msg = "กรุณาระบุชื่อรูปภาพ"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If


        sqlCheck += " SELECT * FROM [TB_IndexHome] "
        sqlCheck += "  "
        dt = DB_HR.GetDataTable(sqlCheck)
        If dt.Rows.Count <= 0 Then

            If FileUpload1.FileName = "" Then
                sms.Msg = "กรุณาเลือกรูปภาพ"
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
                Exit Sub
            End If

            lblNum.Text = "1"
            insert()
        Else
            'Update
            If lblNum.Text <> "0" Then
                Update()
            Else

                If FileUpload1.FileName = "" Then
                    sms.Msg = "กรุณาเลือกรูปภาพ"
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
                    Exit Sub
                End If

                genNum()
                insert2()
            End If
        End If

        If FileUpload1.FileName <> "" Then
            Call insertIMG()
            insertLogo()  ' LOGO
        End If

        Clear()

    End Sub


    Sub genNum()
        Dim conn As New Connect_HR
        Dim newID As Integer
        Dim sql2 As String = "Select Max(IndexID) as IndexID from [TB_IndexHome] "

        dt2 = DB_HR.GetDataTable(sql2)
        If dt2.Rows.Count >= 0 Then
            newID = dt2.Rows(0)("IndexID")
        End If


        newID += 1
        lblNum.Text = newID

    End Sub

    Sub insert()

        If rdoStatus.Items.FindByValue("Active").Selected = True Then
            Status = "Active"
        ElseIf rdoStatus.Items.FindByValue("InActive").Selected = True Then
            Status = "InActive"
        Else
            status = "Active"
        End If

        Dim sql As String = "INSERT INTO [TB_IndexHome] (IndexID,Type,TxtSubject,TxtDetail ,Status ,CreateBy ,CreateDate )"
        sql &= "VALUES (1,'Banner','" & txtSubject.Text & "','" & txtDetail.Text & "','" & status & "'"
        sql &= " , '" & Role & "' , '" & Date.Now.ToString("yyyy-MM-dd") & "') "
        dtt = DB_HR.GetDataTable(sql)
        loadGridview()

    End Sub


    Sub insert2()

        If rdoStatus.Items.FindByValue("Active").Selected = True Then
            status = "Active"
        ElseIf rdoStatus.Items.FindByValue("InActive").Selected = True Then
            status = "InActive"
        Else
            status = "Active"
        End If

        Dim sql As String = "INSERT INTO [TB_IndexHome] (IndexID,Type,TxtSubject,TxtDetail ,Status ,CreateBy ,CreateDate )"
        sql &= "VALUES ('" & lblNum.Text & "','Banner','" & txtSubject.Text & "','" & txtDetail.Text & "','" & status & "'"
        sql &= " , '" & Role & "' , '" & Date.Now.ToString("yyyy-MM-dd") & "') "
        dtt = DB_HR.GetDataTable(sql)
        loadGridview()

    End Sub


    Sub Update()

        If rdoStatus.Items.FindByValue("Active").Selected = True Then
            status = "Active"
        ElseIf rdoStatus.Items.FindByValue("InActive").Selected = True Then
            status = "InActive"
        Else
            status = "InActive"
        End If

        Dim sSql As String = "UPDATE [TB_IndexHome] SET "
        sSql += " TxtSubject='" & txtSubject.Text & "' "
        sSql += " , TxtDetail='" & txtDetail.Text & "' "
        sSql += " , Status ='" & status & "'"
        sSql += " , UpdateBy ='" & Role & "'"
        sSql += " , ModifyDate='" & Date.Now.ToString("yyyy-MM-dd") & "' "
        sSql += " Where  IndexID = '" & lblNum.Text & "' "
        dtt = DB_HR.GetDataTable(sSql)
        loadGridview()

    End Sub


    Sub insertIMG()
        Dim CurrentFileName As String
        CurrentFileName = FileUpload1.FileName

        'If (Path.GetExtension(CurrentFileName).ToLower <> ".jpg") Then
        '    sms.Msg = "You choose the file dishonestly !!! , Please choose be file .jpg"
        '    Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        '    Exit Sub
        'End If

        'If FileUpload1.PostedFile.ContentLength > 131072 Then
        '    sms.Msg = "The size of big too file , the size of the file must 128 KB not exceed!!!"
        '    Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        '    Exit Sub
        'End If

        Dim CurrentPath As String = Server.MapPath("~/Temp/")

        'FileUpload1.PostedFile.SaveAs(CurrentPath & FileUpload1.FileName)
        FileUpload1.PostedFile.SaveAs("c:\temp\" & FileUpload1.FileName)
        ' img.ImageUrl = CurrentPath & FileUpload1.FileName
        txtPic.Text = "c:\temp\" & FileUpload1.FileName

    End Sub

    Sub insertLogo()
        Dim db As New Connect_HR
        Dim sql As String
        Dim tr As SqlTransaction
        Dim sqlcon As New SqlConnection(DB_HR.sqlCon)

        If sqlcon.State = ConnectionState.Closed Then
            sqlcon = New SqlConnection(DB_HR.sqlCon)
            sqlcon.Open()
        End If

        tr = sqlcon.BeginTransaction

        Try
            Dim cmd As SqlCommand = New SqlCommand(sql, sqlcon, tr)
            If txtPic.Text <> "" Then
                Dim FileN As FileStream
                Dim Ln As Int32
                Dim oBinaryReader As BinaryReader
                Dim oImgByteArray As Byte()

                FileN = New FileStream(txtPic.Text, FileMode.Open, FileAccess.Read)
                oBinaryReader = New BinaryReader(FileN)
                oImgByteArray = oBinaryReader.ReadBytes(CInt(FileN.Length))
                Ln = CInt(FileN.Length)
                oBinaryReader.Close()
                FileN.Close()

                sql = "update [TB_IndexHome] set image_name=@FF1,image=@FF2 where IndexID  = '" & lblNum.Text & "'"
                Dim cmd3 As SqlCommand = New SqlCommand(sql, sqlcon, tr)
                cmd3.Parameters.Add("@FF1", SqlDbType.VarChar, 0).Value = ImageName ' + Path.GetExtension(FileUpload1.FileName).ToLower
                cmd3.Parameters.Add("@FF2", SqlDbType.Image, 0).Value = oImgByteArray
                cmd3.CommandTimeout = 0
                cmd3.ExecuteNonQuery()
            End If

            tr.Commit()
            sqlcon.Close()
        Catch ex As Exception
            tr.Rollback()
            sqlcon.Close()
            Exit Sub
        End Try

    End Sub


    Protected Sub DataGrid4_DeleteCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid4.DeleteCommand
        Dim SelectID As String
        SelectID = Convert.ToString(DataGrid4.DataKeys(e.Item.ItemIndex))
        Dim s As Integer
        Dim ssql As String = "delete from [TB_IndexHome] where [IndexID] = '" & SelectID & "'  "
        s = DB_HR.Execute(ssql)
        Call loadGridview()
    End Sub


    Protected Sub DataGrid4_EditCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid4.EditCommand
        Dim SelectID As String
        SelectID = Convert.ToString(DataGrid4.DataKeys(e.Item.ItemIndex))

        'Clear image
        'Clear()

        sql = " SELECT IndexID,Type,TxtSubject,TxtDetail ,Status ,image ,image_name "
        sql += " from [TB_IndexHome]  "
        sql += " where IndexID like '%" & SelectID & "%'"

        dt = DB_HR.GetDataTable(sql)

        If dt.Rows.Count > 0 Then
            lblNum.Text = dt.Rows(0)("IndexID")
            txtSubject.Text = dt.Rows(0)("TxtSubject")
            txtDetail.Text = dt.Rows(0)("TxtDetail")

            If dt.Rows(0)("Status") = "Active" Then
                rdoStatus.Items.FindByValue("Active").Selected = True
            Else
                rdoStatus.Items.FindByValue("InActive").Selected = True
            End If

            If Not IsDBNull(dt.Rows(0)("image")) Then
                img.ImageUrl = ReadImage(dt.Rows(0)("IndexID"))
            End If

        End If

    End Sub

    Function ReadImage(strID As String)
        Return ("ReadimageBanner.aspx?ID=" & strID)

    End Function


    Sub Clear()
        lblNum.Text = "0"
        txtSubject.Text = ""
        txtDetail.Text = ""
        img.ImageUrl = ReadImage("0")


    End Sub

    Protected Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        Clear()
    End Sub


End Class