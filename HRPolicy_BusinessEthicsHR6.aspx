﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_BusinessEthicsHR6.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_BusinessEthicsHR6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<style>
	.swiper-container {
		width: 100%;
		height: 100%;
	}
	.swiper-slide {
		text-align: center;
		background: #fff;
	}
	.swiper-slide img 
	{
		width: 100%;
	}
	.swiper-pagination-bullet-active {
		opacity: 1;
		background: #af2f2f;
	}
</style>

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/hr.jpg" alt=""/></div>
<!-- end banner -->

<article class="blog-details-page">
	<div class="container">
		
		<div class="post-heading">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
                <li>/</li>
                <li><a href="HRPolicy_Announce.aspx">ระเบียบต่างๆ</a></li>
                <li>/</li>
                <li><a href="HRPolicy_BusinessEthicsHR.aspx">ประกาศ คำสั่ง ระเบียบทรัพยากรมนุษย์</a></li></li>
            </ul>
			<h2>ระเบียบการลงโทษพนักงานทุจริต</h2>
		</div> <!-- /.post-heading -->
		
		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix">
				<div class="blog-details-post-wrapper">
				
					<img src="new/images/banner/hr-policy/img10.jpg" alt=""/>
         		    
         		    <p style="padding: 30px 0 0;">
         		    	เพื่อให้เป็นระเบียบปฏิบัติ และมีความเป็นระเบียบเรียบร้อยในการบริหารงานทรัพยากรมนุษย์ ว่าด้วยเรื่องพนักงานทุจริต ทาง อโรม่า กรุ๊ป จึงเห็นสมควรกำหนดระเบียบการลงโทษทางวินัย ดังนี้
					</p>
					
					<ol>
						<li>การกระทำผิดตามประกาศฉบับนี้ ได้แก่
							<ul class="a">
								<li>พนักงานทุจริตเงินของบริษัท ทุจริตในหน้าที่ หรือกระทำความผิดทางอาญาแก่บริษัท</li>
								<li>พนักงานยักยอก ขโมยเงิน และ/หรือทรัพย์สินบริษัท</li>
								<li>หรือความประพฤติ การกระทำอื่นใดที่ไม่อยู่ในความสุจริต ซึ่งทำให้บริษัทเสื่อมเสียชื่อเสียง หรือได้รับความเสียหาย</li>
							</ul>
						</li>
						<li>การลงโทษทางวินัย
							<ul class="a">
								<li>กระทำผิดครั้งแรก : ให้ออก ปลดออก ไล่ออกจากงานโดยไม่จ่ายเงินชดเชย และค่าชดเชยพิเศษ</li>
							</ul>
						</li>
						<li>การดำเนินการ
							<ul class="a">
								<li>กรณีมีหนี้สิน ความเสียหาย ค่าเสียหายอื่นใด พนักงานผู้กระทำนั้นๆ จะต้องชดใช้หนี้สิน ความเสียหาย ค่าเสียหาย ที่เกิดขึ้นจริง ให้กับบริษัทจนครบถ้วนในวันที่บริษัททราบการทุจริต 
								 หรือตามวันเวลาบริษัทกำหนด</li>
								<li>กรณีพนักงานในบังคับบัญชาทุจริต แล้วทางผู้บังคับบัญชามาขอผ่อนผัน ทางบริษัทจำเป็นอย่างยิ่งที่ต้องให้ผู้ที่มาร้องขอผ่อนผันนั้นๆ เป็นผู้รับผิดชอบชดใช้หนี้สิน ความเสียหาย ค่าเสียหายอื่นใดที่
									เกิดขึ้นจริงให้กับบริษัทจนครบถ้วน ในวันที่บริษัททราบการทุจริต หรือตามวันเวลาบริษัทกำหนด แทนพนักงานทุจริตนั้นๆ</li>
								<li>ทางบริษัทจะดำเนินดคีตามกฏหมายขั้นสูงสุด</li>
								<li>พนักงานที่ทุจริต ไม่สามารถรับกลับเข้ามาเป็นพนักงานของบริษัทได้อีก หรือตามที่ได้มีมติคณะกรรมการบริหารบริษัทเป็นกรณีไป</li>
							</ul>
						</li>
						<li>บริษัทขอสงวนไว้ซึ่งสิทธิที่จะยกเลิก แก้ไข เปลี่ยนแปลงระเบียบการลงโทษฉบับนี้ ได้ตามเห็นสมควร โดยจะแจ้งให้ทราบเป็นคราวๆ ไป</li>
					</ol>
					
					<p><br>
						จึงประกาศมาเพื่อทราบโดยทั่วกัน และปฏิบัติอย่างเคร่งครัด<br><br>
						
						ทั้งนี้ ให้มีผลบังคับใช้นับตั้งแต่วันที่ 1 พฤษภาคม 2554 เป็นต้นไป<br><br>

						ประกาศ ณ วันที่ 3 พฤษภาคม 2554
					</p>
					
					<div class="blog-details-bt">
						<div class="btn-group">
							<asp:button id="backButton" runat="server" text="Back" class="btn btn-primary" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:button>
						</div>
					</div>
				
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-8 col-xs-12 p-fix">
				<aside>
					<div class="sidebar-news-list">
						<h4>CATEGORY</h4>
						<ul>
							<li>
								<a href="HRPolicy_BusinessEthicsHR.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบทรัพยากรมนุษย์</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAS.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบเกี่ยวกับทรัพย์สิน</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsAC.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบเกี่ยวกับบัญชี การเงิน งบประมาณ จัดซื้อ</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsIT.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบเกี่ยวกับเทคโนโลยีสารสนเทศ</a>
							</li>
							<li>
								<a href="Benefits1_1.aspx" class="tran3s">ประกาศ คำสั่ง ระเบียบอื่นๆ</a>
							</li>
						</ul>
					</div>
				</aside>
				<aside>
					<div class="sidebar-news-list">
						<h4>Related Links</h4>
						<ul>
							<li>
								<a href="HRPolicy_BusinessEthicsHR1.aspx" class="tran3s">
									การคล้อง ติดบัตรพนักงาน
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR2.aspx" class="tran3s">
									การลาพักผ่อนประจำปี
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR3.aspx" class="tran3s">
									การแต่งกายพนักงานขาย (พีซี)
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR4.aspx" class="tran3s">
									การลงนามอนุมัติเกินอำนาจดำเนินการ หรือความเสียหายจากการปฏิบัติหน้าที่
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR5.aspx" class="tran3s">
									ระเบียบบ้านพักพนักงาน
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR6.aspx" class="tran3s">
									ระเบียบการลงโทษพนักงานทุจริต
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR7.aspx" class="tran3s">
									ทดลองจัดวันเวลาทำงานและวันหยุด (เป็นกรณีพิเศษ) ทำงานวันจันทร์ - ศุกร์
								</a>
							</li>
							<li>
								<a href="HRPolicy_BusinessEthicsHR8.aspx" class="tran3s">
									ระเบียบการแต่งกายพนักงานประจำสำนักงาน
								</a>
							</li>
						</ul>
					</div>
				</aside>
			</div>
		</div>
		
	</div>
</article>
    
<!-- Swiper JS -->
<script src="new/js/swiper.min.js"></script>

<!-- Initialize Swiper -->
<script>
	var swiper1 = new Swiper('.swiper1', {
		spaceBetween: 30,
		pagination: {
        el: '.swiper-pagination1',
        clickable: true,
      },
    });
</script>

</asp:Content>
