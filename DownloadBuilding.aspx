﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="DownloadBuilding.aspx.vb" Inherits="Aroma_HRPortal.DownloadBuilding" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 
    <!-- banner -->
<div class="base-banner"><img src="new/images/banner/form/banner.jpg" alt=""/></div>
<!-- end banner -->

<article class="blog-details-page">
	<div class="container">
		
		<div class="post-heading">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
            </ul>
			<h2>แบบฟอร์ม อาคารสถานที่</h2>
		</div> <!-- /.post-heading -->
		
		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix" style="float: none; margin: 0 auto;">
				<div class="blog-details-post-wrapper">
				
					<img src="new/images/banner/form/img1.jpg" alt="Image">

					<p style="padding: 30px 0 0;">
				  		<ul class="p">
							<%--<li><a href="Document/Download/Building/แบบฟอร์มการขอใช้รถยนต์ส่วนกลาง.pdf" target="_blank">แบบฟอร์มการขอใช้รถยนต์ส่วนกลาง</a></li>--%>
                            <li><a href="Document/Download/Building/แบบฟอร์มเช่ารถยนต์ส่วนกลางอัพเดท.pdf" target="_blank">แบบฟอร์มการขอใช้รถยนต์ส่วนกลาง</a></li>
							<li><a href="Document/Download/Building/แบบฟอร์มเครื่องเขียนใหม่ล่าสุด ฉบับ 2021.pdf" target="_blank">แบบฟอร์มขอเบิกอุปกรณ์เครื่องเขียน</a></li>



				  		</ul>
					</p>
				
				</div>
			</div>
		</div>
		
	</div>
</article>

</asp:Content>
