﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_WhoisWho.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_WhoisWho" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/banner.jpg" alt=""/></div>
<!-- end banner -->

<article class="blog-details-page">
	<div class="container">
		
		<div class="post-heading">
            <ul>
                <li><a href="index_hr.aspx">HOME</a></li>
            </ul>
			<h2>ติดต่อฝ่าย-แผนกต่างๆ</h2>
		</div> <!-- /.post-heading -->
		
		<div class="post-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix" style="float: none; margin: 0 auto;">
				<div class="blog-details-post-wrapper">
				
					<img src="new/images/banner/hr-policy/img26.jpg" alt=""/>

					<p style="padding: 30px 0 0;">
						<ul class="p">
							<li><a href="Document/WhoisWho/1.รายชื่อติดต่อ-สำนักคณะกรรมการ,เลขานุการ%20(Board%20Director).pdf" target="_blank">1.รายชื่อติดต่อ-สำนักคณะกรรมการ,เลขานุการ (Board Director).pdf</a></li>
							<li><a href="Document/WhoisWho/2.รายชื่อติดต่อ-ขาย%20Aroma%20Shop.pdf" target="_blank">2.รายชื่อติดต่อ-ขาย Aroma Shop.pdf</a></li>
							<li><a href="Document/WhoisWho/3.รายชื่อติดต่อ.pdf" target="_blank">3.รายชื่อติดต่อ-ขายโรงแรม&โมเดิร์นเทรด (Horeca&Moderntrade).pdf</a></li>

                            <li><a href="Document/WhoisWho/4.รายชื่อติดต่อ-กลุ่มลูกค่าองค์กร (Corporate Account).pdf" target="_blank">4.รายชื่อติดต่อ-กลุ่มลูกค่าองค์กร (Corporate Account).pdf</a></li>
                            <li><a href="Document/WhoisWho/5.รายชื่อติดต่อ-ชาวดอย.pdf" target="_blank">5.รายชื่อติดต่อ-ชาวดอย (Chao Doi)</a></li>
                            <li><a href="Document/WhoisWho/6.รายชื่อติดต่อ-พัฒนาธุรกิจระหว่างประเทศ  (Inter Business Development).pdf" target="_blank">6.รายชื่อติดต่อ-พัฒนาธุรกิจระหว่างประเทศ  (Inter Business Development)</a></li>
                            <li><a href="Document/WhoisWho/7.รายชื่อติดต่อ-พัฒนาธุรกิจ (Business Development).pdf" target="_blank">7.รายชื่อติดต่อ-พัฒนาธุรกิจ (Business Development)</a></li>
                            <li><a href="Document/WhoisWho/8.รายชื่อติดต่อ-ออกแบบตกแต่งภายใน (interior design).pdf" target="_blank">8.รายชื่อติดต่อ-ออกแบบตกแต่งภายใน (interior design)</a></li>
                            <li><a href="Document/WhoisWho/9.รายชื่อติดต่อ-การตลาด (Marketing).pdf" target="_blank">9.รายชื่อติดต่อ-การตลาด (Marketing)</a></li>
                            <li><a href="Document/WhoisWho/10.รายชื่อติดต่อ-ผลิตภัณฑ์.pdf" target="_blank">10.รายชื่อติดต่อ-ผลิตภัณฑ์ (Product)</a></li>
                            <li><a href="Document/WhoisWho/11.รายชื่อติดต่อ-สถาบันพัฒนาธุรกิจกาแฟ (ACA).pdf" target="_blank">11.รายชื่อติดต่อ-สถาบันพัฒนาธุรกิจกาแฟ (ACA)</a></li>
                            <li><a href="Document/WhoisWho/12.รายชื่อติดต่อ-ไลอ้อน ทรี-สตาร์ (Lion 3 star).pdf" target="_blank">12.รายชื่อติดต่อ-ไลอ้อน ทรี-สตาร์ (Lion 3 star)</a></li>
                            <li><a href="Document/WhoisWho/13.รายชื่อติดต่อ-การเงิน (Finance).pdf" target="_blank">13.รายชื่อติดต่อ-การเงิน (Finance)</a></li>

                            <li><a href="Document/WhoisWho/14.รายชื่อติดต่อ-บัญชี (Accounting).pdf" target="_blank">14.รายชื่อติดต่อ-บัญชี (Accounting)</a></li>
                            <li><a href="Document/WhoisWho/15.รายชื่อติดต่อ-จัดซื้อ (Purchasing).pdf" target="_blank">15.รายชื่อติดต่อ-จัดซื้อ (Purchasing)</a></li>
                            <li><a href="Document/WhoisWho/16.รายชื่อติดต่อ-จัดซื้อต่างประเทศ (Oversea Purchasing).pdf" target="_blank">16.รายชื่อติดต่อ-จัดซื้อต่างประเทศ (Oversea Purchasing)</a></li>
                            <li><a href="Document/WhoisWho/17.รายชื่อติดต่อ-งบประมาณและตรวจติดตาม (Budget).pdf" target="_blank">17.รายชื่อติดต่อ-งบประมาณและตรวจติดตาม (Budget)</a></li>
                            <li><a href="Document/WhoisWho/18.รายชื่อติดต่อ-ตรวจสอบภายใน (Internal Control).pdf" target="_blank">18.รายชื่อติดต่อ-ตรวจสอบภายใน (Internal Control)</a></li>
                            <li><a href="Document/WhoisWho/19.รายชื่อติดต่อ- Supply Chain.pdf" target="_blank">19.รายชื่อติดต่อ- Supply Chain</a></li>
                            <li><a href="Document/WhoisWho/20.รายชื่อติดต่อ-โรงงานบางปะกง (Plant).pdf" target="_blank">20.รายชื่อติดต่อ-โรงงานบางปะกง (Plant)</a></li>
                            <li><a href="Document/WhoisWho/21.รายชื่อติดต่อ-วิจัยและพัฒนา (RD).pdf" target="_blank">21.รายชื่อติดต่อ-วิจัยและพัฒนา (RD)</a></li>
                            <li><a href="Document/WhoisWho/22.รายชื่อติดต่อ-เทคโนโลยีสารสนเทศ (MIS).pdf" target="_blank">22.รายชื่อติดต่อ-เทคโนโลยีสารสนเทศ (MIS)</a></li>
                           <li><a href="Document/WhoisWho/23.รายชื่อติดต่อ-กฏหมาย (Legal).pdf" target="_blank">23.รายชื่อติดต่อ-กฏหมาย (Legal)</a></li>
                           <li><a href="Document/WhoisWho/24.รายชื่อติดต่อ-อาคารสถานที่ (Building).pdf" target="_blank">24.รายชื่อติดต่อ-อาคารสถานที่ (Building)</a></li>
                           
      
                           
							
						</ul>
					</p>
				
				</div>
			</div>
		</div>
		
	</div>
</article>

</asp:Content>
