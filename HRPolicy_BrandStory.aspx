﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_BrandStory.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_BrandStory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/HR.jpg" alt=""/></div>
<!-- end banner -->

<meta name="viewport" content="width=device-width" />

<section id="about-us" class="blog-category">
<div class="container">
	<div class="theme-title tt-edit category-title">
         <ul>
            <li><a href="index_hr.aspx">HOME</a></li>
        </ul>
		<h2>ประวัติความเป็นมา โครงสร้างการบริหาร</h2>
		<!--p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p-->
	</div> <!-- /.theme-title -->

	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i style="padding-left: 9px;" class="far icon-117-edit"></i>
				</div>
				<h3><a href="https://aromathailand.com/brand-story/" target="_blank" class="tran3s">Brand Story</a></h3>
				<!--p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p-->
				<a href="https://aromathailand.com/brand-story/" target="_blank" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
<%--		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="far icon-019-handshake"></i>
				</div>
				<h3><a href="http://www.aromathailand.com/2015/aboutus.php?catid=3" target="_blank" class="tran3s">Vision</a></h3>
				<!--p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p-->
				<a href="http://www.aromathailand.com/2015/aboutus.php?catid=3" target="_blank" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->--%>
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="far icon-068-skyline"></i>
				</div>
				<h3><a href="Document/HR Policy/1/1.Organization of Aroma Group-1.pdf" target="_blank" class="tran3s">โครงสร้างองค์กร (Organization of Aroma Group)</a></h3>
				<!--p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p-->
				<a href="Document/HR%20Policy/1/Organization of Aroma Group.pdf" 
                    target="_blank" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="far icon-052-company"></i>
				</div>
				<h3><a href="Document/HR%20Policy/1/2017.pdf" 
                        target="_blank" class="tran3s">โครงสร้างปฏิบัติการแต่ละบริษัท (Organization of KVN&LION)</a></h3>
				<!--p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p-->
				<a href="Document/HR%20Policy/1/Organization of KVN Lion.pdf" 
                    target="_blank" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="far icon-039-people-3"></i>
				</div>
				<h3><a href="Document/HR Policy/1/3.Organization of Department-Division.pdf" target="_blank" class="tran3s">โครงสร้างปฏิบัติการแต่ละแผนก (Organization of Department-Division)</a></h3>
				<!--p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p-->
				<a href="Document/HR Policy/1/3.Organization of Department-Division.pdf" target="_blank" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
	
    	<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-104-programmer"></i>
				</div>
				<h3><a href="HRPolicy_BusinessEthicsIT.aspx" class="tran3s">Company Profile <br />Video</a></h3>
				<!--p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p-->
				<a href="HRPolicy_BrandStory9.aspx" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
     
    
    <div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i style="padding-left: 9px;" class="far icon-117-edit"></i>
				</div>
				<h3><a href="Document/HR Policy/1/Aroma brand activity summary 27Aug2015 revised.pdf" target="_blank" class="tran3s">Aroma Brand IDENTITY</a></h3>
				<!--p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p-->
				<a href="Document/HR Policy/1/Aroma brand activity summary 27Aug2015 revised.pdf" target="_blank" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->


	</div> <!-- /.row -->
</div> <!-- /.container -->
</section>

</asp:Content>
