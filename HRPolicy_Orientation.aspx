﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master_HR.Master" CodeBehind="HRPolicy_Orientation.aspx.vb" Inherits="Aroma_HRPortal.HRPolicy_Orientation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- banner -->
<div class="base-banner"><img src="new/images/banner/hr-policy/hr.jpg" alt=""/></div>
<!-- end banner -->

<section id="about-us" class="blog-category">
<div class="container">
	<div class="theme-title tt-edit category-title">
        <ul>
            <li><a href="index_hr.aspx">HOME</a></li>
        </ul>
		<h2>คู่มือพนักงาน</h2>
		<!--p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p-->
	</div> <!-- /.theme-title -->

	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-026-man-1"></i>
				</div>
				<%--<h3><a href="Document/2Orientation/Orientation/ชี้แจงแนะนำพนักงานใหม่%20KVNLION_introducenewemployees.pdf" --%>
                <h3><a href="#" 
                        target="_blank" class="tran3s">คู่มือแนะนำพนักงานใหม่ <br>AROMA GROUP<br/><b>(On Boarding Program)<br/>
                        <%--<h3><h4 style="color:red;">ข้อมูลอยู่ระหว่างปรับปรุง</h4> --%>
                        </a></h3>
				<!--p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p-->
				<a href="Document/2Orientation/On Boarding Hardbook 631209 - แบบ4.pdf" target="_blank" class="more tran3s hvr-bounce-to-right">More Details</a>
               
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-028-food-2"></i>
				</div>
				<h3><a href="Document/2Orientation/2.คู่มือแนะนำพนักงานใหม่สำหรับพนักงานสายการผลิตเบเกอรี่ เค้ก (introduce new employees)-2 .pdf" target="_blank" class="tran3s">คู่มือแนะนำพนักงานใหม่<br>สำหรับพนักงานสายการผลิต<br>เบเกอรี่ เค้ก </a></h3>
				<!--p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p-->
				<a href="Document/2Orientation/2.คู่มือแนะนำพนักงานใหม่สำหรับพนักงานสายการผลิตเบเกอรี่ เค้ก (introduce new employees)-2 .pdf" target="_blank" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-053-coffee-cup"></i>
				</div>
				<h3><a href="Document/2Orientation/3.ระเบียบปฏิบัติสำหรับพนักงานประจำร้านกาแฟ 94 Coffee (เบื้องต้น)-2.pdf" target="_blank" class="tran3s">ระเบียบปฏิบัติสำหรับพนักงานประจำร้านกาแฟ 94 Coffee (เบื้องต้น)</a></h3>
				<!--p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p-->
				<a href="Document/2Orientation/3.ระเบียบปฏิบัติสำหรับพนักงานประจำร้านกาแฟ 94 Coffee (เบื้องต้น)-2.pdf" target="_blank" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-098-coffee-machine"></i>
				</div>
				<h3><a href="Document/2Orientation/4.ระเบียบปฏิบัติสำหรับพนักงานประจำร้านกาแฟสดชาวดอย (เบื้องต้น)-2.pdf" target="_blank" class="tran3s">ระเบียบปฏิบัติสำหรับพนักงานประจำร้านกาแฟสดชาวดอย (เบื้องต้น)</a></h3>
				<!--p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p-->
				<a href="Document/2Orientation/4.ระเบียบปฏิบัติสำหรับพนักงานประจำร้านกาแฟสดชาวดอย (เบื้องต้น)-2.pdf" target="_blank" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->

     
        <div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single-about-content menu-content">
				<div class="icon round-border tran3s">
					<i class="fas icon-026-man-1"></i>
				</div>
				<h3><a href="Document/Manual/วิธีการเข้าระบบ BUD  20182.pdf" target="_blank" class="tran3s">วิธีการเข้าระบบ<br>BUDGET CONTROL SYSTEM 2018</a></h3>
				<!--p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p-->
				<a href="Document/Manual/วิธีการเข้าระบบ BUD  20182.pdf" target="_blank" class="more tran3s hvr-bounce-to-right">More Details</a>
			</div> <!-- /.single-about-content menu-content -->
		</div> <!-- /.col -->



	</div> <!-- /.row -->


</div> <!-- /.container -->

</section>
 
</asp:Content>
