﻿Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Data
Imports System.IO
Imports System.Configuration
Imports System.Collections
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports ICSharpCode.SharpZipLib.Zip
Imports System.Data.SqlClient.SqlConnection

Imports System.Data.OleDb
Imports System.Web
Imports System.Net
Imports System.Net.Mail
Imports System.Net.Mail.MailMessage
Imports System.Web.Mail.SmtpMail
Imports System.Net.Mail.SmtpClient
Imports System.Web.Mail.MailFormat


Public Class A_IndexNews
    Inherits System.Web.UI.Page
    Dim dtt As New DataTable
    Private sms As New PKMsg("")
    Dim DB_HR As New Connect_HR
    Dim dt, dt1, dt2, dt3 As New DataTable
    Dim Cmd As OleDbCommand
    Dim Dr As OleDbDataReader
    Dim sqlCheck, sql, ssql, sql1, sql2, sql3, sql4, sqlCheckDay As String
    Dim ImageName As String
    Private dp As New DBProc
    Dim status, Role, IndexId As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Role = Request.QueryString("Role")
        IndexId = Request.QueryString("IndexID")

        If Not Page.IsPostBack Then
            loaddata(IndexId)
        End If

    End Sub


    Private Sub loaddata(Optional ByVal sCond As String = "")

        sql = " SELECT IndexID,Type,TxtSubject,TxtDetail ,image ,image_name,Status ,CreateBy ,CreateDate	"
        sql += " FROM TB_IndexHome "
        sql += " where IndexID = '" & IndexId & "' "

        dt = DB_HR.GetDataTable(sql)

        If dt.Rows.Count > 0 Then
            txtSubject.Text = dt.Rows(0)("TxtSubject")
            txtDetail.Text = dt.Rows(0)("TxtDetail")
            lblNum.Text = dt.Rows(0)("IndexID")

            If Not IsDBNull(dt.Rows(0)("image")) Then
                img.ImageUrl = Readimage(dt.Rows(0)("IndexID"))
            End If

        End If

        att_Read()

    End Sub

    Function ReadImage(ByVal IndexID As String)
        'Return ("Readimage.aspx?ImageID=" & IndexID)
        Return ("ReadimageBanner.aspx?ID=" & IndexID)
    End Function



    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If txtSubject.Text = "" Then
            sms.Msg = "กรุณาระบุชื่อหัวข้อ"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If

        ImageName = FileUpload1.FileName

        sqlCheck += " SELECT * FROM [TB_IndexHome] "
        sqlCheck += " "
        dt = DB_HR.GetDataTable(sqlCheck)
        If dt.Rows.Count <= 0 Then
            lblNum.Text = "1"
            insert()
        Else
            'Update
            If lblNum.Text <> "0" Then
                Update()
            Else
                genNum()
                insert2()
            End If
        End If

        If FileUpload1.FileName <> "" Then
            Call insertIMG()
            insertLogo()  ' LOGO
        End If

        'Clear()
        Server.Transfer("A_IndexNewsMain.aspx?Role=" & Role)
    End Sub

    Sub genNum()
        Dim conn As New Connect_HR
        Dim sql2 As String = "Select Max(IndexID) as IndexID from [TB_IndexHome] "

        dt2 = DB_HR.GetDataTable(sql2)

        Dim newID As Integer = CInt(dt2.Rows(0)("IndexID"))
        newID += 1
        lblNum.Text = newID

    End Sub


    Sub insert()

        'If rdoStatus.Items.FindByValue("Active").Selected = True Then
        '    Status = "Active"
        'ElseIf rdoStatus.Items.FindByValue("InActive").Selected = True Then
        '    Status = "InActive"
        'Else
        '    Status = "InActive"
        'End If

        Dim sql As String = "INSERT INTO [TB_IndexHome] (IndexID,Type,TxtSubject,TxtDetail ,Status ,CreateBy ,CreateDate )"
        sql &= "VALUES (1,'News','" & txtSubject.Text & "','" & txtDetail.Text & "','Active'"
        sql &= " , '" & Role & "' , '" & Date.Now.ToString("yyyy-MM-dd") & "') "
        dtt = DB_HR.GetDataTable(sql)

        loaddata()

    End Sub


    Sub insert2()

        'If rdoStatus.Items.FindByValue("Active").Selected = True Then
        '    status = "Active"
        'ElseIf rdoStatus.Items.FindByValue("InActive").Selected = True Then
        '    status = "InActive"
        'Else
        '    status = "InActive"
        'End If

        Dim sql As String = "INSERT INTO [TB_IndexHome] (IndexID,Type,TxtSubject,TxtDetail ,Status ,CreateBy ,CreateDate )"
        sql &= "VALUES (" & lblNum.Text & ",'News','" & txtSubject.Text & "','" & txtDetail.Text & "','Active'"
        sql &= " , '" & Role & "' , '" & Date.Now.ToString("yyyy-MM-dd") & "') "
        dtt = DB_HR.GetDataTable(sql)
        loaddata()

    End Sub


    Sub Update()

        'If rdoStatus.Items.FindByValue("Active").Selected = True Then
        '    status = "Active"
        'ElseIf rdoStatus.Items.FindByValue("InActive").Selected = True Then
        '    status = "InActive"
        'Else
        '    status = "InActive"
        'End If

        Dim sSql As String = "UPDATE [TB_IndexHome] SET "
        sSql += " TxtSubject='" & txtSubject.Text & "' "
        sSql += " , TxtDetail='" & txtDetail.Text & "' "
        sSql += " , Status ='" & status & "'"
        sSql += " , UpdateBy ='" & Role & "'"
        sSql += " , ModifyDate='" & Date.Now.ToString("yyyy-MM-dd") & "' "
        sSql += " Where  IndexID = '" & lblNum.Text & "' "
        dtt = DB_HR.GetDataTable(sSql)
        loaddata()

    End Sub

    Sub insertIMG()
        Dim CurrentFileName As String
        CurrentFileName = FileUpload1.FileName

        'If (Path.GetExtension(CurrentFileName).ToLower <> ".jpg") Then
        '    sms.Msg = "You choose the file dishonestly !!! , Please choose be file .jpg"
        '    Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        '    Exit Sub
        'End If

        'If FileUpload1.PostedFile.ContentLength > 131072 Then
        '    sms.Msg = "The size of big too file , the size of the file must 128 KB not exceed!!!"
        '    Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        '    Exit Sub
        'End If

        Dim CurrentPath As String = Server.MapPath("~/Temp/")

        'FileUpload1.PostedFile.SaveAs(CurrentPath & FileUpload1.FileName)
        FileUpload1.PostedFile.SaveAs("c:\temp\" & FileUpload1.FileName)
        ' img.ImageUrl = CurrentPath & FileUpload1.FileName
        txtPic.Text = "c:\temp\" & FileUpload1.FileName

    End Sub

    Sub insertLogo()
        Dim db As New Connect_HR
        Dim sql As String
        Dim tr As SqlTransaction
        Dim sqlcon As New SqlConnection(DB_HR.sqlCon)

        If sqlcon.State = ConnectionState.Closed Then
            sqlcon = New SqlConnection(DB_HR.sqlCon)
            sqlcon.Open()
        End If

        tr = sqlcon.BeginTransaction

        Try
            Dim cmd As SqlCommand = New SqlCommand(sql, sqlcon, tr)
            If txtPic.Text <> "" Then
                Dim FileN As FileStream
                Dim Ln As Int32
                Dim oBinaryReader As BinaryReader
                Dim oImgByteArray As Byte()

                FileN = New FileStream(txtPic.Text, FileMode.Open, FileAccess.Read)
                oBinaryReader = New BinaryReader(FileN)
                oImgByteArray = oBinaryReader.ReadBytes(CInt(FileN.Length))
                Ln = CInt(FileN.Length)
                oBinaryReader.Close()
                FileN.Close()

                sql = "update [TB_IndexHome] set image_name=@FF1,image=@FF2 where IndexID  = '" & lblNum.Text & "'"
                Dim cmd3 As SqlCommand = New SqlCommand(sql, sqlcon, tr)
                cmd3.Parameters.Add("@FF1", SqlDbType.VarChar, 0).Value = ImageName ' + Path.GetExtension(FileUpload1.FileName).ToLower
                cmd3.Parameters.Add("@FF2", SqlDbType.Image, 0).Value = oImgByteArray
                cmd3.CommandTimeout = 0
                cmd3.ExecuteNonQuery()
            End If

            tr.Commit()
            sqlcon.Close()
        Catch ex As Exception
            tr.Rollback()
            sqlcon.Close()
            Exit Sub
        End Try

    End Sub



    Protected Sub btnSaveFile_Click(sender As Object, e As EventArgs) Handles btnSaveFile.Click
        Dim IndexID As Integer


        If lblNum.Text <> "0" Then
            IndexID = lblNum.Text
            Call att_Save(IndexID)
            att_Read()
        Else
            genNum()
            insert2()
            IndexID = lblNum.Text
            Call att_Save(IndexID)
            att_Read()

        End If

    End Sub


    Private Sub att_Save(ByVal IndexID As String)
        Dim ImgPath As String = Server.MapPath(".") & "\Upload"
        Dim filename3 As String
        Dim fileN2 As FileStream
        Dim fileN3 As FileStream
        Dim extension As String
        Dim zip3 As ZipOutputStream
        Dim Zip4 As ZipEntry
        Dim IDE As String
        Dim isN As String = ""
        Dim msg As String = ""

        If Me.FileUpload2.HasFile Then

            filename3 = Path.GetFileName(FileUpload2.PostedFile.FileName)
            FileUpload2.PostedFile.SaveAs(ImgPath & "\" & Trim(filename3))
            Dim ss() As String = filename3.Split(".")
            Dim sFiletype As String = (".") & ss(ss.GetUpperBound(0))

            If filename3.IndexOf("Upload/") = -1 Then
                If ImgPath & "\" & IndexID & sFiletype = ImgPath & "\" & IndexID & sFiletype Then
                    Dim FileIn As New FileInfo(Server.MapPath("Upload/" & IndexID & sFiletype))
                    If FileIn.Exists Then
                        FileIn.Delete()
                    End If
                End If

                Rename(ImgPath & "\" & Trim(filename3), ImgPath & "\" & IndexID & sFiletype)

                fileN3 = New FileStream(ImgPath & "\" & IndexID & sFiletype, FileMode.Open, FileAccess.Read)
                fileN2 = New FileStream(ImgPath & "\" & IndexID & "-" & Left(FileUpload2.FileName, Len(FileUpload2.FileName) - 4) & ".zip", FileMode.Create, FileAccess.Write)

                zip3 = New ZipOutputStream(fileN2)
                Dim buffer((CheckSize(ImgPath & "\" & IndexID & sFiletype))) As Byte
                Zip4 = New ZipEntry(Path.GetFileName(ImgPath & "\" & IndexID & sFiletype))
                zip3.PutNextEntry(Zip4)

                Dim size As Int32
                Do
                    size = fileN3.Read(buffer, 0, buffer.Length)
                    zip3.Write(buffer, 0, size)
                Loop While size > 0
                zip3.Close()
                fileN3.Close()
                fileN2.Close()
            End If

            IDE = ImgPath & "\" & IndexID & "-" & Left(FileUpload2.FileName, Len(FileUpload2.FileName) - 4) & ".zip"
            Dim filename As String
            Dim fileN As FileStream
            Dim Ln As Int32
            Dim oBinaryReader As BinaryReader
            Dim oImgByteArray As Byte()
            Dim file As String
            'Dim supcodeT As String  '----
            'supcodeT = supcode      '-----
            'Dim ResidT As String
            'ResidT = resid

            filename = Path.GetFileName(IndexID & sFiletype)
            file = Left(FileUpload2.FileName, Len(FileUpload2.FileName) - 4) & ".zip"
            fileN = New FileStream(IDE, FileMode.Open, FileAccess.Read)
            oBinaryReader = New BinaryReader(fileN)
            oImgByteArray = oBinaryReader.ReadBytes(CInt(fileN.Length))
            Ln = CInt(fileN.Length)

            Dim s As Integer = dp.InsertAttatchmentIndex(IndexID, file, oImgByteArray)
        End If

    End Sub

    Private Sub att_Read()
        Dim dt As New Data.DataTable
        Dim sSql As String = "select * from [TB_IndexHomeAtt] where [IndexID] = '" & lblNum.Text & "'  "
        dt = DB_HR.GetDataTable(sSql)
        If dt.Rows.Count > 0 Then
            DataGrid1.DataSource = dt
            DataGrid1.DataBind()
        Else
        End If
    End Sub

    Function CheckSize(ByVal f As String) As Integer
        Dim oBinaryReader As BinaryReader
        Dim oImgByteArray As Byte()
        Dim FileN As FileStream
        Dim ln As Long
        FileN = New FileStream(f, FileMode.Open, FileAccess.Read)
        oBinaryReader = New BinaryReader(FileN)
        oImgByteArray = oBinaryReader.ReadBytes(CInt(FileN.Length))
        ln = CInt(FileN.Length)

        CheckSize = ln
    End Function

    Protected Sub DataGrid1_DeleteCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.DeleteCommand
        Dim SelectID As Integer
        SelectID = Convert.ToString(DataGrid1.DataKeys(e.Item.ItemIndex))
        Dim s As Integer
        Dim ssql As String = "delete from [TB_IndexHomeAtt] where [Att_ID] = " & SelectID
        s = DB_HR.Execute(ssql)
        DataGrid1.Controls.Clear()
        att_Read()
    End Sub

    Protected Sub DataGrid1_EditCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.EditCommand
        If e.Item.ItemIndex <> -1 Then
            Dim SelectID As Integer
            SelectID = Convert.ToString(DataGrid1.DataKeys(e.Item.ItemIndex))
            Dim dt As New Data.DataTable
            Dim ssql As String = "select * from [TB_IndexHomeAtt] with (nolock) where [IndexID] = '" & lblNum.Text & "' and [Att_ID] = " & SelectID
            dt = DB_HR.GetDataTable(ssql)

            Response.ClearContent()
            Response.ClearHeaders()
            If Right(dt.Rows(0)("Att_Filetype").ToString, 4) = ".pdf" Then
                Response.ContentType = "application/pdf" '"application/pdf"
                Response.BinaryWrite(dt.Rows(0)("Att"))
            ElseIf Right(dt.Rows(0)("Att_Filetype").ToString, 4) = ".xls" Then
                Response.ContentType = "application/vnd.ms-EXCEL" '"vnd.ms/"
                Response.BinaryWrite(dt.Rows(0)("Att"))
            ElseIf Right(dt.Rows(0)("Att_Filetype").ToString, 4) = ".doc" Then
                Response.ContentType = "application/msword" '"application/vnd.ms-WORD" '"vnds/"
                Response.BinaryWrite(dt.Rows(0)("Att"))
            ElseIf Right(dt.Rows(0)("Att_Filetype").ToString, 4) = ".zip" Then
                Response.ContentType = "application/x-zip-compressed"
                Response.BinaryWrite(dt.Rows(0)("Att"))

            Else
                Response.ContentType = dp.GetContentType(dt.Rows(0)("Att_Filetype"))
                Response.BinaryWrite(dt.Rows(0)("Att"))
            End If

            Response.Flush()
            Response.Close()
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        Clear()
    End Sub

    Sub Clear()
        lblNum.Text = "0"
        txtSubject.Text = ""
        txtDetail.Text = ""
        img.ImageUrl = ReadImage("0")

        Dim dt As New Data.DataTable
        Dim sSql As String = "select * from [TB_IndexHomeAtt] where [IndexID] = '0'  "
        dt = DB_HR.GetDataTable(sSql)
        If dt.Rows.Count = 0 Then
            DataGrid1.DataSource = dt
            DataGrid1.DataBind()
        Else
        End If

    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Server.Transfer("A_IndexNewsMain.aspx?Role=" & Role)
    End Sub


End Class